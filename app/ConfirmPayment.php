<?php

namespace Solivis;

use Illuminate\Database\Eloquent\Model;

class ConfirmPayment extends Model
{
    protected $table = 'confirm_payments';
    
    protected $fillable = ['reservation_id', 'transfer_from_bank', 'transfer_to_bank', 'transfer_proof_photo_url', 'information'];

    protected $hidden = [];

	public function reservation(){ 
		return $this->belongsTo('Solivis\Reservation');
	}

    public function scopeCountTotalPayment($query){
        return $query->join('reservations','confirm_payments.reservation_id','=','reservations.id')
            ->join('reservation_menus','reservation_menus.reservation_id','=','reservations.id')
            ->selectRaw('confirm_payments.*, sum(reservation_menus.item_count * reservation_menus.menu_base_price) as total_payment')->where('reservations.status_reservation_id','=','2')->groupBy('confirm_payments.id');
    }

}
