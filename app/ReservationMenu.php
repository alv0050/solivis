<?php

namespace Solivis;

use Illuminate\Database\Eloquent\Model;

class ReservationMenu extends Model
{
    protected $table = 'reservation_menus';

    protected $fillable = ['item_count', 'reservation_id', 'menu_name', 'menu_base_price'];

    protected $hidden = [];

	public function reservation(){ 
		return $this->belongsTo('Solivis\Reservation');
	}

}
