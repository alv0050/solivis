<?php

namespace Solivis;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Carbon\Carbon;
use Solivis\Cuisine;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class RestaurantManagement extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'restaurant_managements';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'address', 'password', 'photo_url', 'website_url', 'phone_number', 'open_time', 'close_time'];
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function restaurant_cuisines(){ 
        return $this->hasMany('Solivis\RestaurantCuisine');
    }

    public function restaurant_services(){ 
        return $this->hasMany('Solivis\RestaurantService');
    }

    public function restaurant_food_photos(){ 
        return $this->hasMany('Solivis\RestaurantFoodPhoto');
    }

    public function restaurant_inside_photos(){ 
        return $this->hasMany('Solivis\RestaurantInsidePhoto');
    }

    public function menus(){
        return $this->hasMany('Solivis\Menu');
    }

    public function restaurant_banks(){ 
        return $this->hasMany('Solivis\RestaurantBank');
    }

    public function restaurant_history_balances(){ 
        return $this->hasMany('Solivis\RestaurantHistoryBalance');
    }

    public function getLastLogin($date){
        return Carbon::parse($date);        
    }    

    public function removeDoubleZero($price){ //remove .00
        $remove_doublezero_price = preg_replace('~\.0+$~','',$price); 
        return $remove_doublezero_price;
    }

    private function changeToKorM($price){
        if ($price > 999 && $price <= 999999) { //change to k or m
            $result = floor($price / 1000).'k';
        } 
        else if ($price > 999999) {
            $result = floor($price / 1000000).'m';
        } 
        else {
            $result = $value;
        } 
        return $result;       
    }

    //RESTAURANT HISTORY BALANCE
    public function getProcessDate($process_date){
        return date('j', strtotime($process_date));
    }

    public function getProcessMonth($process_date){
        return date('n', strtotime($process_date));
    }

    public function getProcessYear($process_date){
        return date('Y', strtotime($process_date));
    }       
    //END: RESTAURANT HISTORY BALANCE   

    // RESTAURANT PROFILE
    public function getOpenTimeHourAndMinute(){
        return date('H:i', strtotime($this->open_time));
    }

    public function getCloseTimeHourAndMinute(){
        return date('H:i', strtotime($this->close_time));
    }

    public function getLowestPrice(){
        if(count($this->menus) == 0){
            return 0;
        }
        else{
            $price = $this->menus->min('item_price');
            $price = $this->removeDoubleZero($price);
            $price = $this->changeToKorM($price);
        }

        return $price;
    }

    public function getHighestPrice(){
        if(count($this->menus) == 0){
            return 0;
        }        
        else{
            $price = $this->menus->max('item_price');
            $price = $this->removeDoubleZero($price);
            $price = $this->changeToKorM($price);
        }

        return $price;
    }
    // END: RESTAURANT PROFILE

    public function getHighestPriceNumeric(){
        $price = $this->menus->first()->max('item_price');
        $price = $this->removeDoubleZero($price);

        return $price;        
    }

    public function favorites(){
        return $this->hasMany('Solivis\Favorite');
    }

    public function reservations(){
        return $this->hasMany('Solivis\Reservation');
    }

    public function getOnlyDate($date_time){
        return date('d-m-Y', strtotime($date_time));
    }

    public function getOnlyTime($date_time){
        return date('H:i ', strtotime($date_time));
    }

    public function getCuisines(){
        $cuisines = $this->restaurant_cuisines;

        $cuisine_ids = array();
        $cuisine_names = "";

        for($i = 0; $i < count($cuisines); $i++){
            $cuisine_ids[$i] = $cuisines[$i]->cuisine_id;
            $cuisine_names .= Cuisine::where('id', $cuisine_ids[$i])->first()->name.", ";
        }     
        return $cuisine_names;   
    }

    public function getServices(){
        $services = $this->restaurant_services;
        $service_ids = array();
        $service_names = "";

        for($i = 0; $i < count($services); $i++){
            $service_ids[$i] = $services[$i]->service_id;
            $service_names .= Service::where('id', $service_ids[$i])->first()->name.", ";
        }        

        return $service_names;
    }

    public function getTotalRatings(){
        $total_ratings = 0;

        for($i=0; $i<count($this->reservations); $i++){
            if($this->reservations[$i]->review_rating){
                $total_ratings += $this->reservations[$i]->review_rating->rating;
            }
        }

        return $total_ratings;
    }

    public function getTotalRatingsAverage(){
        $total_ratings = 0;
        $total_ratings_average = 0;
        $total_users_rate = 0;

        for($i=0; $i<count($this->reservations); $i++){
            if($this->reservations[$i]->review_rating){
                $total_users_rate++;
                $total_ratings += $this->reservations[$i]->review_rating->rating;
            }
        }
        if($total_users_rate == 0){
           $total_ratings_average = 0; 
        }
        else{
            $total_ratings_average = round($total_ratings/$total_users_rate); 
        }

        return $total_ratings_average;       
    }

    // Scope for filter searching
    public function scopeSearchPriceRange($query, $price_range)
    {
        if($price_range == 'lt100000'){
            $query->where('status', '1')->WhereHas('menus', function($menus){
                        $menus->where('item_price', '<', '100000');
                    }); 
        }
        else if($price_range == '100001-300000'){
            $query->where('status', '1')->WhereHas('menus', function($menus){
                        $menus->whereBetween('item_price', array('100001', '300000'));
                    }); 
        }
        else if($price_range == '300001-500000'){
            $query->where('status', '1')->WhereHas('menus', function($menus){
                        $menus->whereBetween('item_price', array('300001', '500000'));
                    }); 
        }
        else{
            $query->where('status', '1')->WhereHas('menus', function($menus){
                        $menus->where('item_price', '>', '500000');
                    }); 
        }
    }

    public function scopeSearchCuisine($query, $cuisine_name)
    {
        $query->where('status', '1')->WhereHas('restaurant_cuisines', function($restaurant_cuisines) use ($cuisine_name){
                            $restaurant_cuisines->whereHas('cuisine', function($cuisine) use ($cuisine_name){
                                $cuisine->where('name', $cuisine_name);
                            }); 
                        });         
    }    

    public function scopeSearchService($query, $service_name)
    {
        $query->where('status', '1')->WhereHas('restaurant_services', function($restaurant_services) use ($service_name){
                            $restaurant_services->whereHas('service', function($cuisine) use ($service_name){
                                $cuisine->where('name', $service_name);
                            }); 
                        });         
    }    

    // Join table to get relationship count
    public function scopeCountRestaurantManagementReservationsSuccess($query){
        return $query->join('reservations','reservations.restaurant_management_id','=','restaurant_managements.id')
            ->selectRaw('restaurant_managements.*, count(reservations.id) as count')->where('reservations.status_reservation_id','=','4')->groupBy('restaurant_managements.id');
    }


    public function scopeCountRestaurantManagementTotalRatings($query){
        return $query->join('reservations','reservations.restaurant_management_id','=','restaurant_managements.id')
            ->join('review_ratings','review_ratings.reservation_id','=','reservations.id')
            ->selectRaw('restaurant_managements.*, sum(rating) as total_ratings')->groupBy('restaurant_managements.id');
    }

    public function generatePassword() {
        $length = 8;
        $str = "";
        $characters = array_merge(range('a','z'), range('0','7'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        return $str;
    }    

}