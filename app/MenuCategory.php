<?php

namespace Solivis;

use Illuminate\Database\Eloquent\Model;

class MenuCategory extends Model
{
   	protected $table = 'menu_categories';

   	protected $fillable = ['name'];

   	protected $hidden = [];

	public function menu(){ 
		return $this->hasOne('Solivis\Menu');
	}
}
