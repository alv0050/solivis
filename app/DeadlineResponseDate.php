<?php

namespace Solivis;

use Illuminate\Database\Eloquent\Model;

class DeadlineResponseDate extends Model
{
   	protected $table = 'deadline_response_dates';

   	protected $fillable = ['deadline_date', 'reservation_id'];

   	protected $hidden = [];

	public function reservation(){ 
		return $this->belongsTo('Solivis\Reservation');
	}
}
