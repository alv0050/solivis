<?php

namespace Solivis;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    protected $table = 'favorites';
    
    protected $fillable = ['restaurant_management_id', 'user_id'];

    protected $hidden = [];

	public function restaurant_management(){
		return $this->belongsTo('Solivis\RestaurantManagement');
	}

	public function user(){ 
		return $this->belongsTo('Solivis\User');
	}

}
