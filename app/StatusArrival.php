<?php

namespace Solivis;

use Illuminate\Database\Eloquent\Model;

class StatusArrival extends Model
{
   	protected $table = 'status_arrivals';

   	protected $fillable = ['status'];

   	protected $hidden = [];

	public function reservation(){ 
		return $this->hasOne('Solivis\Reservation');
	}
}
