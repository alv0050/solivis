<?php

namespace Solivis;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'phone_number', 'email', 'password', 'birth_date', 'gender', 'photo_url', 'confirmation_code'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    //BIRTH DATE    
    public function getDate(){
        $date = $this->birth_date;
        return date('j', strtotime($date));
    }

    public function getMonth(){
        $date = $this->birth_date;
        return date('n', strtotime($date));
    }

    public function getYear(){
        $date = $this->birth_date;
        return date('Y', strtotime($date));
    }
    // END: BIRTH DATE

    public function removeDoubleZero($price){ //remove .00
        $remove_doublezero_price = preg_replace('~\.0+$~','',$price); 
        return $remove_doublezero_price;
    }

    //HISTORY BALANCE
    public function getProcessDate($process_date){
        return date('j', strtotime($process_date));
    }

    public function getProcessMonth($process_date){
        return date('n', strtotime($process_date));
    }

    public function getProcessYear($process_date){
        return date('Y', strtotime($process_date));
    }       
    //END: HISTORY BALANCE

    public function getLastLogin($date){
        return Carbon::parse($date);        
    }

    public function user_banks(){ 
        return $this->hasMany('Solivis\UserBank');
    }

    public function history_balances(){ 
        return $this->hasMany('Solivis\HistoryBalance');
    }

    public function favorites(){
        return $this->hasMany('Solivis\Favorite');
    }

    public function reservations(){
        return $this->hasMany('Solivis\Reservation');        
    }

    // Join table to get relationship count
    public function scopeCountUserReservationsSuccess($query){
        return $query->join('reservations','reservations.user_id','=','users.id')
            ->selectRaw('users.*, count(reservations.id) as count')->where('reservations.status_reservation_id','=','4')->groupBy('users.id');
    }
}
