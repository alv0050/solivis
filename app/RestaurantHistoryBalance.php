<?php

namespace Solivis;

use Illuminate\Database\Eloquent\Model;

class RestaurantHistoryBalance extends Model
{
    protected $table = 'restaurant_history_balances';
    
    protected $fillable = ['process_date', 'balance_processed', 'status_balance_id', 'restaurant_management_id', 'transfer_target'];

    protected $hidden = [];

	public function restaurant_management(){ 
		return $this->belongsTo('Solivis\RestaurantManagement');
	}

	public function status_balance(){
		return $this->belongsTo('Solivis\StatusBalance', 'status_balance_id');
	}

    public function getOnlyDate($date_time){
        return date('d-m-Y', strtotime($date_time));
    }

}
