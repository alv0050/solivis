<?php

namespace Solivis;

use Illuminate\Database\Eloquent\Model;

class RestaurantInsidePhoto extends Model
{
    protected $table = 'restaurant_inside_photos';
    
    protected $fillable = ['inside_restaurant_photo_url', 'inside_restaurant_photo_name', 'restaurant_management_id'];

    protected $hidden = [];

	public function restaurant_management(){ 
		return $this->belongsTo('Solivis\RestaurantManagement');
	}
}
