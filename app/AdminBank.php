<?php

namespace Solivis;

use Illuminate\Database\Eloquent\Model;

class AdminBank extends Model
{
    protected $table = 'admin_banks';
    
    protected $fillable = ['account_number', 'account_name', 'bank_id', 'admin_id'];

    protected $hidden = [];

	public function admin(){ 
		return $this->belongsTo('Solivis\Admin');
	}

	public function bank(){
		return $this->belongsTo('Solivis\Bank');		
	}	
}
