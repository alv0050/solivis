<?php

namespace Solivis;

use Illuminate\Database\Eloquent\Model;

class RestaurantService extends Model
{
    protected $table = 'restaurant_services';
    
    protected $fillable = ['service_id', 'restaurant_management_id'];

    protected $hidden = [];

	public function restaurant_management(){ 
		return $this->belongsTo('Solivis\RestaurantManagement');
	}

	public function service(){
		return $this->belongsTo('Solivis\Service', 'service_id');
	}
}
