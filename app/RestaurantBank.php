<?php

namespace Solivis;

use Illuminate\Database\Eloquent\Model;

class RestaurantBank extends Model
{
    protected $table = 'restaurant_banks';
    
    protected $fillable = ['account_number', 'account_name', 'bank_id', 'restaurant_management_id'];

    protected $hidden = [];

	public function restaurant_management(){ 
		return $this->belongsTo('Solivis\RestaurantManagement');
	}
	public function bank(){
		return $this->belongsTo('Solivis\Bank');		
	}
}
