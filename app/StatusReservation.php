<?php

namespace Solivis;

use Illuminate\Database\Eloquent\Model;

class StatusReservation extends Model
{
   	protected $table = 'status_reservations';

   	protected $fillable = ['status'];

   	protected $hidden = [];

	public function reservation(){ 
		return $this->hasOne('Solivis\Reservation');
	}

}
