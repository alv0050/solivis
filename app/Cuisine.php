<?php

namespace Solivis;

use Illuminate\Database\Eloquent\Model;

class Cuisine extends Model
{
   	protected $table = 'cuisines';

   	protected $fillable = ['name'];

   	protected $hidden = [];

	public function restaurant_cuisine(){ 
		return $this->hasOne('Solivis\RestaurantCuisine');
	}
}
