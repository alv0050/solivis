<?php

namespace Solivis;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Menu extends Model
{
    protected $table = 'menus';
    
    protected $fillable = ['item_name', 'item_description', 'item_price', 'menu_category_id', 'restaurant_management_id'];

    protected $hidden = [];

	public function restaurant_management(){ 
		return $this->belongsTo('Solivis\RestaurantManagement');
	}

	public function menu_category(){
		return $this->belongsTo('Solivis\MenuCategory', 'menu_category_id');
	}
	
}
