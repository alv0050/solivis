<?php

namespace Solivis;

use Illuminate\Database\Eloquent\Model;

class ExpiredComingConfirmation extends Model
{
   	protected $table = 'expired_coming_confirmations';

   	protected $fillable = ['expired_date', 'reservation_id', 'status_sent'];

   	protected $hidden = [];

	public function reservation(){ 
		return $this->belongsTo('Solivis\Reservation');
	}
}
