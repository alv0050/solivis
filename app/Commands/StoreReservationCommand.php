<?php

namespace Solivis\Commands;

use Solivis\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling; 
use Solivis\Reservation;

class StoreReservationCommand extends Command implements SelfHandling{

	public $paxes;
	public $reservation_date;
	public $user_id;
	public $restaurant_management_id;
	public $status_reservation_id;
	public $status_arrival_id;

	public function __construct($paxes, $reservation_date, $user_id, $restaurant_management_id){
		$this->paxes = $paxes;
		$this->reservation_date = $reservation_date;
		$this->user_id = $user_id;
		$this->restaurant_management_id = $restaurant_management_id;
		$this->status_reservation_id = 1; //Belum Proses
		$this->status_arrival_id = 2; //Tidak
	}

	public function handle(){
		return Reservation::create([
			'paxes' => $this->paxes,
			'reservation_date' => $this->reservation_date,
			'user_id' => $this->user_id,
			'restaurant_management_id' => $this->restaurant_management_id,
			'status_reservation_id' => $this->status_reservation_id,
			'status_arrival_id' => $this->status_arrival_id,
		]);
	}
}