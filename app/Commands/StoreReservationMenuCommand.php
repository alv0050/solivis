<?php

namespace Solivis\Commands;

use Solivis\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling; 
use Solivis\ReservationMenu;
use Solivis\Menu;

class StoreReservationMenuCommand extends Command implements SelfHandling{

	public $reservation_id;
	public $item_name;
	public $item_count;
	public $menu;
	public $restaurant_management_id;

	public function __construct($reservation_id, $restaurant_management_id, $item_name, $item_count){
		$this->reservation_id = $reservation_id;
		$this->item_name = $item_name;
		$this->item_count = $item_count;
		$this->restaurant_management_id = $restaurant_management_id;

		$this->menu = Menu::where('item_name', $this->item_name)
						->where('restaurant_management_id', $this->restaurant_management_id)
						->first();
	}

	public function handle(){
		return ReservationMenu::create([
			'item_count' => $this->item_count,
			'reservation_id' => $this->reservation_id,
			'menu_name' => $this->menu->item_name,
			'menu_base_price' => $this->menu->item_price
		]);
	}
}