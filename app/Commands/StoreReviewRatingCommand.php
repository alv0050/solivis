<?php

namespace Solivis\Commands;

use Solivis\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling; 
use Solivis\ReviewRating;

class StoreReviewRatingCommand extends Command implements SelfHandling{

	public $reservation_id;
	public $rating;

	public function __construct($reservation_id, $rating){
		$this->reservation_id = $reservation_id;
		$this->rating = $rating;
	}

	public function handle(){
		return ReviewRating::create([
			'reservation_id' => $this->reservation_id,
			'rating' => $this->rating,
		]);			   
	}

}