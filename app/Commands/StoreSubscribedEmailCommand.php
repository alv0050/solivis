<?php

namespace Solivis\Commands;

use Solivis\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling; 
use Solivis\SubscribedEmail;

class StoreSubscribedEmailCommand extends Command implements SelfHandling{

	public $email;

	public function __construct($email){
		$this->email = $email;
	}

	public function handle(){
		return SubscribedEmail::create([
			'email' => $this->email
		]);
	}
}