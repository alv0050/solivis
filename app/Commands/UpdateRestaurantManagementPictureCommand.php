<?php

namespace Solivis\Commands;

use Solivis\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling; 
use Solivis\RestaurantManagement;

class UpdateRestaurantManagementPictureCommand extends Command implements SelfHandling{

	public $id;
	public $photo_url;

	public function __construct($id, $photo_url){
		$this->id   = $id;
		$this->photo_url = $photo_url;
	}

	public function handle(){
		return RestaurantManagement::find($this->id)->update([
			'photo_url' => $this->photo_url
		]);
	}

}