<?php

namespace Solivis\Commands;

use Solivis\Commands\Command;
use Solivis\Favorite;

class CheckExistFavoriteCommand extends Command{
	public $restaurant_management_id;
	public $user_id;

	public function __construct($restaurant_management_id, $user_id){
		$this->restaurant_management_id = $restaurant_management_id;
		$this->user_id = $user_id;
	}

	public function checkExistFavorite(){
		$favorite = Favorite::where('restaurant_management_id', $this->restaurant_management_id)
							->where('user_id', $this->user_id)->first();
		if ($favorite){
			return true;
		}
		else{
			return false;
		}
	}	
}