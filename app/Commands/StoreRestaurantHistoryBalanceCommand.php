<?php

namespace Solivis\Commands;

use Solivis\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling; 
use Solivis\RestaurantHistoryBalance;
use Solivis\RestaurantBank;
use Carbon\Carbon;

class StoreRestaurantHistoryBalanceCommand extends Command implements SelfHandling{

	public $process_date;
	public $status_balance_id; //Penarikan dana sedang diproses
	public $restaurant_management_id;
	public $balance_processed;
	public $restaurant_bank_id;
	public $transfer_target;

	public function __construct($restaurant_management_id, $balance_processed, $restaurant_bank_id){
		$this->process_date = Carbon::now();
		$this->status_balance_id = 3;

		$this->restaurant_management_id = $restaurant_management_id;
		$this->balance_processed = $balance_processed;
		$this->restaurant_bank_id = $restaurant_bank_id;

		$restaurant_management_bank = RestaurantBank::find($this->restaurant_bank_id);
		$this->transfer_target = $restaurant_management_bank->bank->name."-".$restaurant_management_bank->account_number." a/n ".$restaurant_management_bank->account_name;
	}

	public function handle(){
		return RestaurantHistoryBalance::create([
			'process_date' => $this->process_date,
			'balance_processed' => $this->balance_processed,
			'status_balance_id' => $this->status_balance_id,
			'restaurant_management_id' => $this->restaurant_management_id,
			'transfer_target' => $this->transfer_target,
		]);
	}
}