<?php

namespace Solivis\Commands;

use Solivis\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling; 
use Solivis\RestaurantFoodPhoto;
use File;

class DeleteRestaurantManagementFoodPhotoCommand extends Command implements SelfHandling{

	public $id;
	public $photo_url;

	public function __construct($id, $photo_url){
		$this->id = $id;
		$this->photo_url = $photo_url;
	}

	public function handle(){
		//delete file from server storage
		File::delete(public_path('images/restaurant_uploads/food_photos/'.$this->photo_url));
		return RestaurantFoodPhoto::find($this->id)->delete();
	}
}
