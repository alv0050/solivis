<?php

namespace Solivis\Commands;

use Solivis\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling; 
use Solivis\User;

class UpdatePasswordCommand extends Command implements SelfHandling{

	public $id;
	public $password;

	public function __construct($id, $password){
		$this->id = $id;
		$this->password = $password;
	}

	public function handle(){
		return User::find($this->id)->update([
		    'password' => $this->password
		]);
	}
}