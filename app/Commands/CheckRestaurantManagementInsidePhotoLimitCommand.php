<?php

namespace Solivis\Commands;

use Solivis\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling; 
use Solivis\RestaurantInsidePhoto;
use File;

class CheckRestaurantManagementInsidePhotoLimitCommand extends Command{

	public $restaurant_management_id;

	public function __construct($restaurant_management_id){
		$this->restaurant_management_id = $restaurant_management_id;
	}

	public function checkOverLimitInsidePhoto(){

		$restaurant_management_inside_photos = RestaurantInsidePhoto::where('restaurant_management_id', $this->restaurant_management_id)->get();

		if ($restaurant_management_inside_photos->count() == 5){ 
			return true;
		}
		else{
			return false;			
		}
	}

}
