<?php

namespace Solivis\Commands;

use Solivis\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling; 
use Solivis\ConfirmPayment;
use Solivis\UserBank;
use Solivis\AdminBank;

class StoreConfirmPaymentCommand extends Command implements SelfHandling{

	public $reservation_id;
	public $transfer_from_bank_id;
	public $transfer_to_bank_id;
	public $transfer_from_bank;
	public $transfer_to_bank;
	public $transfer_proof_photo_url;
	public $information;

	public function __construct($reservation_id, $transfer_from_bank_id, $transfer_to_bank_id, $transfer_proof_photo_url, $information){
		$this->reservation_id   = $reservation_id;
		$this->transfer_from_bank_id = $transfer_from_bank_id;
		$this->transfer_to_bank_id = $transfer_to_bank_id;
		$this->transfer_proof_photo_url = $transfer_proof_photo_url;
		$this->information = $information;

		$user_bank = UserBank::find($this->transfer_from_bank_id);
		$this->transfer_from_bank = $user_bank->bank->name."-".$user_bank->account_number." a/n ".$user_bank->account_name;
		$admin_bank = AdminBank::find($this->transfer_to_bank_id);
		$this->transfer_to_bank = $admin_bank->bank->name."-".$admin_bank->account_number." a/n ".$admin_bank->account_name;
	}

	public function handle(){
		return ConfirmPayment::create([
			'reservation_id' => $this->reservation_id,
			'transfer_from_bank' => $this->transfer_from_bank,
			'transfer_to_bank' => $this->transfer_to_bank,
			'transfer_proof_photo_url' => $this->transfer_proof_photo_url,
			'information' => $this->information,
		]);			   
	}

}