<?php

namespace Solivis\Commands;

use Solivis\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling; 
use Solivis\Menu;

class UpdateRestaurantManagementMenuCommand extends Command implements SelfHandling{

	public $id;
	public $item_description;
	public $item_price;

	public function __construct($id, $item_description, $item_price){
		$this->id = $id;
		$this->item_description = $item_description;
		$this->item_price = $item_price;
	}

	public function handle(){
      	return Menu::where('id', $this->id)
      			   ->update([
	      			   	'item_description' => $this->item_description,
	      			   	'item_price' => $this->item_price,
      			   	]);
	}
}