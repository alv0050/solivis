<?php

namespace Solivis\Commands;

use Solivis\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling; 
use Solivis\RestaurantFoodPhoto;

class StoreRestaurantManagementFoodPhotoCommand extends Command implements SelfHandling{

	public $restaurant_management_id;
	public $food_photo_url;
	public $food_photo_name;

	public function __construct($restaurant_management_id, $food_photo_url, $food_photo_name){
		$this->restaurant_management_id   = $restaurant_management_id;
		$this->food_photo_url = $food_photo_url;
		$this->food_photo_name = $food_photo_name;
	}

	public function handle(){
		
		return RestaurantFoodPhoto::create([
			'restaurant_management_id' => $this->restaurant_management_id,
			'food_photo_url' => $this->food_photo_url,
			'food_photo_name' => $this->food_photo_name,
		]);			   
	}

}