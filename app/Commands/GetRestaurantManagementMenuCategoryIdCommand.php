<?php

namespace Solivis\Commands;

use Solivis\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling; 
use Solivis\MenuCategory;

class GetRestaurantManagementMenuCategoryIdCommand extends Command{

	public $category_name;

	public function __construct($category_name){
		$this->category_name = $category_name;
	}

	public function getMenuCategoryId(){

		$exist_menu_category = MenuCategory::where('name', $this->category_name)->first();

		if ($exist_menu_category){ 
			return $exist_menu_category->id;
		}
		else{
			MenuCategory::create([
				'name' => $this->category_name
			]);			
			return MenuCategory::where('name', $this->category_name)->first()->id;
		}
	}

}
