<?php

namespace Solivis\Commands;

use Solivis\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling; 
use Solivis\User;

class UpdateProfileCommand extends Command implements SelfHandling{

	public $id;
	public $name;
	public $email;
	public $phone_number;
	public $gender;
	public $birth_date;

	public function __construct($id, $name, $email, $phone_number, $gender, $birth_date){
		$this->id   = $id;
		$this->name = $name;
		$this->email = $email;
		$this->phone_number = $phone_number;
		$this->gender = $gender;
		$this->birth_date = $birth_date;
	}

	public function handle(){
		return User::find($this->id)->update([
			'name' => $this->name,
			'email' => $this->email,
			'phone_number' => $this->phone_number,
			'gender' => $this->gender,
			'birth_date' => $this->birth_date,
		]);
	}

}