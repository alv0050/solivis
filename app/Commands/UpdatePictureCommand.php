<?php

namespace Solivis\Commands;

use Solivis\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling; 
use Solivis\User;

class UpdatePictureCommand extends Command implements SelfHandling{

	public $id;
	public $photo_url;

	public function __construct($id, $photo_url){
		$this->id   = $id;
		$this->photo_url = $photo_url;
	}

	public function handle(){
		return User::find($this->id)->update([
			'photo_url' => $this->photo_url
		]);
	}

}