<?php

namespace Solivis\Commands;

use Solivis\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling; 
use Solivis\Menu;

class GetTotalReservationPriceCommand extends Command{

	public $total_price = 0;
	public $item_count;
	public $menu_base_price;

	public function setTotalPrice($reservation_menu){
		$this->item_count = $reservation_menu->item_count;
		$this->menu_base_price = $reservation_menu->menu_base_price;

		$this->total_price += ($this->item_count * $this->menu_base_price);
	}

	public function getTotalPrice(){
		return $this->total_price;
	}

}
