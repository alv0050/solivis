<?php

namespace Solivis\Commands;

use Solivis\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling; 
use Solivis\Menu;

class DeleteRestaurantManagementMenuCommand extends Command implements SelfHandling{

	public $id;

	public function __construct($id){
		$this->id = $id;
	}

	public function handle(){
		return Menu::find($this->id)->delete();
	}
}