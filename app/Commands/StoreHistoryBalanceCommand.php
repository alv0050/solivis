<?php

namespace Solivis\Commands;

use Solivis\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling; 
use Solivis\HistoryBalance;
use Solivis\UserBank;
use Carbon\Carbon;

class StoreHistoryBalanceCommand extends Command implements SelfHandling{

	public $process_date;
	public $status_balance_id; //Penarikan dana sedang diproses
	public $user_id;
	public $balance_processed;
	public $user_bank_id;
	public $transfer_target;

	public function __construct($user_id, $balance_processed, $user_bank_id){
		$this->process_date = Carbon::now();
		$this->status_balance_id = 3;

		$this->user_id = $user_id;
		$this->balance_processed = $balance_processed;
		$this->user_bank_id = $user_bank_id;

		$user_bank = UserBank::find($this->user_bank_id);
		$this->transfer_target = $user_bank->bank->name."-".$user_bank->account_number." a/n ".$user_bank->account_name;
	}

	public function handle(){
		return HistoryBalance::create([
			'process_date' => $this->process_date,
			'balance_processed' => $this->balance_processed,
			'status_balance_id' => $this->status_balance_id,
			'user_id' => $this->user_id,
			'transfer_target' => $this->transfer_target,
		]);
	}
}