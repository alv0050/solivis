<?php

namespace Solivis\Commands;

use Solivis\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling; 
use Solivis\UserBank;

class StoreUserBankCommand extends Command implements SelfHandling{

	public $account_number;
	public $account_name;
	public $bank_id;
	public $user_id;

	public function __construct($account_number, $account_name, $bank_id, $user_id){
		$this->account_number = $account_number;
		$this->account_name = $account_name;
		$this->bank_id = $bank_id;
		$this->user_id = $user_id;
	}

	public function handle(){

		return UserBank::create([
			'account_number' => $this->account_number,
			'account_name' => $this->account_name,
			'bank_id' => $this->bank_id,
			'user_id' => $this->user_id,
		]);

	}
}