<?php

namespace Solivis\Commands;

use Solivis\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling; 
use Solivis\ReviewComment;

class StoreReviewCommentCommand extends Command implements SelfHandling{

	public $reservation_id;
	public $comment;

	public function __construct($reservation_id, $comment){
		$this->reservation_id = $reservation_id;
		$this->comment = $comment;
	}

	public function handle(){
		return ReviewComment::create([
			'reservation_id' => $this->reservation_id,
			'comment' => $this->comment,
		]);			   
	}

}