<?php

namespace Solivis\Commands;

use Solivis\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling; 
use Solivis\Menu;

class StoreRestaurantManagementMenuCommand extends Command implements SelfHandling{

	public $restaurant_management_id;
	public $item_name;
	public $item_description;
	public $item_price;
	public $menu_category_id;

	public function __construct($item_name, $item_description, $item_price, $menu_category_id, $restaurant_management_id){
		$this->item_name = $item_name;
		$this->item_description = $item_description;
		$this->item_price = $item_price;
		$this->menu_category_id = $menu_category_id;
		$this->restaurant_management_id = $restaurant_management_id;	
	}

	public function handle(){
		return Menu::create([
			'item_name' => $this->item_name,
			'item_description' => $this->item_description,
			'item_price' => $this->item_price,
			'menu_category_id' => $this->menu_category_id,
			'restaurant_management_id' => $this->restaurant_management_id,
		]);			   
	}

}