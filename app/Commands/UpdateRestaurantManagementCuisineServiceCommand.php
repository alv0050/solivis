<?php

namespace Solivis\Commands;

use Solivis\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling; 
use Solivis\RestaurantCuisine;
use Solivis\RestaurantService;

class UpdateRestaurantManagementCuisineServiceCommand extends Command implements SelfHandling{

	public $cuisines;
	public $services;
	public $restaurant_management_id;

	public function __construct($restaurant_management_id, $cuisines, $services){
		$this->restaurant_management_id = $restaurant_management_id;
		$this->cuisines = $cuisines;
		$this->services = $services;
	}

	public function handle(){

		$cuisines_old =  RestaurantCuisine::where('restaurant_management_id', $this->restaurant_management_id)
										   ->lists('cuisine_id')->toArray();

		$services_old =  RestaurantService::where('restaurant_management_id', $this->restaurant_management_id)
										   ->lists('service_id')->toArray();

	   	$cuisines_removed = array_diff($cuisines_old, $this->cuisines); 
	   	if ($cuisines_removed){
			foreach ($cuisines_removed as $cuisine_removed) {
				RestaurantCuisine::where('cuisine_id', $cuisine_removed)->delete();
	   		}	   		
	   	}

	   	$cuisines_added = array_diff($this->cuisines, $cuisines_old); 
	   	if ($cuisines_added){
			foreach ($cuisines_added as $cuisine_added) {
				RestaurantCuisine::create([
					'cuisine_id' => $cuisine_added,
					'restaurant_management_id' => $this->restaurant_management_id
				]);
	   		}	   			   		
	   	}

	   	$services_removed = array_diff($services_old, $this->services); 
	   	if ($services_removed){
			foreach ($services_removed as $service_removed) {
				RestaurantService::where('service_id', $service_removed)->delete();
	   		}	   		
	   	}

	   	$services_added = array_diff($this->services, $services_old); 
	   	if ($services_added){
			foreach ($services_added as $service_added) {
				RestaurantService::create([
					'service_id' => $service_added,
					'restaurant_management_id' => $this->restaurant_management_id
				]);
	   		}	   			   		
	   	}


	}
}