<?php

namespace Solivis\Commands;

use Solivis\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling; 
use Solivis\QrCodePlain;

class StoreQrCodePlainCommand extends Command implements SelfHandling{

	public $code;
	public $reservation_id;

	public function __construct($code, $reservation_id){
		$this->code = $code;
		$this->reservation_id = $reservation_id;
	}

	public function handle(){
		return QrCodePlain::create([
			'code' => $this->code,
			'reservation_id' => $this->reservation_id
		]);
	}
}