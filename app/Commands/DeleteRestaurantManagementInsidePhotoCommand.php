<?php

namespace Solivis\Commands;

use Solivis\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling; 
use Solivis\RestaurantInsidePhoto;
use File;

class DeleteRestaurantManagementInsidePhotoCommand extends Command implements SelfHandling{

	public $id;
	public $photo_url;

	public function __construct($id, $photo_url){
		$this->id = $id;
		$this->photo_url = $photo_url;
	}

	public function handle(){
		File::delete(public_path('images/restaurant_uploads/inside_restaurant_photos/'.$this->photo_url));
		return RestaurantInsidePhoto::find($this->id)->delete();
	}
}
