<?php

namespace Solivis\Commands;

use Solivis\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling; 
use Solivis\ExpiredComingConfirmation;
use Solivis\Reservation;
use Carbon\Carbon;

class StoreExpiredComingConfirmationCommand extends Command implements SelfHandling{

	public $reservation_id;
	public $expired_date;

	public function __construct($reservation_id){
		$this->reservation_id = $reservation_id;
		$this->expired_date = Carbon::parse(Reservation::find($reservation_id)->reservation_date)->subHour(1);
	}

	public function handle(){
		return ExpiredComingConfirmation::create([
			'expired_date' => $this->expired_date,
			'reservation_id' => $this->reservation_id,
		]);
	}
}