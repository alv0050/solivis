<?php

namespace Solivis\Commands;

use Solivis\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling; 
use Solivis\RestaurantManagement;

class DeleteRestaurantManagementCommand extends Command implements SelfHandling{

	public $id;

	public function __construct($id){
		$this->id = $id;
	}

	public function handle(){
		return RestaurantManagement::find($this->id)->delete();
	}
}
