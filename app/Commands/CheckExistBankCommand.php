<?php

namespace Solivis\Commands;

use Solivis\Commands\Command;
use Solivis\UserBank;
use Solivis\Bank;
use Solivis\RestaurantBank;

class CheckExistBankCommand extends Command{

	public $bank_id;
	public $account_number;
	public $id;
	public $user_or_restaurant_bank_id;

	public function __construct($bank_id, $account_number, $id, $user_or_restaurant_bank_id){
		$this->bank_id = $bank_id;
		$this->account_number = $account_number;
		$this->id = $id;
		$this->user_or_restaurant_bank_id = $user_or_restaurant_bank_id;
	}

	public function checkExistUserBank(){

		$user_banks = UserBank::where('user_id', $this->id)->get();

		foreach ($user_banks as $user_bank)
		{
			//check if same bank with same account_number already exist 
			if ($user_bank->bank->id == $this->bank_id && $user_bank->account_number == $this->account_number){
				return true;
			}
		}
		return false;
	}

	public function checkExistUserBankForUpdate(){
		$user_banks = UserBank::where('user_id', $this->id)->get();

		foreach ($user_banks as $user_bank)
		{
			//check if same bank with same account_number already exist 
			if ($user_bank->bank->id == $this->bank_id && $user_bank->account_number == $this->account_number && $user_bank->id != $this->user_or_restaurant_bank_id){
				return true;
			}
		}
		return false;	
	}

	public function checkExistRestaurantManagementBank(){

		$restaurant_banks = RestaurantBank::where('restaurant_management_id', $this->id)->get();

		foreach ($restaurant_banks as $restaurant_bank)
		{
			//check if same bank with same account_number already exist 
			if ($restaurant_bank->bank->id == $this->bank_id && $restaurant_bank->account_number == $this->account_number){
				return true;
			}
		}
		return false;		
	}

	public function checkExistRestaurantManagementBankForUpdate(){
		$restaurant_banks = RestaurantBank::where('restaurant_management_id', $this->id)->get();

		foreach ($restaurant_banks as $restaurant_bank)
		{
			//check if same bank with same account_number already exist 
			if ($restaurant_bank->bank->id == $this->bank_id && $restaurant_bank->account_number == $this->account_number && $restaurant_bank->id != $this->user_or_restaurant_bank_id){
				return true;
			}
		}
		return false;	
	}	

}