<?php

namespace Solivis\Commands;

use Solivis\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling; 
use Solivis\DeclinedReason;

class StoreDeclinedReasonCommand extends Command implements SelfHandling{

	public $reservation_id;
	public $reason;

	public function __construct($reservation_id, $reason){
		$this->reservation_id = $reservation_id;
		$this->reason = $reason;
	}

	public function handle(){
		return DeclinedReason::create([
			'reservation_id' => $this->reservation_id,
			'reason' => $this->reason
		]);
	}
}