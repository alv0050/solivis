<?php

namespace Solivis\Commands;

use Solivis\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling; 
use Solivis\RestaurantManagement;

class UpdateRestaurantManagementProfileCommand extends Command implements SelfHandling{

	public $id;
	public $name;
	public $address;
	public $email;
	public $website_url;
	public $phone_number;
	public $open_time;
	public $close_time;

	public function __construct($id, $name, $address, $email, $website_url, $phone_number, $open_time, $close_time){
		$this->id   = $id;
		$this->name = $name;
		$this->address = $address;
		$this->email = $email;
		$this->website_url = $website_url;
		$this->phone_number = $phone_number;
		$this->open_time = $open_time;
		$this->close_time = $close_time;
	}

	public function handle(){
		return RestaurantManagement::find($this->id)->update([
			'name' => $this->name,
			'address' => $this->address,
			'email' => $this->email,
			'website_url' => $this->website_url,
			'phone_number' => $this->phone_number,
			'open_time' => $this->open_time,
			'close_time' => $this->close_time,
		]);
	}

}