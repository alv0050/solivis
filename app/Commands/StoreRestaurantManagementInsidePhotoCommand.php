<?php

namespace Solivis\Commands;

use Solivis\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling; 
use Solivis\RestaurantInsidePhoto;

class StoreRestaurantManagementInsidePhotoCommand extends Command implements SelfHandling{

	public $restaurant_management_id;
	public $inside_restaurant_photo_url;
	public $inside_restaurant_photo_name;

	public function __construct($restaurant_management_id, $inside_restaurant_photo_url, $inside_restaurant_photo_name){
		$this->restaurant_management_id   = $restaurant_management_id;
		$this->inside_restaurant_photo_url = $inside_restaurant_photo_url;
		$this->inside_restaurant_photo_name = $inside_restaurant_photo_name;
	}

	public function handle(){
		
		return RestaurantInsidePhoto::create([
			'restaurant_management_id' => $this->restaurant_management_id,
			'inside_restaurant_photo_url' => $this->inside_restaurant_photo_url,
			'inside_restaurant_photo_name' => $this->inside_restaurant_photo_name,
		]);			   
	}

}