<?php

namespace Solivis\Commands;

use Solivis\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling; 
use Solivis\RestaurantManagement;

class UpdateRestaurantManagementPasswordCommand extends Command implements SelfHandling{

	public $id;
	public $password;

	public function __construct($id, $password){
		$this->id = $id;
		$this->password = $password;
	}

	public function handle(){
		return RestaurantManagement::find($this->id)->update([
		    'password' => $this->password
		]);
	}
}