<?php

namespace Solivis\Commands;

use Solivis\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling; 
use Solivis\RestaurantBank;

class UpdateRestaurantManagementBankCommand extends Command implements SelfHandling{

	public $account_number;
	public $account_name;
	public $restaurant_bank_id;

	public function __construct($account_number, $account_name, $restaurant_bank_id){
		$this->account_number = $account_number;
		$this->account_name = $account_name;
		$this->restaurant_bank_id = $restaurant_bank_id;
	}

	public function handle(){
      	return RestaurantBank::find($this->restaurant_bank_id)
      			   ->update([
	      			   	'account_number' => $this->account_number,
	      			   	'account_name' => $this->account_name
      			   	]);
	}
}