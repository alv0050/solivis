<?php

namespace Solivis\Commands;

use Solivis\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling; 
use Solivis\Favorite;

class StoreFavoriteCommand extends Command implements SelfHandling{

	public $restaurant_management_id;
	public $user_id;

	public function __construct($restaurant_management_id, $user_id){
		$this->restaurant_management_id = $restaurant_management_id;
		$this->user_id = $user_id;
	}

	public function handle(){
		return Favorite::create([
			'restaurant_management_id' => $this->restaurant_management_id,
			'user_id' => $this->user_id
		]);
	}
}