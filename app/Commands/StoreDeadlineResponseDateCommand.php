<?php

namespace Solivis\Commands;

use Solivis\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling; 
use Solivis\DeadlineResponseDate;

class StoreDeadlineResponseDateCommand extends Command implements SelfHandling{

	public $reservation_id;
	public $deadline_date;

	public function __construct($reservation_id, $deadline_date){
		$this->reservation_id = $reservation_id;
		$this->deadline_date = $deadline_date;
	}

	public function handle(){
		return DeadlineResponseDate::create([
			'reservation_id' => $this->reservation_id,
			'deadline_date' => $this->deadline_date
		]);
	}
}