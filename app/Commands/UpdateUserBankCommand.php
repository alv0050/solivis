<?php

namespace Solivis\Commands;

use Solivis\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling; 
use Solivis\UserBank;

class UpdateUserBankCommand extends Command implements SelfHandling{

	public $account_number;
	public $account_name;
	public $user_bank_id;

	public function __construct($account_number, $account_name, $user_bank_id){
		$this->account_number = $account_number;
		$this->account_name = $account_name;
		$this->user_bank_id = $user_bank_id;
	}

	public function handle(){

      	return UserBank::find($this->user_bank_id)
      			   ->update([
	      			   	'account_number' => $this->account_number,
	      			   	'account_name' => $this->account_name
      			   	]);
	}
}