<?php

namespace Solivis;

use Illuminate\Database\Eloquent\Model;

class SubscribedEmail extends Model
{

   	protected $table = 'subscribed_emails';

   	protected $fillable = ['email'];

   	protected $hidden = [];

}
