<?php

namespace Solivis;

use Illuminate\Database\Eloquent\Model;

class ReviewRating extends Model
{
    protected $table = 'review_ratings';
    
    protected $fillable = ['rating', 'reservation_id'];

    protected $hidden = [];

	public function reservation(){ 
		return $this->belongsTo('Solivis\Reservation');
	}
}
