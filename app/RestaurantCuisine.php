<?php

namespace Solivis;

use Illuminate\Database\Eloquent\Model;

class RestaurantCuisine extends Model
{
    protected $table = 'restaurant_cuisines';
    
    protected $fillable = ['cuisine_id', 'restaurant_management_id'];

    protected $hidden = [];

	public function restaurant_management(){ 
		return $this->belongsTo('Solivis\RestaurantManagement');
	}

	public function cuisine(){
		return $this->belongsTo('Solivis\Cuisine', 'cuisine_id');
	}
}
