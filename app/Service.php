<?php

namespace Solivis;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
   	protected $table = 'services';

   	protected $fillable = ['name'];

   	protected $hidden = [];

	public function restaurant_service(){ 
		return $this->hasOne('Solivis\RestaurantServices');
	}
}
