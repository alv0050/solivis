<?php

namespace Solivis;

use Illuminate\Database\Eloquent\Model;

class UserBank extends Model
{
    protected $table = 'user_banks';
    
    protected $fillable = ['account_number', 'account_name', 'user_id', 'bank_id'];

    protected $hidden = [];

	public function user(){ 
		return $this->belongsTo('Solivis\User');
	}

	public function bank(){
		return $this->belongsTo('Solivis\Bank');		
	}
}
