<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Make Blade can perform break or continue in looping
Blade::extend(function($value)
{
  return preg_replace('/(\s*)@(break|continue)(\s*)/', '$1<?php $2; ?>$3', $value);
});

Route::group(['middleware' => ['cookies', 'cookies.queue', 'start_session', 'error_session', 'csrf']], function () {
    // USER
    // Authentication
    Route::get('register', 'Auth\AuthController@getRegister');
    Route::post('register', 'Auth\AuthController@postRegister');
    Route::get('login', 'Auth\AuthController@getLogin');
    Route::post('login', 'Auth\AuthController@postLogin');
    Route::get('logout', 'Auth\AuthController@getLogout');

    // Email Verification 
    Route::get('register/verify/{confirmationCode}', [
        'as' => 'confirmation_path',
        'uses' => 'RegistrationController@confirm'
    ]);

    // Send Link Reset Password
    Route::get('password/email', 'Auth\PasswordController@getEmail');
    Route::post('password/email', 'Auth\PasswordController@postEmail');

    //Reset Password
    Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
    Route::post('password/reset', 'Auth\PasswordController@postReset');

    // Profile
    Route::controller('profile', 'ProfileController'); 

    // Balance
    Route::controller('balance', 'BalanceController');

    // Base
    Route::get('/', 'BaseController@getIndex');

    Route::get('about', 'BaseController@getAbout');

    Route::get('contact', 'BaseController@getContact');
    Route::post('contact', 'BaseController@postContact');

    Route::get('business', 'BaseController@getBusiness');
    Route::post('business', 'BaseController@postBusiness');

    Route::get('privacy-policy', 'BaseController@getPrivacyPolicy');

    Route::get('terms', 'BaseController@getTerms');

    Route::get('search','BaseController@getSearchReservation');

    Route::get('sort-restaurant','BaseController@getSortRestaurant');

    Route::get('search-for-restaurant-direct','BaseController@getSearchForRestaurantDirect');

    Route::get('filter-search','BaseController@getFilterSearch');

    Route::post('subscribe','BaseController@postSubscribe');

    // Restaurant
    Route::controller('restaurant', 'RestaurantController');

    // Reservation
    Route::controller('reservation', 'ReservationController');

    //Payment
    Route::controller('payment', 'PaymentController');

    // RESTAURANT MANAGEMENT
    Route::get('restaurant-management/login', 'Auth\RestaurantManagementAuthController@getLogin');
    Route::post('restaurant-management/login', 'Auth\RestaurantManagementAuthController@postLogin');
    Route::get('restaurant-management/logout', 'Auth\RestaurantManagementAuthController@getLogout');

    Route::controller('restaurant-management', 'RestaurantManagementController');
    // END: RESTAURANT MANAGEMENT

    // ADMIN
    Route::get('admin/login', 'Auth\AdminAuthController@getLogin');
    Route::post('admin/login', 'Auth\AdminAuthController@postLogin');
    Route::get('admin/logout', 'Auth\AdminAuthController@getLogout');

    Route::controller('admin', 'AdminController');
    // END: ADMIN

    //CRON JOB
    Route::get('delete-reservation-over-deadline', 'CronJobController@getDeleteReservation');
    Route::get('check-user-coming-over-deadline', 'CronJobController@getCheckUserComing');
    Route::get('send-email-confirmation', 'CronJobController@getSendEmail');
});

Route::group(['middleware' => ['cookies', 'cookies.queue', 'start_session', 'error_session', 'csrf']], function () {
    Route::get('token', function(){
        header('Content-type: application/json');
        $response = array();
        $response['token'] = csrf_token();
        $response = json_encode(array($response));
        
        return response($response, 200)->header('Content-Type', 'application/json');
    });
    
    // Customer
    Route::get('api/customer/login', 'Auth\ApiCustomerAuthController@getLogin');
    Route::post('api/customer/login', 'Auth\ApiCustomerAuthController@postLogin');
    Route::get('api/customer/logout', 'Auth\ApiCustomerAuthController@getLogout');
    
    Route::get('api/customer/profile', 'ApiCustomerProfileController@index');
    
    // Restaurant
    Route::get('api/restaurant/login', 'Auth\ApiRestaurantAuthController@getLogin');
    Route::post('api/restaurant/login', 'Auth\ApiRestaurantAuthController@postLogin');
    Route::get('api/restaurant/logout', 'Auth\ApiRestaurantAuthController@getLogout');
    
    Route::resource('api/restaurant/profile', 'ApiRestaurantProfileController', ['only' => ['index', 'store']]);
    
    Route::resource('api/restaurant/reservation', 'ApiReservationController');
    Route::resource('api/restaurant/reservation/order-item', 'ApiOrderItemController');
});