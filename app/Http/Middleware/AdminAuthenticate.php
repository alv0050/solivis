<?php
 
namespace Solivis\Http\Middleware;
 
use Closure;
use Illuminate\Contracts\Auth\Guard;
 
class AdminAuthenticate
{
    protected $auth;
    
    public function __construct(Guard $auth)
    {
        $this->auth = $auth->with('admin');
    }

    public function handle($request, Closure $next)
    {
        if (\Auth::guest("admin")) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('admin/login');
            }
        }
 
        return $next($request);
    }
}