<?php

namespace Solivis\Http\Middleware;
 
use Closure;
use Illuminate\Contracts\Auth\Guard;

class RestaurantManagementGuest
{
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth->with('restaurant_management');
    }
    
    public function handle($request, Closure $next)
    {
        if (\Auth::check('restaurant_management')) {
            return redirect('restaurant-management/dashboard');
        }
 
        return $next($request);
    }
}