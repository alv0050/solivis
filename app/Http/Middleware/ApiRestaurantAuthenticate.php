<?php

namespace Solivis\Http\Middleware;

use Closure;
// use Illuminate\Contracts\Auth\Guard;

class ApiRestaurantAuthenticate
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    // protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    // public function __construct(Guard $auth)
    // {
    //     $this->auth = $auth->with('restaurant_management');
    // }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::guest('restaurant_management')) {
            $response = array();
            $response['message'] = 'Unauthorized';
            $response = json_encode(array($response));
            return response($response, 401)->header('Content-Type', 'application/json');
        }

        return $next($request);
    }
}
