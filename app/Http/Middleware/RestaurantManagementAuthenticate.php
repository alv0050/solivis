<?php

namespace Solivis\Http\Middleware;
 
use Closure;
use Illuminate\Contracts\Auth\Guard;
 
class RestaurantManagementAuthenticate
{
    protected $auth;
    
    public function __construct(Guard $auth)
    {
        $this->auth = $auth->with('restaurant_management');
    }

    public function handle($request, Closure $next)
    {
        if (\Auth::guest("restaurant_management")) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('restaurant-management/login');
            }
        }
 
        return $next($request);
    }
}