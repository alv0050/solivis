<?php
 
namespace Solivis\Http\Middleware;
 
use Closure;
use Illuminate\Contracts\Auth\Guard;

class AdminGuest
{
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth->with('admin');
    }
    
    public function handle($request, Closure $next)
    {
        if (\Auth::check('admin')) {
            return redirect('admin/index');
        }
 
        return $next($request);
    }
}