<?php

namespace Solivis\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class ApiRestaurantGuest
{
    
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth->with('restaurant_management');
    }
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::check('restaurant_management')) {
            $response = array();
            $response['message'] = 'Logged In';
            $response = json_encode(array($response));
            return response($response, 200)->header('Content-Type', 'application/json');
        }
 
        return $next($request);
    }
}
