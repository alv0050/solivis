<?php

namespace Solivis\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        // \Solivis\Http\Middleware\EncryptCookies::class,
        // \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        // \Illuminate\Session\Middleware\StartSession::class,
        // \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        // \Solivis\Http\Middleware\VerifyCsrfToken::class,
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \Solivis\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'guest' => \Solivis\Http\Middleware\RedirectIfAuthenticated::class,
        'admin' => \Solivis\Http\Middleware\AdminAuthenticate::class,
        'admin.guest' => \Solivis\Http\Middleware\AdminGuest::class,
        'restaurant_management' => \Solivis\Http\Middleware\RestaurantManagementAuthenticate::class,
        'restaurant_management.guest' => \Solivis\Http\Middleware\RestaurantManagementGuest::class,
        
        'cookies' => \Solivis\Http\Middleware\EncryptCookies::class,
        'cookies.queue' => \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        'start_session' => \Illuminate\Session\Middleware\StartSession::class,
        'error_session' => \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        'csrf' => \Solivis\Http\Middleware\VerifyCsrfToken::class,
        
        // REST-API
        // Customer
        'api.customer.auth' => \Solivis\Http\Middleware\ApiCustomerAuthenticate::class,
        'api.customer.guest' => \Solivis\Http\Middleware\ApiCustomerGuest::class,
        // Restaurant
        'api.restaurant.auth' => \Solivis\Http\Middleware\ApiRestaurantAuthenticate::class,
        'api.restaurant.guest' => \Solivis\Http\Middleware\ApiRestaurantGuest::class,
    ];
}
