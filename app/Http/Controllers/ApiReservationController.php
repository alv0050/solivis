<?php

namespace Solivis\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use DB;
use Solivis\RestaurantManagement;
use Solivis\Reservation;
use Solivis\Http\Requests;
use Solivis\Http\Controllers\Controller;
use Illuminate\Http\Response;

class ApiReservationController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('api.restaurant.auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $restaurant_id = Auth::user('restaurant_management')->id;
        $response = DB::select('SELECT Reservation.id AS ReservationID, Customer.id AS CustomerID, Customer.name AS Name , paxes AS Paxes, 
            DATE_FORMAT(reservation_date, \'%Y-%m-%d %H:%i:%s\') AS ReservationDate FROM reservations AS Reservation
            JOIN `users` AS Customer
            ON Customer.id = Reservation.user_id
            AND Reservation.restaurant_management_id = :restaurant_id
            JOIN qr_code_plains AS QRCode
            ON Reservation.id = QRCode.reservation_id
            AND QRCode.status_processed = TRUE
            AND Reservation.status_arrival_id = 1
            ORDER BY Reservation.reservation_date DESC
            LIMIT 10',
            ['restaurant_id' => $restaurant_id]);
        
        foreach ($response as $key => $value) {
            $order_item = DB::select('SELECT OrderItem.reservation_id AS ReservationID, OrderItem.id AS ItemID, OrderItem.menu_name AS Name, OrderItem.item_count AS Qty, OrderItem.menu_base_price AS BasePrice
            FROM reservation_menus AS OrderItem
            WHERE OrderItem.reservation_id = ?', [$response[$key]->ReservationID]);
            $response[$key]->OrderItem = $order_item;
        }
        return response(json_encode($response), 200)->header('Content-Type', 'application/json');
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->input('update') == 'scan'){
            $restaurant_id = Auth::user('restaurant_management')->id;
            $response = array();
            
            $reservation = DB::select('SELECT Reservation.id AS ReservationID, status_processed AS `Status` FROM reservations AS Reservation
                JOIN expired_coming_confirmations
                ON Reservation.id = expired_coming_confirmations.reservation_id
                AND Reservation.restaurant_management_id = :restaurant_id
                AND expired_coming_confirmations.status_sent = TRUE
                JOIN qr_code_plains AS QRCode
                ON Reservation.id = QRCode.reservation_id
                AND QRCode.code = :code',
                ['restaurant_id' => $restaurant_id,
                 'code' => $request->input('code')]);
            
            if(count($reservation)){
                if($reservation[0]->Status == 0){
                    DB::table('qr_code_plains')
                    ->where('code', $request->input('code'))
                    ->update(['status_processed' => 1]);
                    
                    DB::table('reservations')
                    ->where('id', $reservation[0]->ReservationID)
                    ->update(['status_arrival_id' => 1]);

                    //add total net income to restaurant management balance
                    $scanned_reservation = Reservation::find($reservation[0]->ReservationID);
                    $total_net_income = $scanned_reservation->getTotalNetIncome();
                    $restaurant_management_id = $scanned_reservation->restaurant_management_id;
                    $restaurant_management = RestaurantManagement::find($restaurant_management_id);
                    $restaurant_management->balance += $total_net_income;
                    $restaurant_management->save();
                    
                    $response['ReservationID'] = $reservation[0]->ReservationID;
                    return response(json_encode(array($response)), 200)->header('Content-Type', 'application/json');
                }
                else{
                    $response['message'] = 'QR Code has been used!';
                    return response(json_encode(array($response)), 400)->header('Content-Type', 'application/json');
                }
            }
            else{
                $response['message'] = 'Reservation not found.';
                return response(json_encode(array($response)), 404)->header('Content-Type', 'application/json');
            }
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $restaurant_id = Auth::user('restaurant_management')->id;
        $response = DB::select('SELECT Reservation.id AS ReservationID, Customer.id AS CustomerID, Customer.name AS Name , paxes AS Paxes, 
            DATE_FORMAT(reservation_date, \'%Y-%m-%d %H:%i:%s\') AS ReservationDate FROM reservations AS Reservation
            JOIN `users` AS Customer
            ON Customer.id = Reservation.user_id
            AND Reservation.restaurant_management_id = :restaurant_id
            AND Reservation.id = :reservation_id', ['restaurant_id' => $restaurant_id, 'reservation_id' => $id]);
            
        foreach ($response as $key => $value) {
            $order_item = DB::select('SELECT OrderItem.reservation_id AS ReservationID, OrderItem.id AS ItemID, OrderItem.menu_name AS Name, OrderItem.item_count AS Qty, OrderItem.menu_base_price AS BasePrice
            FROM reservation_menus AS OrderItem
            WHERE OrderItem.reservation_id = ?', [$response[$key]->ReservationID]);
            $response[$key]->OrderItem = $order_item;
        }
        
        return response(json_encode($response), 200)->header('Content-Type', 'application/json');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
