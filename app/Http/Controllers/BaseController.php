<?php

namespace Solivis\Http\Controllers;

use Illuminate\Http\Request;
use Snowfire\Beautymail\Beautymail;
use Solivis\Cuisine;
use Solivis\RestaurantCuisine;
use Solivis\RestaurantManagement;
use Solivis\Commands\StoreSubscribedEmailCommand;

use Solivis\Http\Requests;
use Solivis\Http\Controllers\Controller;

class BaseController extends Controller
{

    public function getIndex(){
        return view('user.index');
    }

    public function getAbout(){
        return view('user.about');
    }

	public function getContact(){
		return view('user.contact');
	}

	public function postContact(Request $request){
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'message' => 'required'
        ]);

        $logo['path'] = '';
        $logo['width'] = '0px';
        $logo['height'] = '0px';
        $beautymail = app()->make(Beautymail::class);
        $beautymail->send('emails.contact', 
            [
                'logo' => $logo,
                'user_name' => $request->name, 
                'user_email' => $request->email, 
                'user_message' => $request->message           
            ],  function($message) use($request) {
                    $message->from($request->email, $request->name)
                            ->to('solivisads@gmail.com', 'Solivisads')
                            ->subject('Pesan Contact Us dari pengguna Solivis');
        });

        return redirect('/')->with([
        	'info' => 'Pesan anda telah berhasil dikirim! Terima kasih.'
    	]);
	}

    public function getBusiness(){
        return view('user.business');
    }

    public function getPrivacyPolicy(){
        return view('user.privacy-policy');
    }

    public function getTerms(){
        return view('user.terms');
    }

    public function getSearchReservation(Request $request){
        $restaurant_name = $request->restaurant_name;
        $cuisine_name = $request->cuisine;
        $paxes = $request->paxes;
        $reservation_date = $request->reservation_date;
        $reservation_time = $request->reservation_time;

        $address = $request->address;
        $total_search_results = 0;

        if($address){
            $restaurants_search = RestaurantManagement::where('status', '1')->where('address', 'LIKE', '%'.$address.'%')->paginate(3);
            $total_search_results = RestaurantManagement::where('status', '1')->where('address', 'LIKE', '%'.$address.'%')->get();
        }
        //restaurant name is more tight than other keyword. 
        else if($restaurant_name){
            if($reservation_time){
                $convert_reservation_time = date('H:i:s', strtotime($reservation_time));
                $restaurants_search = RestaurantManagement::where('status', '1')->where('name', 'LIKE', '%'.$restaurant_name.'%')
                                      ->where('open_time', '<=' , $reservation_time)
                                      ->where('close_time', '>=', $reservation_time)
                                      ->paginate(3);

                $total_search_results = RestaurantManagement::where('status', '1')->where('name', 'LIKE', '%'.$restaurant_name.'%')
                                      ->where('open_time', '<=' , $reservation_time)
                                      ->where('close_time', '>=', $reservation_time)
                                      ->get();
            }
            else{
                $restaurants_search = RestaurantManagement::where('status', '1')->where('name', 'LIKE', '%'.$restaurant_name.'%')->paginate(3);
                $total_search_results = RestaurantManagement::where('status', '1')->where('name', 'LIKE', '%'.$restaurant_name.'%')->get();                
            }
        }
        else if($cuisine_name){
            if($reservation_time){
                $restaurants_search = RestaurantManagement::where('status', '1')->WhereHas('restaurant_cuisines', function($restaurant_cuisines) use ($cuisine_name){
                                    $restaurant_cuisines->whereHas('cuisine', function($cuisine) use ($cuisine_name){
                                        $cuisine->where('name', $cuisine_name);
                                        });
                                    })->where('open_time', '<=' , $reservation_time)
                                    ->where('close_time', '>=', $reservation_time)
                                    ->paginate(3);
                $total_search_results = RestaurantManagement::where('status', '1')->WhereHas('restaurant_cuisines', function($restaurant_cuisines) use ($cuisine_name){
                                    $restaurant_cuisines->whereHas('cuisine', function($cuisine) use ($cuisine_name){
                                        $cuisine->where('name', $cuisine_name);
                                        });
                                    })->where('open_time', '<=' , $reservation_time)
                                    ->where('close_time', '>=', $reservation_time)->get();                

            }
            else{
                $restaurants_search = RestaurantManagement::where('status', '1')->WhereHas('restaurant_cuisines', function($restaurant_cuisines) use ($cuisine_name){
                                    $restaurant_cuisines->whereHas('cuisine', function($cuisine) use ($cuisine_name){
                                        $cuisine->where('name', $cuisine_name);
                                        });
                                    })->paginate(3);   

                $total_search_results = RestaurantManagement::where('status', '1')->WhereHas('restaurant_cuisines', function($restaurant_cuisines) use ($cuisine_name){
                                    $restaurant_cuisines->whereHas('cuisine', function($cuisine) use ($cuisine_name){
                                        $cuisine->where('name', $cuisine_name);
                                        });
                                    })->get();                
            }
        }
        else if($reservation_time){
                $restaurants_search = RestaurantManagement::where('status', '1')->where('open_time', '<=' , $reservation_time)
                                    ->where('close_time', '>=', $reservation_time)
                                    ->paginate(3);

                $total_search_results = RestaurantManagement::where('status', '1')->where('open_time', '<=' , $reservation_time)
                                    ->where('close_time', '>=', $reservation_time)
                                    ->get();
        }
        else{ //if user does not input restaurant name or cuisine name
            $restaurants_search = RestaurantManagement::where('status', '1')->paginate(3);
            $total_search_results = RestaurantManagement::where('status', '1')->get();
        }

        $recommendation_restaurants = (RestaurantManagement::where('status', '1')->with('reservations')->CountRestaurantManagementTotalRatings()->orderBy('total_ratings', 'desc')->get()->take(3));

        return view('user/search', compact('restaurants_search', 'address', 'restaurant_name', 'cuisine_name', 'paxes', 'reservation_date', 'reservation_time', 'recommendation_restaurants', 'total_search_results'));
    }

    public function getSortRestaurant(Request $request){
        if($request->sort_search_restaurant == "all"){
            $restaurants_search = RestaurantManagement::where('status', '1')->paginate(3);
            $total_search_results = RestaurantManagement::where('status', '1')->get();
        }
        else if($request->sort_search_restaurant == "restaurant_name"){
            $restaurants_search = RestaurantManagement::where('status', '1')->orderBy('name')->paginate(3);
            $total_search_results = RestaurantManagement::where('status', '1')->orderBy('name')->get();
        }
        else if($request->sort_search_restaurant == "rating"){
            $restaurants_search = (RestaurantManagement::where('status', '1')->with('reservations')->CountRestaurantManagementTotalRatings()->orderBy('total_ratings', 'desc')->paginate(3));
            $total_search_results = (RestaurantManagement::where('status', '1')->with('reservations')->CountRestaurantManagementTotalRatings()->orderBy('total_ratings', 'desc')->get());
        }
        else{ //Most Booked
            $restaurants_search = (RestaurantManagement::where('status', '1')->with('reservations')->CountRestaurantManagementReservationsSuccess()->orderBy('count', 'desc')->paginate(3));
            $total_search_results = (RestaurantManagement::where('status', '1')->with('reservations')->CountRestaurantManagementReservationsSuccess()->orderBy('count', 'desc')->get());
        }

        $recommendation_restaurants = (RestaurantManagement::where('status', '1')->with('reservations')->CountRestaurantManagementTotalRatings()->orderBy('total_ratings', 'desc')->get()->take(4));

        return view('user/search', compact('restaurants_search', 'recommendation_restaurants', 'total_search_results'));           
    }

    public function postBusiness(Request $request){
        $messages = [
            'email.unique' => 'Alamat Email sudah terdaftar.'
        ];

        $this->validate($request, [
            'user_name' => 'required|max:255',
            'name' => 'required|max:255',
            'address' => 'required|max:255',
            'phone_number' => 'required|numeric',
            'email' => 'required|email|max:255|unique:restaurant_managements'
        ], $messages);

        //send email
        $logo['path'] = '';
        $logo['width'] = '0px';
        $logo['height'] = '0px';
        $beautymail = app()->make(Beautymail::class);
        $beautymail->send('emails.business', 
            [
                'logo' => $logo,
                'user_name' => $request->user_name, 
                'user_business_name' => $request->name, 
                'user_address' => $request->address, 
                'user_phone_number' => $request->phone_number, 
                'user_email' => $request->email            
            ],  function($message) use($request) {
                    $message->from($request->email, $request->name)
                            ->to('solivisads@gmail.com', 'Solivisads')
                            ->subject('Pesan Konsultasi Bisnis dari pengguna Solivis');
        });  

        RestaurantManagement::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => '', //no password for guest restaurant_management
            'address' => $request->address,
            'phone_number' => $request->phone_number,
            'photo_url' => 'restaurant-default.png',
        ]);

        return redirect('/business')->with([
            'alert-send-business-message' => 'Sukses'
        ]);        
    }

    public function getSearchForRestaurantDirect(Request $request){
        $restaurant_name = $request->restaurant_name;
        $restaurant_management_id = RestaurantManagement::where('status', '1')->where('name', $restaurant_name)->first()->id;

        return redirect()->action('RestaurantController@getShow', [$restaurant_management_id]);
    }

    public function getFilterSearch(Request $request){
        $price_range = $request->prices;
        $cuisines_name = $request->cuisines;
        $services_name = $request->services;

        if(!$price_range && !$cuisines_name && !$services_name){
            return redirect('/search');
        }

        $query = RestaurantManagement::query();
        if($price_range){
            $query->SearchPriceRange($price_range);
        }
        if($cuisines_name){
            foreach($cuisines_name as $cuisine_name){
                $query->SearchCuisine($cuisine_name);
            }
        }
        if($services_name){
            foreach($services_name as $service_name){
                $query->SearchService($service_name);
            }
        }

        $recommendation_restaurants = (RestaurantManagement::where('status', '1')->with('reservations')->CountRestaurantManagementTotalRatings()->orderBy('total_ratings', 'desc')->get()->take(4));

        $total_search_results = $query->get();
        $restaurants_search = $query->paginate(3);

        return view('user/search', compact('restaurants_search', 'recommendation_restaurants', 'total_search_results'));
    }

    public function postSubscribe(Request $request){
        $messages = [
            'email.unique' => 'Alamat Email ini sudah subscribe.'
        ];

        $this->validate($request, [
            'email' => 'required|email|max:255|unique:subscribed_emails',
        ], $messages);

        $logo['path'] = '';
        $logo['width'] = '0px';
        $logo['height'] = '0px';
        $beautymail = app()->make(Beautymail::class);
        $beautymail->send('emails.subscribe', 
            [
                'logo' => $logo,
                'user_email' => $request->email
            ],  function($message) use($request) {
                    $message->from($request->email, 'User')
                            ->to('solivisads@gmail.com', 'Solivisads')
                            ->subject('Subscribe dari pengguna Solivis');
        });  

        $command = new StoreSubscribedEmailCommand($request->email);
        $this->dispatch($command);

        return redirect('/')->with([
            'info' => 'Anda telah berhasil subcribe! Terima kasih.'
        ]);
    }

}
