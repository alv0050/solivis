<?php

namespace Solivis\Http\Controllers;

use Illuminate\Http\Request;

use Solivis\RestaurantManagement;
use Solivis\Http\Requests;
use Solivis\Http\Controllers\Controller;
use Solivis\Commands\StoreReviewRatingCommand;
use Solivis\Commands\StoreReviewCommentCommand;
use Solivis\Commands\GetTotalReservationPriceCommand;
use Solivis\Reservation;
use Solivis\ReservationMenu;
use Solivis\User;
use Solivis\ExpiredComingConfirmation;
use Solivis\QrCodePlain;
use Solivis\DeadlineResponseDate;
use Session;
use Auth;
use Snowfire\Beautymail\Beautymail;
use Carbon\Carbon;
use Response;

class ReservationController extends Controller
{

    public function __construct(){
        //except because of entering from email
        $this->middleware('auth', ['except' => 'getChooseCancelReservationOptions']);
    }

    public function postIndex(Request $request){
        //only pay with balance
        if($request->pay_with_balance && $request->pay_with_balance == 1){
            Session::set('pay_with_balance', 1);
        }
        else{
            Session::set('pay_with_balance', 0);
        }

        $paxes = $request->hidden_pax;

        $date_reserve = date("Y-m-d", strtotime($request->hidden_date_reserve));
        $time_reserve = date("H:i:s", strtotime($request->hidden_time_reserve)); // H = 24 hours format
        $reservation_date = $date_reserve." ".$time_reserve;

        $user_id = $request->user_hidden_id;
        $restaurant_management_id = $request->restaurant_management_hidden_id;

        $restaurant_management = RestaurantManagement::find($restaurant_management_id);

        if($time_reserve < $restaurant_management->open_time || $time_reserve > $restaurant_management->close_time){
            return redirect()->back()->with([
                'error' => 'Jam reservasi anda tidak sesuai dengan jam buka restoran.'
            ]);  
        }

        $total_reservation_price = 0;

        $item_names = $request->item_name;
        $item_counts = $request->item_count;
        $item_prices = $request->item_price;
        $each_item_prices = [];

        for($i=0; $i<count($item_prices); $i++){
            $each_item_prices[$i] = $item_prices[$i] / $item_counts[$i];
            $total_reservation_price += $item_prices[$i];
        }

        return view('user/reservation/index', compact('restaurant_management', 'paxes', 'reservation_date', 'item_names', 'item_counts', 'item_prices', 'total_reservation_price', 'each_item_prices'));
    }

    public function getEditReservation(Request $request, $id = null){
        $edit_paxes = $request->edit_paxes;
        $edit_reservation_date = $request->edit_reservation_date;
        $edit_item_names = $request->edit_item_names;
        $edit_item_counts = $request->edit_item_counts;

        return redirect()->action('RestaurantController@getShow', [$id])->with([
            'edit_paxes' => $request->edit_paxes,
            'edit_reservation_date' => $request->edit_reservation_date,
            'edit_item_names' => $request->edit_item_names,
            'edit_item_counts' => $request->edit_item_counts,
        ]);
    }

    public function getHistory(){
        $user = Auth::user('user');
        $user_reservations = $user->reservations()->paginate(4);

        $recommendation_restaurants = (RestaurantManagement::with('reservations')->CountRestaurantManagementTotalRatings()->orderBy('total_ratings', 'desc')->get()->take(4));

        return view('user/reservation/history', compact('user_reservations', 'recommendation_restaurants'));
    }

    public function getShowReservation($id = null){
        if($id == null){
            abort(404);
        }
        $reservation = Reservation::find($id);
        if(!$reservation){
            abort(404);
        }
        else{
            if($reservation->status_reservation_id != 4){
                abort(404);     
            }
        }

        return view('user/reservation/show', compact('reservation'));
    }

    public function deleteReservation($id){
        Reservation::find($id)->delete();
        return redirect()->back()->with([
            'info' => 'Reservasi dengan ID : '.$id.' telah dibatalkan.'
        ]);
    }

    public function postReservationReview(Request $request){
        $reservation_id = $request->reservation_id;
        $rating = $request->rating;
        $comment = $request->comment;

        $command = new StoreReviewRatingCommand($reservation_id, $rating);
        $this->dispatch($command);

        if($comment){
            $command = new StoreReviewCommentCommand($reservation_id, $comment);
            $this->dispatch($command);
        }

        return redirect()->back()->with([
            'info' => 'Terima kasih. Review anda telah berhasil disimpan.'
        ]);

    }

    public function getCheckDuplicateReservation(Request $request){
        $restaurant_management_id = $request->restaurant_management_id;
        $paxes = $request->paxes;
        $reservation_date = $request->reservation_date;
        $user_id = $request->user_id;

        $reservation_before = Reservation::where('user_id', $user_id)
                              ->where('restaurant_management_id', $restaurant_management_id)
                              ->where('reservation_date', $reservation_date)
                              ->where('status_reservation_id', '1')
                              ->first();

        if($reservation_before){
            //only for unprocess reservation
            if($reservation_before->status_reservation_id == 1){
                $duplicate_data = $reservation_before->id;                
            }
            else{
                $duplicate_data = 0;
            }
        }
        else{
            $duplicate_data = 0;
        }
        return Response::json($duplicate_data);
    }

    public function getCheckAvailableBalance(Request $request){
        $reservation_prices = $request->reservation_prices;
        $user_id = $request->user_id;

        $user_balance = User::find($user_id)->balance;

        if($user_balance >= $reservation_prices){

            return Response::json(true);            
        }
        else{
            return Response::json(false);
        }
    }    

    public function getSortHistory(Request $request){
        $recommendation_restaurants = (RestaurantManagement::with('reservations')->CountRestaurantManagementTotalRatings()->orderBy('total_ratings', 'desc')->get()->take(4));

        $user = Auth::user('user');
        if($request->sort_history == "all"){
            $user_reservations = $user->reservations()->paginate(4);
        }
        else if($request->sort_history == "reservation_id"){
            $user_reservations = $user->reservations()->orderBy('id')->paginate(4);
        }
        else if($request->sort_history == "restaurant_name"){
            $user_reservations = $user->reservations()->whereHas('restaurant_management', function($restaurant_management){
                $restaurant_management->orderBy('name');
            })->paginate(4);
        }
        else if($request->sort_history == "reservation_date"){
            $user_reservations = $user->reservations()->orderBy('reservation_date')->paginate(4);
        }
        else{ //Status Reservation
            $user_reservations = $user->reservations()->where('status_reservation_id', $request->sort_history)->orderBy('reservation_date')->paginate(4);
        }

        return view('user/reservation/history', compact('user_reservations', 'recommendation_restaurants'));        
    }

    public function getChooseCancelReservationOptions($id){
        $reservation = Reservation::find($id);
        if(!$reservation){
            abort(404);
        }

        $expired_coming_confirmation = ExpiredComingConfirmation::where('reservation_id', $id)->first();

        //check if user has taken action before or the message has not sent yet(for changing reservation)
        if($expired_coming_confirmation->status_taken_action == 1 || $expired_coming_confirmation->status_sent == 0){
            abort(404);
        }

        //check if has passed expired_date
        if(Carbon::now() > $expired_coming_confirmation->expired_date){
            abort(404);            
        }        

        //first time choose cancel reservation options or ever choose before
        if(($reservation->status_reservation_id == '4' && $reservation->status_arrival_id == '2') || ($reservation->status_reservation_id == '4' && $reservation->status_arrival_id == '3')){
            if(Carbon::parse($reservation->reservation_date) >= Carbon::now()){ // has not passed current time
                $expired_coming_confirmation = ExpiredComingConfirmation::where('reservation_id', $reservation->id)->first();
                if(Carbon::now() > Carbon::parse($expired_coming_confirmation->expired_date)){
                    abort(404);
                }
                else{
                    $user = Reservation::find($id)->user;
                    Auth::login($user);

                    $reservation_id = $reservation->id;
                    $reservation = Reservation::find($reservation_id);
                    return view('/user/reservation/choose', compact('reservation_id', 'reservation'));
                }
            }
        }
        else{
            abort(404);
        }
    }

    public function postChooseScorchReservation(Request $request){
        $reservation = Reservation::find($request->reservation_id);
        if(!$reservation){
            abort(404);
        }

        //check if user has taken action before or the message has not sent yet(for changing reservation)
        if($expired_coming_confirmation->status_taken_action == 1 || $expired_coming_confirmation->status_sent == 0){
            abort(404);
        }

        $expired_coming_confirmation = ExpiredComingConfirmation::where('reservation_id', $reservation->id)->first();
        //check if has passed expired_date
        if(Carbon::now() > $expired_coming_confirmation->expired_date){
            abort(404);            
        }

        // set status_taken_action to already been done
        $expired_coming_confirmation->status_taken_action = 1;
        $expired_coming_confirmation->save();

        $total_reservation_price = $reservation->getTotalReservationPrice();
        $total_net_income = $reservation->getTotalNetIncome();

        //add this total net income to restaurant management balance 
        $restaurant_management = $reservation->restaurant_management;
        $restaurant_management->balance += $total_net_income;
        $restaurant_management->save();

        $qr_code_plains = QrCodePlain::where('reservation_id', $reservation->id)->first();
        //change this qr code to already been processed
        $qr_code_plains->status_processed = 1;
        $qr_code_plains->save();

        //send email to restaurant
        $user_name = $reservation->user->name;
        $restaurant_management_name = $reservation->restaurant_management->name;
        $restaurant_management_email = $reservation->restaurant_management->email;

        $reservation_date_only = $reservation->convertDate();
        $reservation_time_only = $reservation->convertTime();
        $total_reservation_price = $reservation->getTotalReservationPrice();

        $logo['path'] = '';
        $logo['width'] = '0px';
        $logo['height'] = '0px';
        $beautymail = app()->make(Beautymail::class);
        $beautymail->send('emails.scorch_reservation', 
            [
                'logo' => $logo,
                'reservation_id' => $reservation->id,
                'user_name' => $user_name,
                'reservation_date_only' => $reservation_date_only, 
                'reservation_time_only' => $reservation_time_only, 
                'total_net_income' => $total_net_income, 
            ],  function($message) use($restaurant_management_email, $restaurant_management_name) {
                        $message->to($restaurant_management_email, $restaurant_management_name)
                                ->subject('Konsumen Batal Datang');
        });

        return redirect('/')->with([
            'info' => 'Anda telah menghanguskan reservasi ID : '.$reservation->id. '. QrCode anda sekarang sudah tidak valid.'
        ]);
    }

    public function getChangeReservation($id, Request $request){
        $expired_coming_confirmation = ExpiredComingConfirmation::where('reservation_id', $id)->first();
        
        //check if user has taken action before or the message has not sent yet(for changing reservation)
        if($expired_coming_confirmation->status_taken_action == 1 || $expired_coming_confirmation->status_sent == 0){
            abort(404);
        }

        //check if has passed expired_date
        if(Carbon::now() > $expired_coming_confirmation->expired_date){
            abort(404);  
        }

        $reservation_id = $id;
        return view('/user/reservation/change', compact('reservation_id'));
    }

    public function postChangeReservationDate(Request $request){
        $expired_coming_confirmation = ExpiredComingConfirmation::where('reservation_id', $request->reservation_id)->first();

        //check if user has taken action before or the message has not sent yet(for changing reservation)
        if($expired_coming_confirmation->status_taken_action == 1 || $expired_coming_confirmation->status_sent == 0){
            abort(404);
        }
        
        //check if has passed expired_date
        if(Carbon::now() > $expired_coming_confirmation->expired_date){
            abort(404);            
        }

        // set status_taken_action to already been done
        $expired_coming_confirmation->status_taken_action = 1;
        $expired_coming_confirmation->save();        

        $date_reserve = date("Y-m-d", strtotime($request->date));
        $time_reserve = date("H:i:s", strtotime($request->time)); // H = 24 hours format
        $reservation_date = $date_reserve." ".$time_reserve;

        $reservation_id = $request->reservation_id;
        $restaurant_management_id = Reservation::find($reservation_id)->restaurant_management_id;
        $restaurant_management = RestaurantManagement::find($restaurant_management_id);

        if($time_reserve < $restaurant_management->open_time || $time_reserve > $restaurant_management->close_time){
            return redirect()->back()->with([
                'error' => 'Jam reservasi anda tidak sesuai dengan jam buka restoran.'
            ]);  
        }

        // change deadline response date to expired_date_coming_confirmation
        $deadline_response_date = DeadlineResponseDate::where('reservation_id', $reservation_id)->first();
        $expired_coming_comfirmation_date = ExpiredComingConfirmation::where('reservation_id', $reservation_id)->first()->expired_date;

        $deadline_response_date->deadline_date = $expired_coming_comfirmation_date;
        $deadline_response_date->save();        

        // delete old qr code
        QrCodePlain::where('reservation_id', $reservation_id)->first()->delete();

        // delete expired_coming
        ExpiredComingConfirmation::where('reservation_id', $reservation_id)->first()->delete();

        // update old reservation date, change status_reservation_id to 3 (telah bayar) and status_arrival_id to 3 (tunda)
        Reservation::find($reservation_id)->update([
            'reservation_date' => $reservation_date,
            'status_reservation_id' => 3,
            'status_arrival_id' => 3
        ]);

        $reservation = Reservation::find($reservation_id);
        // send email
        $user_name = $reservation->user->name;
        $restaurant_management_name = $reservation->restaurant_management->name;
        $restaurant_management_email = $reservation->restaurant_management->email;        
        $reservation_date_only = $reservation->convertDate();
        $reservation_time_only = $reservation->convertTime();
        $total_reservation_price = $reservation->getTotalReservationPrice();

        //restaurant_deadline_date
        $restaurant_deadline_date = Carbon::parse($deadline_response_date->deadline_date)->addHour(1);
        $deadline_date_only = $reservation->convertToCommonDate($restaurant_deadline_date);
        $deadline_time_only = $reservation->convertToCommonTime($restaurant_deadline_date);

        $logo['path'] = '';
        $logo['width'] = '0px';
        $logo['height'] = '0px';
        $beautymail = app()->make(Beautymail::class);
        $beautymail->send('emails.change_reservation_date', 
            [
                'logo' => $logo,
                'reservation_id' => $reservation->id,
                'user_name' => $user_name,
                'reservation_date_only' => $reservation_date_only, 
                'reservation_time_only' => $reservation_time_only, 
                'total_reservation_price' => $total_reservation_price, 
                'deadline_date_only' => $deadline_date_only, 
                'deadline_time_only' => $deadline_time_only, 
            ],  function($message) use($restaurant_management_email, $restaurant_management_name) {
                        $message->to($restaurant_management_email, $restaurant_management_name)
                                ->subject('Konsumen Ganti Jadwal Reservasi');
        });

        return redirect('/')->with([
            'info' => 'Pergantian jadwal untuk reservasi ID : '.$reservation->id. ' sedang diproses. Mohon menunggu hasil pemberitahuan di email anda.'
        ]);                

    }

    public function getConfirmComing($id){
        $expired_coming_confirmation = ExpiredComingConfirmation::where('reservation_id', $id)->first();

        //check if user has taken action before or the message has not sent yet(for changing reservation)
        if($expired_coming_confirmation->status_taken_action == 1 || $expired_coming_confirmation->status_sent == 0){
            abort(404);
        }

        //check if has passed expired_date
        if(Carbon::now() > $expired_coming_confirmation->expired_date){
            abort(404);            
        }

        // set status_taken_action to already been done
        $expired_coming_confirmation->status_taken_action = 1;
        $expired_coming_confirmation->save();

        $reservation = Reservation::find($id);
        $user_name = $reservation->user->name;
        $restaurant_management_name = $reservation->restaurant_management->name;
        $restaurant_management_email = $reservation->restaurant_management->email;        
        $reservation_date = $reservation->convertDate();
        $reservation_time = $reservation->convertTime();        
        $paxes = $reservation->paxes; 

        $reservation_menus = $reservation->reservation_menus;
        $getTotalReservationPriceCommand = new GetTotalReservationPriceCommand();
        for($i=0; $i<count($reservation_menus); $i++){
            $getTotalReservationPriceCommand->setTotalPrice($reservation_menus[$i]);
        }
        $total_reservation_price = $getTotalReservationPriceCommand->getTotalPrice();             

        //send email
        $logo['path'] = '';
        $logo['width'] = '0px';
        $logo['height'] = '0px';
        $beautymail = app()->make(Beautymail::class);
        $beautymail->send('emails.confirm_coming', 
            [
                'logo' => $logo,
                'reservation_id' => $reservation->id,
                'user_name' => $user_name,
                'reservation_date' => $reservation_date, 
                'reservation_time' => $reservation_time, 
                'paxes' => $paxes, 
                'total_reservation_price' => $total_reservation_price, 
                'reservation_menus' => $reservation_menus, 
            ],  function($message) use($restaurant_management_email, $restaurant_management_name) {
                        $message->to($restaurant_management_email, $restaurant_management_name)
                                ->subject('Konsumen Pasti Datang');
        });                

        //redirect to solivis home
        return redirect('/')->with([
            'info' => 'Terima kasih. Notifikasi kepastian kedatangan anda sudah dikirim ke pihak restoran.'
        ]);
    }

}
