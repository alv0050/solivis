<?php

namespace Solivis\Http\Controllers;

use Illuminate\Http\Request;

use Solivis\Reservation;
use Solivis\ReservationMenu;
use Solivis\Menu;
use Solivis\User;
use Solivis\UserBank;
use Solivis\Admin;
use Solivis\AdminBank;
use Solivis\DeadlineResponseDate;
use Session;
use Auth;
use Snowfire\Beautymail\Beautymail;
use Carbon\Carbon;
use Solivis\Commands\StoreReservationCommand;
use Solivis\Commands\StoreReservationMenuCommand;
use Solivis\Commands\GetTotalReservationPriceCommand;
use Solivis\Commands\StoreConfirmPaymentCommand;
use Solivis\Commands\StoreDeadlineResponseDateCommand;
use Solivis\Http\Requests;
use Solivis\Http\Controllers\Controller;

class PaymentController extends Controller
{

	public function __construct(){
        $this->middleware('auth');
	}

    public function postIndex(Request $request){ 
        $new_reservation = true; //prevent from sending two times email for same reservation ID

        $user_id = Auth::user('user')->id;
        $paxes = $request->paxes;
        $reservation_date = $request->reservation_date;
        $item_names = $request->item_names;
        $item_counts = $request->item_counts;
        $restaurant_management_id = $request->restaurant_management_id;

        //update reservation before
        if($request->reservation_id_before){
            $reservation_id = $request->reservation_id_before;
            //update paxes
            Reservation::find($reservation_id)->update([
                'paxes' => $paxes
            ]);
            //delete old reservation menu
            ReservationMenu::where('reservation_id', $reservation_id)->delete();
            $new_reservation = false;
        }
        else{
            $command = new StoreReservationCommand($paxes, $reservation_date, $user_id, $restaurant_management_id);
            $this->dispatch($command);

            $reservation_id = Reservation::where('paxes', $paxes)
                         ->where('reservation_date', $reservation_date)
                         ->where('user_id', $user_id)
                         ->where('restaurant_management_id', $restaurant_management_id)
                         ->orderBy('created_at', 'desc')
                         ->first()->id;

            //only pay with balance
            if(Session::has('pay_with_balance')){
                if(Session::get('pay_with_balance') == 1){
                    $reservation = Reservation::find($reservation_id);
                    $reservation->update([
                        'method_payment_id' => '2'
                    ]);
                }
            }
        }

        for($i=0; $i<count($item_names); $i++){
            $command = new StoreReservationMenuCommand($reservation_id, $restaurant_management_id, $item_names[$i], $item_counts[$i]);
            $this->dispatch($command);
        }

        $reservation = Reservation::find($reservation_id);
        if(Carbon::parse($reservation->reservation_date)->toDateString() == Carbon::now()->toDateString() || Carbon::parse($reservation->reservation_date)->toDateString() == Carbon::tomorrow()->toDateString()){
            $deadline_date = Carbon::parse($reservation->reservation_date)->subHour(5);
            //because need at least
            //1 hour for admin verify
            //1 hour for restaurant management respond
            //2 hour for confirming user's presence
            //1 hour for restaurant management prepare
        }
        else{ //normal deadline = 1 day after choose payment with transfer
            //if user create reservation after 16:00(today) and before 08:00(tomorrow) 
            $today_16_01_00 = Carbon::today()->addHour(16)->addMinute(1);
            $today_23_59_59 = Carbon::today()->addHour(23)->addMinute(59)->addSecond(59);

            $today_00_00_00 = Carbon::today();
            $today_07_59_59 = Carbon::today()->addHour(7)->addMinute(59)->addSecond(59);

            if(Carbon::now()->between($today_16_01_00, $today_23_59_59)){ //deadline response is tomorrow 16:00
                $deadline_date = Carbon::tomorrow()->addHour(16);
            }
            else if(Carbon::now()->between($today_00_00_00, $today_07_59_59)){
                $deadline_date = Carbon::today()->addHour(16);
            }
            else{
                $deadline_date = Carbon::now()->addDay(1);
            }
        }

        //add deadline_date
        $command = new StoreDeadlineResponseDateCommand($reservation_id, $deadline_date);
        $this->dispatch($command);

        //send email to user about reservation detail and deadline date
        $user_name = $reservation->user->name;
        $user_email = $reservation->user->email;
        $reservation_date_only = $reservation->convertDate();
        $reservation_time_only = $reservation->convertTime();

        $deadline_date_only = $reservation->convertToCommonDate($deadline_date);
        $deadline_time_only = $reservation->convertToCommonTime($deadline_date);
        $paxes = $reservation->paxes;

        $reservation_menus = $reservation->reservation_menus;
        $getTotalReservationPriceCommand = new GetTotalReservationPriceCommand();
        
        for($i=0; $i<count($reservation_menus); $i++){
            $getTotalReservationPriceCommand->setTotalPrice($reservation_menus[$i]);
        }
        $total_reservation_price = $getTotalReservationPriceCommand->getTotalPrice();

        if($new_reservation){ //send email for only new reservation
            $logo['path'] = '';
            $logo['width'] = '0px';
            $logo['height'] = '0px';
            $beautymail = app()->make(Beautymail::class);
            $beautymail->send('emails.user_reservation_info', 
                [
                    'logo' => $logo,
                    'reservation_id' => $reservation->id,
                    'user_name' => $user_name,
                    'reservation_date_only' => $reservation_date_only, 
                    'reservation_time_only' => $reservation_time_only, 
                    'paxes' => $paxes, 
                    'total_reservation_price' => $total_reservation_price, 
                    'reservation_menus' => $reservation_menus,
                    'deadline_date_only' => $deadline_date_only,       
                    'deadline_time_only' => $deadline_time_only        
                ],  function($message) use($user_email, $user_name) {
                            $message->to($user_email, $user_name)
                                    ->subject('Anda Berhasil Melakukan Reservasi');
            });
        }

        return redirect()->action('PaymentController@getShow', [$reservation_id]);
    }

    public function getShow($id = null){
        if($id == null){
            abort(404);
        }
        $reservation = Reservation::find($id);
        //reservation not found or other reservation except unprocessed
        if(!$reservation || $reservation->status_reservation_id != '1'){
            abort(404);            
        }

        $getTotalReservationPriceCommand = new GetTotalReservationPriceCommand();
        $reservation_menus = $reservation->reservation_menus;

        for($i=0; $i<count($reservation_menus); $i++){
            $getTotalReservationPriceCommand->setTotalPrice($reservation_menus[$i]);
        }

        $admin = Admin::find('1');
        $admin_banks = $admin->admin_banks()->get();

        $total_reservation_price = $getTotalReservationPriceCommand->getTotalPrice();

        return view('user/payment/index', compact('reservation', 'total_reservation_price', 'admin_banks'));
    }

    public function patchBalancePayment(Request $request){

        $reservation = Reservation::find($request->reservation_id);

        $getTotalReservationPriceCommand = new GetTotalReservationPriceCommand();
        $reservation_menus = $reservation->reservation_menus;

        for($i=0; $i<count($reservation_menus); $i++){
            $getTotalReservationPriceCommand->setTotalPrice($reservation_menus[$i]);
        }        
        $total_reservation_price = $getTotalReservationPriceCommand->getTotalPrice();

        $user = $reservation->user;

        $user_name = $user->name;
        $reservation_date = $reservation->convertDate();
        $reservation_time = $reservation->convertTime();
        $paxes = $reservation->paxes;

        $restaurant_management_name = $reservation->restaurant_management->name;
        $restaurant_management_email = $reservation->restaurant_management->email;

        //deadline for restaurant to response reservation
        $deadline_date = Carbon::parse(DeadlineResponseDate::where('reservation_id', $reservation->id)->first()->deadline_date)->addHour(1);
        $deadline_date_only = $reservation->convertToCommonDate($deadline_date);
        $deadline_time_only = $reservation->convertToCommonTime($deadline_date);

        $logo['path'] = '';
        $logo['width'] = '0px';
        $logo['height'] = '0px';
        $beautymail = app()->make(Beautymail::class);
        $beautymail->send('emails.new_reservation', 
            [
                'logo' => $logo,
                'reservation_id' => $request->reservation_id, 
                'user_name' => $user_name, 
                'reservation_date' => $reservation_date, 
                'reservation_time' => $reservation_time, 
                'paxes' => $paxes, 
                'total_reservation_price' => $total_reservation_price, 
                'reservation_menus' => $reservation_menus,           
                'deadline_date_only' => $deadline_date_only,           
                'deadline_time_only' => $deadline_time_only,           
            ],  function($message) use ($restaurant_management_name, $restaurant_management_email){
                    $message->to($restaurant_management_email, $restaurant_management_name)
                            ->subject('Reservasi Baru');
        });

        //subtract with user balance
        $user_remain_balance = $user->balance - $total_reservation_price;
        $user->balance = $user_remain_balance;
        $user->save();

        $reservation->update([
            "status_reservation_id" => 3,
            "method_payment_id" => 2,
        ]);

        return redirect('reservation/history')->with([
            'info' => 'Terima kasih. Reservasi anda telah berhasil dilakukan. Mohon menunggu pemberitahuan hasil reservasi di email anda.'
        ]);  

    }

    public function patchTransferBankPayment(Request $request){
        Reservation::find($request->reservation_id)->update([
            "method_payment_id" => 1,
        ]);

        $reservation = Reservation::find($request->reservation_id);
        $restaurant_management = $reservation->restaurant_management->first();

        //send reservation id as a value to let pay modal continue to confirm payment
        return redirect()->action('RestaurantController@getShow', [$restaurant_management->id])->with([
            'alert-pay-transfer-bank' => $reservation->id
        ]);  

    }

    public function getConfirm($id = null){
        if($id == null){
            abort(404);
        }

        $reservation = Reservation::find($id);
        if (!$reservation || Auth::user('user')->id != $reservation->user_id){
            abort(404);
        }

        $getTotalReservationPriceCommand = new GetTotalReservationPriceCommand();
        $reservation_menus = $reservation->reservation_menus;

        for($i=0; $i<count($reservation_menus); $i++){
            $getTotalReservationPriceCommand->setTotalPrice($reservation_menus[$i]);
        }

        $total_reservation_price = $getTotalReservationPriceCommand->getTotalPrice();

        $user = User::find($reservation->user_id);

        $names = array();
        foreach($user->user_banks as $user_bank){
            $names[$user_bank->id] = $user_bank->bank->name;
        }
        $account_numbers = $user->user_banks()->lists('account_number', 'id');
        $account_names = $user->user_banks()->lists('account_name', 'id');

        $user_bank_complete_info = array();
        foreach($names as $id => $name){
            $user_bank_complete_info[$id] = $names[$id]."-".$account_numbers[$id]." a/n ".$account_names[$id];
        }

        $admin = Admin::find('1');

        $names = array();
        foreach($admin->admin_banks as $admin_bank){
            $names[$admin_bank->id] = $admin_bank->bank->name;
        }

        $account_numbers = $admin->admin_banks()->lists('account_number', 'id');
        $account_names = $admin->admin_banks()->lists('account_name', 'id');

        $admin_bank_complete_info = array();
        foreach($names as $id => $name){
            $admin_bank_complete_info[$id] = $names[$id]."-".$account_numbers[$id]." a/n ".$account_names[$id];
        }        

        return view('user/payment/confirm', compact('reservation', 'total_reservation_price', 'user_bank_complete_info', 'admin_bank_complete_info'));
    }

    public function postConfirm(Request $request){

        $messages = [
            'transfer_proof_photo_url.required' => 'Foto Bukti Pembayaran harus diupload.',
            'transfer_proof_photo_url.image' => 'Bukti pembayaran yang anda upload harus berbentuk image.',
            'transfer_proof_photo_url.max' => 'Maksimal ukuran foto bukti pembayaran yang dapat diupload adalah 1MB',
        ];

        $this->validate($request, [
            'transfer_proof_photo_url' => 'required|image|max:1000',
        ], $messages);

        $reservation_id = $request->reservation_id;        
        $transfer_from_bank_id = $request->transfer_from_bank_id;        
        $transfer_to_bank_id = $request->transfer_to_bank_id;        
        $transfer_proof_photo_url = $request->transfer_proof_photo_url;        
        $information = $request->information;

        if ($transfer_proof_photo_url){
            $transfer_proof_photo_url_filename = $transfer_proof_photo_url->getClientOriginalName();
            $transfer_proof_photo_url->move(public_path('images/transfer_proof_uploads'), $transfer_proof_photo_url_filename);

            $command = new StoreConfirmPaymentCommand($reservation_id, $transfer_from_bank_id, $transfer_to_bank_id, $transfer_proof_photo_url_filename, $information);
            $this->dispatch($command);

            $reservation = Reservation::find($request->reservation_id);
            $reservation->status_reservation_id = 2; //Sedang diproses
            $reservation->save();

            $getTotalReservationPriceCommand = new GetTotalReservationPriceCommand();
            $reservation_menus = $reservation->reservation_menus;

            for($i=0; $i<count($reservation_menus); $i++){
                $getTotalReservationPriceCommand->setTotalPrice($reservation_menus[$i]);
            }         

            $total_reservation_price = $getTotalReservationPriceCommand->getTotalPrice();

            $user_bank = UserBank::find($transfer_from_bank_id);
            $transfer_from_bank = $user_bank->bank->name."-".$user_bank->account_number." a/n ".$user_bank->account_name;

            $admin_bank = AdminBank::find($transfer_to_bank_id);
            $transfer_to_bank = $admin_bank->bank->name."-".$admin_bank->account_number." a/n ".$admin_bank->account_name;

            $user_name = $reservation->user->name;
            $user_email = $reservation->user->email;

            $logo['path'] = '';
            $logo['width'] = '0px';
            $logo['height'] = '0px';
            $beautymail = app()->make(Beautymail::class);
            $beautymail->send('emails.confirm_payment', 
                [
                    'logo' => $logo,
                    'reservation_id' => $reservation_id, 
                    'total_reservation_price' => $total_reservation_price, 
                    'transfer_from_bank' => $transfer_from_bank,
                    'transfer_to_bank' => $transfer_to_bank,
                    'transfer_proof_photo_url_filename' => $transfer_proof_photo_url_filename,
                    'information' => $information,         
                ],  function($message) use($user_email, $user_name) {
                        $message->from($user_email, $user_name)
                                ->to('solivisads@gmail.com', 'Solivisads')
                                ->subject('Konfirmasi Pembayaran dari user Solivis');
            });

            return redirect()->action('ReservationController@getHistory')->with([
                'info' => 'Terima kasih. Konfirmasi pembayaran anda telah berhasil dilakukan. Mohon menunggu hasil verifikasi kami di email anda.'
            ]);
        }        
    }

}
