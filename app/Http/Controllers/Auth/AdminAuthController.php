<?php
 
namespace Solivis\Http\Controllers\Auth;
 
use Illuminate\Http\Request;

use Validator;
use Solivis\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Sarav\Multiauth\Foundation\AuthenticatesAndRegistersUsers;
use Auth;
 
class AdminAuthController extends Controller
{
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;
 
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->user = "admin";
        $this->middleware('admin.guest', ['except' => 'getLogout']);
    }

    public function getLogin()
    {
        return view('admin.auth.login');
    } 

    public function postLogin(Request $request){
        $this->validate($request, [
            'username'    => 'required',
            'password' => 'required',
        ]);

        if (!Auth::attempt("admin", $request->only(['username', 'password']), $request->has('remember'))){
            return redirect()->back()->with([ 
                "error" => "Username atau password yang anda masukkan salah."
            ]);
        }
        return redirect('admin/index');        
    }

    public function getLogout(){
        Auth::logout('admin');
        return redirect('admin/login');
    }

}