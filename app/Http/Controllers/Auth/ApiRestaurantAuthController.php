<?php

namespace Solivis\Http\Controllers\Auth;

use Illuminate\Http\Request;

use Auth;
use Solivis\User;
use Solivis\Http\Requests;
use Solivis\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Sarav\Multiauth\Foundation\AuthenticatesAndRegistersUsers;

class ApiRestaurantAuthController extends Controller
{
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;
    
    public function __construct()
    {
        $this->user = "restaurant_management";
        // $this->middleware('api.restaurant.guest', ['except' => 'getLogout']);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function getLogin(){
        header('Content-type: application/json');
        $response = array();
        $response['token'] = csrf_token();
        $response = json_encode(array($response));
        
        return response($response, 200)->header('Content-Type', 'application/json');
    }
    
    public function postLogin(Request $request)
    {
        $email = $request->input('email');
        $password = $request->input('password');
        
        if (Auth::attempt('restaurant_management', ['email' => $email, 'password' => $password])){
            $response = array();
            $response['message'] = 'OK';
            $response = json_encode(array($response));
            return response($response, 200)->header('Content-Type', 'application/json');
            // return redirect('api/restaurant/profile');
        }
        else{
            $response = array();
            $response['message'] = 'Wrong username or password';
            $response = json_encode(array($response));
            return response($response, 401)->header('Content-Type', 'application/json');
        }
    }
    
    public function getLogout(){
        Auth::logout('restaurant_management');
        $response = array();
        $response['message'] = 'Restaurant logged out';
        $response = json_encode(array($response));
        return response($response, 200)->header('Content-Type', 'application/json');
    }
}
