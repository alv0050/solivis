<?php

namespace Solivis\Http\Controllers\Auth;

use Solivis\User;
use Validator;
use Solivis\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Sarav\Multiauth\Foundation\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Snowfire\Beautymail\Beautymail;
use Carbon\Carbon;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->user = "user";
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    public function getRegister()
    {
        return view('user.auth.register');
    }

    public function postRegister(Request $request){
        $messages = [
            'email.unique' => 'Alamat Email sudah terdaftar.'
        ];

        $confirmation_code = str_random(30);

        $this->validate($request, [
            'name' => 'required|max:255',
            'phone_number' => 'required|numeric',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'date' => 'required|numeric',
            'month' => 'required|numeric',
            'year' => 'required|numeric',
            'gender' => 'required'
        ], $messages);

        User::create([
            'name' => $request->name,
            'phone_number' => $request->phone_number,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'birth_date' => date($request->year.'-'.$request->month.'-'.$request->date),
            'gender' => $request->gender,
            'photo_url' => 'person-default.png',
            'confirmation_code' => $confirmation_code,
            'balance' => 0
        ]);

        $logo['path'] = '';
        $logo['width'] = '0px';
        $logo['height'] = '0px';
        $beautymail = app()->make(Beautymail::class);
        $beautymail->send('emails.verify', 
            [
                'logo' => $logo,
                'confirmation_code' => $confirmation_code         
            ], function($message) use($request) {
                $message->to($request->email, $request->name)
                        ->subject('Verifikasi email anda');
        });

        return redirect('/')->with([
            "info" => "Terima kasih sudah mendaftar! Silahkan cek email anda."
        ]); 
    }

    public function getLogin()
    {
        return view('user.auth.login');
    }    

    public function postLogin(Request $request){
        $this->validate($request, [
            'email'    => 'required',
            'password' => 'required',
        ]);

        $user = User::where('email', $request->email)->first();
        if($user){
            //not confirm yet
            if ($user->confirmed == 0){
                return redirect('/login')->with([ 
                    "error" => "Maaf :( Email ini belum diverifikasi."
                ]);
            }
            else{
                if (!Auth::attempt($request->only(['email', 'password']), $request->has('remember'))){
                    return redirect('/login')->with([ 
                        "error" => 'Email atau password yang anda masukkan salah.'
                    ]);
                }         
                else{
                    $user->last_login = Carbon::now();
                    $user->save();
                    
                    return redirect('/'); //let user continue page he wants (intent)
                }
            }
        }
        else{
            return redirect('/login')->with([ 
                "error" => "Maaf :( Email ini belum terdaftar."
            ]);            
        }
    }

    public function getLogout(){
        Auth::logout();
        return redirect('/');
    }

}
