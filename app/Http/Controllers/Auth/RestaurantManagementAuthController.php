<?php

namespace Solivis\Http\Controllers\Auth;
 
use Illuminate\Http\Request;

use Validator;
use Solivis\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Sarav\Multiauth\Foundation\AuthenticatesAndRegistersUsers;
use Auth;
use Solivis\RestaurantManagement;
use Carbon\Carbon;
 
class RestaurantManagementAuthController extends Controller
{
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;
 
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->user = "restaurant_management";
        $this->middleware('restaurant_management.guest', ['except' => 'getLogout']);
    }

    public function getLogin()
    {
        return view('restaurant-management.auth.login');
    } 

    public function postLogin(Request $request){
        $this->validate($request, [
            'email'    => 'required|email|max:255',
            'password' => 'required',
        ]);

        $guest_email = RestaurantManagement::where('email', $request->email)
                 ->where('status', '0')->first();

         if($guest_email){
            return redirect()->back()->with([ 
                "error" => "Akun anda masih dalam proses verifikasi admin."
            ]);
         }

        if (!Auth::attempt("restaurant_management", $request->only(['email', 'password']), $request->has('remember'))){
            return redirect()->back()->with([ 
                "error" => "Email atau password yang anda masukkan salah."
            ]);
        }
        $restaurant_management = Auth::user('restaurant_management');
        $restaurant_management->last_login = Carbon::now();
        $restaurant_management->save();
        return redirect('restaurant-management/dashboard');        
    }

    public function getLogout(){
        Auth::logout('restaurant_management');
        return redirect('restaurant-management/login');
    }

}