<?php

namespace Solivis\Http\Controllers\Auth;

use Solivis\Http\Controllers\Controller;
use Sarav\Multiauth\Foundation\ResetsPasswords;
use Illuminate\Support\Facades\Password;
use Snowfire\Beautymail\Beautymail;
use Illuminate\Http\Request;
use Solivis\User;
use Auth;


class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */

    //set subject in email content
    protected $subject = "Link Reset Password Anda";  

    //redirect to / after reset successfull
    protected $redirectTo = '/';

    public function __construct()
    {
        $this->user = "user";
        $this->middleware('guest');
    }

    public function getEmail(){
        return view('user.auth.password');
    }

    public function getReset($token = null){
        if (is_null($token)) {
            throw new NotFoundHttpException;
        }

        return view('user.auth.reset')->with('token', $token);
    }
    
    public function postEmail(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);

        $user = User::where('email', $request->only('email'))->first();

        if($user){
            $logo['path'] = '';
            $logo['width'] = '0px';
            $logo['height'] = '0px';
            $beautymail = app()->make(Beautymail::class);
            $beautymail->send('emails.password', 
                [
                    'logo' => $logo,
                    'token' => $user->remember_token,          
                ],  function($message) use($user) {
                        $message->to($user->email, $user->name)
                                ->subject('Reset Password');
            });        
            return redirect()->back()->with([
                "info" => "Link reset password telah dikirim ke email anda."
            ]);
        }
        else{
            return redirect()->back()->with([
                "error" => "Alamat email tersebut tidak terdaftar di aplikasi Solivis."
            ]);
        }
    }

    public function postReset(Request $request)
    {
        $this->validate($request, [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6',
        ]);
        $user = User::where('email', $request->only('email'))->first();

        if($user){
            //if token matched
            if($request->token == $user->remember_token){
                $user->password = bcrypt($request->password);
                $user->save();
                Auth::login($user);

                return redirect('/')->with([
                    'info' => 'Password anda telah berhasil diubah.'
                ]);                
            }
            else{
                return redirect()->back()->with([
                    "error" => "Token reset password ini tidak valid."
                ]);
            }

        }
        else{
            return redirect()->back()->with([
                "error" => "Alamat email tersebut tidak terdaftar di aplikasi Solivis."
            ]);
        }
    }
}
