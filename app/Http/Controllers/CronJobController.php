<?php

namespace Solivis\Http\Controllers;

use Illuminate\Http\Request;

use Solivis\Http\Requests;
use Solivis\Http\Controllers\Controller;
use Log;
use Snowfire\Beautymail\Beautymail;
use Solivis\Reservation;
use Carbon\Carbon;
use Solivis\DeclinedReason;
use Solivis\HistoryBalance;
use Solivis\ExpiredComingConfirmation;

class CronJobController extends Controller
{

    public function getDeleteReservation(){
		// only unprocessed reservation
        $unresponsed_reservations = Reservation::has('deadline_response_date')
                                    ->where('status_reservation_id', '1')
                                    ->get();

        foreach($unresponsed_reservations as $unresponsed_reservation){
            if(Carbon::now()->gt(Carbon::parse($unresponsed_reservation->deadline_response_date->deadline_date))){
                $unresponsed_reservation->update([
                    'status_reservation_id' => '5' //batal
                ]);

                $reason = "Reservasi anda telah jatuh tempo";
                DeclinedReason::create([
                    'reservation_id' => $unresponsed_reservation->id,
                    'reason' => $reason
                ]);   

                Log::info("Reservasi ID : ".$unresponsed_reservation->id." dibatalkan karena jatuh tempo");
            }
        }

        // has paid reservation but not responded by restaurant
        $waiting_restaurant_management_confirm_reservations = Reservation::has('deadline_response_date')
                                                                ->where('status_reservation_id', '3')
                                                                ->get();

        foreach($waiting_restaurant_management_confirm_reservations as $waiting_restaurant_management_confirm_reservation){
            //transfer bank
            if($waiting_restaurant_management_confirm_reservation->method_payment_id == 1){
                //add two hour because one hour before is used by admin verify 
                $deadline_response_date_restaurant = Carbon::parse($waiting_restaurant_management_confirm_reservation->deadline_response_date->deadline_date)->addHour(2);
            }
            else{
                $deadline_response_date_restaurant = Carbon::parse($waiting_restaurant_management_confirm_reservation->deadline_response_date->deadline_date)->addHour(1);
            }

            if(Carbon::now()->gt($deadline_response_date_restaurant)){
                $waiting_restaurant_management_confirm_reservation->update([
                    'status_reservation_id' => '5', //batal
                ]); 

                $reason = "Restoran tidak menanggapi reservasi anda";
                DeclinedReason::create([
                    'reservation_id' => $waiting_restaurant_management_confirm_reservation->id,
                    'reason' => $reason
                ]);

                $reservation_id = $waiting_restaurant_management_confirm_reservation->id;
                $user_name = $waiting_restaurant_management_confirm_reservation->user->name;
                $user_email = $waiting_restaurant_management_confirm_reservation->user->email;
                $reservation_date = $waiting_restaurant_management_confirm_reservation->convertDate();
                $reservation_time = $waiting_restaurant_management_confirm_reservation->convertTime();
                $paxes = $waiting_restaurant_management_confirm_reservation->paxes;
                $total_reservation_price = $waiting_restaurant_management_confirm_reservation->getTotalReservationPrice();

                $logo['path'] = '';
                $logo['width'] = '0px';
                $logo['height'] = '0px';
                $beautymail = app()->make(Beautymail::class);
                $beautymail->send('emails.not_responded_by_restaurant', 
                    [
                        'logo' => $logo,
                        'reservation_id' => $reservation_id, 
                        'reservation_date' => $reservation_date, 
                        'reservation_time' => $reservation_time, 
                        'paxes' => $paxes,                         
                        'total_reservation_price' => $total_reservation_price,
                    ],  function($message) use ($user_name, $user_email){
                            $message->to($user_email, $user_name)
                                    ->subject('Reservasi tidak direspon oleh pihak restoran');
                });

                // 6. Reservasi tidak direspon (dana dikembalikan)
                HistoryBalance::create([
                    'process_date' => Carbon::now(),
                    'balance_processed' => $total_reservation_price,
                    'status_balance_id' => 6,
                    'user_id' => $waiting_restaurant_management_confirm_reservation->user->id,
                    'transfer_target' => 'Ke saldo solivis',
                 ]);

                //add to user balance
                $user = $waiting_restaurant_management_confirm_reservation->user;
                $user->balance += $total_reservation_price;
                $user->save();   

                Log::info("Reservasi ID : ".$waiting_restaurant_management_confirm_reservation->id." dibatalkan karena tidak direspon oleh pihak restoran");                             
            }
        }    	
    }

    public function getSendEmail(){
		//sending email coming confirmation to user
        $coming_reservations = Reservation::where('status_reservation_id', '4')
                                ->where('reservation_date', '>=', Carbon::now())->get();
        foreach($coming_reservations as $coming_reservation){
            if(Carbon::parse($coming_reservation->reservation_date)->toDateString() == Carbon::now()->toDateString()){
                $sending_time = Carbon::parse($coming_reservation->reservation_date)->subHour(3);
                if(Carbon::now()->hour == $sending_time->hour){
                    $reservation_id = $coming_reservation->id;
                    $user_name = $coming_reservation->user->name;
                    $user_email = $coming_reservation->user->email;
                    $reservation_date = $coming_reservation->convertDate();
                    $reservation_time = $coming_reservation->convertTime();
                    $paxes = $coming_reservation->paxes;
                    $expired_confirmation = ExpiredComingConfirmation::where('reservation_id', $reservation_id)->first();
                    if($expired_confirmation->status_sent == 0){ //has not sent email before
                        $expired_confirmation->update([
                            'status_sent' => '1'
                        ]);
                        $ever_delayed_before = "";

                        //remove changing reservation schedule feature 
                        if($coming_reservation->status_arrival_id == 3){
                            $ever_delayed_before = true;
                        }
                        else{
                            $ever_delayed_before = false;
                        }

                        $expired_confirmation_date = $coming_reservation->convertToCommonDate($expired_confirmation->expired_date);
                        $expired_confirmation_time = $coming_reservation->convertToCommonTime($expired_confirmation->expired_date);

                        $logo['path'] = '';
                        $logo['width'] = '0px';
                        $logo['height'] = '0px';
                        $beautymail = app()->make(Beautymail::class);
                        $beautymail->send('emails.coming_confirmation', 
                            [
                                'logo' => $logo,
                                'reservation_id' => $reservation_id, 
                                'reservation_date' => $reservation_date, 
                                'reservation_time' => $reservation_time, 
                                'paxes' => $paxes,                       
                                'expired_confirmation_date' => $expired_confirmation_date, 
                                'expired_confirmation_time' => $expired_confirmation_time,                               
                                'ever_delayed_before' => $ever_delayed_before,                               
                            ],  function($message) use ($user_name, $user_email){
                                    $message->to($user_email, $user_name)
                                            ->subject('Konfirmasi Kedatangan');
                        });
                        Log::info("Kirim email konfirmasi kedatangan ke reservasi ID : ".$coming_reservation->id);
                    }
                }
            }
        }    	
    }    

    public function getCheckUserComing(){
		$reservations = Reservation::where('status_reservation_id', '4')->get();
        foreach($reservations as $reservation){
            if($reservation->qr_code_plain && $reservation->qr_code_plain->status_processed == 0 && $reservation->expired_coming_confirmation->status_sent == '1'){
                if(Carbon::parse($reservation->reservation_date)->diffInMinutes(Carbon::now()) > 20){
                    // change qr code to already been processed
                    $reservation->qr_code_plain->status_processed = 1;
                    $reservation->qr_code_plain->save();

                    // add to restaurant balance
                    $total_net_income = $reservation->getTotalNetIncome();
                    $restaurant_management = $reservation->restaurant_management;
                    $restaurant_management->balance += $total_net_income;
                    $restaurant_management->save();

                    $restaurant_management_name = $reservation->restaurant_management->name;
                    $restaurant_management_email = $reservation->restaurant_management->email;
                    $reservation_id = $reservation->id;
                    $reservation_date = $reservation->convertDate();
                    $reservation_time = $reservation->convertTime();
                    $user_name = $reservation->user->name;
                    $user_email = $reservation->user->email;
                    $total_reservation_price = $reservation->getTotalReservationPrice();

                    //send email to user
                    $logo['path'] = '';
                    $logo['width'] = '0px';
                    $logo['height'] = '0px';
                    $beautymail = app()->make(Beautymail::class);
                    $beautymail->send('emails.coming_over_deadline_to_user', 
                        [
                            'logo' => $logo,
                            'reservation_id' => $reservation_id, 
                            'reservation_date' => $reservation_date, 
                            'reservation_time' => $reservation_time, 
                        ],  function($message) use ($user_name, $user_email){
                                $message->to($user_email, $user_name)
                                        ->subject('Reservasi konsumen sudah hangus');
                    });

                    //send email to restaurant management
                    $beautymail->send('emails.coming_over_deadline_to_restaurant_management', 
                        [
                            'logo' => $logo,
                            'user_name' => $user_name, 
                            'reservation_id' => $reservation_id, 
                            'reservation_date' => $reservation_date, 
                            'reservation_time' => $reservation_time, 
                            'total_net_income' => $total_net_income,
                        ],  function($message) use ($restaurant_management_name, $restaurant_management_email){
                                $message->to($restaurant_management_email, $restaurant_management_name)
                                        ->subject('Reservasi konsumen sudah hangus');
                    });                    

                    Log::info("Reservasi ID : ".$reservation->id." dinyatakan hangus karena konsumen sudah terlambat lebih 20 menit dari waktu reservasi.");
                }
            }
        }
    }
}
