<?php

namespace Solivis\Http\Controllers;

use Illuminate\Http\Request;

use Solivis\RestaurantManagement;
use Solivis\Cuisine;
use Solivis\Service;
use Solivis\Menu;
use Solivis\MenuCategory;
use Solivis\Favorite;
use Solivis\Reservation;
use Auth;
use Session;

use Solivis\Commands\CheckExistFavoriteCommand;
use Solivis\Commands\DeleteFavoriteCommand;
use Solivis\Commands\StoreFavoriteCommand;

use Solivis\Http\Requests;
use Solivis\Http\Controllers\Controller;

class RestaurantController extends Controller
{

    public function __construct(){
        $this->middleware('auth', ['except' => ['getShow', 'getFavorite']]);
    }

	public function getShow(Request $request, $id = null){
       if ($id == null){
            abort(404);
        }
        $restaurant_management = RestaurantManagement::find($id);

        if (!$restaurant_management || $restaurant_management->status == '0'){
            abort(404);
        }            

        $cuisines = $restaurant_management->restaurant_cuisines;        
        $cuisine_ids = array();
        $cuisine_names = "";

        for($i = 0; $i < count($cuisines); $i++){
            $cuisine_ids[$i] = $cuisines[$i]->cuisine_id;
            $cuisine_names .= Cuisine::where('id', $cuisine_ids[$i])->first()->name.", ";
        }        

        $services = $restaurant_management->restaurant_services;
        $service_ids = array();
        $service_names = "";

        for($i = 0; $i < count($services); $i++){
            $service_ids[$i] = $services[$i]->service_id;
            $service_names .= Service::where('id', $service_ids[$i])->first()->name.", ";
        }

        $photos = $restaurant_management->restaurant_inside_photos;
        $photo_urls = array();

        for($i=0; $i<count($photos); $i++){
            $photo_urls[$i] = $photos[$i]->inside_restaurant_photo_url;
        }

        $category_names = array();
        $menu_category_id_uniques = array();
        $menus = Menu::where('restaurant_management_id', $restaurant_management->id)->get();
        $index = 0;

        for ($i=0; $i<count($menus); $i++){
            $menu_category_uniques[$i] = $menus[$i]->menu_category_id;   
            if (!in_array($menu_category_uniques[$i], $menu_category_id_uniques)) { //false mean, current value has not assigned to array
                $category_names[$index] = MenuCategory::where('id', $menu_category_uniques[$i])->first()->name;
                $menu_category_id_uniques[$category_names[$index]] =  $menu_category_uniques[$i];
                $index++;        
            }
        }

        if (Auth::user('user')){
            $user = Auth::user('user');

            $favorite = new CheckExistFavoriteCommand($restaurant_management->id, $user->id);
            $has_favorite = $favorite->checkExistFavorite();
        }
        else{
            $has_favorite = false;
        }
        Session::set('restaurant_management_hidden_id', $restaurant_management->id);

        //Total this restaurant got favorited
        $total_favorites = Favorite::where('restaurant_management_id', $restaurant_management->id)->count();

        //Rating
        $total_ratings = 0;
        $total_ratings_average = 0;
        $total_users_rate = 0;
        for($i=0; $i<count($restaurant_management->reservations); $i++){
            if($restaurant_management->reservations[$i]->review_rating){
                $total_users_rate++;
                $total_ratings += $restaurant_management->reservations[$i]->review_rating->rating;
            }
        }
        if($total_users_rate == 0){
           $total_ratings_average = 0; 
        }
        else{
            $total_ratings_average = round($total_ratings/$total_users_rate);
        }

        //filter reservation for only has rating
        $reservations_rated = Reservation::where('restaurant_management_id', $restaurant_management->id)
                                         ->has('review_rating')->paginate(3);

        //edit reservation
        $edit_item_names = array();
        $edit_item_counts = array();
        $index;
        if(Session::has('edit_item_names') && Session::has('edit_item_counts')){
            $index = 0;
            foreach(Session::get('edit_item_names') as $edit_item_name){
                $edit_item_names[$index] = $edit_item_name;
                $index++;
            }
            $index = 0;
            foreach(Session::get('edit_item_counts') as $edit_item_count){
                $edit_item_counts[$index] = $edit_item_count;
                $index++;
            }
            return view('user/restaurant/show', compact('user', 'restaurant_management', 'cuisine_names', 'service_names', 'photo_urls', 'category_names', 'menu_category_id_uniques', 'has_favorite', 'total_favorites', 'total_ratings', 'total_ratings_average', 'total_users_rate', 'reservations_rated', 'edit_item_names', 'edit_item_counts'));
        }
        else{
            return view('user/restaurant/show', compact('user', 'restaurant_management', 'cuisine_names', 'service_names', 'photo_urls', 'category_names', 'menu_category_id_uniques', 'has_favorite', 'total_favorites', 'total_ratings', 'total_ratings_average', 'total_users_rate', 'reservations_rated'));            
        }
	}

    public function getFavorite(){ //for intented redirect
        if (Session::has('restaurant_management_hidden_id')){
            $restaurant_management_hidden_id = Session::get('restaurant_management_hidden_id');
            return redirect()->action('RestaurantController@getShow', [$restaurant_management_hidden_id]);
        }
        else{
            abort(404);
        }
    }

    public function postFavorite(Request $request){

        $restaurant_management_id = $request->restaurant_management_hidden_id;
        $user_id = $request->user_hidden_id;

        $command = new CheckExistFavoriteCommand($restaurant_management_id, $user_id);
        
        if ($command->checkExistFavorite()){ //if favorite exists before
            $command = new DeleteFavoriteCommand($restaurant_management_id, $user_id);
        }       
        else{
            $command = new StoreFavoriteCommand($restaurant_management_id, $user_id);
        } 
        $this->dispatch($command);            

        return redirect()->action('RestaurantController@getShow', [$request->restaurant_management_hidden_id]);
    }

}
