<?php

namespace Solivis\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Solivis\Http\Requests;
use Solivis\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Solivis\User;

class ApiCustomerProfileController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('api.customer.auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return redirect()->away('http://solivis.com/images/uploads/john-cena-intense1_original_crop_340x234_original_crop_exact.jpg');
        
        $customer = Auth::user('user');
        $customer['photo'] = $customer['photo_url'];
        unset($customer['photo_url']);
        $customer['photo'] = base64_encode(file_get_contents( asset('images/uploads/' . $customer['photo'] )));
        $customer = json_encode(array($customer));
        return response($customer, 200)->header('Content-Type', 'application/json');
        // $profile = User::where('');
        
        // $image = file_get_contents(asset('images/uploads/john-cena-intense1_original_crop_340x234_original_crop_exact.jpg'));
        // return response($image, 200)->header('Content-Type', 'image/jpeg');
        
        // return base64_encode($image);
        // return redirect()->away(asset('images/uploads/john-cena-intense1_original_crop_340x234_original_crop_exact.jpg'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer = Auth::user('user');
        $customer['photo'] = $customer['photo_url'];
        unset($customer['photo_url']);
        $customer['photo'] = base64_encode(file_get_contents( asset('images/uploads/' . $customer['photo'] )));
        $customer = json_encode(array($customer));
        return response($customer, 200)->header('Content-Type', 'application/json');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request_type = $request->input('request_type');
        if($request_type == 'profile'){
            $customer = User::find(Auth::user()->id);
            $customer->name = $request->name;
            $customer->email = $request->email;
            $customer->phone_number = $request->phone;
            $customer->gender = $request->gender;
            $customer->birth_date = $request->birth;
            $customer->save();
            
            $response = array();
            $response['message'] = 'Success';
            $response = json_encode(array($response));
            return response($response, 200)->header('Content-Type', 'application/json');
        }
        else if($request_type == 'password'){
            $customer = User::find(Auth::user()->id);
            if(bcrypt($request->input('old_password')) == $customer->password){
                $customer->password = bcrypt($request->input('new_password'));
                
                $response = array();
                $response['message'] = 'Success';
                $response = json_encode(array($response));
                return response($response, 200)->header('Content-Type', 'application/json');
            }
            else{
                $response = array();
                $response['message'] = 'Old password not match.';
                $response = json_encode(array($response));
                return response($response, 401)->header('Content-Type', 'application/json');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
