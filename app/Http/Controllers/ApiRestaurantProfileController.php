<?php

namespace Solivis\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Solivis\Http\Requests;
use Solivis\Http\Controllers\Controller;
use Illuminate\Http\Response;

class ApiRestaurantProfileController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('api.restaurant.auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $restaurant = Auth::user('restaurant_management');
        // $restaurant['photo'] = $restaurant['photo_url'];
        // unset($restaurant['photo_url']);
        // $restaurant['photo'] = base64_encode(file_get_contents( asset('images/restaurant_uploads/' . $restaurant['photo'] )));
        $restaurant['photo'] = $restaurant['photo_url'];
        unset($restaurant['photo_url']);
        $restaurant['photo'] = base64_encode(file_get_contents( asset('images/restaurant_uploads/' . $restaurant['photo'] )));
        $restaurant = json_encode(array($restaurant));
        return response($restaurant, 200)->header('Content-Type', 'application/json');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $restaurant = Auth::user('restaurant_management');
        // $restaurant['photo'] = $restaurant['photo_url'];
        // unset($restaurant['photo_url']);
        // $restaurant['photo'] = base64_encode(file_get_contents( asset('images/restaurant_uploads/' . $restaurant['photo'] )));
        $restaurant['photo'] = $restaurant['photo_url'];
        unset($restaurant['photo_url']);
        $restaurant['photo'] = base64_encode(file_get_contents( asset('images/restaurant_uploads/' . $restaurant['photo'] )));
        $restaurant = json_encode(array($restaurant));
        return response($restaurant, 200)->header('Content-Type', 'application/json');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
