<?php

namespace Solivis\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Hash;
use Snowfire\Beautymail\Beautymail;
use File;
use Carbon\Carbon;
use Solivis\RestaurantManagement;
use Solivis\Menu;
use Solivis\MenuCategory;
use Solivis\RestaurantHistoryBalance;
use Solivis\Reservation;
use Solivis\HistoryBalance;
use Solivis\Bank;
use Solivis\RestaurantBank;
use Solivis\Admin;
use Solivis\Commands\UpdateRestaurantManagementPasswordCommand;
use Solivis\Commands\CheckExistBankCommand;
use Solivis\Commands\StoreRestaurantManagementBankCommand;
use Solivis\Commands\UpdateRestaurantManagementBankCommand;
use Solivis\Commands\UpdateRestaurantManagementPictureCommand;
use Solivis\Commands\UpdateRestaurantManagementProfileCommand;
use Solivis\Commands\UpdateRestaurantManagementCuisineServiceCommand;
use Solivis\Commands\StoreRestaurantManagementFoodPhotoCommand;
use Solivis\Commands\DeleteRestaurantManagementFoodPhotoCommand;
use Solivis\Commands\StoreRestaurantManagementInsidePhotoCommand;
use Solivis\Commands\DeleteRestaurantManagementInsidePhotoCommand;
use Solivis\Commands\CheckRestaurantManagementInsidePhotoLimitCommand;
use Solivis\Commands\GetRestaurantManagementMenuCategoryIdCommand;
use Solivis\Commands\StoreRestaurantManagementMenuCommand;
use Solivis\Commands\UpdateRestaurantManagementMenuCommand;
use Solivis\Commands\DeleteRestaurantManagementMenuCommand;
use Solivis\Commands\StoreRestaurantHistoryBalanceCommand;
use Solivis\Commands\GetTotalReservationPriceCommand;
use Solivis\Commands\StoreDeclinedReasonCommand;
use Solivis\Commands\StoreQrCodePlainCommand;
use Solivis\Commands\StoreExpiredComingConfirmationCommand;
use Solivis\Http\Requests;
use Solivis\Http\Controllers\Controller;

class RestaurantManagementController extends Controller
{

	public function __construct(){
        $this->middleware('restaurant_management');
	}

	public function getIndex(){
		return redirect('restaurant-management/dashboard');
	}

	public function getDashboard(){
		$section_title = 'Dashboard';
        $restaurant_management = Auth::user('restaurant_management');

        $names = array();
        foreach($restaurant_management->restaurant_banks as $restaurant_bank){
            $names[$restaurant_bank->id] = $restaurant_bank->bank->name;
        }

        $account_numbers = $restaurant_management->restaurant_banks()->lists('account_number', 'id');
        $account_names = $restaurant_management->restaurant_banks()->lists('account_name', 'id');

        $complete_info = array();
        foreach($names as $id => $name){
            $complete_info[$id] = $names[$id]."-".$account_numbers[$id]." a/n ".$account_names[$id];
        }

        // show only paid, accepted or declined (limit only 6)
        $reservations = Reservation::where(function ($query) {
                            $query->where('status_reservation_id', '=', '3')
                                  ->orWhere('status_reservation_id', '=', '4')
                                  ->orWhere('status_reservation_id', '=', '5');
                        })->where(function ($query) use ($restaurant_management){
                            $query->where('restaurant_management_id', '=', $restaurant_management->id);
                        });

        $restaurant_reservations = $reservations->orderBy('updated_at', 'desc')->get()->take(6);

        //get only success reservation with qr_code_plain already processed in 2016
        //2017-01-01 because of 2016-12-31 need to be included
        $reservation_2016_years = Reservation::WhereHas('qr_code_plain', function($qr_code_plain){
                                    $qr_code_plain->where('status_processed', '1');
                                  })->whereBetween('reservation_date', array('2016-01-01', '2017-01-01'))
                                  ->where('restaurant_management_id', $restaurant_management->id)
                                  ->where('status_reservation_id', '4')
                                  ->get();

        $total_reservation_2016_years = count($reservation_2016_years);

        //restaurant incomes
        $total_income_2016_years = [];
        $until_month = 0;
        $until_year = 2016;
        for($month=1; $month<=12; $month++){
            $total_income_per_month = 0;
            $until_month = $month + 1;
            if($until_month == 13){
                $until_year = 2017;
                $until_month = 1;
            }
            $reservation_per_month = Reservation::WhereHas('qr_code_plain', function($qr_code_plain){
                                        $qr_code_plain->where('status_processed', '1');
                                    })->whereBetween('reservation_date', array('2016-'.$month.'-01', $until_year.'-'.$until_month.'-01'))
                                    ->where('restaurant_management_id', $restaurant_management->id)
                                    ->where('status_reservation_id', '4')->get();

            for($i=0; $i<count($reservation_per_month); $i++){
                $getTotalReservationPriceCommand = new GetTotalReservationPriceCommand();            
                $total_income_per_month += $reservation_per_month[$i]->getTotalNetIncome();
            }
            $total_income_2016_years[$month] = $total_income_per_month;
        }

        $only_accepted_restaurant_reservations =  $restaurant_management->reservations()
                                                ->orderBy('updated_at', 'desc')        
                                                ->where('status_reservation_id', '4')
                                                ->get();


		return view('restaurant-management/dashboard', compact('section_title', 'restaurant_management', 'complete_info', 'restaurant_reservations', 'total_reservation_2016_years', 'total_income_2016_years', 'only_accepted_restaurant_reservations'));
	}

	public function getReservation(){
		$section_title = 'Reservation';
        $restaurant_management = Auth::user('restaurant_management');

        // show only paid, accepted or declined (limit only 6)
        $reservations = Reservation::where(function ($query) {
                            $query->where('status_reservation_id', '=', '3')
                                  ->orWhere('status_reservation_id', '=', '4')
                                  ->orWhere('status_reservation_id', '=', '5');
                        })->where(function ($query) use ($restaurant_management){
                            $query->where('restaurant_management_id', '=', $restaurant_management->id);
                        });

        $restaurant_reservations = $reservations->orderBy('updated_at', 'desc')->paginate(6);

        //get only success reservation in 2016
        $reservation_2016_years = Reservation::whereBetween('reservation_date', array('2016-01-01', '2017-01-01'))
                                  ->where('restaurant_management_id', $restaurant_management->id)
                                  ->where('status_reservation_id', '4')
                                  ->get();

        $only_accepted_restaurant_reservations =  $restaurant_management->reservations()
                                                ->orderBy('updated_at', 'desc')        
                                                ->where('status_reservation_id', '4')
                                                ->get();

		return view('restaurant-management/reservation', compact('section_title', 'restaurant_management', 'restaurant_reservations', 'only_accepted_restaurant_reservations'));
	}

    public function putReservationStatus(Request $request){
        $reservation_id = $request->reservation_id_hidden;        
        $reservation = Reservation::find($reservation_id);

        $user_name = $reservation->user->name;
        $user_email = $reservation->user->email;
        $reservation_date = $reservation->convertDate();
        $reservation_time = $reservation->convertTime();
        $paxes = $reservation->paxes;

        $total_reservation_price = $reservation->getTotalReservationPrice();
        $reservation_menus = $reservation->reservation_menus;

        if (!$request->reason){ //if accept reservation
            $reservation_result_info = "Reservasi diterima";

            //prevent duplicate qrcode
            $unique = false;
            while(!$unique){
                $code = $reservation->generateQrCodePlain();
                if($reservation->checkUniqueQrCodePlain($code)){
                    $unique = true;
                }
            }

            $command = new StoreQrCodePlainCommand($code, $reservation->id);
            $this->dispatch($command);             

            //Restaurant Management send mail through Admin to user
            $logo['path'] = '';
            $logo['width'] = '0px';
            $logo['height'] = '0px';
            $beautymail = app()->make(Beautymail::class);
            $beautymail->send('emails.reservation_result', 
                [
                    'logo' => $logo,
                    'reservation_result_info' => $reservation_result_info,
                    'reservation_id' => $reservation->id,
                    'user_name' => $user_name,
                    'reservation_date' => $reservation_date, 
                    'reservation_time' => $reservation_time, 
                    'paxes' => $paxes, 
                    'total_reservation_price' => $total_reservation_price, 
                    'reservation_menus' => $reservation_menus, 
                    'code' => $code,          
                ],  function($message) use($user_email, $user_name) {
                        $message->to($user_email, $user_name)
                                ->subject('Reservasi Diterima oleh Pihak Restoran');
            });

            $reservation->status_reservation_id = 4; //SUKSES
            $reservation->save();

            // add expired coming confirmations
            $command = new StoreExpiredComingConfirmationCommand($reservation->id);
            $this->dispatch($command);

            $total_net_income = $reservation->getTotalNetIncome();

            return redirect()->back()->with([
                'info' => 'Anda telah menerima reservasi ini. Saldo anda akan ditambahkan sebesar Rp. '.number_format($total_net_income,0,'.',',').' setelah konsumen menggunakan reservasinya.'
            ]);
        }
        else{
            $reservation_result_info = "Reservasi ditolak";
            // Admin send mail to User
            $logo['path'] = '';
            $logo['width'] = '0px';
            $logo['height'] = '0px';
            $beautymail = app()->make(Beautymail::class);
            $beautymail->send('emails.reservation_result', 
                [
                    'logo' => $logo,
                    'reservation_result_info' => $reservation_result_info,
                    'reservation_id' => $reservation->id,
                    'user_name' => $user_name,
                    'reservation_date' => $reservation_date, 
                    'reservation_time' => $reservation_time, 
                    'paxes' => $paxes, 
                    'total_reservation_price' => $total_reservation_price, 
                    'reservation_menus' => $reservation_menus,  
                    'reason' => $request->reason         
                ],  function($message) use($user_email, $user_name) {
                        $message->to($user_email, $user_name)
                                ->subject('Reservasi Ditolak oleh Pihak Restoran');
            });            

            $command = new StoreDeclinedReasonCommand($reservation->id, $request->reason);
            $this->dispatch($command); 

            $reservation->status_reservation_id = 5; //BATAL
            $reservation->save();

            //status_balance_id
            // 1 Reservasi ditolak (dana dikembalikan)
            HistoryBalance::create([
                'process_date' => Carbon::now(),
                'balance_processed' => $total_reservation_price,
                'status_balance_id' => 1,
                'user_id' => $reservation->user->id,
                'transfer_target' => 'Ke saldo solivis',
             ]);

            //add to user balance
            $user = $reservation->user;
            $user->balance += $total_reservation_price;
            $user->save();

            return redirect()->back()->with([
                'info' => 'Anda telah menolak reservasi dengan ID: '.$reservation->id 
            ]);            
        }

    }

	public function getRestaurant(){
        $restaurant_management = Auth::user('restaurant_management');
		$section_title = 'Restaurant';

        $menu_category_id_uniques = array();
        $menus = Menu::where('restaurant_management_id', $restaurant_management->id)->get();

        for ($i=0; $i<count($menus); $i++){
            $menu_category_uniques[$i] = $menus[$i]->menu_category_id;   
            if (!in_array($menu_category_uniques[$i], $menu_category_id_uniques)) { //false mean, current value has not assigned to array
                $category_name = MenuCategory::where('id', $menu_category_uniques[$i])->first()->name;
                $menu_category_id_uniques[$category_name] =  $menu_category_uniques[$i];
            }
        }
		return view('restaurant-management/restaurant', compact('restaurant_management', 'section_title', 'menu_category_id_uniques'));
	}

    public function putEditPicture(Request $request, $id){
        $messages = [
            'photo_url.image' => 'Gambar profil yang anda upload harus berbentuk image.',
            'photo_url.max' => 'Maksimal ukuran gambar profil yang dapat diupload adalah 1MB.'
        ];

        $this->validate($request, [
            'photo_url' => 'required|image|max:1000'
        ], $messages);

        $restaurant_management = RestaurantManagement::find($id);
        //Delete old restaurant_management's image profile from directory.
        if($restaurant_management->photo_url && $restaurant_management->photo_url != "restaurant-default.png"){
            File::delete(public_path('images/restaurant_uploads/'.$restaurant_management->photo_url));
        }

        $photo_url = $request->photo_url;

        if ($photo_url){
            //get image name
            $photo_url_filename = $photo_url->getClientOriginalName();
            // //move image to public/images/uplaods directory
            $photo_url->move(public_path('images/restaurant_uploads'), $photo_url_filename);

            $command = new UpdateRestaurantManagementPictureCommand($id, $photo_url_filename);
            $this->dispatch($command);

            return redirect()->action('RestaurantManagementController@getRestaurant')->with([
                'info' => 'Profil gambar restoran anda sudah berhasil diupdate.'
            ]);
        }
    }

    public function putEditProfile(Request $request, $id){
        if (!$request->cuisine){
            return redirect()->action('RestaurantManagementController@getRestaurant')->with([
                'error' => 'Jenis Kuliner harus diisi.'
            ]);              
        }
        else if (!$request->service){
            return redirect()->action('RestaurantManagementController@getRestaurant')->with([
                'error' => 'Services harus diisi.'
            ]);              
        }

        $this->validate($request, [
            'name' => 'required|max:255',
            'address' => 'required|max:255',
            'email' => 'required|email|max:255|unique:restaurant_managements,email,'.$id,
            'website_url' => 'max:255',
            'phone_number' => 'required|numeric',
            'open_time' => 'required',
            'close_time' => 'required',
        ]);

        $name = $request->name;
        $address = $request->address;
        $email = $request->email;
        $website_url = $request->website_url;
        $phone_number = $request->phone_number;
        $open_time = $request->open_time;
        $close_time = $request->close_time;
        $cuisines = $request->cuisine;
        $services = $request->service;

        $command = new UpdateRestaurantManagementProfileCommand($id, $name, $address, $email, $website_url, $phone_number, $open_time, $close_time);
        $this->dispatch($command);

        $command = new UpdateRestaurantManagementCuisineServiceCommand($id, $cuisines, $services);
        $this->dispatch($command);

        return redirect()->action('RestaurantManagementController@getRestaurant')->with([
            'info' => 'Profil restoran anda sudah berhasil diupdate.'
        ]);  
    }
    
    public function putEditFoodPhoto(Request $request, $id){
        $messages = [
            'food_photo_url.image' => 'Foto makanan yang anda upload harus berbentuk image.',
            'food_photo_url.max' => 'Maksimal ukuran foto makanan yang dapat diupload adalah 1MB.'
        ];

        $this->validate($request, [
            'food_photo_url' => 'required|image|max:1000'
        ], $messages);  

        $food_photo_url = $request->food_photo_url;

        if ($food_photo_url){
            $food_photo_url_filename = $food_photo_url->getClientOriginalName();
            $food_photo_url->move(public_path('images/restaurant_uploads/food_photos'), $food_photo_url_filename);

            //remove extension
            $info = pathinfo($food_photo_url_filename);
            $food_photo_only_name = basename($food_photo_url_filename,'.'.$info['extension']); 

            $command = new StoreRestaurantManagementFoodPhotoCommand($id, $food_photo_url_filename, $food_photo_only_name);
            $this->dispatch($command);

            return redirect()->action('RestaurantManagementController@getRestaurant')->with([
                'info' => 'Foto makanan restoran anda sudah berhasil diupload.'
            ]);
        }        
    }

    public function deleteFoodPhoto(Request $request){
        $food_photo_id = $request->food_photo_id;
        $food_photo_url = $request->food_photo_url;

        $command = new DeleteRestaurantManagementFoodPhotoCommand($food_photo_id, $food_photo_url);
        $this->dispatch($command);

        return redirect()->action('RestaurantManagementController@getRestaurant')->with([
            'info' =>'Foto '.$food_photo_url.' sudah berhasil dihapus.'  
        ]); 
    }

    public function putEditInsideRestaurantPhoto(Request $request, $id){
        $messages = [
            'inside_restaurant_photo_url.image' => 'Foto suasana restoran yang anda upload harus berbentuk image.',
            'inside_restaurant_photo_url.max' => 'Maksimal ukuran foto suasana restoran yang dapat diupload adalah 1MB.'
        ];

        $this->validate($request, [
            'inside_restaurant_photo_url' => 'required|image|max:1000'
        ], $messages); 

        $inside_restaurant_photo_url = $request->inside_restaurant_photo_url;

        if ($inside_restaurant_photo_url){

            $command = new CheckRestaurantManagementInsidePhotoLimitCommand($id);

            if ($command->checkOverLimitInsidePhoto()){ //if inside photos already 5
                return redirect()->action('RestaurantManagementController@getRestaurant')->with([
                    'error' => 'Maaf :( Maksimal foto suasana restoran yang boleh diupload hanya 5.'
                ]);
            }
            else{
                $inside_restaurant_photo_url_filename = $inside_restaurant_photo_url->getClientOriginalName();
                $inside_restaurant_photo_url->move(public_path('images/restaurant_uploads/inside_restaurant_photos'), $inside_restaurant_photo_url_filename);

                $info = pathinfo($inside_restaurant_photo_url_filename);
                $inside_restaurant_photo_only_name = basename($inside_restaurant_photo_url_filename,'.'.$info['extension']); 

                $command = new StoreRestaurantManagementInsidePhotoCommand($id, $inside_restaurant_photo_url_filename, $inside_restaurant_photo_only_name);
                $this->dispatch($command);

                return redirect()->action('RestaurantManagementController@getRestaurant')->with([
                    'info' => 'Foto suasana restoran anda sudah berhasil diupload.'
                ]);
            }
        }        
    }

    public function deleteInsideRestaurantPhoto(Request $request){
        $inside_restaurant_photo_id = $request->inside_restaurant_photo_id;
        $inside_restaurant_photo_url = $request->inside_restaurant_photo_url;

        $command = new DeleteRestaurantManagementInsidePhotoCommand($inside_restaurant_photo_id, $inside_restaurant_photo_url);
        $this->dispatch($command);

        return redirect()->action('RestaurantManagementController@getRestaurant')->with([
            'info' =>'Foto '.$inside_restaurant_photo_url.' sudah berhasil dihapus.'  
        ]); ;
    }    

    public function postAddMenu(Request $request, $id){
        $category_name = $request->category_name;

        $command = new GetRestaurantManagementMenuCategoryIdCommand($category_name);
        $menu_category_id = $command->getMenuCategoryId();

        $item_names = $request->item_name;
        $item_descriptions = $request->item_description;
        $item_prices = $request->item_price;

        for($i=0; $i<count($item_names); $i++){
            // check existing menu
            $existing_menu = Menu::where('restaurant_management_id', $id)
                            ->where('item_name', $item_names[$i])
                            ->first();

            if($existing_menu){
                return redirect()->action('RestaurantManagementController@getRestaurant')->with([
                    'error' => 'Menu makanan bernama '.$existing_menu->item_name. ' sudah pernah disimpan di kategori '.$existing_menu->menu_category->name.'.'
                ]);                                
            }

        }

        for ($i=0; $i<count($item_names); $i++){ //name, description and price has same length
            $existing_menu = Menu::where('restaurant_management_id', $id)
                            ->where('menu_category_id', $menu_category_id)
                            ->where('item_name', $item_names[$i])
                            ->first();  

            $command = new StoreRestaurantManagementMenuCommand($item_names[$i], $item_descriptions[$i], $item_prices[$i], $menu_category_id, $id, '1');
            $this->dispatch($command);
        }   

        return redirect()->action('RestaurantManagementController@getRestaurant')->with([
            'info' => 'Menu makanan restoran anda sudah berhasil ditambah.'
        ]);
    }

    public function putEditMenu(Request $request){
        $removed_menus = $request->removed_menu; //menu id

        //Delete Menu 
        if ($removed_menus){
            for ($i=0; $i<count($removed_menus); $i++){
                $command = new DeleteRestaurantManagementMenuCommand($removed_menus[$i]);
                $this->dispatch($command);
            }
        }

        //Update Menu
        $item_ids = $request->item_id;
        $item_descriptions = $request->item_description;
        $item_prices = $request->item_price;

        for ($i=0; $i<count($item_ids); $i++){
            $command = new UpdateRestaurantManagementMenuCommand($item_ids[$i], $item_descriptions[$i], $item_prices[$i], '1');
            $this->dispatch($command);            
        }

        return redirect()->action('RestaurantManagementController@getRestaurant')->with([
            'info' => 'Menu makanan restoran anda sudah berhasil diupdate.'
        ]);  
    }

	public function getReport(){

		$section_title = 'Report';
        $restaurant_management = Auth::user('restaurant_management');

        $total_income = 0;
        $reservations = Reservation::where('status_reservation_id', '4')
                        ->WhereHas('qr_code_plain', function($qr_code_plain){
                            $qr_code_plain->where('status_processed', '1');
                        })->where('restaurant_management_id', $restaurant_management->id)
                        ->get();

        for($i=0; $i<count($reservations); $i++){
            $total_income += $reservations[$i]->getTotalNetIncome();
        }

        $restaurant_reservations = Reservation::WhereHas('qr_code_plain', function($qr_code_plain){
                                    $qr_code_plain->where('status_processed', '1');
                                   })->where('restaurant_management_id', $restaurant_management->id)
                                   ->where('status_reservation_id', 4)
                                   ->orderBy('reservation_date')
                                   ->paginate(4);        

		return view('restaurant-management/report', compact('section_title', 'restaurant_reservations', 'total_income'));
	}

    public function getHistoryReportDate(Request $request){
        $section_title = 'Report';
        $restaurant_management = Auth::user('restaurant_management');

        $reservations = Reservation::where('status_reservation_id', '4')
                        ->WhereHas('qr_code_plain', function($qr_code_plain){
                            $qr_code_plain->where('status_processed', '1');
                        })->where('restaurant_management_id', $restaurant_management->id)
                        ->get();

        $total_income = 0;
        for($i=0; $i<count($reservations); $i++){
            $total_income += $reservations[$i]->getTotalNetIncome();
        }

        $date_start = $request->date_start;
        $date_end = $request->date_end;

        $date_start = date("Y-m-d", strtotime($request->date_start));
        $date_end = Carbon::parse(date("Y-m-d", strtotime($request->date_end)))->addDay(1);

        $restaurant_reservations_date_search = Reservation::WhereHas('qr_code_plain', function($qr_code_plain){
                                                $qr_code_plain->where('status_processed', '1');
                                              })->where('restaurant_management_id', $restaurant_management->id)
                                              ->where('status_reservation_id', 4)
                                              ->whereBetween('reservation_date', array($date_start, $date_end))
                                              ->orderBy('reservation_date')
                                              ->paginate(4);

        return view('restaurant-management/report', compact('section_title', 'restaurant_reservations_date_search', 'total_income'));
    }

    public function getHistoryReportKeyword(Request $request){
        $section_title = 'Report';
        $restaurant_management = Auth::user('restaurant_management');

        $reservations = Reservation::where('status_reservation_id', '4')
                        ->WhereHas('qr_code_plain', function($qr_code_plain){
                            $qr_code_plain->where('status_processed', '1');
                        })->where('restaurant_management_id', $restaurant_management->id)
                        ->get();

        $total_income = 0;
        for($i=0; $i<count($reservations); $i++){
            $total_income += $reservations[$i]->getTotalNetIncome();
        }

        $search = $request->search;

        $restaurant_reservations_search = Reservation::whereHas('user', function($user) use ($search){
                                            $user->where('name','LIKE','%'.$search.'%');
                                          })
                                          ->WhereHas('qr_code_plain', function($qr_code_plain){
                                                $qr_code_plain->where('status_processed', '1');
                                          })
                                          ->where('status_reservation_id', 4)
                                          ->orderBy('reservation_date')
                                          ->orWhereHas('reservation_menus', function($reservation_menus) use ($search){
                                            $reservation_menus->where('menu_name', 'LIKE', '%'.$search.'%');
                                          })
                                          ->WhereHas('qr_code_plain', function($qr_code_plain){
                                                $qr_code_plain->where('status_processed', '1');
                                          })                                          
                                          ->where('status_reservation_id', 4)
                                          ->orderBy('reservation_date')
                                          ->orWhere('id', 'LIKE', '%'.$search.'%')
                                          ->WhereHas('qr_code_plain', function($qr_code_plain){
                                                $qr_code_plain->where('status_processed', '1');
                                          })                                          
                                          ->where('status_reservation_id', 4)
                                          ->orderBy('reservation_date')
                                          ->paginate(4);

        return view('restaurant-management/report', compact('section_title', 'restaurant_reservations_search', 'total_income'));
    }

    public function getPrintReport(Request $request){
        $section_title = 'Print Report';
        $restaurant_management = Auth::user('restaurant_management');

        $date_start = $request->date_start;
        $date_end = $request->date_end;
        $date_start = date("Y-m-d", strtotime($request->date_start));
        $date_end = Carbon::parse(date("Y-m-d", strtotime($request->date_end)))->addDay(1);

        $restaurant_reservations_date_print =   Reservation::WhereHas('qr_code_plain', function($qr_code_plain){
                                                    $qr_code_plain->where('status_processed', '1');
                                                })->where('restaurant_management_id', $restaurant_management->id)
                                                  ->where('status_reservation_id', 4)
                                                  ->whereBetween('reservation_date', array($date_start, $date_end))
                                                  ->orderBy('reservation_date')
                                                  ->get();

        if(count($restaurant_reservations_date_print) == 0){
            $reservation = Reservation::all()->first();
            $date_end = Carbon::parse(date("Y-m-d", strtotime($request->date_end)));            
            return redirect()->back()->with([
                'error' =>'Tidak ada reservasi pada tgl. '.$reservation->convertNumericHypenDate($date_start).' s/d tgl. '.$reservation->convertNumericHypenDate($date_end)
            ]);
        }
        else{
            $total_income = 0;
            for($i=0; $i<count($restaurant_reservations_date_print); $i++){
                $total_income += $restaurant_reservations_date_print[$i]->getTotalNetIncome();
            }
            $date_end = Carbon::parse(date("Y-m-d", strtotime($request->date_end)));            
            return view('restaurant-management/print-report', compact('section_title', 'restaurant_reservations_date_print', 'total_income', 'date_start', 'date_end'));            
        }
    }

    public function getSortReport(Request $request){
        $section_title = 'Report';
        $restaurant_management = Auth::user('restaurant_management');

        $total_income = 0;
        $reservations = Reservation::where('status_reservation_id', '4')
                        ->WhereHas('qr_code_plain', function($qr_code_plain){
                            $qr_code_plain->where('status_processed', '1');
                        })->where('restaurant_management_id', $restaurant_management->id)
                        ->get();

        for($i=0; $i<count($reservations); $i++){
            $total_income += $reservations[$i]->getTotalNetIncome();
        }

        if($request->sort_report == "all"){
            $restaurant_reservations =  Reservation::WhereHas('qr_code_plain', function($qr_code_plain){
                                            $qr_code_plain->where('status_processed', '1');
                                        })->where('restaurant_management_id', $restaurant_management->id)
                                          ->where('status_reservation_id', 4)
                                          ->orderBy('reservation_date')            
                                          ->paginate(4);
        }  
        else if($request->sort_report == "reservation_id"){
            $restaurant_reservations =  Reservation::WhereHas('qr_code_plain', function($qr_code_plain){
                                            $qr_code_plain->where('status_processed', '1');
                                        })->where('restaurant_management_id', $restaurant_management->id)
                                          ->where('status_reservation_id', 4)
                                          ->orderBy('id')            
                                          ->paginate(4);
        }      
        else if($request->sort_report == "user_name"){
            $restaurant_reservations =  Reservation::WhereHas('qr_code_plain', function($qr_code_plain){
                                            $qr_code_plain->where('status_processed', '1');
                                        })->where('restaurant_management_id', $restaurant_management->id)
                                          ->where('status_reservation_id', 4)
                                          ->whereHas('user', function($user){
                                            $user->orderBy('name');
                                          })->paginate(4);
        }
        else if($request->sort_report == "reservation_date"){
            $restaurant_reservations =  Reservation::WhereHas('qr_code_plain', function($qr_code_plain){
                                            $qr_code_plain->where('status_processed', '1');
                                        })->where('restaurant_management_id', $restaurant_management->id)
                                          ->where('status_reservation_id', 4)
                                          ->orderBy('reservation_date')            
                                          ->paginate(4);
        }
        else{
            $restaurant_reservations = Reservation::WhereHas('qr_code_plain', function($qr_code_plain){
                                            $qr_code_plain->where('status_processed', '1');
                                        })->where('restaurant_management_id', $restaurant_management->id)
                                          ->where('status_reservation_id', 4)
                                          ->CountUserPayment()->orderBy('total_payment')
                                          ->paginate(4);
        }

        return view('restaurant-management/report', compact('section_title', 'restaurant_reservations', 'total_income'));
    }

	public function getBalance(){
		$section_title = 'Balance';
		$restaurant_management = Auth::user('restaurant_management');

        $names = array();
        foreach($restaurant_management->restaurant_banks as $restaurant_bank){
            $names[$restaurant_bank->id] = $restaurant_bank->bank->name;
        }
        $account_numbers = $restaurant_management->restaurant_banks()->lists('account_number', 'id');
        $account_names = $restaurant_management->restaurant_banks()->lists('account_name', 'id');

        $complete_info = array();
        foreach($names as $id => $name){
            $complete_info[$id] = $names[$id]."-".$account_numbers[$id]." a/n ".$account_names[$id];
        }

        $banks = Bank::all()->lists('name', 'id');

        $restaurant_banks_info = array();

        $restaurant_banks = $restaurant_management->restaurant_banks;
        for($i=0; $i<count($restaurant_banks); $i++){
            $restaurant_banks_info[$restaurant_banks[$i]->id] = $restaurant_banks[$i]->bank->name;
        }

        $restaurant_history_balances = $restaurant_management->restaurant_history_balances()->orderBy('process_date', 'desc')->paginate(6);

		return view('restaurant-management/balance', compact('restaurant_management', 'complete_info', 'section_title', 'banks', 'restaurant_banks_info', 'restaurant_history_balances'));
	}

    public function postAddBank(Request $request, $id){
        $this->validate($request, [
            'account_number' => 'required|numeric',
            'account_name' => 'required',
            'password' => 'required'
        ]);        

        $bank_id = $request->bank_id;
        $account_number = $request->account_number;
        $account_name = $request->account_name;
        $password = $request->password;

        if (Hash::check($request->password, RestaurantManagement::find($id)->password)){
            //check bank and account number exist
            $checkexistbank_command = new CheckExistBankCommand($bank_id, $account_number, $id, null);
            $result = $checkexistbank_command->checkExistRestaurantManagementBank();

            // create new Bank data
            if (!$result){
                $command = new StoreRestaurantManagementBankCommand($account_number, $account_name, $bank_id, $id);
                $this->dispatch($command);
            }
            else{
                return redirect()->back()->with([
                    'error' => 'Bank dengan nomor rekening yang sama sudah terdaftar.'
                ]);
            }

            return redirect()->back()->with([
                'info' => 'Data Bank anda sudah berhasil ditambah.'
            ]); 
        }
        else{
            return redirect()->back()->with([
                'error' => 'Password yang anda masukkan salah.'
            ]);                        
        }              
    }

    public function deleteBank(Request $request){
        $restaurant_bank_id = $request->restaurant_bank_id_del;
        RestaurantBank::where('id', $restaurant_bank_id)->delete();        

        return redirect()->action('RestaurantManagementController@getBalance')->with([
            'info' => 'Data Bank anda sudah dihapus.'
        ]);

    }    

    public function putEditBank(Request $request, $id){
        $this->validate($request, [
            'account_number' => 'required|numeric',
            'account_name' => 'required',
            'password' => 'required'
        ]);        

        $bank_id = $request->bank_id_ori;
        $restaurant_bank_id = $request->restaurant_bank_id;
        $account_number = $request->account_number;
        $account_name = $request->account_name;
        $password = $request->password;

        if (Hash::check($request->password, RestaurantManagement::find($id)->password)){   
            //check bank and account number exist
            $checkexistbank_command = new CheckExistBankCommand($bank_id, $account_number, $id, $restaurant_bank_id);
            $result = $checkexistbank_command->checkExistRestaurantManagementBankForUpdate();

            // update Bank Data
            if (!$result){
                $command = new UpdateRestaurantManagementBankCommand($account_number, $account_name, $restaurant_bank_id);
                $this->dispatch($command);

                return redirect()->action('RestaurantManagementController@getBalance')->with([
                    'info' => 'Data Bank anda sudah berhasil diupdate.'
                ]);             
            }
            else{
                return redirect()->back()->with([
                    'error' => 'Bank dengan nomor rekening yang sama sudah terdaftar.'
                ]);
            }                 

        }
        else{
            return redirect()->back()->with([
                'error' => 'Password yang anda masukkan salah.'
            ]);                        
        }              
    }

    public function postWithdraw(Request $request, $id){
        $this->validate($request, [
            'balance_processed' => 'required|numeric',
            'restaurant_bank_id' => 'required|numeric',
            'password' => 'required'
        ]);

       //if restaurant_management had unprocessed balance withdraw before
        $history_balance = RestaurantHistoryBalance::where('restaurant_management_id', $id)
                           ->where('status_balance_id', 3)
                           ->orderBy('process_date', 'desc')
                           ->first();

        if (!$history_balance){
            if (Hash::check($request->password, RestaurantManagement::find($id)->password)){
                //balance to be withdraw more than restaurant_management has
                if ($request->balance_processed > RestaurantManagement::find($id)->balance){
                    return redirect()->back()->with([
                        'error' => 'Jumlah penarikan yang anda minta melebihi jumlah saldo anda.'
                    ]);                     
                }
                else{
                    $balance_processed = $request->balance_processed;
                    $restaurant_bank_id = $request->restaurant_bank_id;

                    $command = new StoreRestaurantHistoryBalanceCommand($id, $balance_processed, $restaurant_bank_id);
                    $this->dispatch($command);

                    return redirect()->action('RestaurantManagementController@getBalance')->with([
                        'info' =>'Permintaan tarik dana anda akan diproses paling lama dalam waktu 2x24 jam ke depan.'
                    ]);  
                }
            }
            else{
                return redirect()->back()->with([
                    'error' => 'Password yang anda masukkan salah.'
                ]);                        
            }
        }
        else{
            // if the last withdraw balance is today
            if(Carbon::parse($history_balance->process_date)->toDateString() == Carbon::now()->toDateString()){
                return redirect()->back()->with([
                    'error' => 'Mohon maaf :( Permintaan tarik dana hanya dapat dilakukan 1 kali dalam sehari. Silahkan coba kembali besok.'
                ]);                                  
            }
            else{
                if (Hash::check($request->password, RestaurantManagement::find($id)->password)){
                    //balance to be withdraw more than restaurant_management has
                    if ($request->balance_processed > RestaurantManagement::find($id)->balance){
                        return redirect()->back()->with([
                            'error' => 'Jumlah penarikan yang anda minta melebihi jumlah saldo anda.'
                        ]);                     
                    }
                    else{
                        $balance_processed = $request->balance_processed;
                        $restaurant_bank_id = $request->restaurant_bank_id;

                        $command = new StoreRestaurantHistoryBalanceCommand($id, $balance_processed, $restaurant_bank_id);
                        $this->dispatch($command);

                        //subtract restaurant_management's balance
                        $restaurant_management = RestaurantManagement::find($id);
                        $balance = $restaurant_management->balance;
                        $balance -= $balance_processed; 
                        //update restaurant_management balance
                        $restaurant_management->balance = $balance;
                        $restaurant_management->save();                         

                        return redirect()->action('RestaurantManagementController@getBalance')->with([
                            'info' =>'Permintaan tarik dana anda akan diproses paling lama dalam waktu 2x24 jam ke depan.'
                        ]);  
                    }
                }
                else{
                    return redirect()->back()->with([
                        'error' => 'Password yang anda masukkan salah.'
                    ]);                        
                }                
            }            
                      
        }
    }

    public function getHistoryBalance(Request $request){

        $restaurant_management =  Auth::user('restaurant_management');
        $section_title = 'Balance';

        if (!$restaurant_management){
            abort(404);
        }

        $names = array();
        foreach($restaurant_management->restaurant_banks as $restaurant_bank){
            $names[$restaurant_bank->id] = $restaurant_bank->bank->name;
        }
        $account_numbers = $restaurant_management->restaurant_banks()->lists('account_number', 'id');
        $account_names = $restaurant_management->restaurant_banks()->lists('account_name', 'id');

        $complete_info = array();
        foreach($names as $id => $name){
            $complete_info[$id] = $names[$id]."-".$account_numbers[$id]." a/n ".$account_names[$id];
        }

        $date_start = $request->date_start;
        $date_end = $request->date_end;

        if (!$date_start && !$date_end){
            return redirect('/restaurant-management/balance');
        }

        $date_start = date("Y-m-d", strtotime($request->date_start));
        $date_end = Carbon::parse(date("Y-m-d", strtotime($request->date_end)))->addDay(1);

        $banks = Bank::all()->lists('name', 'id');        

        $restaurant_banks_info = array();
        $restaurant_banks = $restaurant_management->restaurant_banks;
        for($i=0; $i<count($restaurant_banks); $i++){
            $restaurant_banks_info[$restaurant_banks[$i]->id] = $restaurant_banks[$i]->bank->name;
        }

        $history_balances_search = RestaurantHistoryBalance::whereBetween('process_date', array($date_start, $date_end))->orderBy('process_date')->paginate(6);
        return view('restaurant-management/balance', compact('restaurant_management', 'complete_info', 'history_balances_search', 'section_title', 'banks', 'restaurant_banks_info')); 
        
    }

	public function getPassword(){
        $restaurant_management = Auth::user('restaurant_management');
		$section_title = 'Password';
 		return view('restaurant-management/password', compact('restaurant_management', 'section_title'));
	}

    public function putEditPassword(Request $request, $id){
        $this->validate($request, [
            'old_password' => 'required',
            'password' => 'required|confirmed|min:6',
        ]);

        if (Hash::check($request->old_password, RestaurantManagement::find($id)->password)){
            $new_password = bcrypt($request->password);

            $command = new UpdateRestaurantManagementPasswordCommand($id, $new_password);
            $this->dispatch($command);

            return redirect()->action('RestaurantManagementController@getPassword')->with([
                'info' =>'Password anda sudah berhasil diubah.'
            ]);  
        }
        else{
            return redirect()->action('RestaurantManagementController@getPassword')->with([
                'error' => 'Password yang anda masukkan salah.'
            ]);                        
        }
    }

}
