<?php

namespace Solivis\Http\Controllers;

use Illuminate\Http\Request;
use Solivis\User;
use Solivis\Bank;
use Solivis\UserBank;
use Solivis\Reservation;
use Auth;
use Hash;
use File;

use Solivis\Http\Requests;
use Solivis\Commands\UpdateProfileCommand;
use Solivis\Commands\UpdatePasswordCommand;
use Solivis\Commands\UpdatePictureCommand;
use Solivis\Commands\CheckExistBankCommand;
use Solivis\Commands\StoreUserBankCommand;
use Solivis\Commands\UpdateUserBankCommand;
use Solivis\Http\Controllers\Controller;

class ProfileController extends Controller
{

    public function __construct(){
        $this->middleware('auth', ['except' => ['getIndex', 'getShow']]);
    }

    public function getIndex(){
        $user = Auth::user('user');
        if (!$user){
            abort(404);
        }
    	return view('user.profile.index', compact('user'));
    }

    public function getShow($id = null){
        if ($id == null){
            $user = Auth::user('user');
        }
        else{
            $user = User::find($id);
            if (!$user){
                abort(404);
            }            
        }

        $total_reviews_from_user = 0;
        for($i=0; $i<count($user->reservations); $i++){
            if($user->reservations[$i]->review_rating){
                $total_reviews_from_user++;
            }
        }

        $reservations_rated = Reservation::where('user_id', $user->id)
                                         ->has('review_rating')->paginate(3);

        return view('user.profile.index', compact('user', 'total_reviews_from_user', 'reservations_rated'));
    }

    public function getEdit($id = null){
        if ($id == null){
            $id = Auth::user('user')->id;
            return redirect()->action('ProfileController@getEdit', [$id]);             
        }        
        $user = User::find($id);
		if (!$user){
    		abort(404);
    	}

        $user_banks_info = array();

        $user_banks = $user->user_banks;
        for($i=0; $i<count($user_banks); $i++){
            $user_banks_info[$user_banks[$i]->id] = $user_banks[$i]->bank->name;
        }

        $banks = Bank::all()->lists('name', 'id');

        $total_reviews_from_user = 0;
        for($i=0; $i<count($user->reservations); $i++){
            if($user->reservations[$i]->review_rating){
                $total_reviews_from_user++;
            }
        }

		return view('user.profile.edit', compact('user', 'user_banks_info', 'banks', 'total_reviews_from_user'));
    }

    public function putEditProfile(Request $request, $id){
        
        $this->validate($request, [
            'name' => 'required|max:255',
            'phone_number' => 'required|numeric',
            'email' => 'required|email|max:255|unique:users,email,'.$id,
            'date' => 'required|numeric',
            'month' => 'required|numeric',
            'year' => 'required|numeric',
            'gender' => 'required'
        ]);

        $date = $request->date;
        $month = $request->month;
        $year = $request->year;

        $name = $request->name;
        $email = $request->email;
        $phone_number = $request->phone_number;
        $gender = $request->gender;
        $birth_date = date($year.'-'.$month.'-'.$date);

        $command = new UpdateProfileCommand($id, $name, $email, $phone_number, $gender, $birth_date);
        $this->dispatch($command);

        return redirect()->action('ProfileController@getEdit', [$id])->with([
            'info' => 'Profil anda sudah berhasil diupdate.'
        ]);        
    }

    public function putEditPassword(Request $request, $id){
        $this->validate($request, [
            'old_password' => 'required',
            'password' => 'required|confirmed|min:6',
        ]);

        //check password 
        if (Hash::check($request->old_password, User::find($id)->password)){
            $new_password = bcrypt($request->password);

            $command = new UpdatePasswordCommand($id, $new_password);
            $this->dispatch($command);

            return redirect()->action('ProfileController@getEdit', [$id])->with([
                'info' =>'Password anda sudah berhasil diubah.'
            ]);  
        }
        else{
            return redirect()->action('ProfileController@getEdit', [$id])->with([
                'error' => 'Password yang anda masukkan salah.'
            ]);                        
        }
    }

    public function putEditPicture(Request $request, $id){
        $messages = [
            'photo_url.image' => 'Gambar profil yang anda upload harus berbentuk image.',
            'photo_url.max' => 'Maksimal ukuran gambar profil yang dapat diupload adalah 1MB.'
        ];

        $this->validate($request, [
            'photo_url' => 'required|image|max:1000'
        ], $messages);

        $photo_url = $request->photo_url;

        $user = User::find($id);
        //Delete old user's image profile from directory.
        if($user->photo_url && $user->photo_url != "person-default.png"){
            File::delete(public_path('images/uploads/'.$user->photo_url));
        }

        if ($photo_url){
            //get image name
            $photo_url_filename = $photo_url->getClientOriginalName();
            // //move image to public/images/uplaods directory
            $photo_url->move(public_path('images/uploads'), $photo_url_filename);

            $command = new UpdatePictureCommand($id, $photo_url_filename);
            $this->dispatch($command);

            return redirect()->action('ProfileController@getEdit', [$id])->with([
                'info' => 'Profil gambar anda sudah berhasil diupdate.'
            ]);         
        }
    }

    public function deleteBank(Request $request){
        $user_bank_id = $request->user_bank_id_del;
        UserBank::where('id', $user_bank_id)->delete();        

        return redirect()->back()->with([
            'info' => 'Data Bank anda sudah dihapus.'
        ]);

    }

    public function postAddBank(Request $request, $id){
        $this->validate($request, [
            'account_number' => 'required|numeric',
            'account_name' => 'required',
            'password' => 'required'
        ]);        

        $bank_id = $request->bank_id;
        $account_number = $request->account_number;
        $account_name = $request->account_name;
        $password = $request->password;

        if (Hash::check($request->password, User::find($id)->password)){
            //check bank and account number exist
            $checkexistbank_command = new CheckExistBankCommand($bank_id, $account_number, $id, null);
            $result = $checkexistbank_command->checkExistUserBank();

            // create new Bank data
            if (!$result){
                $command = new StoreUserBankCommand($account_number, $account_name, $bank_id, $id);
                $this->dispatch($command);
            }
            else{
                return redirect()->back()->with([
                    'error' => 'Bank dengan nomor rekening yang sama sudah terdaftar.'
                ]);
            }

            return redirect()->action('ProfileController@getEdit', [$id])->with([
                'info' => 'Data Bank anda sudah berhasil ditambah.'
            ]); 
        }
        else{
            return redirect()->back()->with([
                'error' => 'Password yang anda masukkan salah.'
            ]);                        
        }              
    }

    public function putEditBank(Request $request, $id){
        $this->validate($request, [
            'account_number' => 'required|numeric',
            'account_name' => 'required',
            'password' => 'required'
        ]);        

        $bank_id = $request->bank_id_ori;
        $user_bank_id = $request->user_bank_id;
        $account_number = $request->account_number;
        $account_name = $request->account_name;
        $password = $request->password;

        if (Hash::check($request->password, User::find($id)->password)){   
            //check bank and account number exist
            $checkexistbank_command = new CheckExistBankCommand($bank_id, $account_number, $id, $user_bank_id);
             $result = $checkexistbank_command->checkExistUserBankForUpdate();

            // update Bank Data
            if (!$result){
                $command = new UpdateUserBankCommand($account_number, $account_name, $user_bank_id);
                $this->dispatch($command);

                return redirect()->action('ProfileController@getEdit', [$id])->with([
                    'info' => 'Data Bank anda sudah berhasil diupdate.'
                ]);             
            }
            else{
                return redirect()->back()->with([
                    'error' => 'Bank dengan nomor rekening yang sama sudah terdaftar.'
                ]);
            }                 

        }
        else{
            return redirect()->back()->with([
                'error' => 'Password yang anda masukkan salah.'
            ]);                        
        }              
    }

}
