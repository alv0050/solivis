<?php

namespace Solivis\Http\Controllers;

use Illuminate\Http\Request;

use Solivis\User;
use Solivis\RestaurantManagement;
use Solivis\Http\Requests;
use Solivis\QrCodePlain;
use Solivis\Reservation;
use Solivis\HistoryBalance;
use Solivis\RestaurantHistoryBalance;
use Solivis\UserBank;
use Solivis\ConfirmPayment;
use Solivis\Menu;
use Solivis\DeadlineResponseDate;
use Solivis\DeclinedReason;
use \Snowfire\Beautymail\Beautymail as BeautyMail;
use Solivis\Commands\GetTotalReservationPriceCommand;
use Solivis\Commands\DeleteUserCommand;
use Solivis\Commands\DeleteRestaurantManagementCommand;
use Solivis\Http\Controllers\Controller;
use DB;
use Carbon\Carbon;

class AdminController extends Controller
{

  	public function __construct(){
      $this->middleware('admin');
  	}

    public function getIndex(){
      return redirect('/admin/accepted-reservation');
    }

    public function getAcceptedReservation(){
		  $section_title = 'Accepted Reservations';

      //Get only success reservation
      $reservations = Reservation::where('status_reservation_id', 4)->get();

      return view('admin.accepted-reservation', compact('section_title', 'reservations'));
    }

    public function getUserDatabase(){
      $section_title = 'User Database';
      //get all users in DB, sort by latest login user
      $users = User::orderBy('last_login', 'desc')->paginate(10);

      return view('admin.user-database', compact('section_title', 'users'));
    }

    public function deleteUser(Request $request){
      $user_name = User::find($request->user_id)->name;

      $command = new DeleteUserCommand($request->user_id);
      $this->dispatch($command); 

      return redirect()->back()->with([
          'info' => 'User bernama '.$user_name.' telah dihapus.'
      ]);
    }

    public function getSearchUserDatabase(Request $request){
      $section_title = 'User Database';
      $search = $request->search;

      if(!$search){
          return redirect('admin/user-database');
      }

      //search id, name, email and phone_number
      $users_search = User::where('id','LIKE','%'.$search.'%')
                    ->orderBy('last_login', 'desc')
                    ->orWhere('name', 'LIKE', '%'.$search.'%')
                    ->orderBy('last_login', 'desc')
                    ->orWhere('email', 'LIKE', '%'.$search.'%')
                    ->orderBy('last_login', 'desc')
                    ->orWhere('phone_number', 'LIKE', '%'.$search.'%')
                    ->orderBy('last_login', 'desc')
                    ->paginate(10);

      return view('admin.user-database', compact('section_title', 'users_search'));
    }

    public function putFillBalance(Request $request){
      $reason_choice = $request->reason_choice;
      $user_id = $request->user_id;
      $balance_processed = $request->balance_processed;

      $user = User::find($user_id);
      $user_name = $user->name;

      HistoryBalance::create([
          'process_date' => Carbon::now(),
          'balance_processed' => $balance_processed,
          'status_balance_id' => $reason_choice,
          'user_id' => $user->id,
          'transfer_target' => 'Ke saldo solivis',
       ]);

      //add to user balance
      $user->balance += $balance_processed;
      $user->save();

      return redirect()->back()->with([
          'info' => 'User bernama '.$user_name.' telah ditambahkan saldo sebesar Rp. '.$balance_processed.'.'
      ]);
    }

    public function getRestaurantManagementDatabase(){
		  $section_title = 'Restaurant Database';
      //get all restaurant management in DB, sort by latest login user
      $restaurant_managements = RestaurantManagement::paginate(10);

      return view('admin.restaurant-database', compact('section_title', 'restaurant_managements'));
    }

    public function deleteRestaurantManagement(Request $request){
      $restaurant_management_name = RestaurantManagement::find($request->restaurant_management_id)->name;

      $command = new DeleteRestaurantManagementCommand($request->restaurant_management_id);
      $this->dispatch($command);

      return redirect()->back()->with([
          'info' => 'Restoran bernama '.$restaurant_management_name.' telah dihapus.'
      ]);
    }

    public function getSearchRestaurantManagementDatabase(Request $request){
      $section_title = 'Restaurant Database';
      $search = $request->search;

      if(!$search){
          return redirect('admin/restaurant-management-database');
      }

      //search id, name and email 
      $restaurant_managements_search = RestaurantManagement::where('id','LIKE','%'.$search.'%')
                    ->orderBy('last_login', 'desc')
                    ->orWhere('name', 'LIKE', '%'.$search.'%')
                    ->orderBy('last_login', 'desc')
                    ->orWhere('email', 'LIKE', '%'.$search.'%')
                    ->orderBy('last_login', 'desc')
                    ->paginate(10);

      return view('admin.restaurant-database', compact('section_title', 'restaurant_managements_search'));        
    }

    public function putAcceptRestaurantManagement(Request $request){

      $accepted_restaurant_management_id = $request->restaurant_management_id;
      $restaurant_management = RestaurantManagement::find($accepted_restaurant_management_id);

      //change status to member
      $restaurant_management->status = '1';

      $generated_password = $restaurant_management->generatePassword();
      $restaurant_management_name = $restaurant_management->name;
      $restaurant_management_email = $restaurant_management->email;

      $restaurant_management->password = bcrypt($generated_password);
      $restaurant_management->save();  

      // send email and password to new restaurant management
      $logo['path'] = '';
      $logo['width'] = '0px';
      $logo['height'] = '0px';
      $beautymail = app()->make(Beautymail::class);
      $beautymail->send('emails.accept_new_restaurant', 
          [
              'logo' => $logo,
              'restaurant_management_email' => $restaurant_management_email, 
              'generated_password' => $generated_password, 
          ],  function($message) use ($restaurant_management_name, $restaurant_management_email){
                  $message->to($restaurant_management_email, $restaurant_management_name)
                          ->subject('Selamat Bergabung ke Solivis');
      });       

      return redirect()->back()->with([
        "info" => "Restoran dengan ID: ".$accepted_restaurant_management_id." telah diterima menjadi member."
      ]);           
    }

    public function getSortUserDatabase(Request $request){
      $section_title = 'User Database';

      if($request->sort_user == "all"){
        $users = User::paginate(10);
      } 
      else if($request->sort_user == "id"){
        $users = User::orderBy('id')->paginate(10);
      } 
      else if($request->sort_user == "name"){
        $users = User::orderBy('name')->paginate(10);    
      }
      else if($request->sort_user == "gender"){
        $users = User::orderBy('gender')->paginate(10);
      }      
      else if($request->sort_user == "total_reservation"){
        //sort only user with success reservations
        $users = (User::with('reservations')->CountUserReservationsSuccess()->orderBy('count', 'desc')->paginate(10));
      }
      else{
        $users = User::orderBy('last_login', 'desc')->paginate(10);
      }

      return view('admin.user-database', compact('section_title', 'users'));        
    }

    public function getSortRestaurantManagementDatabase(Request $request){
      $section_title = 'Restaurant Database';

      if($request->sort_restaurant_management == "all"){
          $restaurant_managements = RestaurantManagement::paginate(10);
      }  
      else if($request->sort_restaurant_management == "id"){
          $restaurant_managements = RestaurantManagement::orderBy('id')->paginate(10);
      }      
      else if($request->sort_restaurant_management == "name"){
          $restaurant_managements = RestaurantManagement::orderBy('name')->paginate(10);
      }      
      else if($request->sort_restaurant_management == "address"){
          $restaurant_managements = RestaurantManagement::orderBy('address')->paginate(10);
      }      
      else if($request->sort_restaurant_management == "total_reservation"){
        $restaurant_managements = (RestaurantManagement::with('reservations')->CountRestaurantManagementReservationsSuccess()->orderBy('count', 'desc')->paginate(10));
      }
      else if($request->sort_restaurant_management == "status"){
        $restaurant_managements = RestaurantManagement::orderBy('status')->paginate(10);
      }
      else{
        $restaurant_managements = RestaurantManagement::orderBy('last_login', 'desc')->paginate(10);
      }

      return view('admin.restaurant-database', compact('section_title', 'restaurant_managements'));        
    }

    public function getFluxUserBalance(){
      $section_title = 'Flux User Balance';

      $history_balances = HistoryBalance::where('status_balance_id', '3')->paginate(10);

      return view('admin.flux-user-balance', compact('section_title', 'history_balances'));
    }  

    public function putFluxUserBalance(Request $request){
      $history_balance_id = $request->history_balance_id;

      $history_balance = HistoryBalance::find($history_balance_id);
      $history_balance->update([
          'process_date' => Carbon::now(),
          'status_balance_id' => '4', // Dana telah dikirim (tuntas)
      ]);     

      return redirect()->back()->with([
        "info" => "Dana untuk history balance ID: ".$history_balance_id." telah dicairkan (tuntas)."
      ]);      
    }

    public function getSortHistoryBalance(Request $request){
      $section_title = 'Flux User Balance';

      if($request->sort_history_balance == "all"){
        $history_balances = HistoryBalance::where('status_balance_id', '3')->paginate(10);
      } 
      else if($request->sort_history_balance == "id"){
        $history_balances = HistoryBalance::where('status_balance_id', '3')->orderBy('id')->paginate(10);
      } 
      else if($request->sort_history_balance == "process_date"){
        $history_balances = HistoryBalance::where('status_balance_id', '3')->orderBy('process_date')->paginate(10);    
      }
      else if($request->sort_history_balance == "balance_processed"){
        $history_balances = HistoryBalance::where('status_balance_id', '3')->orderBy('balance_processed')->paginate(10);    
      }
      else if($request->sort_history_balance == "user_id"){
        $history_balances = HistoryBalance::where('status_balance_id', '3')->orderBy('user_id')->paginate(10);
      }      
      else{
        $history_balances = HistoryBalance::where('status_balance_id', '3')->orderBy('transfer_target')->paginate(10);
      }

      return view('admin.flux-user-balance', compact('section_title', 'history_balances'));
    }

    public function getSearchHistoryBalance(Request $request){
      $section_title = 'Flux User Balance';
      $search = $request->search;

      if(!$search){
          return redirect('admin/flux-user-balance');
      }

      //search id, name, email and phone_number
      $history_balances_search = HistoryBalance::where('id','LIKE','%'.$search.'%')
                        ->where('status_balance_id', '3')
                        ->orWhere('balance_processed', 'LIKE', '%'.$search.'%')
                        ->where('status_balance_id', '3')                        
                        ->orWhere('user_id', 'LIKE', '%'.$search.'%')
                        ->where('status_balance_id', '3')
                        ->orWhere('transfer_target', 'LIKE', '%'.$search.'%')
                        ->where('status_balance_id', '3')                        
                        ->paginate(10);

      return view('admin.flux-user-balance', compact('section_title', 'history_balances_search'));
    }

    public function getFluxRestaurantBalance(){
      $section_title = 'Flux Restaurant Balance';

      $restaurant_history_balances = RestaurantHistoryBalance::where('status_balance_id', '3')->paginate(10);

      return view('admin.flux-restaurant-balance', compact('section_title', 'restaurant_history_balances'));
    }  

    public function putFluxRestaurantBalance(Request $request){
      $restaurant_history_balance_id = $request->restaurant_history_balance_id;

      $restaurant_history_balance = RestaurantHistoryBalance::find($restaurant_history_balance_id);
      $restaurant_history_balance->update([
          'process_date' => Carbon::now(),
          'status_balance_id' => '4', // Dana telah dikirim (tuntas)
      ]);     

      return redirect()->back()->with([
        "info" => "Dana untuk restaurant history balance ID: ".$restaurant_history_balance->id." telah dicairkan (tuntas)."
      ]);      
    }

    public function getSortRestaurantHistoryBalance(Request $request){
      $section_title = 'Flux Restaurant Balance';

      if($request->sort_restaurant_history_balance == "all"){
        $restaurant_history_balances = RestaurantHistoryBalance::where('status_balance_id', '3')->paginate(10);
      } 
      else if($request->sort_restaurant_history_balance == "id"){
        $restaurant_history_balances = RestaurantHistoryBalance::where('status_balance_id', '3')->orderBy('id')->paginate(10);
      } 
      else if($request->sort_restaurant_history_balance == "process_date"){
        $restaurant_history_balances = RestaurantHistoryBalance::where('status_balance_id', '3')->orderBy('process_date')->paginate(10);    
      }
      else if($request->sort_restaurant_history_balance == "balance_processed"){
        $restaurant_history_balances = RestaurantHistoryBalance::where('status_balance_id', '3')->orderBy('balance_processed')->paginate(10);    
      }
      else if($request->sort_restaurant_history_balance == "restaurant_management_id"){
        $restaurant_history_balances = RestaurantHistoryBalance::where('status_balance_id', '3')->orderBy('restaurant_management_id')->paginate(10);
      }      
      else{
        $restaurant_history_balances = RestaurantHistoryBalance::where('status_balance_id', '3')->orderBy('transfer_target')->paginate(10);
      }

      return view('admin.flux-restaurant-balance', compact('section_title', 'restaurant_history_balances'));
    }

    public function getSearchRestaurantHistoryBalance(Request $request){
      $section_title = 'Flux Restaurant Balance';
      $search = $request->search;

      if(!$search){
          return redirect('admin/flux-restaurant-balance');
      }

      //search id, name, email and phone_number
      $restaurant_history_balances_search = RestaurantHistoryBalance::where('id','LIKE','%'.$search.'%')
                        ->where('status_balance_id', '3')
                        ->orWhere('balance_processed', 'LIKE', '%'.$search.'%')
                        ->where('status_balance_id', '3')                        
                        ->orWhere('restaurant_management_id', 'LIKE', '%'.$search.'%')
                        ->where('status_balance_id', '3')
                        ->orWhere('transfer_target', 'LIKE', '%'.$search.'%')
                        ->where('status_balance_id', '3')                        
                        ->paginate(10);

      return view('admin.flux-restaurant-balance', compact('section_title', 'restaurant_history_balances_search'));
    }


    public function getVerifyPayment(){
      $section_title = 'Verify Payment';
      $confirm_payments = ConfirmPayment::WhereHas('reservation', function($reservation){
                            $reservation->where('status_reservation_id', '2');
                          })->paginate(10);

      return view('admin.verify-payment', compact('section_title', 'confirm_payments'));
    }    

    public function putVerifyPayment(Request $request){
      $reservation_id = $request->reservation_id;

      $status_reservation_id = '3'; //Telah Bayar

      $reservation = Reservation::find($reservation_id);
      //update reservation from process to has paid
      $reservation->update([
          'status_reservation_id' => $status_reservation_id,
      ]); 

      $user_name = $reservation->user->name;
      $reservation_date = $reservation->convertDate();
      $reservation_time = $reservation->convertTime();
      $paxes = $reservation->paxes;

      $reservation_menus = $reservation->reservation_menus;
      $getTotalReservationPriceCommand = new GetTotalReservationPriceCommand();        

      for($i=0; $i<count($reservation_menus); $i++){
          $getTotalReservationPriceCommand->setTotalPrice($reservation_menus[$i]);
      }

      $total_reservation_price = $getTotalReservationPriceCommand->getTotalPrice();

      $restaurant_management_name = $reservation->restaurant_management->name;
      $restaurant_management_email = $reservation->restaurant_management->email;

      //deadline for restaurant to response reservation (1 hour need to verify)
      $deadline_date = Carbon::parse(DeadlineResponseDate::where('reservation_id', $reservation->id)->first()->deadline_date)->addHour(2);
      $deadline_date_only = $reservation->convertToCommonDate($deadline_date);
      $deadline_time_only = $reservation->convertToCommonTime($deadline_date);

      $logo['path'] = '';
      $logo['width'] = '0px';
      $logo['height'] = '0px';
      $beautymail = app()->make(Beautymail::class);
      $beautymail->send('emails.new_reservation', 
          [
              'logo' => $logo,
              'reservation_id' => $reservation_id, 
              'user_name' => $user_name, 
              'reservation_date' => $reservation_date, 
              'reservation_time' => $reservation_time, 
              'paxes' => $paxes, 
              'total_reservation_price' => $total_reservation_price, 
              'reservation_menus' => $reservation_menus, 
              'deadline_date_only' => $deadline_date_only, 
              'deadline_time_only' => $deadline_time_only, 
          ],  function($message) use ($restaurant_management_name, $restaurant_management_email){
                  $message->to($restaurant_management_email, $restaurant_management_name)
                          ->subject('Reservasi Baru');
      }); 

      return redirect()->back()->with([
        "info" => "Reservasi ID: ".$reservation_id." telah dilanjutkan ke pihak restoran."
      ]);  

    }

    public function postDeclineReservation(Request $request){
        $reservation_id = $request->reservation_id;
        $reservation = Reservation::find($reservation_id);

        $reason = $request->reason;
        DeclinedReason::create([
          'reservation_id' => $reservation->id,
          'reason' => $reason
        ]);

        $status_reservation_id = '5'; //Batal
        $reservation->update([
            'status_reservation_id' => $status_reservation_id,
        ]); 

        $user_name = $reservation->user->name;
        $user_email = $reservation->user->email;

        $reservation_date = $reservation->convertDate();
        $reservation_time = $reservation->convertTime();

        $logo['path'] = '';
        $logo['width'] = '0px';
        $logo['height'] = '0px';
        $beautymail = app()->make(Beautymail::class);
        $beautymail->send('emails.cancel_reservation', 
            [
                'logo' => $logo,
                'reservation_id' => $reservation_id, 
                'reservation_date' => $reservation_date, 
                'reservation_time' => $reservation_time, 
                'user_name' => $user_name, 
                'reason' => $reason
            ],  function($message) use ($user_name, $user_email){
                    $message->to($user_email, $user_name)
                            ->subject('Reservasi ditolak oleh Admin');
        });

        return redirect()->back()->with([
        "info" => "Reservasi ID: ".$reservation_id." telah dibatalkan."
      ]);
    }

    public function getSortConfirmPayment(Request $request){
      $section_title = 'Verify Payment';

      if($request->sort_confirm_payment == "all"){
        $confirm_payments = ConfirmPayment::WhereHas('reservation', function($reservation){
                            $reservation->where('status_reservation_id', '2');
                          })->paginate(10);
      } 
      else if($request->sort_confirm_payment == "id"){
        $confirm_payments = ConfirmPayment::WhereHas('reservation', function($reservation){
                              $reservation->where('status_reservation_id', '2');
                            })->orderBy('id')->paginate(10);
      } 
      else if($request->sort_confirm_payment == "reservation_id"){
        $confirm_payments = ConfirmPayment::WhereHas('reservation', function($reservation){
                              $reservation->where('status_reservation_id', '2');
                            })->orderBy('reservation_id')->paginate(10);    
      }
      else if($request->sort_confirm_payment == "balance_processed"){
        $confirm_payments = (ConfirmPayment::with('reservation')->CountTotalPayment()->orderBy('total_payment')->paginate(10));
      }
      else if($request->sort_confirm_payment == "transfer_from_bank"){
        $confirm_payments = ConfirmPayment::WhereHas('reservation', function($reservation){
                              $reservation->where('status_reservation_id', '2');
                            })->orderBy('transfer_from_bank')->paginate(10);
      }      
      else{
        $confirm_payments = ConfirmPayment::WhereHas('reservation', function($reservation){
                              $reservation->where('status_reservation_id', '2');
                            })->orderBy('transfer_to_bank')->paginate(10);
      }

      return view('admin.verify-payment', compact('section_title', 'confirm_payments'));
    }

    public function getSearchConfirmPayment(Request $request){
      $section_title = 'Verify Payment';
      $search = $request->search;

      if(!$search){
          return redirect('admin/verify-payment');
      }

      $confirm_payments_search = ConfirmPayment::where('id','LIKE','%'.$search.'%')
                                ->WhereHas('reservation', function($reservation){
                                  $reservation->where('status_reservation_id', '2');
                                })
                                ->orWhere('reservation_id', 'LIKE', '%'.$search.'%')
                                ->WhereHas('reservation', function($reservation){
                                  $reservation->where('status_reservation_id', '2');
                                })
                                ->orWhere('transfer_from_bank', 'LIKE', '%'.$search.'%')
                                ->WhereHas('reservation', function($reservation){
                                  $reservation->where('status_reservation_id', '2');
                                })
                                ->orWhere('transfer_to_bank', 'LIKE', '%'.$search.'%')
                                ->WhereHas('reservation', function($reservation){
                                  $reservation->where('status_reservation_id', '2');
                                })                             
                                ->paginate(10);

      return view('admin.verify-payment', compact('section_title', 'confirm_payments_search'));
    }

    public function getIncome(Request $request){
      $section_title = 'Income';

      $admin_income = 0;
      $get_reservations = Reservation::where('status_reservation_id', '4')->get();

      for($i=0; $i<count($get_reservations); $i++){
          $admin_income += $get_reservations[$i]->getAdminIncome();
      }

      $reservations = Reservation::where('status_reservation_id', 4)
                                 ->orderBy('reservation_date')
                                 ->paginate(10);  

      return view('admin.income', compact('section_title', 'admin_income', 'reservations'));
    }

    public function getSortReservation(Request $request){
      $section_title = 'Income';

      $admin_income = 0;
      $get_reservations = Reservation::where('status_reservation_id', '4')->get();

      for($i=0; $i<count($get_reservations); $i++){
          $admin_income += $get_reservations[$i]->getAdminIncome();
      }

      if($request->sort_reservation == "all"){
        $reservations = Reservation::where('status_reservation_id', '4')->paginate(10);
      } 
      else if($request->sort_reservation == "id"){
        $reservations = Reservation::where('status_reservation_id', '4')
                        ->orderBy('id')->paginate(10);
      } 
      else if($request->sort_reservation == "reservation_date"){
        $reservations = Reservation::where('status_reservation_id', '4')
                        ->orderBy('reservation_date')->paginate(10);
      }
      else if($request->sort_reservation == "status"){
        $reservations = Reservation::where('status_reservation_id', '4')
                      ->JoinWithQrCodePlain()->orderBy('status')
                      ->paginate(10); 
      }
      else{
        $reservations = Reservation::where('status_reservation_id', '4')
                      ->CountUserPayment()->orderBy('total_payment')
                      ->paginate(10);        
      }

      return view('admin.income', compact('section_title', 'reservations', 'admin_income'));
    }    

    public function getSearchReservation(Request $request){
      $section_title = 'Income';
      $search = $request->search;

      $admin_income = 0;
      $get_reservations = Reservation::where('status_reservation_id', '4')->get();

      for($i=0; $i<count($get_reservations); $i++){
          $admin_income += $get_reservations[$i]->getAdminIncome();
      }      

      if(!$search){
          return redirect('admin/income');
      }

      $reservations_search = Reservation::where('id','LIKE','%'.$search.'%')
                              ->where('status_reservation_id', '4')
                              ->paginate(10);

      return view('admin.income', compact('section_title', 'reservations_search', 'admin_income'));
    }

}
