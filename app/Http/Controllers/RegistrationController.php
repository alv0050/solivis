<?php

namespace Solivis\Http\Controllers;

use Solivis\Http\Controllers\Controller;
use Solivis\User;
use Auth;

class RegistrationController extends Controller
{
    public function confirm($confirmation_code)
    {
        if(!$confirmation_code)
        {
            throw new InvalidConfirmationCodeException;
        }

        $user = User::where('confirmation_code', $confirmation_code)->first();

        if (!$user)
        {
            throw new InvalidConfirmationCodeException;
        }

        $user->confirmed = 1;
        $user->confirmation_code = null;
        $user->save();

        //immediately login after confirmation code is matched
        Auth::loginUsingId($user->id);

        return redirect('/')->with([
            "info" => "Sukses! Email anda sudah berhasil diverifikasi."
        ]);
    }    
}
