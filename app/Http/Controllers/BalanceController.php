<?php

namespace Solivis\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Hash;
use Solivis\Bank;
use Solivis\HistoryBalance;
use Solivis\User;
use Carbon\Carbon;
use Solivis\Http\Requests;
use Solivis\Http\Controllers\Controller;
use Solivis\Commands\StoreHistoryBalanceCommand;

class BalanceController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function getIndex(){
        $user =  Auth::user('user');
        
        if (!$user){
            abort(404);
        }

        $names = array();
        foreach($user->user_banks as $user_bank){
            $names[$user_bank->id] = $user_bank->bank->name;
        }

        $account_numbers = $user->user_banks()->lists('account_number', 'id');
        $account_names = $user->user_banks()->lists('account_name', 'id');

        $complete_info = array();
        foreach($names as $id => $name){
            $complete_info[$id] = $names[$id]."-".$account_numbers[$id]." a/n ".$account_names[$id];
        }

        $history_balances = $user->history_balances()->orderBy('process_date', 'desc')->paginate(12);

    	return view('user.balance.index', compact('user', 'complete_info','history_balances'));
    }

    public function postWithdraw(Request $request, $id){

        $this->validate($request, [
            'balance_processed' => 'required|numeric',
            'user_bank_id' => 'required|numeric',
            'password' => 'required'
        ]);

       //if user had unprocessed balance withdraw, get the latest withdraw
        $history_balance = HistoryBalance::where('user_id', $id)
                           ->where('status_balance_id', 3)
                           ->orderBy('process_date', 'desc')
                           ->first();

        if (!$history_balance){
            if (Hash::check($request->password, User::find($id)->password)){

                //balance to be withdraw more than user has
                if ($request->balance_processed > User::find($id)->balance){
                    return redirect()->back()->with([
                        'error' => 'Jumlah penarikan yang anda minta melebihi jumlah saldo anda.'
                    ]);                     
                }
                else{
                    $balance_processed = $request->balance_processed;
                    $user_bank_id = $request->user_bank_id;

                    $command = new StoreHistoryBalanceCommand($id, $balance_processed, $user_bank_id);
                    $this->dispatch($command);

                    //subtract user's balance
                    $user = User::find($id);
                    $balance = $user->balance;
                    $balance -= $balance_processed; 
                    //update user balance
                    $user->balance = $balance;
                    $user->save();                    

                    return redirect()->action('BalanceController@getIndex')->with([
                        'info' =>'Permintaan tarik dana anda akan diproses paling lama dalam waktu 2x24 jam ke depan.'
                    ]);  
                }
            }
            else{
                return redirect()->back()->with([
                    'error' => 'Password yang anda masukkan salah.'
                ]);                        
            }
        }
        else{
            // if the last withdraw balance is today
            if(Carbon::parse($history_balance->process_date)->toDateString() == Carbon::now()->toDateString()){
                return redirect()->back()->with([
                    'error' => 'Mohon maaf :( Permintaan tarik dana hanya dapat dilakukan 1 kali dalam sehari. Silahkan coba kembali besok.'
                ]);                                  
            }
            else{              
                if (Hash::check($request->password, User::find($id)->password)){
                    //balance to be withdraw more than user has
                    if ($request->balance_processed > User::find($id)->balance){
                        return redirect()->back()->with([
                            'error' => 'Jumlah penarikan yang anda minta melebihi jumlah saldo anda.'
                        ]);                     
                    }
                    else{
                        $balance_processed = $request->balance_processed;
                        $user_bank_id = $request->user_bank_id;

                        $command = new StoreHistoryBalanceCommand($id, $balance_processed, $user_bank_id);
                        $this->dispatch($command);

                        //subtract user's balance
                        $user = User::find($id);
                        $balance = $user->balance;
                        $balance -= $balance_processed; 
                        //update user balance
                        $user->balance = $balance;
                        $user->save();                    

                        return redirect()->action('BalanceController@getIndex')->with([
                            'info' =>'Permintaan tarik dana anda akan diproses paling lama dalam waktu 2x24 jam ke depan.'
                        ]);  
                    }
                }
                else{
                    return redirect()->back()->with([
                        'error' => 'Password yang anda masukkan salah.'
                    ]);                        
                }             
            }
        }
    }

    public function getHistoryBalance(Request $request){
        $user =  Auth::user('user');
        
        if (!$user){
            abort(404);
        }

        $names = array();
        foreach($user->user_banks as $user_bank){
            $names[$user_bank->id] = $user_bank->bank->name;
        }

        $account_numbers = $user->user_banks()->lists('account_number', 'id');
        $account_names = $user->user_banks()->lists('account_name', 'id');

        $complete_info = array();
        foreach($names as $id => $name){
            $complete_info[$id] = $names[$id]."-".$account_numbers[$id]." a/n ".$account_names[$id];
        }

        $date_start = $request->date_start;
        $date_end = $request->date_end;

        if (!$date_start && !$date_end){
            return redirect('/balance');
        }

        $date_start = date("Y-m-d", strtotime($request->date_start));
        $date_end = Carbon::parse(date("Y-m-d", strtotime($request->date_end)))->addDay(1);
        
        $history_balances_search = HistoryBalance::whereBetween('process_date', array($date_start, $date_end))->orderBy('process_date')->paginate(12);
        return view('user.balance.index', compact('user', 'complete_info', 'history_balances_search'));    
    }
}
