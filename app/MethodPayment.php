<?php

namespace Solivis;

use Illuminate\Database\Eloquent\Model;

class MethodPayment extends Model
{
   	protected $table = 'method_payments';

   	protected $fillable = ['method'];

   	protected $hidden = [];

	public function reservation(){ 
		return $this->hasOne('Solivis\Reservation');
	}
}
