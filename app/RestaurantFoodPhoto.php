<?php

namespace Solivis;

use Illuminate\Database\Eloquent\Model;

class RestaurantFoodPhoto extends Model
{
    protected $table = 'restaurant_food_photos';
    
    protected $fillable = ['food_photo_url', 'food_photo_name', 'restaurant_management_id'];

    protected $hidden = [];

	public function restaurant_management(){ 
		return $this->belongsTo('Solivis\RestaurantManagement');
	}

}
