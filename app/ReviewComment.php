<?php

namespace Solivis;

use Illuminate\Database\Eloquent\Model;

class ReviewComment extends Model
{
    protected $table = 'review_comments';
    
    protected $fillable = ['comment', 'reservation_id'];

    protected $hidden = [];

	public function reservation(){ 
		return $this->belongsTo('Solivis\Reservation');
	}
}
