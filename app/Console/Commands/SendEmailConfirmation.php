<?php

namespace Solivis\Console\Commands;

use Carbon\Carbon;
use Log;
use Solivis\Reservation;
use Solivis\ExpiredComingConfirmation;
use Snowfire\Beautymail\Beautymail;
use Illuminate\Console\Command;

class SendEmailConfirmation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reservation:send_email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email confirmation';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //sending email coming confirmation to user
        $coming_reservations = Reservation::where('status_reservation_id', '4')
                                ->where('reservation_date', '>=', Carbon::now())->get();
        foreach($coming_reservations as $coming_reservation){
            if(Carbon::parse($coming_reservation->reservation_date)->toDateString() == Carbon::now()->toDateString()){
                $sending_time = Carbon::parse($coming_reservation->reservation_date)->subHour(3);
                if(Carbon::now()->hour == $sending_time->hour){
                    $reservation_id = $coming_reservation->id;
                    $user_name = $coming_reservation->user->name;
                    $user_email = $coming_reservation->user->email;
                    $reservation_date = $coming_reservation->convertDate();
                    $reservation_time = $coming_reservation->convertTime();
                    $paxes = $coming_reservation->paxes;
                    $expired_confirmation = ExpiredComingConfirmation::where('reservation_id', $reservation_id)->first();
                    if($expired_confirmation->status_sent == 0){ //has not sent email before
                        $expired_confirmation->update([
                            'status_sent' => '1'
                        ]);
                        $ever_delayed_before = "";

                        //remove changing reservation schedule feature 
                        if($coming_reservation->status_arrival_id == 3){
                            $ever_delayed_before = true;
                        }
                        else{
                            $ever_delayed_before = false;
                        }

                        $expired_confirmation_date = $coming_reservation->convertToCommonDate($expired_confirmation->expired_date);
                        $expired_confirmation_time = $coming_reservation->convertToCommonTime($expired_confirmation->expired_date);

                        $logo['path'] = '';
                        $logo['width'] = '0px';
                        $logo['height'] = '0px';
                        $beautymail = app()->make(Beautymail::class);
                        $beautymail->send('emails.coming_confirmation', 
                            [
                                'logo' => $logo,
                                'reservation_id' => $reservation_id, 
                                'reservation_date' => $reservation_date, 
                                'reservation_time' => $reservation_time, 
                                'paxes' => $paxes,                       
                                'expired_confirmation_date' => $expired_confirmation_date, 
                                'expired_confirmation_time' => $expired_confirmation_time,                               
                                'ever_delayed_before' => $ever_delayed_before,                               
                            ],  function($message) use ($user_name, $user_email){
                                    $message->to($user_email, $user_name)
                                            ->subject('Konfirmasi Kedatangan');
                        });
                        Log::info("Kirim email konfirmasi kedatangan ke reservasi ID : ".$coming_reservation->id);
                    }
                }
            }
        }               
    }
}
