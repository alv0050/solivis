<?php

namespace Solivis\Console\Commands;

use Log;
use Snowfire\Beautymail\Beautymail;
use Solivis\Reservation;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CheckUserComingOverDeadline extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reservation:checkdeadlinecoming';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check user coming over deadline';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $reservations = Reservation::where('status_reservation_id', '4')->get();
        foreach($reservations as $reservation){
            if($reservation->qr_code_plain && $reservation->qr_code_plain->status_processed == 0 && $reservation->expired_coming_confirmation->status_sent == '1'){
                if(Carbon::parse($reservation->reservation_date)->diffInMinutes(Carbon::now(), false) > 20){ // false is to return minus (if current time has not passed reservation time over 20 minutes)
                    Log::info(Carbon::now()->diffInMinutes(Carbon::parse($reservation->reservation_date)));
                    Log::info(Carbon::parse($reservation->reservation_date)->diffInMinutes(Carbon::now(), false));
                    Log::info(Carbon::parse($reservation->reservation_date));
                    Log::info(Carbon::now());
                    // change qr code to already been processed
                    $reservation->qr_code_plain->status_processed = 1;
                    $reservation->qr_code_plain->save();

                    // add to restaurant balance
                    $total_net_income = $reservation->getTotalNetIncome();
                    $restaurant_management = $reservation->restaurant_management;
                    $restaurant_management->balance += $total_net_income;
                    $restaurant_management->save();

                    $restaurant_management_name = $reservation->restaurant_management->name;
                    $restaurant_management_email = $reservation->restaurant_management->email;
                    $reservation_id = $reservation->id;
                    $reservation_date = $reservation->convertDate();
                    $reservation_time = $reservation->convertTime();
                    $user_name = $reservation->user->name;
                    $user_email = $reservation->user->email;
                    $total_reservation_price = $reservation->getTotalReservationPrice();

                    //send email to user
                    $logo['path'] = '';
                    $logo['width'] = '0px';
                    $logo['height'] = '0px';
                    $beautymail = app()->make(Beautymail::class);
                    $beautymail->send('emails.coming_over_deadline_to_user', 
                        [
                            'logo' => $logo,
                            'reservation_id' => $reservation_id, 
                            'reservation_date' => $reservation_date, 
                            'reservation_time' => $reservation_time, 
                        ],  function($message) use ($user_name, $user_email){
                                $message->to($user_email, $user_name)
                                        ->subject('Reservasi konsumen sudah hangus');
                    });

                    //send email to restaurant management
                    $beautymail->send('emails.coming_over_deadline_to_restaurant_management', 
                        [
                            'logo' => $logo,
                            'user_name' => $user_name, 
                            'reservation_id' => $reservation_id, 
                            'reservation_date' => $reservation_date, 
                            'reservation_time' => $reservation_time, 
                            'total_net_income' => $total_net_income,
                        ],  function($message) use ($restaurant_management_name, $restaurant_management_email){
                                $message->to($restaurant_management_email, $restaurant_management_name)
                                        ->subject('Reservasi konsumen sudah hangus');
                    });                    

                    Log::info("Reservasi ID : ".$reservation->id." dinyatakan hangus karena konsumen sudah terlambat lebih 20 menit dari waktu reservasi.");
                }
                else{
                    // Log::info(Carbon::parse($reservation->reservation_date)->diffInMinutes(Carbon::now(), false));
                }
            }
        }
    }
}
