<?php

namespace Solivis\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use Log;
use Solivis\Reservation;
use Solivis\DeclinedReason;
use Solivis\HistoryBalance;
use Snowfire\Beautymail\Beautymail;

class DeleteReservationOverDeadline extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reservation:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete reservation over deadline';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // only unprocessed reservation
        $unresponsed_reservations = Reservation::has('deadline_response_date')
                                    ->where('status_reservation_id', '1')
                                    ->get();

        foreach($unresponsed_reservations as $unresponsed_reservation){
            if(Carbon::now()->gt(Carbon::parse($unresponsed_reservation->deadline_response_date->deadline_date))){
                $unresponsed_reservation->update([
                    'status_reservation_id' => '5' //batal
                ]);

                $reason = "Reservasi anda telah jatuh tempo";
                DeclinedReason::create([
                    'reservation_id' => $unresponsed_reservation->id,
                    'reason' => $reason
                ]);   

                Log::info("Reservasi ID : ".$unresponsed_reservation->id." dibatalkan karena jatuh tempo");
            }
        }

        // has paid reservation but not responded by restaurant
        $waiting_restaurant_management_confirm_reservations = Reservation::has('deadline_response_date')
                                                                ->where('status_reservation_id', '3')
                                                                ->get();

        foreach($waiting_restaurant_management_confirm_reservations as $waiting_restaurant_management_confirm_reservation){
            //transfer bank
            if($waiting_restaurant_management_confirm_reservation->method_payment_id == 1){
                //add two hour because one hour before is used by admin verify 
                $deadline_response_date_restaurant = Carbon::parse($waiting_restaurant_management_confirm_reservation->deadline_response_date->deadline_date)->addHour(2);
            }
            else{
                $deadline_response_date_restaurant = Carbon::parse($waiting_restaurant_management_confirm_reservation->deadline_response_date->deadline_date)->addHour(1);
            }

            if(Carbon::now()->gt($deadline_response_date_restaurant)){
                $waiting_restaurant_management_confirm_reservation->update([
                    'status_reservation_id' => '5', //batal
                ]); 

                $reason = "Restoran tidak menanggapi reservasi anda";
                DeclinedReason::create([
                    'reservation_id' => $waiting_restaurant_management_confirm_reservation->id,
                    'reason' => $reason
                ]);

                $reservation_id = $waiting_restaurant_management_confirm_reservation->id;
                $user_name = $waiting_restaurant_management_confirm_reservation->user->name;
                $user_email = $waiting_restaurant_management_confirm_reservation->user->email;
                $reservation_date = $waiting_restaurant_management_confirm_reservation->convertDate();
                $reservation_time = $waiting_restaurant_management_confirm_reservation->convertTime();
                $paxes = $waiting_restaurant_management_confirm_reservation->paxes;
                $total_reservation_price = $waiting_restaurant_management_confirm_reservation->getTotalReservationPrice();

                $logo['path'] = '';
                $logo['width'] = '0px';
                $logo['height'] = '0px';
                $beautymail = app()->make(Beautymail::class);
                $beautymail->send('emails.not_responded_by_restaurant', 
                    [
                        'logo' => $logo,
                        'reservation_id' => $reservation_id, 
                        'reservation_date' => $reservation_date, 
                        'reservation_time' => $reservation_time, 
                        'paxes' => $paxes,                         
                        'total_reservation_price' => $total_reservation_price,
                    ],  function($message) use ($user_name, $user_email){
                            $message->to($user_email, $user_name)
                                    ->subject('Reservasi tidak direspon oleh pihak restoran');
                });

                // 6. Reservasi tidak direspon (dana dikembalikan)
                HistoryBalance::create([
                    'process_date' => Carbon::now(),
                    'balance_processed' => $total_reservation_price,
                    'status_balance_id' => 6,
                    'user_id' => $waiting_restaurant_management_confirm_reservation->user->id,
                    'transfer_target' => 'Ke saldo solivis',
                 ]);

                //add to user balance
                $user = $waiting_restaurant_management_confirm_reservation->user;
                $user->balance += $total_reservation_price;
                $user->save();   

                Log::info("Reservasi ID : ".$waiting_restaurant_management_confirm_reservation->id." dibatalkan karena tidak direspon oleh pihak restoran");                             
            }
        }          
    }
}
