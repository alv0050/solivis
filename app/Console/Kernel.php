<?php

namespace Solivis\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \Solivis\Console\Commands\Inspire::class,
        \Solivis\Console\Commands\DeleteReservationOverDeadline::class,
        \Solivis\Console\Commands\SendEmailConfirmation::class,
        \Solivis\Console\Commands\CheckUserComingOverDeadline::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
        $schedule->command('reservation:delete')->everyMinute(); //run this command every minute
        $schedule->command('reservation:send_email')->everyMinute(); 
        $schedule->command('reservation:checkdeadlinecoming')->everyMinute(); 
    }
}
