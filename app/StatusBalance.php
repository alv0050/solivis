<?php

namespace Solivis;

use Illuminate\Database\Eloquent\Model;

class StatusBalance extends Model
{
   	protected $table = 'status_balances';

   	protected $primaryKey = 'id';

   	protected $fillable = ['status'];

   	protected $hidden = [];

	public function history_balance(){ 
		return $this->hasOne('Solivis\HistoryBalance');
	}

	public function restaurant_history_balance(){ 
		return $this->hasOne('Solivis\RestaurantHistoryBalance');
	}

}
