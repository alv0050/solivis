<?php

namespace Solivis;

use Illuminate\Database\Eloquent\Model;
use Solivis\Menu;
use Solivis\Commands\GetTotalReservationPriceCommand;

class Reservation extends Model
{
    protected $table = 'reservations';

    protected $fillable = ['paxes', 'reservation_date', 'user_id', 'restaurant_management_id', 'status_reservation_id', 'method_payment_id', 'status_arrival_id'];

    protected $hidden = [];

	public function user(){ 
		return $this->belongsTo('Solivis\User');
	}

	public function restaurant_management(){ 
		return $this->belongsTo('Solivis\RestaurantManagement');
	}

	public function reservation_menus(){
		return $this->hasMany('Solivis\ReservationMenu');		
	}

    public function convertDate(){
        return date('d M, Y', strtotime($this->reservation_date));
    }

    public function convertNumericHypenDate($date){
        return date('d-m-Y', strtotime($date));
    }

    public function convertNumericDate(){
        return date('d n Y', strtotime($this->reservation_date));
    }

    public function convertReservationDate(){
        return date('d M Y', strtotime($this->reservation_date));
    }

    public function convertFullReservationDate(){
        return date('l, d M Y', strtotime($this->reservation_date));
    }

    public function convertTime(){
        return date('H:i', strtotime($this->reservation_date));     
    }

    public function convertToCommonDate($date){
        return date('d M, Y', strtotime($date));
    }

	public function convertToCommonTime($date){
		return date('H:i', strtotime($date));		
	}

	public function status_reservation(){
		return $this->belongsTo('Solivis\StatusReservation');
	}

    public function method_payment(){
        return $this->belongsTo('Solivis\MethodPayment');        
    }

	public function confirm_payment(){
		return $this->hasOne('Solivis\ConfirmPayment');		
	}

    public function declined_reason(){
        return $this->hasOne('Solivis\DeclinedReason');        
    }

    public function qr_code_plain(){
        return $this->hasOne('Solivis\QrCodePlain');        
    }

    public function status_arrival(){
        return $this->belongsTo('Solivis\StatusArrival');
    }

    public function review_rating(){
        return $this->hasOne('Solivis\ReviewRating');        
    }

    public function review_comment(){
        return $this->hasOne('Solivis\ReviewComment');        
    }

    public function deadline_response_date(){
        return $this->hasOne('Solivis\DeadlineResponseDate');        
    }

    public function expired_coming_confirmation(){
        return $this->hasOne('Solivis\ExpiredComingConfirmation');        
    }

    public function getTotalReservationPrice(){
        $getTotalReservationPriceCommand = new GetTotalReservationPriceCommand();       

        $reservation_menus = $this->reservation_menus;

        for($i=0; $i<count($reservation_menus); $i++){
            $getTotalReservationPriceCommand->setTotalPrice($reservation_menus[$i]);
        }

        return $getTotalReservationPriceCommand->getTotalPrice();

    }

	public function getTotalNetIncome(){
        $getTotalReservationPriceCommand = new GetTotalReservationPriceCommand();		

		$reservation_menus = $this->reservation_menus;

        for($i=0; $i<count($reservation_menus); $i++){
            $getTotalReservationPriceCommand->setTotalPrice($reservation_menus[$i]);
        }

        $net_income = $getTotalReservationPriceCommand->getTotalPrice() - ($getTotalReservationPriceCommand->getTotalPrice() * 0.02);

        return $net_income;
	}

    public function getAdminIncome(){
        $getTotalReservationPriceCommand = new GetTotalReservationPriceCommand();       

        $reservation_menus = $this->reservation_menus;

        for($i=0; $i<count($reservation_menus); $i++){
            $getTotalReservationPriceCommand->setTotalPrice($reservation_menus[$i]);
        }

        $admin_income = $getTotalReservationPriceCommand->getTotalPrice() * 0.02;

        return $admin_income;
    }

    public function getFoodNames(){
    	$food_names = "";
    	for($i=0; $i<count($this->reservation_menus); $i++){
    		$food_names .= $this->reservation_menus[$i]->menu_name.',';
    	}
    	return $food_names;

    }

    public function getFoodCounts(){
    	$food_count = "";
    	for($i=0; $i<count($this->reservation_menus); $i++){
    		$food_count .= $this->reservation_menus[$i]->item_count.',';
    	}
    	return $food_count;

    }

    public function getFoodPrices(){
    	$food_prices = "";
    	for($i=0; $i<count($this->reservation_menus); $i++){
    		$food_prices .= ($this->reservation_menus[$i]->item_count * $this->reservation_menus[$i]->menu_base_price).',';
    	}
    	return $food_prices;
    }

    public function generateQrCodePlain() {
        $length = 10;
        $str = "";
        $characters = array_merge(range('a','z'), range('0','9'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        return $str;
    }

    public function checkUniqueQrCodePlain($code){
        $all_qr_code_plains = QrCodePlain::where('code', $code)->first();
        if(!$all_qr_code_plains){
            return true;
        }
        else{
            return false;
        }
    }

    // Join table to get relationship count
    public function scopeCountUserPayment($query){
        return $query->join('reservation_menus','reservation_menus.reservation_id','=','reservations.id')
            ->selectRaw('reservations.*, sum(reservation_menus.item_count * reservation_menus.menu_base_price) as total_payment')->where('reservations.status_reservation_id','=','4')->groupBy('reservations.id');
    }

    public function scopeJoinWithQrCodePlain($query){
        return $query->join('qr_code_plains','qr_code_plains.reservation_id','=','reservations.id')
            ->selectRaw('reservations.*, qr_code_plains.status_processed as status')->groupBy('reservations.id');
    }

}
