<?php

namespace Solivis;

use Illuminate\Database\Eloquent\Model;

class DeclinedReason extends Model
{
   	protected $table = 'declined_reasons';

   	protected $fillable = ['reservation_id', 'reason'];

   	protected $hidden = [];

	public function reservation(){ 
		return $this->belongsTo('Solivis\Reservation');
	}
}
