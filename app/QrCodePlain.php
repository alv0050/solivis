<?php

namespace Solivis;

use Illuminate\Database\Eloquent\Model;

class QrCodePlain extends Model
{
    protected $table = 'qr_code_plains';
    
    protected $fillable = ['code', 'reservation_id', 'status_processed'];

    protected $hidden = [];

	public function reservation(){ 
		return $this->belongsTo('Solivis\Reservation');
	}

}
