<?php

namespace Solivis;

use Illuminate\Database\Eloquent\Model;

class HistoryBalance extends Model
{
    protected $table = 'history_balances';
    
    protected $fillable = ['process_date', 'balance_processed', 'status_balance_id', 'user_id', 'transfer_target'];

    protected $hidden = [];

	public function user(){ 
		return $this->belongsTo('Solivis\User');
	}

	public function status_balance(){
		return $this->belongsTo('Solivis\StatusBalance', 'status_balance_id');
	}

    public function getOnlyDate($date_time){
        return date('d-m-Y', strtotime($date_time));
    }


}
