<?php

namespace Solivis;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $table = 'banks';
    
    protected $fillable = ['name'];

    protected $hidden = [];

	public function user_bank(){
		return $this->hasOne('Solivis\UserBank');
	}

	public function restaurant_bank(){
		return $this->hasOne('Solivis\RestaurantBank');
	}

	public function admin_bank(){
		return $this->hasOne('Solivis\AdminBank');
	}
}
