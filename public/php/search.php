<?php

$dbhost = "localhost";
$dbname = "solivis";
$dbuser = "homestead";
$dbpass = "secret";

global $data_solivis;

$data_solivis = new mysqli();
$data_solivis->connect($dbhost, $dbuser, $dbpass, $dbname);
$data_solivis->set_charset("utf8");

$no_result_location = false;
$no_result_cuisine = false;
$no_result_restaurant = false;

if ($data_solivis->connect_errno) {
    printf("Connect failed: %sn", $data_solivis->connect_error);
    exit();
}

$html_location = '';
$html_location .= '<li class="result_location">';
$html_location .= '<a href="urlString">';
$html_location .= '<h4>nameString</h4>';
$html_location .= '</a>';
$html_location .= '</li>';

$html_cuisine = '';
$html_cuisine .= '<li class="result_cuisine">';
$html_cuisine .= '<a href="urlString">';
$html_cuisine .= '<h4>nameString</h4>';
$html_cuisine .= '</a>';
$html_cuisine .= '</li>';

$html_restaurant = '';
$html_restaurant .= '<li class="result_restaurant">';
$html_restaurant .= '<a href="urlString">';
$html_restaurant .= '<h4>nameString</h4>';
$html_restaurant .= '</a>';
$html_restaurant .= '</li>';

$html_notfound = '';
$html_notfound = '<li class="result_notfound">nameString</li>';

//except alphanumeric character, others will be replaced by space
$search_string = preg_replace("/[^A-Za-z0-9]/", " ", $_POST['query']);
//prevent sql injection by escape the string
$search_string = $data_solivis->real_escape_string($search_string);

if (strlen($search_string) >= 1 && $search_string !== ' ') {
	$query_location = 'SELECT * FROM restaurant_managements WHERE status = "1" AND address LIKE "%'.$search_string.'%"';
	$query_cuisine = 'SELECT * FROM cuisines WHERE name LIKE "%'.$search_string.'%"';
	$query_restaurant = 'SELECT * FROM restaurant_managements WHERE status = "1" AND name LIKE "%'.$search_string.'%"';

	$result_location = $data_solivis->query($query_location);
	$result_cuisine = $data_solivis->query($query_cuisine);
	$result_restaurant = $data_solivis->query($query_restaurant);

	//fetch data and saved in array
	while($results_location = $result_location->fetch_array()){
	    $result_location_array[] = $results_location;		
	}
	while($results_cuisine = $result_cuisine->fetch_array()) {
	    $result_cuisine_array[] = $results_cuisine;
	}
	while($results_restaurant = $result_restaurant->fetch_array()){
	    $result_restaurant_array[] = $results_restaurant;		
	}

	if (isset($result_location_array)){
		foreach ($result_location_array as $result) {
			$display_name = preg_replace("/".$search_string."/i", "<b>".$search_string."</b>", $result['address']);
			$display_url = 'http://solivis.com/search?address='.urlencode($result['address']);
			$output = str_replace('nameString', $display_name, $html_location);
			$output = str_replace('urlString', $display_url, $output);			
			echo $output;
	    }		
	}
	else{
		$no_result_location = true;
	}

	if (isset($result_cuisine_array)){
		foreach ($result_cuisine_array as $result) {
			$display_name = preg_replace("/".$search_string."/i", "<b>".$search_string."</b>", $result['name']);
			$display_url = 'http://solivis.com/search?cuisine='.urlencode($result['name']);
			$output = str_replace('nameString', $display_name, $html_cuisine);
			$output = str_replace('urlString', $display_url, $output);			
			echo $output;
	    }		
	}
	else{
		$no_result_cuisine = true;
	}

	if (isset($result_restaurant_array)){
		foreach ($result_restaurant_array as $result) {
			$display_name = preg_replace("/".$search_string."/i", "<b>".$search_string."</b>", $result['name']);
			$display_url = 'http://solivis.com/search-for-restaurant-direct?restaurant_name='.urlencode($result['name']);
			$output = str_replace('nameString', $display_name, $html_restaurant);
			$output = str_replace('urlString', $display_url, $output);			
			echo $output;
	    }		
	}
	else{
		$no_result_restaurant = true;
	}

	//no data to display
	if ($no_result_location && $no_result_cuisine && $no_result_restaurant){
		$output = str_replace('nameString', 'Data tidak ketemu...', $html_notfound);
		echo $output;
	}
}