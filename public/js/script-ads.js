$(document).ready(function(){
// SCROLLING
$('html').niceScroll({cursorcolor:" #959595"});
$('.notification-group-container').niceScroll({cursorcolor:" #959595"});
// END: SCROLLING

// DETAIL RESERVATION
$('.btn-detail').click(function(e){
	if ($('.detail-reservation').css('display') == 'block'){
		$('.detail-reservation .qrcode-container .thumbnail').children().remove();		
		$('.detail-reservation').fadeOut('fast');
	}
	else{
		var reservation_id_detail = $(this).children('.reservation-id-detail').html();
		var reservation_user_name_detail = $(this).children('.reservation-user-name-detail').html();
		var reservation_restaurant_name_detail = $( this).children('.reservation-restaurant-name-detail').html();
		var reservation_date_detail = $(this).children('.reservation-date-detail').html();
		var reservation_time_detail = $(this).children('.reservation-time-detail').html();
		var reservation_paxes_detail = $(this).children('.reservation-paxes-detail').html();
		var reservation_total_price_detail = $(this).children('.reservation-total-price-detail').html();
		var reservation_method_payment_detail = $(this).children('.reservation-method-payment-detail').html();
		var reservation_qr_code_plain_detail = $(this).children('.reservation-qr-code-plain-detail').html();
		var reservation_qr_code_detail = $(this).children('.reservation-qr-code-detail').children();

		$('.detail-reservation .reservation_id_detail').html(reservation_id_detail);
		$('.detail-reservation .reservation_user_name_detail').html(reservation_user_name_detail);
		$('.detail-reservation .reservation_restaurant_name_detail').html(reservation_restaurant_name_detail);
		$('.detail-reservation .reservation_date_detail').html(reservation_date_detail);
		$('.detail-reservation .reservation_time_detail').html(reservation_time_detail);
		$('.detail-reservation .reservation_paxes_detail').html(reservation_paxes_detail + " orang");
		$('.detail-reservation .reservation_total_price_detail').html("Rp. " + reservation_total_price_detail);
		$('.detail-reservation .reservation_method_payment_detail').html(reservation_method_payment_detail);
		$('.detail-reservation .qrcode-container h5').html(reservation_qr_code_plain_detail);

		//use clone to not remove element from the old one
		$(reservation_qr_code_detail).clone().appendTo(".detail-reservation .qrcode-container .thumbnail");

		$('.detail-reservation').fadeIn('fast');
		e.stopPropagation();
	}
});
$('html').click(function() {
	if ($('.detail-reservation').css('display') == 'block'){
		$('.detail-reservation .qrcode-container .thumbnail').children().remove();		
		$('.detail-reservation').fadeOut('fast');		
	}
});
// END: DETAIL RESERVATION

// DELETE USER AND FILL USER BALANCE
var user_name = "";
$(".table-user-database-report tbody tr").click(function(){
   $(this).addClass('selected').siblings().removeClass('selected');    
   var user_id = $(this).find('td:first').html();
   user_name = $(this).find('td:first').next().html();

	$('.form-delete-user .user_id_hidden').val(user_id);
	$('.form-fill-balance .user_id_hidden_fill_balance').val(user_id);

	$('.form-delete-user .btn-delete').removeAttr('disabled');
	$('#btn-fill-balance').removeAttr('disabled');
});
$('.form-delete-user .btn-delete').click(function(){
	//only delete if user confirm Yes
	swal({   
		title: "Hapus user bernama " + user_name +" ?" ,
		showCancelButton: true,   
		type: "warning",
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Ya", 
		closeOnConfirm: false, 
		cancelButtonText: 'Tidak',		
	}, function(){   
		$('.form-delete-user').submit();
	});	
});
$('#btn-fill-balance').click(function(){
	$('.form-fill-balance #user_name_fill_balance').val(user_name);
    $('#fillBalanceModal').modal();
});
// END: DELETE USER AND FILL USER BALANCE

// DELETE RESTAURANT
$(".table-restaurant-database-report tbody tr").click(function(){
   $(this).addClass('selected').siblings().removeClass('selected');    
    var restaurant_management_id = $(this).find('td:first').html();
	$('.form-delete-restaurant-management .restaurant_management_id_hidden').val(restaurant_management_id);
	$('.form-accept-restaurant-management .restaurant_management_id_accepted_hidden').val(restaurant_management_id);

	var status = $(this).find('td:nth-child(6)').html();

	$('.form-delete-restaurant-management .btn-delete').removeAttr('disabled');
	if(status == "Guest"){
		$('.form-accept-restaurant-management #btn-accept-restaurant-management').removeAttr('disabled');
	}
	else{
		$('.form-accept-restaurant-management #btn-accept-restaurant-management').prop('disabled', 'true');
	}
});
$('.form-delete-restaurant-management .btn-delete').click(function(){
	//only delete if user confirm Yes
	swal({   
		title: "Apakah anda yakin?",
		showCancelButton: true,   
		type: "warning",
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Ya", 
		closeOnConfirm: false,
		cancelButtonText: 'Tidak'
	}, function(){   
		$('.form-delete-restaurant-management').submit();
	});	
});
// END: DELETE RESTAURANT

$('.form-accept-restaurant-management #btn-accept-restaurant-management').click(function(){
	//only delete if user confirm Yes
	swal({   
		title: "Apakah anda yakin?",
		showCancelButton: true,   
		type: "warning",
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Ya", 
		closeOnConfirm: false,
		cancelButtonText: 'Tidak'
	}, function(){   
		$('.form-accept-restaurant-management').submit();
	});	
});

$(".table-flux-user-balance tbody tr").click(function(){
   $(this).addClass('selected').siblings().removeClass('selected');    
   var history_balance_id = $(this).find('td:first').html();
	$('.form-put-flux-user-balance .history_balance_id_hidden').val(history_balance_id);

	$('.form-put-flux-user-balance .btn-flux').removeAttr('disabled');

});
$('.form-put-flux-user-balance .btn-flux').click(function(){
	swal({   
		title: "Apakah anda yakin?",
		showCancelButton: true,   
		type: "warning",
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Ya", 
		closeOnConfirm: false,
		cancelButtonText: 'Tidak'
	}, function(){   
		$('.form-put-flux-user-balance').submit();
	});	
})

$(".table-flux-restaurant-balance tbody tr").click(function(){
   $(this).addClass('selected').siblings().removeClass('selected');    
   var restaurant_history_balance_id = $(this).find('td:first').html();
	$('.form-put-flux-restaurant-balance .restaurant_history_balance_id_hidden').val(restaurant_history_balance_id);

	$('.form-put-flux-restaurant-balance .btn-flux').removeAttr('disabled');

});
$('.form-put-flux-restaurant-balance .btn-flux').click(function(){
	swal({   
		title: "Apakah anda yakin?",
		showCancelButton: true,   
		type: "warning",
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Ya", 
		closeOnConfirm: false,
		cancelButtonText: 'Tidak'
	}, function(){   
		$('.form-put-flux-restaurant-balance').submit();
	});	
});

var reservation_id = "";
$(".table-verify-payment tbody tr").click(function(){
   $(this).addClass('selected').siblings().removeClass('selected');    
    reservation_id = $(this).find('td:first').next().html();
	$('.form-put-verify-payment .reservation_id_hidden').val(reservation_id);

	$('.form-put-verify-payment .btn-approve').removeAttr('disabled');
	$('#btn-decline-reservation').removeAttr('disabled');
});
$('.form-put-verify-payment .btn-approve').click(function(){
	swal({   
		title: "Apakah anda yakin?",
		showCancelButton: true,   
		type: "warning",
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Ya", 
		closeOnConfirm: false,
		cancelButtonText: 'Tidak'
	}, function(){   
		$('.form-put-verify-payment').submit();
	});	
})

$('#btn-decline-reservation').click(function(){
	$('.form-decline-reservation #reservation_id_decline').val(reservation_id);
	$('.form-decline-reservation #reservation_id_decline_hidden').val(reservation_id);

    $('#declineReservationModal').modal();
});

function getParameterByName(name) {
    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}

// SORT USER DATABASE
$('.sort-user-database').change(
	function() { 
		this.form.submit(); 
	}
);
// END: SORT USER DATABASE

// CHANGE USER COMBO BOX VALUE AFTER SORT
var url = window.location.href;
if(url.indexOf('?sort_user=') != -1){
	$('.sort-user-database').val(getParameterByName('sort_user'));
}
// END: CHANGE USER COMBO BOX VALUE AFTER SORT

// SORT RESTAURANT MANAGEMENT DATABASE
$('.sort-restaurant-management-database').change(
	function() { 
		this.form.submit(); 
	}
);
// END: SORT RESTAURANT MANAGEMENT DATABASE

// CHANGE RESTAURANT MANAGEMENT COMBO BOX VALUE AFTER SORT
var url = window.location.href;
if(url.indexOf('?sort_restaurant_management=') != -1){
	$('.sort-restaurant-management-database').val(getParameterByName('sort_restaurant_management'));
}
// END: CHANGE RESTAURANT MANAGEMENT COMBO BOX VALUE AFTER SORT

// SORT HISTORY BALANCE
$('.sort-history-balance').change(
	function() { 
		this.form.submit(); 
	}
);
// END: SORT HISTORY BALANCE

// CHANGE USER COMBO BOX VALUE AFTER SORT
var url = window.location.href;
if(url.indexOf('?sort_history_balance=') != -1){
	$('.sort-history-balance').val(getParameterByName('sort_history_balance'));
}
// END: CHANGE USER COMBO BOX VALUE AFTER SORT

// SORT CONFIRM PAYMENT
$('.sort-confirm-payment').change(
	function() { 
		this.form.submit(); 
	}
);
// END: SORT HISTORY BALANCE

// CHANGE USER COMBO BOX VALUE AFTER SORT
var url = window.location.href;
if(url.indexOf('?sort_confirm_payment=') != -1){
	$('.sort-confirm-payment').val(getParameterByName('sort_confirm_payment'));
}
// END: CHANGE USER COMBO BOX VALUE AFTER SORT

// SORT INCOME
$('.sort-reservation').change(
	function() { 
		this.form.submit(); 
	}
);
// END: SORT INCOME

// CHANGE USER COMBO BOX VALUE AFTER SORT
var url = window.location.href;
if(url.indexOf('?sort_reservation=') != -1){
	$('.sort-reservation').val(getParameterByName('sort_reservation'));
}
// END: CHANGE USER COMBO BOX VALUE AFTER SORT

// FORM VALIDATE
$('.form-login').validate({
	rules:{
		username:{ required: true},
		password:{ required: true }
	},
	messages:{
		username:{
			required: "Username harus diisi.",
		},
		password: {
			required: "Kata Sandi harus diisi.",
		}    		
	},
	showErrors: function(errorMap, errorList) {
		$.each(this.validElements(), function (index, element) {
			var $element = $(element);
				$element.removeClass('error');
				$element.addClass('default');
				$element.data("title", "")
						.tooltip("destroy");
	  	});
	  	$.each(errorList, function (index, error) {
		    var $element = $(error.element);
			$element.removeClass('default');
		    $element.addClass('error');
			$element.tooltip("destroy")
					.data("title", error.message)
					.tooltip();		  		
	  	});
	},
	submitHandler: function(form) {
		form.submit();
	}
});
$('.form-add-restaurant').validate({
	rules:{
		name:{ required: true},
		email:{ required: true, email: true},
		password:{ required: true },
		address:{ required: true }, 
	},
	messages:{
		name:{
			required: "Nama harus diisi.",
		},
		email:{
			required: "Alamat Email harus diisi.",
			email: "Alamat Email tidak valid.",
		},
		password:{
			required: "Kata Sandi harus diisi.",
		},
		address: {
			required: "Alamat harus diisi.",
		}    		
	},
	showErrors: function(errorMap, errorList) {
		$.each(this.validElements(), function (index, element) {
			var $element = $(element);
				$element.removeClass('error');
				$element.addClass('default-grey');
				$element.data("title", "")
						.tooltip("destroy");
	  	});
	  	$.each(errorList, function (index, error) {
		    var $element = $(error.element);
			$element.removeClass('default-grey');
		    $element.addClass('error');
			$element.tooltip("destroy")
					.data("title", error.message)
					.tooltip();		  		
	  	});
	},
	submitHandler: function(form) {
		form.submit();
	}
});
$('.form-fill-balance').validate({
	rules:{
		balance_processed:{ required: true, digits: true},
		reason:{ required: true},
	},
	messages:{
		balance_processed:{
			required: "Jumlah Saldo harus diisi.",
			digits: "Jumlah Saldo hanya boleh mengandung angka."
		},   				
		reason:{
			required: "Alasan harus diisi.",
		}   				
	},
	showErrors: function(errorMap, errorList) {
		$.each(this.validElements(), function (index, element) {
			var $element = $(element);
				$element.removeClass('error');
				$element.addClass('default-grey');
				$element.data("title", "")
						.tooltip("destroy");
	  	});
	  	$.each(errorList, function (index, error) {
		    var $element = $(error.element);
			$element.removeClass('default-grey');
		    $element.addClass('error');
			$element.tooltip("destroy")
					.data("title", error.message)
					.tooltip();	  		
	  	});
	},
	submitHandler: function(form) {
		form.submit();
	}
});
$('.form-decline-reservation').validate({
	rules:{
		reason:{ required: true},
	},
	messages:{
		reason:{
			required: "Alasan penolakan harus diisi.",
		}   				
	},
	showErrors: function(errorMap, errorList) {
		$.each(this.validElements(), function (index, element) {
			var $element = $(element);
				$element.removeClass('error');
				$element.addClass('default-grey');
				$element.data("title", "")
						.tooltip("destroy");
	  	});
	  	$.each(errorList, function (index, error) {
		    var $element = $(error.element);
			$element.removeClass('default-grey');
		    $element.addClass('error');
			$element.tooltip("destroy")
					.data("title", error.message)
					.tooltip();	  		
	  	});
	},
	submitHandler: function(form) {
		form.submit();
	}
});
// END: FORM VALIDATE
});