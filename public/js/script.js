$(document).ready(function(){

// FORMATTING CURRENCY
function addCommas(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}
// END: FORMATTING CURRENCY

// SCROLLING
$('html').niceScroll({cursorcolor:" #959595"});
$('.info-time').niceScroll({cursorcolor:" #959595"});
$('.info-time-reserve').niceScroll({cursorcolor:" #959595"});
$('.info-change-time-reserve').niceScroll({cursorcolor:" #959595"});
$('.category-menu').niceScroll({cursorcolor:" #959595"});
$('.menu_category-container').niceScroll({cursorcolor:" #959595"});
$('.order-content').niceScroll({cursorcolor:" #959595"});
$('.photo-detail-container').niceScroll({cursorcolor:" #959595"});
// END: SCROLLING

// DROPDOWN WITH IMAGE
$('#rating_dropdown').ddslick();
// END: DROPDOWN WITH IMAGE

// MODAL
$('#login').click(function(){
    $('#loginModal').modal();
});
$('#list_holidays').click(function(){
    $('#listHolidayModal').modal();
});
$('#withdraw').click(function(){
    $('#withdrawModal').modal();
});
$('#consult_business').click(function(){
	$('#consult_businessModal').modal();
});
if ($('#success-send-business-message').length){ //if exists
	$('#success_send_business_messageModal').modal();
}
if ($('#success-pay-transfer-bank').length){
	var reservation_id = $('#success-pay-transfer-bank').attr('reservation-id');
	$('.to-confirm-payment').attr('href', 'http://solivis.com/payment/confirm/' + reservation_id);
	$('#payModal').modal();
}
$('.btn-share').click(function(){ 
	$('#shareModal').modal();
});
$('.btn-add-account-bank').click(function(){
	$('#addAccountBankModal').modal();
});
$('.btn-edit-account-bank').click(function(){
	$('#editAccountBankModal').modal();
});
$('.btn-show-declined-reason').click(function(){
	var declined_reason = $(this).children('span').html();

	$('#declinedReasonModal .declined_reason_detail').html(declined_reason);
	$('#declinedReasonModal').modal();
});
$('.btn-comment').click(function(){
	var reservation_id = $(this).next().html();
	$('.form-review .reservation_id').val(reservation_id);
	$('#reviewModal').modal();
});
$('.view-order-detail-link').click(function(e){
	e.preventDefault();
	var food_names = $(this).children('.food-names').html().split(',');
	food_names.splice(-1,1); //remove "" in last index

	var food_counts = $(this).children('.food-counts').html().split(',');
	food_counts.splice(-1,1);

	var food_prices = $(this).children('.food-prices').html().split(',');
	food_prices.splice(-1,1);

	var total_food_prices = 0;

    $('#orderDetailModal tbody').children().remove();	

	for(var i=0; i<food_names.length; i++){
		total_food_prices += parseInt(food_prices[i]);
	    $('#orderDetailModal tbody').append(
	        '<tr>' + 
	          '<td>' + food_names[i] + '</td>' +
	          '<td class="text-center">' + food_counts[i] + '</td>' +
	          '<td class="text-right">' + addCommas(food_prices[i]) + '</td>' +
	        '</tr>'
		);
	}
	$('.total-pay span').html("Rp. " + addCommas(total_food_prices));
    $('#orderDetailModal').modal();
});

$('.user-auth-group').click(function(e) {
	if ($('.user-auth-info').css('display') == 'block'){
		$('.user-auth-info').fadeOut('fast');
	}
	else{
		$('.user-auth-info').fadeIn('fast');
		e.stopPropagation();
	}
});
$('html').click(function() {
	if ($('.user-auth-info').css('display') == 'block'){
		$('.user-auth-info').fadeOut('fast');		
	}
	if ($('.btn-changepicture').css('display') == 'block'){
		$('.btn-changepicture').fadeOut('fast');		
	}
});
$('.profile_picture').click(function(e){
	if ($('.btn-changepicture').css('display') == 'block'){	
		$('.btn-changepicture').fadeOut('fast');
	}
	else{
		$('.btn-changepicture').fadeIn('fast');
		e.stopPropagation();
	}
});
// END: MODAL

// QUICKLY UPLOAD IMAGE PROFILE
$('.upload').change(
	function() { 
		this.form.submit(); 
	}
);
// END: QUICKLY UPLOAD IMAGE PROFILE

// UPLOAD TRANSFER PROOF
$('.upload-only').change(function() { 
 	var path_name = $(this).val().replace(/C:\\fakepath\\/i, '');
	$('#path_name').attr('value', path_name);
	$('#path_name').html(path_name); 	
});
// END: QUICKLY UPLOAD IMAGE PROFILE

// INPUT DATA IN RESTAURANT PROFILE AFTER SEARCHED

//GET PARAMETER VALUE
function getParameterByName(name) {
    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}
//END: GET PARAMETER VALUE

var url = window.location.href;

// INPUT DATA IN RESTAURANT PROFILE AFTER SEARCHED
if(url.indexOf('?cuisine_name=') != -1){
	$('#input-pax-reserve').val(getParameterByName('paxes'));
	$('#input-date-reserve').val(getParameterByName('reservation_date'));
	$('#input-time-reserve').val(getParameterByName('reservation_time'));
}
// END: INPUT DATA IN RESTAURANT PROFILE AFTER SEARCHED

// SELECT RADIO BUTTON AND CHECKBOX AFTER SEARCHED
var search = window.location.search.substring(1); //get all parameters
if(search){
	//parse %5B to [ and %5D to ]
	var query_obj = JSON.parse('{"' + decodeURI(search.replace(/&/g, "\",\"").replace(/=/g,"\":\"")) + '"}');
	for(var key in query_obj){
		if(key == 'prices'){
			$('#radio-prices-'+query_obj[key]).prop('checked', true);
		}
		else {
			if(key.slice(0,-3) == 'cuisines'){ //remove 3 last character
				$('#checkbox-cuisine-'+query_obj[key]).prop('checked', true);
			}
			else if(key.slice(0,-3) == 'services'){ //services
				//remove + and space
				var query_string = query_obj[key].replace(/\+/g, '').replace(/\s/g, '');
				$('#checkbox-service-'+query_string).prop('checked', true);
			}
		}
	}
}
// END: SELECT RADIO BUTTON AND CHECKBOX AFTER SEARCHED

// SLIDE AND FADE
// LIVE SEARCH
var search = function(){
	var query_value = $('#input-restaurant_name').val();
	var index;
	var URL_address;

	var loc = window.location.pathname.split('/');
	loc.shift();
	if(loc.length == 3){
		URL_address= '../../php/search.php';
	}
	else{
		URL_address= '../php/search.php';
	}

	if (query_value !== ""){
		$.ajax({
			type: 'POST',
			url: URL_address,
			data: {query: query_value},
			cache: false,
			success: function(html){
				$('ul#results_food').empty();
				$('ul#results_location').empty();
				$('ul#results_cuisine').empty();
				$('ul#results_restaurant').empty();
				$('ul#results_notfound').empty();

				for (var i=0; i<html.length; i++){
					index = $(html).get(i);
					if ($(index).attr('class') == 'result_location'){
						$('ul#results_location').append(index);						
					}
					else if ($(index).attr('class') == 'result_cuisine'){
						$('ul#results_cuisine').append(index);						
					}
					else if ($(index).attr('class') == 'result_restaurant'){
						$('ul#results_restaurant').append(index);						
					}
					else if ($(index).attr('class') == 'result_notfound'){
						$('ul#results_notfound').append(index);						
					}
				}

				if ($('#results_food').has('li').length == 0){
					$('.recommendation-title-food').addClass('hide');					
				}
				else{
					$('.recommendation-title-food').removeClass('hide');			
				}
				if ($('#results_location').has('li').length == 0){
					$('.recommendation-title-location').addClass('hide');					
				}
				else{
					$('.recommendation-title-location').removeClass('hide');			
				}
				if ($('#results_cuisine').has('li').length == 0){
					$('.recommendation-title-cuisine').addClass('hide');					
				}
				else{
					$('.recommendation-title-cuisine').removeClass('hide');			
				}
				if ($('#results_restaurant').has('li').length == 0){
					$('.recommendation-title-restaurant').addClass('hide');					
				}
				else{
					$('.recommendation-title-restaurant').removeClass('hide');			
				}
			}
		});
	}
	return false;
}

$("#input-restaurant_name").on("keyup", function(e) {
	clearTimeout($.data(this, 'timer'));
	var search_string = $(this).val();
	if (search_string == ""){
		$('.group-results').slideUp('fast');
	}
	else{
		$('.group-results').slideDown('fast');
		$(this).data('timer', setTimeout(search, 100));	
	}
})
// END: LIVE SEARCH
$('#input-restaurant_name').click(function(){
	if ($(this).val() == ""){
		$('.info-keywords').slideDown('fast').delay(400).slideUp('fast');
	}
})
var previous_pax_index = "";
var previous_time_index = "";
$('#input-pax').focus(function(){
	$('.info-pax').fadeIn('normal');
})
$('#input-pax').focusout(function(){
	$('.info-pax').fadeOut('normal');
})
//only allow user to type number
$("#input-pax").keypress(function (e) {
	// 8 = null, 0 = backspace, 48-56 = 1-9
	if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
   		return false;
	}
});
$('.table-pax tr td').click(function(){
	if ($('#input-pax').val() != ""){
		if (previous_pax_index != ""){
			$('.table-pax tr td#' + previous_pax_index).removeClass('active');	
		}
	}
	$(this).addClass('active');
	previous_pax_index = $(this).attr('id');
	$('#input-pax').val($(this).text());
	$('.info-pax').fadeOut('normal');
})
$('#input-date').datepicker({
	language: 'en',
	autoClose: true,
	minDate: new Date()
})
$('#input-time').focus(function(){
	$('.info-time').fadeIn('normal');
	$('.pointer').removeClass('hide').addClass('show');
})
$('#input-time').focusout(function(){
	$('.info-time').fadeOut('normal');
	$('.pointer').removeClass('show').addClass('hide');
})
$('.table-time tr td').click(function(){
	if ($('#input-time').val() != ""){
		if (previous_time_index != ""){
			$('.table-time tr td#' + previous_time_index).removeClass('active');	
		}
	}
	$(this).addClass('active');
	previous_time_index = $(this).attr('id');
	$('#input-time').val($(this).text());
	$('.info-time').fadeOut('normal');
	$('.pointer').removeClass('show').addClass('hide');
})
$('#input-date-start').datepicker({
	language: 'en',
	autoClose: true,
})
$('#input-date-end').datepicker({
	language: 'en',
	autoClose: true,
})
var previous_pax_reserve_index = "";
var previous_time_reserve_index = "";
$('#input-pax-reserve').focus(function(){
	$('.info-pax-reserve').fadeIn('normal');
})
$('#input-pax-reserve').focusout(function(){
	$('.info-pax-reserve').fadeOut('normal');
})
$('.table-pax-reserve tr td').click(function(){
	if ($('#input-pax-reserve').val() != ""){
		if (previous_pax_reserve_index != ""){
			$('.table-pax-reserve tr td#' + previous_pax_reserve_index).removeClass('active');	
		}
	}
	$(this).addClass('active');
	previous_pax_reserve_index = $(this).attr('id');
	$('#input-pax-reserve').val($(this).text());

	$('#hidden_pax_id').val($(this).text());	//hidden pax

	$('.info-pax-reserve').fadeOut('normal');
})
//for manual input
//only allow user to type number
$("#input-pax-reserve").keypress(function (e) {
	if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
	   	return false;
	}
});
$('#input-pax-reserve').change(function(){
	$('#hidden_pax_id').val($(this).val());
});
$('#input-date-reserve').datepicker({
	language: 'en',
	autoClose: true,
	minDate: new Date(),	
	onSelect: function onSelect(date) {
		$('#hidden_date_reserve_id').val(date);	//hidden pax		
	}
});
$('#input-time-reserve').focus(function(){
	$('.info-time-reserve').fadeIn('normal');
	$('.second_pointer').removeClass('hide').addClass('show');
})
$('#input-time-reserve').focusout(function(){
	$('.info-time-reserve').fadeOut('normal');
	$('.second_pointer').removeClass('show').addClass('hide');
})
$('.table-time-reserve tr td').click(function(){
	if ($('#input-time-reserve').val() != ""){
		if (previous_time_reserve_index != ""){
			$('.table-time-reserve tr td#' + previous_time_reserve_index).removeClass('active');	
		}
	}
	$(this).addClass('active');
	previous_time_reserve_index = $(this).attr('id');
	$('#input-time-reserve').val($(this).text());

	$('#hidden_time_reserve_id').val($(this).text());	//hidden time reserve

	$('.info-time-reserve').fadeOut('normal');
	$('.second_pointer').removeClass('show').addClass('hide');
})

var previous_change_time_reserve_index = "";
$('#input-change-date-reserve').datepicker({
	language: 'en',
	autoClose: true,
	minDate: new Date(),	
});
$('#input-change-time-reserve').focus(function(){
	$('.info-change-time-reserve').fadeIn('normal');
	$('.third_pointer').removeClass('hide').addClass('show');
});
$('#input-change-time-reserve').focusout(function(){
	$('.info-change-time-reserve').fadeOut('normal');
	$('.third_pointer').removeClass('show').addClass('hide');
});
$('.table-change-time-reserve tr td').click(function(){
	if ($('#input-change-time-reserve').val() != ""){
		if (previous_change_time_reserve_index != ""){
			$('.table-change-time-reserve tr td#' + previous_change_time_reserve_index).removeClass('active');	
		}
	}
	$(this).addClass('active');
	previous_change_time_reserve_index = $(this).attr('id');
	$('#input-change-time-reserve').val($(this).text());
	$('.info-change-time-reserve').fadeOut('normal');
	$('.third_pointer').removeClass('show').addClass('hide');
});

var field = 'page';
var url = window.location.href;
if(url.indexOf('?' + field + '=') != -1){
	$('ul.nav-results-fade li.menus').removeClass('active');	
	$('ul.nav-results-fade li.reviews').addClass('active');	

	$('.menu-container').addClass('hide');
	$('.review-container').removeClass('hide');	

	$('ul.nav-results-fade li.menus a').click(function(){
		$('ul.category-menu li:first-child').addClass('active');
		$('.menu_category-container:first').removeClass('hide');
	});
}
else{
	//DEFAULT
	$('ul.category-menu li:first-child').addClass('active');
	$('.menu_category-container:first').removeClass('hide');
}
$('ul.category-menu li a').click(function(e) {
	e.preventDefault();
    $('ul.category-menu li.active').removeClass('active');
    $(this).closest('li').addClass('active');

	if ($('ul.category-menu li#'+$(this).closest('li').attr('id')).hasClass('active')) {
		if ($('.menu_category-container').not('.hide')){
			$('.menu_category-container').addClass('hide');
		}		
		$('#container-' + $(this).closest('li').attr('id')).fadeIn('slow');
		$('#container-' + $(this).closest('li').attr('id')).removeClass('hide');
	}
});

$('ul.nav-results-fade li a').click(function(e) {
	e.preventDefault();
    $('ul.nav-results-fade li.active').removeClass('active');
    $(this).closest('li').addClass('active');
	if ($('ul.nav-results-fade li.menus').hasClass('active')) {
		$('.menu-container').fadeIn('slow');	
		$('.menu-container').removeClass('hide');
		if ($('.photo-container').not('.hide')){
			$('.photo-container').addClass('hide');			
		}		
		if ($('.review-container').not('.hide')){
			$('.review-container').addClass('hide');			
		}				
	}
	else if ($('ul.nav-results-fade li.photos').hasClass('active')) {
		$('.photo-container').fadeIn('slow');	
		$('.photo-container').removeClass('hide');
		if ($('.menu-container').not('.hide')){
			$('.menu-container').addClass('hide');			
		}		
		if ($('.review-container').not('.hide')){
			$('.review-container').addClass('hide');			
		}
	}
	else if ($('ul.nav-results-fade li.reviews').hasClass('active')) {
		$('.review-container').fadeIn('slow');	
		$('.review-container').removeClass('hide');
		if ($('.menu-container').not('.hide')){
			$('.menu-container').addClass('hide');			
		}		
		if ($('.photo-container').not('.hide')){
			$('.photo-container').addClass('hide');			
		}	
	}
});
 $('input[type=radio][name=payment_method]').change(function() {
    if (this.value == '0') {
		$('.info-payment-results-transfer_bank').removeClass('hide');
    	$('.info-payment-results-ewallet').addClass('hide');
		$('.info-payment-results-transfer_bank').fadeIn('slow');    			
    }
    else if (this.value == '1') {
    	$('.info-payment-results-transfer_bank').addClass('hide');
		$('.info-payment-results-ewallet').removeClass('hide');
		$('.info-payment-results-ewallet').fadeIn('slow');    			
    }
});	
// END: SLIDE AND FADE

// CHANGE COLOR OF SELECTED DROPDOWN
$('#select-cuisine').change(function() {
   var current = $(this).val();
   if (current != '') {
       $('#select-cuisine').css('color','#000000');
       $('#select-cuisine option:first-child').css('color', '#959595');
   } 
   else {
       $('#select-cuisine').css('color','#959595');
   }
}); 
// END: CHANGE COLOR OF SELECTED DROPDOWN

// FILTER SEARCHING
// DEFAULT EXPAND ONLY PRICES
$('.detail-filter-prices').css('display', 'block');
$('.filter-title-cuisine').removeClass('border-filter-spc-collapse').addClass('border-filter-spc-expand');

$('.filter-title-prices').click(function(){
	//if display of element before was none
	if ($('.detail-filter-prices').css('display') == 'none'){
		$('.filter-title-prices > img').attr('src', '/images/expand.png').attr('alt', 'expand');				
		$('.filter-title-cuisine').removeClass('border-filter-spc-collapse').addClass('border-filter-spc-expand');
	}
	else{
		$('.filter-title-prices > img').attr('src', '/images/collapse.png').attr('alt', 'collapse');		
		$('.filter-title-cuisine').removeClass('border-filter-spc-expand').addClass('border-filter-spc-collapse');
	}	
	$('.detail-filter-prices').slideToggle('fast');
});
$('.filter-title-cuisine').click(function(){
	//if display of element before was none
	if ($('.detail-filter-cuisine').css('display') == 'none'){
		$('.filter-title-cuisine > img').attr('src', '/images/expand.png').attr('alt', 'expand');
		$('.filter-title-service').removeClass('border-filter-spc-collapse').addClass('border-filter-spc-expand');
	}
	else{
		$('.filter-title-cuisine > img').attr('src', '/images/collapse.png').attr('alt', 'collapse');		
		$('.filter-title-service').removeClass('border-filter-spc-expand').addClass('border-filter-spc-collapse');
	}	
	$('.detail-filter-cuisine').slideToggle('fast');
});
$('.filter-title-service').click(function(){
	//if display of element before was none
	if ($('.detail-filter-service').css('display') == 'none'){
		$('.filter-title-service > img').attr('src', '/images/expand.png').attr('alt', 'expand');				
	}
	else{
		$('.filter-title-service > img').attr('src', '/images/collapse.png').attr('alt', 'collapse');		
	}	
	$('.detail-filter-service').slideToggle('fast');
});
// END: FILTER SEARCHING

// ORDER MENU
var menus = [];
var menu_id = 0;

// ADD VALUE TO INPUT WHEN EDIT RESERVATION
if($('.edit_menu_hidden').has('span')){
	$('.edit_item_name_hidden').each(function(){
		var item_name = $(this).html();
		var item_count = $(this).next().html();
		$('.menu-title').each(function(){
			if($(this).html() == item_name){
				var input = $(this).parent().next().next().find('input');
				input.val(item_count);
			}
		})
	});
}
// END: ADD VALUE TO INPUT WHEN EDIT RESERVATION

// ORDER MENU FOR EDIT RESERVATION
$('.btn-add').each(function(i, obj) {
	if ($(this).prev().val() != 0){
		menu_id += 1;
		menus.push(menu_id);

		var value = $(this).prev().val();

		var menu_title = $(this).parent().prev().prev().find('.menu-title');
		var price_value = $(this).parent().prev().find('.price-value');

		$('.order-content').append(
			'<div class="order-content-menu" id="menu-' + menu_id + '">' +
				'<div class="col-xs-6 col-md-5 col-lg-6 no-padding">' + 
					'<h5>' + menu_title.html() + '</h5>' + 
				'</div>' +
				'<div class="col-xs-2 col-md-2 col-lg-2 no-padding">' +
	                '<h5 class="text-center">' + value + '</h5>' +
	            '</div>' +
	            '<div class="col-xs-4 col-md-5 col-lg-4 no-padding">' +
	                '<h5 class="text-right">' + addCommas(value * price_value.attr('value')) + '</h5>' +
	            '</div>' +
	            '<div class="clearfix"></div>' +
			'<input type="hidden" name="item_name[]" value="' + menu_title.html() + '"/>' + 
			'<input type="hidden" name="item_count[]" value="' + value + '"/>' +
			'<input type="hidden" name="item_price[]" value="' + value * price_value.attr('value') + '"/>' +
			'</div>'
		);
		$('.order-content > #menu-' + menu_id + '> .col-xs-4 > h5').attr('value', value * price_value.attr('value'));

		var total_order = 0;

		if ($('.order-content').has('.order-content-menu')){
			for (var i=0; i<menus.length; i++){
				total_order += parseInt($('.order-content > #menu-' + menus[i] + '> .col-xs-4 > h5').attr('value'));
			}
			$('.prices').attr('value', total_order);
			$('.prices').html('Rp. ' + addCommas(total_order));
			$('#total_prices_hidden_id').val(total_order);		
		}		
	}
});

$('.btn-add').click(function(){
	var input = $(this).prev();
	var value = parseInt(input.val()) + 1;
	input.val(value);

	var menu_title = $(this).parent().prev().prev().find('.menu-title');
	var price_value = $(this).parent().prev().find('.price-value');
	var price_currency;

	var check_exists = false;

	for (var i=0; i<menus.length; i++){
		if ($('.order-content > #menu-' + menus[i] + '> .col-xs-6 > h5').html() == menu_title.html()){
			$('.order-content > #menu-' + menus[i] + '> .col-xs-2 > h5').html(value);
			$('.order-content > #menu-' + menus[i] + '> .col-xs-4').next().next().next().val(value);

			price_currency = price_value.attr('value') * value;
			$('.order-content > #menu-' + menus[i] + '> .col-xs-4 > h5').attr('value', price_currency);
			$('.order-content > #menu-' + menus[i] + '> .col-xs-4 > h5').html(addCommas(price_currency));
			$('.order-content > #menu-' + menus[i] + '> .col-xs-4').next().next().next().next().val(price_currency);
			check_exists = true;
			break;
		} 
	}
	if (!check_exists){
		menu_id += 1;
		menus.push(menu_id);

		$('.order-content').append(
			'<div class="order-content-menu" id="menu-' + menu_id + '">' +
				'<div class="col-xs-6 col-md-5 col-lg-6 no-padding">' + 
					'<h5>' + menu_title.html() + '</h5>' + 
				'</div>' +
				'<div class="col-xs-2 col-md-2 col-lg-2 no-padding">' +
	                '<h5 class="text-center">' + value + '</h5>' +
	            '</div>' +
	            '<div class="col-xs-4 col-md-5 col-lg-4 no-padding">' +
	                '<h5 class="text-right">' + addCommas(price_value.attr('value')) + '</h5>' +
	            '</div>' +
	            '<div class="clearfix"></div>' +
			'<input type="hidden" name="item_name[]" value="' + menu_title.html() + '"/>' + 
			'<input type="hidden" name="item_count[]" value="' + value + '"/>' +
			'<input type="hidden" name="item_price[]" value="' + price_value.attr('value') + '"/>' +			
			'</div>'
		);
		$('.order-content > #menu-' + menu_id + '> .col-xs-4 > h5').attr('value', price_value.attr('value'));
	}

	var total_order = 0;

	if ($('.order-content').has('.order-content-menu')){
		for (var i=0; i<menus.length; i++){
			total_order += parseInt($('.order-content > #menu-' + menus[i] + '> .col-xs-4 > h5').attr('value'));
		}
		$('.prices').attr('value', total_order);
		$('.prices').html('Rp. ' + addCommas(total_order));
		$('#total_prices_hidden_id').val(total_order);		
	}
});
$('.btn-subtract').click(function(){
	var input = $(this).prev().prev();
	if (parseInt(input.val()) == 0){
		input.val(0);
	}
	else{
		var value = parseInt(input.val()) - 1;
		input.val(value);
	}

	var menu_title = $(this).parent().prev().prev().find('.menu-title');
	var price_value = $(this).parent().prev().find('.price-value');
	var price_currency;

	for (var i=0; i<menus.length; i++){
		if ($('.order-content > #menu-' + menus[i] + '> .col-xs-6 > h5').html() == menu_title.html()){
			price_currency = price_value.attr('value') * value;
			$('.order-content > #menu-' + menus[i] + '> .col-xs-4 > h5').attr('value', price_currency);
			$('.order-content > #menu-' + menus[i] + '> .col-xs-4 > h5').html(addCommas(price_currency));
			$('.order-content > #menu-' + menus[i] + '> .col-xs-4').next().next().next().next().val(price_currency);						

			$('.order-content > #menu-' + menus[i] + '> .col-xs-2 > h5').html(value);
			$('.order-content > #menu-' + menus[i] + '> .col-xs-4').next().next().next().val(value);

			if ($('.order-content > #menu-' + menus[i] + '> .col-xs-2 > h5').html() == '0'){
				$('#menu-' + menus[i]).remove();				
				menus.splice(i, 1); //remove based on index
			}
			break;
		} 
	}

	var total_order = 0;

	if ($('.order-content').has('.order-content-menu')){
		for (var i =0; i<menus.length; i++){
			total_order += parseInt($('.order-content > #menu-' + menus[i] + '> .col-xs-4 > h5').attr('value'));
		}
		if (total_order == 0){
			$('.prices').attr('value', '0');
			$('.prices').html('Rp. 0');
			$('#total_prices_hidden_id').val('0');			
		}
		else{
			$('.prices').attr('value', total_order);			
			$('.prices').html('Rp. ' + addCommas(total_order));
			$('#total_prices_hidden_id').val(total_order);
		}
	}

});
// END: ORDER MENU

// HIDDEN VALUE FOR PAX, RESERVATION DATE AND RESERVATION TIME
$('.form-reservation').append(
	'<input type="hidden" name="hidden_pax" value="" id="hidden_pax_id"/>' + 
	'<input type="hidden" name="hidden_date_reserve" value="" id="hidden_date_reserve_id"/>' + 
	'<input type="hidden" name="hidden_time_reserve" value="" id="hidden_time_reserve_id"/>'
);
$('#hidden_pax_id').val($('#input-pax-reserve').val());
$('#hidden_date_reserve_id').val($('#input-date-reserve').val());
$('#hidden_time_reserve_id').val($('#input-time-reserve').val());
// END: HIDDEN VALUE FOR PAX, RESERVATION DATE AND RESERVATION TIME

$('.food-photo').click(function(e){
	e.preventDefault();
	var food_photo_name = $(this).attr('food-photo-name');	
	var food_photo_url = $(this).attr('food-photo-url');	
	var food_photo_id = $(this).attr('food-photo-id');

    $('#photoContainerModal').modal();

    $('.food_photo_name_modal').text(food_photo_name);
    $('.food_photo_name_url').attr('src', '/images/restaurant_uploads/food_photos/'+food_photo_url).attr('alt', food_photo_name); 
    $('.food_photo_url_hidden').val(food_photo_url);
    $('.food_photo_id_hidden').val(food_photo_id);
});

// ACCOUNT NUMBER AND NAME
// DEFAULT
if ($('.user-account-bank_name:first').html() != ""){
	var bank_name = $('.user-account-bank_name:first').html();
	var account_number = $('.user-account-bank_name:first').next().html();
	var account_name = $('.user-account-bank_name:first').next().next().html();

	$('#accountNumber').val(account_number);
	$('#accountName').val(account_name);

	//edit bank
	var user_bank_id = $('.user-account-bank_name:first').next().next().next().html();
	var bank_id = $('.user-account-bank_name:first').next().next().next().next().html();
	$('.form-edit-bank .user_bank_id_hidden').val(user_bank_id);
	$('.form-edit-bank .account_number_detail').val(account_number);
	$('.form-edit-bank .account_name_detail').val(account_name);
	$('.form-edit-bank .account_bank_id_detail').val(bank_id);
	$('.form-edit-bank .bank_id_ori_hidden').val(bank_id);

	//delete bank
	$('.form-delete-bank .user_bank_id_del_hidden').val(user_bank_id);
}
$('#select-bank_name').change(function() {
	var current_value = $("#select-bank_name option:selected").val();
	var current_text = $("#select-bank_name option:selected").text();
	var current = current_value + "-" + current_text;

   	if ($('.user-account-bank_name').hasClass(current)){
		var account_number = $('.user-account-number-' + current).html();
		var account_name = $('.user-account-name-' + current).html();
		
		$('#accountNumber').val(account_number);
		$('#accountName').val(account_name);

		//edit bank
		var user_bank_id = $('.user-account-name-' + current).next().html();
		var bank_id = $('.user-account-name-' + current).next().next().html();

		$('.form-edit-bank .user_bank_id_hidden').val(user_bank_id);
		$('.form-edit-bank .account_number_detail').val(account_number);
		$('.form-edit-bank .account_name_detail').val(account_name);
		$('.form-edit-bank .account_bank_id_detail').val(bank_id);
		$('.form-edit-bank .bank_id_ori_hidden').val(bank_id);

		$('.form-delete-bank .user_bank_id_del_hidden').val(user_bank_id);

	}
	else{
		$('#accountNumber').val("");
		$('#accountName').val("");   	
	}
});
// END: ACCOUNT NUMBER AND NAME

// DELETE RESERVATION
$('.form-delete-reservation .btn-cancel-reservation').click(function(){
	//only delete if user confirm Yes
	swal({   
		title: "Apakah anda yakin?",
		showCancelButton: true,   
		type: "warning",
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Ya", 
		closeOnConfirm: false,
		cancelButtonText: 'Tidak'
	}, function(){   
		$('.form-delete-reservation').submit();
	});	
})
// END: DELETE RESERVATION

// DELETE BANK
$('.form-delete-bank .btn-delete-account-bank').click(function(){
	//only delete if user confirm Yes
	swal({   
		title: "Apakah anda yakin?",
		showCancelButton: true,   
		type: "warning",
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Ya", 
		closeOnConfirm: false,
		cancelButtonText: 'Tidak'
	}, function(){   
		$('.form-delete-bank').submit();
	});	
})
// END: DELETE BANK

// CONTINUE PAYMENT
$('.form-post-payment .btn-continue').click(function(){
	var form_data = {
    	restaurant_management_id: $('#hidden_restaurant_management_id').val(),
    	paxes: $('#hidden_paxes').val(),
    	reservation_date: $('#hidden_reservation_date').val(),
    	user_id: $('#hidden_user_id').val(),
	}

    $.ajax({
        type: "GET",
        dataType: 'json',
        url: 'http://solivis.com/reservation/check-duplicate-reservation',
        data: form_data,
		success: function (data) {
			if(data != 0){
				var text_message = "Anda melakukan reservasi terhadap restoran dan waktu reservasi yang sama dengan reservasi ID : " + data + ". ";
				text_message += "Apakah anda ingin mengupdatenya?"
				swal({   
					title: "Oops",
					text: text_message,
					showCancelButton: true,   
					type: "warning",
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Ya", 
					closeOnConfirm: false,
					cancelButtonText: 'Tidak'
				}, function(){   
					// set reservation id before to this input
					$('#hidden_reservation_id_before').val(data);
					$('.form-post-payment').submit();
				});					
			}
			else{
				$('.form-post-payment').submit();
			}
		},
		error: function (data) {
		    console.log('Error:', data);
		}        
    });
})
// END: CONTINUE PAYMENT


// SORT HISTORY 
$('.sort-history').change(
	function() { 
		this.form.submit(); 
	}
);
// END: SORT HISTORY

// CHANGE HISTORY COMBO BOX VALUE AFTER SORT
var url = window.location.href;
if(url.indexOf('?sort_history=') != -1){
	$('.sort-history').val(getParameterByName('sort_history'));
}
// END: CHANGE HISTORY COMBO BOX VALUE AFTER SORT

// SORT SEARCH RESTAURANT 
$('.sort-search-restaurant').change(
	function() { 
		this.form.submit(); 
	}
);
// END: SORT SEARCH RESTAURANT 

// CHANGE SEARCH RESTAURANT  COMBO BOX VALUE AFTER SORT
var url = window.location.href;
if(url.indexOf('?sort_search_restaurant=') != -1){
	$('.sort-search-restaurant').val(getParameterByName('sort_search_restaurant'));
}
// END: CHANGE SEARCH RESTAURANT  COMBO BOX VALUE AFTER SORT


//CHOOSE TO SCORCH THE RESERVATION
$('.form-scorch-reservation .btn-scorch-reservation').click(function(){
	swal({   
		title: "Apakah anda yakin?",
		showCancelButton: true,   
		type: "warning",
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Ya", 
		closeOnConfirm: false,
		cancelButtonText: 'Tidak'
	}, function(){   
		$('.form-scorch-reservation').submit();
	});	
});
//END: CHOOSE TO SCORCH THE RESERVATION

// FORM VALIDATE
$('.form-login').validate({
	rules:{
		email:{ required: true, email: true },
		password:{ required: true }
	},
	messages:{
		email:{
			required: "Alamat Email harus diisi.",
			email: "Alamat Email tidak valid."
		},
		password: {
			required: "Kata Sandi harus diisi.",
		}    		
	},
	showErrors: function(errorMap, errorList) {
		$.each(this.validElements(), function (index, element) {
			var $element = $(element);
				$element.removeClass('error');
				$element.addClass('default');
				$element.data("title", "")
						.tooltip("destroy");
	  	});
	  	$.each(errorList, function (index, error) {
		    var $element = $(error.element);
			$element.removeClass('default');
		    $element.addClass('error');
			$element.tooltip("destroy")
					.data("title", error.message)
					.tooltip();		  		
	  	});
	},
	submitHandler: function(form) {
		form.submit();
	}
});
$('.form-login2').validate({
	rules:{
		email:{ required: true, email: true },
		password:{ required: true }
	},
	messages:{
		email:{
			required: "Alamat Email harus diisi.",
			email: "Alamat Email tidak valid."
		},
		password: {
			required: "Kata Sandi harus diisi.",
		}    		
	},
	showErrors: function(errorMap, errorList) {
		$.each(this.validElements(), function (index, element) {
			var $element = $(element);
				$element.removeClass('error');
				$element.addClass('default');
				$element.data("title", "")
						.tooltip("destroy");
	  	});
	  	$.each(errorList, function (index, error) {
		    var $element = $(error.element);
			$element.removeClass('default');
		    $element.addClass('error');
			$element.tooltip("destroy")
					.data("title", error.message)
					.tooltip();		  		
	  	});
	},
	submitHandler: function(form) {
		form.submit();
	}
});
$('.form-register').validate({
	rules: {
		name: { required: true, maxlength: 255 },
		phone_number: { required: true, digits: true },
		email: { required: true, email: true, maxlength: 255 },
		password: { required: true, minlength: 6},
		password_confirmation:{ required: true, equalTo: "#password"},
		date: { required: true, digits: true },
		month: { required: true, digits: true },
		year: { required: true, digits: true }
	},
	messages: {
		name: "Nama Lengkap harus diisi.",
		phone_number: {
			required: "Nomor Handphone harus diisi.",
			digits: "Nomor Handphone hanya boleh mengandung angka."
		},
		email:{
			required: "Alamat Email harus diisi.",
			email: "Alamat Email tidak valid."
		},
		password: {
			required: "Kata Sandi harus diisi.",
			minlength: "Kata Sandi terlalu pendek, minimal 6 karakter."
		},
		password_confirmation: {
			required: "Ulangi Kata Sandi harus diisi.",
			equalTo: "Ulangi Kata Sandi tidak sama dengan Kata Sandi."
		},
		date: "Tanggal Lahir harus diisi.",
		month: "Bulan Lahir harus diisi.",
		year: "Tahun Lahir harus diisi."
	},
	showErrors: function(errorMap, errorList) {
		$.each(this.validElements(), function (index, element) {
			var $element = $(element);
			$element.removeClass('error');
			$element.addClass('default');
			$element.data("title", "")
					.tooltip("destroy");				
	  	});
	  	$.each(errorList, function (index, error) {
		    var $element = $(error.element);
			$element.removeClass('default');
		    $element.addClass('error');
			$element.tooltip("destroy")
					.data("title", error.message)
					.tooltip();		  		
	  	});
	},
	submitHandler: function(form) {
		form.submit();
	}
});
$('.form-save-identity').validate({
	rules: {
		name: { required: true, maxlength: 255 },
		phone_number: { required: true, digits: true },
		email: { required: true, email: true, maxlength: 255 },
		date: { required: true, digits: true },
		month: { required: true, digits: true },
		year: { required: true, digits: true }
	},
	messages: {
		name: "Nama Lengkap harus diisi.",	
		phone_number: {
			required: "Nomor Handphone harus diisi.",
			digits: "Nomor Handphone hanya boleh mengandung angka."
		},		
		email: {
			required: "Alamat Email harus diisi.",
			email: "Alamat Email tidak valid."
		},		
		date: "Tanggal Lahir harus diisi.",
		month: "Bulan Lahir harus diisi.",
		year: "Tahun Lahir harus diisi."
	},
	showErrors: function(errorMap, errorList) {
		$.each(this.validElements(), function (index, element) {
			var $element = $(element);
			$element.removeClass('error');
			$element.addClass('default');
			$element.data("title", "")
					.tooltip("destroy");				
	  	});
	  	$.each(errorList, function (index, error) {
		    var $element = $(error.element);
			$element.removeClass('default');
		    $element.addClass('error');
			$element.tooltip("destroy")
					.data("title", error.message)
					.tooltip();		  		
	  	});
	},
	submitHandler: function(form) {
		form.submit();
	}
});
$('.form-change-password').validate({
	rules: {
		old_password: { required: true },
		password: { required: true, minlength: 6},
		password_confirmation:{ required: true, equalTo: "#new_password"},
	},
	messages: {
		old_password: {
			required: "Kata Sandi Lama harus diisi.",
		},
		password: {
			required: "Kata Sandi Baru harus diisi.",
			minlength: "Kata Sandi Baru terlalu pendek, minimal 6 karakter."
		},
		password_confirmation: {
			required: "Ulangi Kata Sandi harus diisi.",
			equalTo: "Ulangi Kata Sandi tidak sama dengan Kata Sandi Baru."
		},
	},
	showErrors: function(errorMap, errorList) {
		$.each(this.validElements(), function (index, element) {
			var $element = $(element);
			$element.removeClass('error');
			$element.addClass('default');
			$element.data("title", "")
					.tooltip("destroy");				
	  	});
	  	$.each(errorList, function (index, error) {
		    var $element = $(error.element);
			$element.removeClass('default');
		    $element.addClass('error');
			$element.tooltip("destroy")
					.data("title", error.message)
					.tooltip();		  		
	  	});
	},
	submitHandler: function(form) {
		form.submit();
	}
});
$('.form-add-bank').validate({
	rules: {
		account_number: { required: true, digits: true },
		account_name: { required: true},
		password: { required: true},
	},
	messages: {
		account_number: {
			required: "Nomor Rekening harus diisi.",
			digits: "Nomor Rekening hanya boleh mengandung angka."
		},
		account_name: {
			required: "Nama Pemilik Rekening harus diisi."
		},
		password: {
			required: "Password harus diisi."
		}
	},
	showErrors: function(errorMap, errorList) {
		$.each(this.validElements(), function (index, element) {
			var $element = $(element);
			$element.removeClass('error');
			$element.addClass('default');
			$element.data("title", "")
					.tooltip("destroy");				
	  	});
	  	$.each(errorList, function (index, error) {
		    var $element = $(error.element);
			$element.removeClass('default');
		    $element.addClass('error');
			$element.tooltip("destroy")
					.data("title", error.message)
					.tooltip();		  		
	  	});
	},
	submitHandler: function(form) {
		form.submit();
	}
});
$('.form-edit-bank').validate({
	rules: {
		account_number: { required: true, digits: true },
		account_name: { required: true},
		password: { required: true},
	},
	messages: {
		account_number: {
			required: "Nomor Rekening harus diisi.",
			digits: "Nomor Rekening hanya boleh mengandung angka."
		},
		account_name: {
			required: "Nama Pemilik Rekening harus diisi."
		},
		password: {
			required: "Password harus diisi."
		}
	},
	showErrors: function(errorMap, errorList) {
		$.each(this.validElements(), function (index, element) {
			var $element = $(element);
			$element.removeClass('error');
			$element.addClass('default');
			$element.data("title", "")
					.tooltip("destroy");				
	  	});
	  	$.each(errorList, function (index, error) {
		    var $element = $(error.element);
			$element.removeClass('default');
		    $element.addClass('error');
			$element.tooltip("destroy")
					.data("title", error.message)
					.tooltip();		  		
	  	});
	},
	submitHandler: function(form) {
		form.submit();
	}
});
$('.form-subscribe').validate({
	rules:{
		email:{ required: true, email: true },
	},
	messages:{
		email:{
			required: "Alamat Email harus diisi.",
			email: "Alamat Email tidak valid."
		}    		
	},
	showErrors: function(errorMap, errorList) {
	  	$.each(this.validElements(), function (index, element) {
			var $element = $(element);
			$element.removeClass('error');
			$element.addClass('default');
			$element.data("title", "")
			.tooltip("destroy");					
	  	});
	  	$.each(errorList, function (index, error) {
		    var $element = $(error.element);
			$element.removeClass('default');
	        $element.addClass('error');
			$element.tooltip("destroy")
				.data("title", error.message)
				.tooltip();			      
	  	});
	}, 
	submitHandler: function(form) {
		form.submit();
	}   	
});
$('.form-send-reset').validate({
	rules:{
		email:{ required: true, email: true },
	},
	messages:{
		email:{
			required: "Alamat Email harus diisi.",
			email: "Alamat Email tidak valid."
		}    		
	},
	showErrors: function(errorMap, errorList) {
	  	$.each(this.validElements(), function (index, element) {
			var $element = $(element);
			$element.removeClass('error');
			$element.addClass('default');
			$element.data("title", "")
			.tooltip("destroy");					
	  	});
	  	$.each(errorList, function (index, error) {
		    var $element = $(error.element);
			$element.removeClass('default');
	        $element.addClass('error');
			$element.tooltip("destroy")
				.data("title", error.message)
				.tooltip();			      
	  	});
	},
	submitHandler: function(form) {
		form.submit();
	}    	
});
$('.form-reset-password').validate({
	rules:{
		email:{ required: true, email: true },
		password: { required: true, minlength: 6},
		password_confirmation:{ required: true, equalTo: "#password"},		
	},
	messages:{
		email:{
			required: "Alamat Email harus diisi.",
			email: "Alamat Email tidak valid."
		},
		password: {
			required: "Kata Sandi harus diisi.",
			minlength: "Kata Sandi terlalu pendek, minimal 6 karakter."
		},
		password_confirmation: {
			required: "Ulangi Kata Sandi harus diisi.",
			equalTo: "Ulangi Kata Sandi tidak sama dengan Kata Sandi."
		},		    		
	},
	showErrors: function(errorMap, errorList) {
	  	$.each(this.validElements(), function (index, element) {
			var $element = $(element);
			$element.removeClass('error');
			$element.addClass('default');
			$element.data("title", "")
			.tooltip("destroy");					
	  	});
	  	$.each(errorList, function (index, error) {
		    var $element = $(error.element);
			$element.removeClass('default');
	        $element.addClass('error');
			$element.tooltip("destroy")
				.data("title", error.message)
				.tooltip();			      
	  	});
	},
	submitHandler: function(form) {
		form.submit();
	}    	
});
$('.form-withdraw').validate({
	rules:{
		balance_processed:{ required: true, digits: true},
		bank_id:{ required: true},
		password:{ required: true},
	},
	messages:{
		balance_processed:{
			required: "Jumlah Penarikan harus diisi.",
			digits: "Jumlah Penarikan hanya boleh mengandung angka."
		},    		
		bank_id:{
			required: "Nomor Rekening harus diisi.",
		},    		
		password:{
			required: "Kata Sandi harus diisi.",
		},    		
	},
	showErrors: function(errorMap, errorList) {
	  	$.each(this.validElements(), function (index, element) {
			var $element = $(element);
			$element.removeClass('error');
			$element.addClass('default');
			$element.data("title", "")
			.tooltip("destroy");					
	  	});
	  	$.each(errorList, function (index, error) {
		    var $element = $(error.element);
			$element.removeClass('default');
	        $element.addClass('error');
			$element.tooltip("destroy")
				.data("title", error.message)
				.tooltip();			      
	  	});
	},
	submitHandler: function(form) {
		form.submit();
	}    	
});
$('.form-contact').validate({
	rules:{
		name:{ required: true, maxlength: 255},
		email:{ required: true, email: true, maxlength: 255},
		message:{ required: true},
	},
	messages:{
		name:{
			required: "Nama anda harus diisi.",
		},    		
		email:{
			required: "Alamat Email anda harus diisi.",
			email: "Alamat Email anda tidak valid."			
		},    		
		message:{
			required: "Pesan harus diisi.",
		}
	},
	showErrors: function(errorMap, errorList) {
	  	$.each(this.validElements(), function (index, element) {
			var $element = $(element);
			$element.removeClass('error');
			$element.addClass('default');
			$element.data("title", "")
			.tooltip("destroy");					
	  	});
	  	$.each(errorList, function (index, error) {
		    var $element = $(error.element);
			$element.removeClass('default');
	        $element.addClass('error');
			$element.tooltip("destroy")
				.data("title", error.message)
				.tooltip();			      
	  	});
	},
	submitHandler: function(form) {
		form.submit();
	}    	
});
$('.form-consult_business').validate({
	rules:{
		user_name:{ required: true, maxlength: 255},
		name:{ required: true, maxlength: 255},
		address:{ required: true},
		phone_number:{ required: true, digits: true},
		email:{ required: true, email: true, maxlength: 255}
	},
	messages:{
		user_name:{
			required: "Nama anda harus diisi.",
		},    		
		name:{
			required: "Nama Usaha anda harus diisi.",
		},    		
		address:{
			required: "Alamat anda harus diisi.",
		},    		
		phone_number:{
			required: "No. Telepon/HP anda harus diisi.",
			digits: "No. Telepon/HP hanya boleh mengandung angka."
		},    		
		email:{
			required: "Alamat Email anda harus diisi.",
			email: "Alamat Email anda tidak valid."			
		}
	},
	showErrors: function(errorMap, errorList) {
	  	$.each(this.validElements(), function (index, element) {
			var $element = $(element);
			$element.removeClass('error');
			$element.addClass('default');
			$element.data("title", "")
			.tooltip("destroy");					
	  	});
	  	$.each(errorList, function (index, error) {
		    var $element = $(error.element);
			$element.removeClass('default');
	        $element.addClass('error');
			$element.tooltip("destroy")
				.data("title", error.message)
				.tooltip();			      
	  	});
	},
	submitHandler: function(form) {
		form.submit();
	}    	
});
// END: FORM VALIDATE

//FORM VALIDATE USING MODAL
function getMonthName(mon_numeric){
	var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
	return months[mon_numeric];
}
$('.btn-reserve').click(function(){
	var d = new Date();

	var day = d.getDate();
	if(parseInt(day) >= 1 && parseInt(day) <= 9){
		day = "0" + day; //01, 02, etc
	}
	var month = getMonthName(d.getMonth());
	var year = d.getFullYear();
	var current_day_month_year = day + " " + month + ", " + year;

	if ($('#hidden_pax_id').val() == "" || $('#hidden_date_reserve_id').val() == "" || $('#hidden_time_reserve_id').val() == ""){
		$('#reservationModal').modal();
	}
	else if ($('.order-content').children().length == 0){ //has no menu reserved
		$('#orderModal').modal();
	}
	else if($('#input-date-reserve').val() == current_day_month_year){ //if reserve today
		var reserve_time = $('#input-time-reserve').val();  
		var reserve_hour = $('#input-time-reserve').val().substr(0,2); 
		var reserve_minute = $('#input-time-reserve').val().substr(3,5);

		var input_date = new Date(d.getFullYear(), d.getMonth(), d.getDate(), reserve_hour, reserve_minute);
		var curr_date = new Date().getTime();
		//check if current time has passed reservation time
		if(curr_date > input_date){
			$('#overReserveTimeModal').modal();
		}
		else{
			var hour = d.getHours();
			if(d.getMinutes() > 30){ //if minutes is over 30, current hour + 1
				hour = d.getHours() + 1;
			}
			//only get hour because faster has no risk
			var reserve_hour = $('#input-time-reserve').val().substr(0,2); 
			var diff_hours = reserve_hour - hour;
	        if(diff_hours < 0) {
				diff_hours = 24 + diff_hours;
	        }

	        if(diff_hours < 6){ //less than 6, can not reserve. 
				$('#reserveTimeLessModal').modal();
	        }
	        else{ 
	        	if((hour + 5) >= 17) { //only pay with balance
	        		$('#onlyUseBalanceModal').modal();
	        	}
	        	else{ //can pay with transfer but deadline more faster than normal
	        		$('.form-reservation').submit();
	        	}
	        }
		}
	}
	else{
		$(".form-reservation").submit();
	}	
})
$('.btn-continue-pay-with-balance').click(function(){
	var form_data = {
    	reservation_prices: $('.prices').attr('value'),
    	user_id: $('#hidden_user_id_reserve').val(),
	}
    $.ajax({
        type: "GET",
        dataType: 'json',
        url: 'http://solivis.com/reservation/check-available-balance',
        data: form_data,
		success: function (data) {
			if(data == false){
				swal("Oops", "Saldo anda tidak cukup untuk membayar reservasi", "error");
			}	
			else{
				$(".form-reservation").append(
					'<input type="hidden" name="pay_with_balance" value="1"/>'
				);
				swal({   
					title: "Saldo anda cukup untuk membayar reservasi",
					text: "Klik OK untuk lanjut",
					type: "success",
				}, function(){ 
					$(".form-reservation").submit();
				});
			}
		},
		error: function (data) {
		    console.log('Error:', data);
		}        
    });
});

$('.btn-change-reserve').click(function(){
	var d = new Date();

	var day = d.getDate();
	if(parseInt(day) >= 1 && parseInt(day) <= 9){
		day = "0" + day; //01, 02, etc
	}
	var month = getMonthName(d.getMonth());
	var year = d.getFullYear();
	var current_day_month_year = day + " " + month + ", " + year;

	if ($('#input-change-date-reserve').val() == "" || $('#input-change-time-reserve').val() == ""){
		$('#changeReservationModal').modal();
	}
	else if($('#input-change-date-reserve').val() == current_day_month_year){ //if reserve today
		var reserve_time = $('#input-change-time-reserve').val();  
		var reserve_hour = $('#input-change-time-reserve').val().substr(0,2); 
		var reserve_minute = $('#input-change-time-reserve').val().substr(3,5);

		var input_date = new Date(d.getFullYear(), d.getMonth(), d.getDate(), reserve_hour, reserve_minute);
		var curr_date = new Date().getTime();
		//check if current time has passed reservation time
		if(curr_date > input_date){
			$('#overReserveTimeModal').modal();
		}
		else{
			var hour = d.getHours();
			if(d.getMinutes() > 30){ //if minutes is over 30, current hour + 1
				hour = d.getHours() + 1;
			}
			//only get hour because faster has no risk
			var reserve_hour = $('#input-change-time-reserve').val().substr(0,2); 
			var diff_hours = reserve_hour - hour;
	        if(diff_hours < 0) {
				diff_hours = 24 + diff_hours;
	        }

	        if(diff_hours < 6){ //less than 6, can not reserve. 
				$('#reserveTimeLessModal').modal();
	        }
        	else{
        		$('.form-change-reservation-schedule').submit();
        	}
		}
	}
	else{
		$(".form-change-reservation-schedule").submit();
	}	
})

//only pay with balance
if($('#checked_balance').is(':checked')){
	$('#checked_transfer_bank').attr('disabled', true);
	$('.info-payment-results-transfer_bank').addClass('hide');
	$('.info-payment-results-ewallet').removeClass('hide');
	$('.info-payment-results-ewallet').fadeIn('slow');  	
}

$('.form-confirm_payment').submit(function(e){
	if ($('#transfer_proof').val() == ""){
		e.preventDefault();		
		$('#transferProofModal').modal();
	}
	else if($('.btn-go-to-profile').length){
		e.preventDefault();
		$('#needToAddAccountBankModal').modal();
	}
});
//END: FORM VALIDATE USING MODAL
});