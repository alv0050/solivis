$(document).ready(function(){

// FORMATTING CURRENCY
function addCommas(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}
// END: FORMATTING CURRENCY

// SCROLLING
$('html').niceScroll({cursorcolor:" #959595"});
$('.detail-filter-category').niceScroll({cursorcolor:" #959595"});
// $('.detail-filter-main').niceScroll({cursorcolor:" #959595"});
// $('.detail-filter-drinks').niceScroll({cursorcolor:" #959595"});
// $('.detail-filter-dessert').niceScroll({cursorcolor:" #959595"});
$('.group-time').niceScroll({cursorcolor:" #959595"});
// END: SCROLLING

// DATEPICKER
var event_dates = [],
	event_months = [],
	event_years = [],
	$picker_dashboard = $('#datepicker-dashboard');
	$picker_reservation = $('#datepicker-reservation');

$('.all-reservation-success-date-detail').each(function(){
	var event_reservation_full_dates = $(this).html().split(' ');
	event_dates.push(parseInt(event_reservation_full_dates[0]));
	event_months.push(parseInt(event_reservation_full_dates[1]));
	event_years.push(parseInt(event_reservation_full_dates[2]));
});
$picker_dashboard.datepicker({
	language: 'en',
	onRenderCell: function (date, cellType) {
		var event_date = date.getDate();
		var event_month = date.getMonth() + 1; // add 1 because of using index
		var event_year = date.getFullYear();
		for(var i=0; i<event_dates.length; i++){
			if(event_dates[i] == event_date && event_months[i] == event_month && event_years[i] == event_year){
				return {
					html: event_date + '<span class="dp-note"></span>'
				}					
			}
		}
	},
	onChangeMonth: function(month, year){
		var total_noted_date = $('.dp-note').length;
		$('.dp-note').each(function(){
			if($(this).parent().hasClass('-other-month-')){
				total_noted_date--;
			}
		})
		var total_reservation_per_month = $('#datepicker-dashboard').next().next().find('.total_reservation_per_month');
		total_reservation_per_month.html(total_noted_date);		
	},
	onChangeView: function(view){
		if(view == 'days'){
			var total_noted_date = $('.dp-note').length;
			$('.dp-note').each(function(){
				if($(this).parent().hasClass('-other-month-')){
					total_noted_date--;
				}
			})
			var total_reservation_per_month = $('#datepicker-dashboard').next().next().find('.total_reservation_per_month');
			total_reservation_per_month.html(total_noted_date);		
		}
	}
})
// SHOW MENU DETAIL
function getMonthName(mon_numeric){
	var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Agt", "Sep", "Oct", "Nov", "Dec"];
	return months[mon_numeric];
}
var total_reservations;
var reservation_date_event;
var reservation_month_and_year_event;
var reservation_user_name_event;
var reservation_paxes_event;
var reservation_time_event;
var reservation_food_names;
var reservation_food_counts;
var reservation_food_names_event;
var reservation_food_counts_event;
var event_date_noted = [];

$picker_reservation.datepicker({
	language: 'en',
	onRenderCell: function (date, cellType) {
		var event_date = date.getDate();
		var event_month = date.getMonth() + 1; // add 1 because of using index
		var event_year = date.getFullYear();
		for(var i=0; i<event_dates.length; i++){
			if(event_dates[i] == event_date && event_months[i] == event_month && event_years[i] == event_year){
				return {
					html: event_date + '<span class="dp-note"></span>'
				}					
			}
		}
	},
	onChangeMonth: function(month, year){
		var total_noted_date = $('.dp-note').length;
		$('.dp-note').each(function(){
			if($(this).parent().hasClass('-other-month-')){
				total_noted_date--;
			}
		})
		var total_reservation_per_month = $('#datepicker-reservation').next().next().find('.total_reservation_per_month');
		total_reservation_per_month.html(total_noted_date);		
	},
	onChangeView: function(view){
		if(view == 'days'){
			var total_noted_date = $('.dp-note').length;
			$('.dp-note').each(function(){
				if($(this).parent().hasClass('-other-month-')){
					total_noted_date--;
				}
			})
			var total_reservation_per_month = $('#datepicker-reservation').next().next().find('.total_reservation_per_month');
			total_reservation_per_month.html(total_noted_date);		
		}
	},
	onSelect: function onSelect(fd, date) {
		total_reservations = 0;
		reservation_date_event = [];
		reservation_month_and_year_event = [];
		reservation_user_name_event = [];
		reservation_paxes_event = [];
		reservation_time_event = [];
		reservation_food_names_event = [];
		reservation_food_counts_event = [];

		var total_reservation_per_month = $('#datepicker-reservation').next().next().find('.total_reservation_per_month');
		total_reservation_per_month.html(total_noted_date);		

		// If date with event is selected, show it
		if (date && event_dates.indexOf(date.getDate()) != -1) {
			$('.all-reservation-hidden-detail').each(function(){
				$('.sub-schedule-container').removeClass('hide');
				var reservation_full_date = $(this).children('.all-reservation-date-detail').html();
				var reservation_username = $(this).children('.all-reservation-user-name-detail').html();
				var reservation_paxes = $(this).children('.all-reservation-paxes-detail').html();
				var reservation_time = $(this).children('.all-reservation-time-detail').html();

				var event_date = date.getDate();
				var event_month = date.getMonth() + 1;
				var event_year = date.getFullYear();

				if(parseInt(event_date) >= 1 && parseInt(event_date) <= 9){
					event_date = "0" + event_date; //01, 02, etc
				}

				var event_fulldate = event_date+" "+event_month+" "+event_year;
				if(event_fulldate == reservation_full_date){
					reservation_food_names = "";
					reservation_food_counts = "";

					reservation_food_names = $(this).children('.all-reservation-food-names').html();
					reservation_food_names_event.push(reservation_food_names);

					reservation_food_counts = $(this).children('.all-reservation-food-counts').html();
					reservation_food_counts_event.push(reservation_food_counts);

					total_reservations += 1;
					var event_month_name = getMonthName(date.getMonth());

					reservation_date_event.push(event_date);
					reservation_month_and_year_event.push(event_month_name+" "+event_year);
					reservation_user_name_event.push(reservation_username);
					reservation_paxes_event.push(reservation_paxes);
					reservation_time_event.push(reservation_time);

					$('.reservation_date_event').html(event_date);
					$('.reservation_month_and_year_event').html(event_month_name+" "+event_year);
				}
			});

			$('.reservation-event-detail').children().remove();
			$('.sub-schedule-container .total_reservations').html(total_reservations);

			for(var i=0; i<total_reservations; i++){
				$('.reservation-event-detail').append(
					'<li>Reservasi atas nama <span class="reservation_user_name_event">'+reservation_user_name_event[i]+'</span>, ' +
					'sebanyak <span class="reservation_paxes_event">'+ reservation_paxes_event[i] +'</span> ' +
					'orang datang jam <span class="reservation_time_event">'+ reservation_time_event[i] +'</span>.'+
						'<button class="btn btn-view-menu" value="close">Lihat Menu</button>' +
					'</li>' +
					'<table class="table table-bordered table-striped table-menu-order table-menu-'+ i +'">' +
						'<thead>' +
							'<tr>' +
								'<th>Menu</th>' +
								'<th class="text-center">@</th>' +
							'</tr>' +
						'</thead>' +
						'<tbody>' +
						'</tbody>' +
					'</table>'	
				);

				var reservation_food_names_detail = reservation_food_names_event[i].split(',');
				reservation_food_names_detail.splice(-1,1); //remove "" in last index
				var reservation_food_counts_detail = reservation_food_counts_event[i].split(',');
				reservation_food_counts_detail.splice(-1,1); //remove "" in last index
				
				for(var j=0; j<reservation_food_names_detail.length; j++){
				    $('.table-menu-'+ i +' tbody').append(	    	
				        '<tr>' + 
				          '<td>' + reservation_food_names_detail[j] + '</td>' +
				          '<td class="text-center">' + reservation_food_counts_detail[j] + '</td>' +
				        '</tr>'
					);
				}
			}
		}
		else{
			$('.sub-schedule-container').addClass('hide');			
		}
	}	
})
if($('#datepicker-dashboard').length){
	var total_noted_date = $('.dp-note').length;
	$('.dp-note').each(function(){
		if($(this).parent().hasClass('-other-month-')){
			total_noted_date--;
		}
	})
	var total_reservation_per_month = $('#datepicker-dashboard').next().next().find('.total_reservation_per_month');
	total_reservation_per_month.html(total_noted_date);
}

if($('#datepicker-reservation').length){
	var total_noted_date = $('.dp-note').length;
	$('.dp-note').each(function(){
		if($(this).parent().hasClass('-other-month-')){
			total_noted_date--;
		}
	})
	var total_reservation_per_month = $('#datepicker-reservation').next().next().find('.total_reservation_per_month');
	total_reservation_per_month.html(total_noted_date);
}

// END: DATEPICKER

// INCOME (Jan until Dec)
var income_per_month = [];
$('.income_per_month').each(function(){
	income_per_month.push($(this).val());
});
var restaurantIncomeData = {
  labels : [
    "Jan", 
    "Feb", 
    "Mar", 
    "Apr", 
    "Mei", 
    "Jun", 
    "Jul", 
    "Agt", 
    "Sep", 
    "Okt", 
    "Nov", 
    "Des"
  ],
  datasets : [
    {
      fillColor : "rgba(254,216,145,1)",
      strokeColor : "rgba(253,175,31,1)",
      pointColor : "rgba(253,175,31,1)",
      pointStrokeColor : "#fff",
      data : income_per_month
    }
  ]
}
//only dashboard can load the data
if(document.getElementById("income") != null){
	var ctx = document.getElementById("income").getContext("2d");
	window.myLine = new Chart(ctx).Line(restaurantIncomeData, {
		responsive: true
	});
}
// END: INCOME

// MODAL
$('.view-link').click(function(e){
	e.preventDefault();
	var food_names = $(this).children('.food-names').html().split(',');
	food_names.splice(-1,1); //remove "" in last index

	var food_counts = $(this).children('.food-counts').html().split(',');
	food_counts.splice(-1,1);

	var food_prices = $(this).children('.food-prices').html().split(',');
	food_prices.splice(-1,1);

	var total_food_prices = 0;

	var reservation_id_detail = $(this).children('.reservation-id-detail').html();
	var reservation_user_name_detail = $(this).children('.reservation-user-name-detail').html();
	var reservation_date_detail = $(this).children('.reservation-date-detail').html();
	var reservation_time_detail = $(this).children('.reservation-time-detail').html();
	var reservation_paxes_detail = $(this).children('.reservation-paxes-detail').html();

	$('#newReservationModal #reservation_id_detail').html(": " + reservation_id_detail);
	$('#newReservationModal #reservation_user_name_detail').html(": " + reservation_user_name_detail);
	$('#newReservationModal #reservation_date_detail').html(": " + reservation_date_detail);
	$('#newReservationModal #reservation_time_detail').html(": " + reservation_time_detail);
	$('#newReservationModal #reservation_paxes_detail').html(": " + reservation_paxes_detail);

    $('#newReservationModal tbody').children().remove();	

	for(var i=0; i<food_names.length; i++){
		total_food_prices += parseInt(food_prices[i]);
	    $('#newReservationModal tbody').append(	    	
	        '<tr>' + 
	          '<td>' + food_names[i] + '</td>' +
	          '<td class="text-center">' + food_counts[i] + '</td>' +
	          '<td class="text-right">' + addCommas(food_prices[i]) + '</td>' +
	        '</tr>'
		);
	}
	$('.total-pay span').html("Rp. " + addCommas(total_food_prices));
	$('.total-fee-per-transaction span').html("Rp. " + addCommas(total_food_prices * 0.02));
	$('.total-net-income span').html("Rp. " + addCommas(total_food_prices - (total_food_prices * 0.02)));
	$('#newReservationModal #reservation_total_payment_detail').html(": " + addCommas(total_food_prices));	

	var status_reservation =  $(this).children('.status-reservation').html();

	$('.form-put-reservation-result > .group-answer-reservation > .group-button').children().remove();
	$('.form-put-reservation-result > .group-answer-reservation > .info-decline').html("");
	$('.form-put-reservation-result > .group-answer-reservation > .group-decline-reservation').children().remove();
	$('.form-put-reservation-result > .group-answer-reservation').addClass('hide');

	$('.decline-agreement').html("");
	$('.decline-agreement').children().remove();
	// if still new reservation
	if (status_reservation == 3){
		$('.modal-title > img').attr('src', '/images/restaurants/new.png').attr('alt', 'new');
		$('.modal-title > span').html("New Reservation");

		$('.form-put-reservation-result > .group-answer-reservation').removeClass('hide');		
		$('.form-put-reservation-result > .group-answer-reservation > .group-button').append(
            '<div class="col-sm-6">' + 
                '<button class="btn text-uppercase btn-accept" value="on">Accept</button>' + 
            '</div>' + 
            '<div class="col-sm-6">' + 
                '<button class="btn text-uppercase btn-decline" type="button">Decline</button>' + 
            '</div>' +
            '<input type="hidden" id="reservation_id_detail_hidden" name="reservation_id_hidden">' +
            '<div class="clearfix"></div>'
		);

		$('.info-decline').html("Berikan keterangan apabila anda menolak reservasi ini.");

		$('.form-put-reservation-result > .group-answer-reservation > .group-decline-reservation').append(
			'<textarea class="form-control default-black" name="reason" cols="50" rows="10"></textarea>' +
			'<div class="group-button-bottom">' +
				'<button class="btn text-uppercase btn-back" type="button">Back</button>' +
				'<button class="btn text-uppercase btn-send">Send</button>' +
			'</div>' +
			'<div class="clearfix"></div>'
		);

		$('.decline-agreement').append(
			'<img src="/images/restaurants/warning.png" alt="warning">Dengan menekan tombol <span>SEND</span>, maka Anda telah yakin menolak reservasi ini.'
		)

		$('#newReservationModal #reservation_id_detail_hidden').val(reservation_id_detail);
	}
	else if(status_reservation == 4){ //SUKSES
		$('.modal-title > img').attr('src', '/images/restaurants/accept.png').attr('alt', 'accept');
		$('.modal-title > span').html("Accepted Reservation");
	}
	else{ //BATAL
		$('.modal-title > img').attr('src', '/images/restaurants/decline.png').attr('alt', 'decline');
		$('.modal-title > span').html("Declined Reservation");
	}

    $('#newReservationModal').modal();
});
//LISTEN TO EVENT CLOSE MODAL
$('#newReservationModal').on('hidden.bs.modal', function () {
	if ($('.btn-accept').val() == 'off'){
		$('.btn-accept').val('on');
		//change back to green
		$('.btn-accept').css('background-color', '#8dc63f');
		$('.info-decline').removeClass('hide');
		$('.group-decline-reservation').addClass('hide');
		$('.decline-agreement').addClass('hide');		
	}
})

$('.btn-withdraw').click(function(){
    $('#withdrawModal').modal();
});

$('.btn-detail-balance').click(function(){
    $('#detailBalanceModal').modal();
});

$('.btn-add-menu').click(function(){
    $('#addMenuModal').modal();
});

$('.btn-edit-menu').click(function(){
    $('#editMenuModal').modal();
});
$('.food-photo').click(function(e){
	e.preventDefault();
	var food_photo_name = $(this).attr('food-photo-name');	
	var food_photo_url = $(this).attr('food-photo-url');	
	var food_photo_id = $(this).attr('food-photo-id');

    $('#foodPhotoContainerModal').modal();

    $('.food_photo_name_modal').text(food_photo_name);
    $('.food_photo_name_url').attr('src', '/images/restaurant_uploads/food_photos/'+food_photo_url).attr('alt', food_photo_name); 
    $('.food_photo_url_hidden').val(food_photo_url);
    $('.food_photo_id_hidden').val(food_photo_id);
});
$('.inside-restaurant-photo').click(function(e){
	e.preventDefault();
	var inside_restaurant_photo_name = $(this).attr('inside-restaurant-photo-name');	
	var inside_restaurant_photo_url = $(this).attr('inside-restaurant-photo-url');	
	var inside_restaurant_photo_id = $(this).attr('inside-restaurant-photo-id');

    $('#insideRestaurantPhotoContainerModal').modal();

    $('.inside_restaurant_photo_name_modal').text(inside_restaurant_photo_name);
    $('.inside_restaurant_photo_name_url').attr('src', '/images/restaurant_uploads/inside_restaurant_photos/'+inside_restaurant_photo_url).attr('alt', inside_restaurant_photo_name); 
    $('.inside_restaurant_url_hidden').val(inside_restaurant_photo_url);
    $('.inside_restaurant_id_hidden').val(inside_restaurant_photo_id);
});

$('.btn-print-report').click(function(){
	$('#printReportModal').modal();
});

$('.btn-add-account-bank').click(function(){
	$('#addAccountBankModal').modal();
});
$('.btn-edit-account-bank').click(function(){
	$('#editAccountBankModal').modal();
});

// ACCOUNT NUMBER AND NAME
// DEFAULT
if ($('.restaurant_management-account-bank_name:first').html() != ""){
	var bank_name = $('.restaurant_management-account-bank_name:first').html();
	var account_number = $('.restaurant_management-account-bank_name:first').next().html();
	var account_name = $('.restaurant_management-account-bank_name:first').next().next().html();

	$('#accountNumber').val(account_number);
	$('#accountName').val(account_name);

	//edit bank
	var restaurant_bank_id = $('.restaurant_management-account-bank_name:first').next().next().next().html();
	var bank_id = $('.restaurant_management-account-bank_name:first').next().next().next().next().html();
	$('.form-edit-bank .restaurant_bank_id_hidden').val(restaurant_bank_id);
	$('.form-edit-bank .account_number_detail').val(account_number);
	$('.form-edit-bank .account_name_detail').val(account_name);
	$('.form-edit-bank .account_bank_id_detail').val(bank_id);
	$('.form-edit-bank .bank_id_ori_hidden').val(bank_id);

	//delete bank
	$('.form-delete-bank .restaurant_bank_id_del_hidden').val(restaurant_bank_id);
}
$('#select-bank_name').change(function() {
	var current_value = $("#select-bank_name option:selected").val();
	var current_text = $("#select-bank_name option:selected").text();
	var current = current_value + "-" + current_text;

   	if ($('.restaurant_management-account-bank_name').hasClass(current)){
		var account_number = $('.restaurant_management-account-number-' + current).html();
		var account_name = $('.restaurant_management-account-name-' + current).html();
		
		$('#accountNumber').val(account_number);
		$('#accountName').val(account_name);

		//edit bank
		var restaurant_bank_id = $('.restaurant_management-account-name-' + current).next().html();
		var bank_id = $('.restaurant_management-account-name-' + current).next().next().html();

		$('.form-edit-bank .restaurant_bank_id_hidden').val(restaurant_bank_id);
		$('.form-edit-bank .account_number_detail').val(account_number);
		$('.form-edit-bank .account_name_detail').val(account_name);
		$('.form-edit-bank .account_bank_id_detail').val(bank_id);
		$('.form-edit-bank .bank_id_ori_hidden').val(bank_id);

		$('.form-delete-bank .restaurant_bank_id_del_hidden').val(restaurant_bank_id);

	}
	else{
		$('#accountNumber').val("");
		$('#accountName').val("");   	
	}
});
// END: ACCOUNT NUMBER AND NAME

//DEFAULT
$('ul.category-selection li a').click(function(e) {
	e.preventDefault();
    $('ul.category-selection li.active').removeClass('active');
    $(this).closest('li').addClass('active');

	if ($('ul.category-selection li#'+$(this).closest('li').attr('id')).hasClass('active')) {
		if ($('.form-edit-menu-category').not('.hide')){
			$('.form-edit-menu-category').addClass('hide');
		}		
		$('#form-' + $(this).closest('li').attr('id')).fadeIn('slow');
		$('#form-' + $(this).closest('li').attr('id')).removeClass('hide');

		// get form validate for only active menu
		$('#form-' + $(this).closest('li').attr('id')).validate({
			rules: {
				'item_name[]': { required: true},
				'item_price[]': { required: true, digits: true},
			},
			messages: {
				'item_name[]': {
					required: "Nama Item harus diisi."
				},
				'item_price[]': {
					required: "Harga Item harus diisi.",
					digits: "Harga Item hanya boleh mengandung angka."
				}
			},
			showErrors: function(errorMap, errorList) {
				$.each(this.validElements(), function (index, element) {
					var $element = $(element);
					$element.removeClass('error');
					$element.addClass('default-grey');
					$element.data("title", "")
							.tooltip("destroy");				
			  	});
			  	$.each(errorList, function (index, error) {
				    var $element = $(error.element);
					$element.removeClass('default-grey');
				    $element.addClass('error');
					$element.tooltip("destroy")
							.data("title", error.message)
						.tooltip();		  		
			  	});
			},
			submitHandler: function(form) {
				form.submit();
			}
		});		
	}
});
// END: MODAL

// PRINT REPORT
var window_location = $(location).attr('pathname');
if(window_location == '/restaurant-management/print-report'){
	window.print();
}
// END: PRINT-REPORT

// ORDER
// DEFAULT EXPAND
$('.detail-order').css('display', 'block');
$('.title-order').click(function(){
	if ($('.detail-order').css('display') == 'none'){
		$('.title-order > img').attr('src', '/images/restaurants/expand.png').attr('alt', 'expand');				
	}
	else{
		$('.title-order > img').attr('src', '/images/restaurants/collapse.png').attr('alt', 'collapse');
	}	
	$('.detail-order').slideToggle('fast');	
});

$(".group-answer-reservation > .group-button").on("click", ".btn-decline", function(){
	$('.btn-accept').attr('disabled', true);
	if ($('.btn-accept').val() == 'on'){
		// // change to grey
		$('.btn-accept').css('background-color', '#acacac');
		$('.info-decline').addClass('hide');
		$('.group-decline-reservation').removeClass('hide');
		$('.decline-agreement').removeClass('hide');
		$('.btn-accept').val('off');
	}

	$('.form-put-reservation-result').validate({
		rules:{
			reason:{ required: true},
		},
		messages:{
			reason:{
				required: "Alasan penolakan harus diisi.",
			}
		},
		showErrors: function(errorMap, errorList) {
			$.each(this.validElements(), function (index, element) {
				var $element = $(element);
					$element.removeClass('error');
					$element.addClass('default-black');
					$element.data("title", "")
							.tooltip("destroy");
		  	});
		  	$.each(errorList, function (index, error) {
			    var $element = $(error.element);
				$element.removeClass('default-black');
			    $element.addClass('error');
				$element.tooltip("destroy")
						.data("title", error.message)
						.tooltip();		  		
		  	});
		},
		submitHandler: function(form) {
			form.submit();
		}
	});

});
$(".group-answer-reservation > .group-decline-reservation").on("click", ".btn-back", function(){
	$('.btn-accept').attr('disabled', false);	
	if ($('.btn-accept').val() == 'off'){
		$('.btn-accept').val('on');
		//change back to green
		$('.btn-accept').css('background-color', '#8dc63f');
		$('.info-decline').removeClass('hide');
		$('.group-decline-reservation').addClass('hide');
		$('.decline-agreement').addClass('hide');		
	}
});
// END: ORDER

// VIEW MENU
$(".reservation-event-detail").on("click", ".btn-view-menu", function(){
	if ($(this).val() == 'close'){
		//change to grey
		$(this).css('background-color', '#acacac');
		$(this).parent().next().fadeIn('fast'); //table menu order
		$(this).val('open'); 
	}
	else{
		//change back to yellow
		$(this).css('background-color', '#ed8f03');
		$(this).parent().next().fadeOut('fast');
		$(this).val('close');
	}
})
// END: VIEW MENU

// DATEPICKER
$('#input_date_start').datepicker({
	language: 'en',
	autoClose: true
});
$('#input_date_end').datepicker({
	language: 'en',
	autoClose: true
})
$('#input_print_date_start').datepicker({
	language: 'en',
	autoClose: true
})
$('#input_print_date_end').datepicker({
	language: 'en',
	autoClose: true
})
// DATEPICKER
});

// RESTAURANT PROFILE
$('.filter-title-cuisine').removeClass('border-filter-spc-collapse').addClass('border-filter-spc-expand');

$('.filter-title-cuisine').click(function(){
	//if display of element before was none
	if ($('.detail-filter-cuisine').css('display') == 'none'){
		$('.filter-title-cuisine > img').attr('src', '/images/restaurants/expand.png').attr('alt', 'expand');
		$('.filter-title-service').removeClass('border-filter-spc-collapse').addClass('border-filter-spc-expand');
	}
	else{
		$('.filter-title-cuisine > img').attr('src', '/images/restaurants/collapse.png').attr('alt', 'collapse');		
		$('.filter-title-service').removeClass('border-filter-spc-expand').addClass('border-filter-spc-collapse');
	}	
	$('.detail-filter-cuisine').slideToggle('fast');
});
$('.filter-title-service').click(function(){
	//if display of element before was none
	if ($('.detail-filter-service').css('display') == 'none'){
		$('.filter-title-service > img').attr('src', '/images/restaurants/expand.png').attr('alt', 'expand');				
	}
	else{
		$('.filter-title-service > img').attr('src', '/images/restaurants/collapse.png').attr('alt', 'collapse');		
	}	
	$('.detail-filter-service').slideToggle('fast');
});
// END: RESTAURANT PROFILE

// MENU
$('.detail-filter-category:first').css('display', 'block');

$('.filter-title-category').click(function(){
	//if display of element before was none
	if ($('#detail-'+$(this).attr('id')).css('display') == 'none'){
		$('#' +$(this).attr('id')+ ' > img').attr('src', '/images/restaurants/expand.png').attr('alt', 'expand');
		$(this).css({"border-bottom":"1px solid #ebebeb"})		
	}
	else{
		$('#' +$(this).attr('id')+ ' > img').attr('src', '/images/restaurants/collapse.png').attr('alt', 'collapse');		
		$(this).css({"border-bottom":"none"})
	}	
	$('#detail-'+$(this).attr('id')).slideToggle('fast');
});
// END: MENU

// ACCOUNT NUMBER AND NAME
// DEFAULT
if ($('.restaurant_management-account-bank_name').hasClass('Mandiri')){
	var account_number = $('.restaurant_management-account-bank_name').next().html();
	var account_name = $('.restaurant_management-account-bank_name').next().next().html();

	$('#accountNumber').val(account_number);
	$('#accountName').val(account_name);
}
$('#select-bank_name').change(function() {
   var current = $(this).val();
   if ($('.restaurant_management-account-bank_name').hasClass(current)){
		var account_number = $('.account-number-' + current).html();
		var account_name = $('.account-name-' + current).html();
		
		$('#accountNumber').val(account_number);
		$('#accountName').val(account_name);
   }
   else{
		$('#accountNumber').val("");
		$('#accountName').val("");   	
   }
});
// END: ACCOUNT NUMBER AND NAME

// CHECKBOX OPEN DAY
$('#check-alldays').click(function() {
	if($(this).is(':checked')){
		$('.all-days').prop('checked', true);
	}
	else{
		$('.all-days').prop('checked', false);
	}
});
$('.all-days').click(function() {
	$(".all-days").each(function(){
		if($(this).prop('checked') == false){
			$('#check-alldays').prop('checked', false);
		}
	});
});
// END: CHECKBOX OPEN DAY

// OPEN DAYS
$('.restaurant-open_day').each(function(){
	var value = $(this).html();
	$('#day-'+value).prop('checked', true);
})
for(var i=1; i<=7; i++){
	$('#check-alldays').prop('checked', true);	
	if ($('#day-'+i).prop('checked') == false){
		$('#check-alldays').prop('checked', false);
		break;
	}
}
// END: OPEN DAYS

// CUISINE
$('.restaurant-cuisine').each(function(){
	var value = $(this).html();
	$('#cuisine-'+value).prop('checked', true);
})
// END: CUISINE

// SERVICE
$('.restaurant-service').each(function(){
	var value = $(this).html();
	$('#service-'+value).prop('checked', true);
})
// END: SERVICE

// QUICKLY UPLOAD IMAGE PROFILE
$('.upload').change(
	function() { 
		this.form.submit(); 
	}
);
// END: QUICKLY UPLOAD IMAGE PROFILE

//ADD MENU
//EVENT CLICK FOR APPEND OR PREPEND ELEMENT
$(".add-menu-content-group").on("click", ".btn-add-per-item", function(){
	$('.add-menu-content-group').append(
		'<div class="add-menu-content">' +
		    '<div class="col-sm-6">' +
		        '<div class="form-group">' +
		            '<input class="form-control default-grey" placeholder="Nama Item" name="item_name[]" type="text">' +
		        '</div>' +
		        '<div class="form-group">' +
		            '<textarea class="form-control default-grey" placeholder="Deskripsi Item (opsional)" name="item_description[]" cols="50" rows="10"></textarea>' +
		        '</div>' +
		    '</div>' +
		    '<div class="col-sm-6">' +
		        '<div class="form-group price-group">' +
		            '<input class="form-control default-grey" placeholder="Harga Item" name="item_price[]" type="text">' +
		        '</div>' +
		        '<div class="button-group">' +
                    '<button class="btn btn-add-per-item" type="button"><img src="/images/restaurants/add-small.png" alt="add-small">Tambah</button>' +
		           '<button class="btn btn-remove-per-item" type="button"><img src="/images/restaurants/remove-small.png" alt="add-small">Hapus</button>' +
		        '</div>' +
		        '<div class="clearfix"></div>' +
		    '</div>' +
		    '<div class="clearfix"></div>' +
		'</div>'
	);	
});

$(".add-menu-content-group").on("click",".btn-remove-per-item", function (){ 
	$(this).closest( ".add-menu-content" ).remove();
});

//END: ADD MENU

// REMOVE MENU ITEM EDIT
var removed_menus = [];
var removed_menu_uniques = [];

$('.btn-remove-per-item-edit').click(function(){
	removed_menus.push($(this).attr('menu-id'));
	var current_btn = $(this);
	$.each(removed_menus, function(i, el){
	    if($.inArray(el, removed_menu_uniques) === -1){
		    removed_menu_uniques.push(el);
			current_btn.closest('.form-edit-menu-category').append(
				'<input name="removed_menu[]" type="hidden" value="' + el + '">'
			);
	    }
	});
	$($(this).closest('.edit-menu-content')).remove();	
})
// REMOVE MENU ITEM EDIT

// GET TOTAL NEW RESERVATION
if ($('.new_reservation').length == 0){
	$('.upcoming-reservation-container > h3 > .badge').addClass('hide');
}
else{
	$('.upcoming-reservation-container > h3 > .badge').html($('.new_reservation').length);	
}
// END: GET TOTAL NEW RESERVATION

// SORT REPORT 
$('.sort-report').change(
	function() { 
		this.form.submit(); 
	}
);
// END: SORT REPORT

function getParameterByName(name) {
    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}
// CHANGE REPORT COMBO BOX VALUE AFTER SORT
var url = window.location.href;
if(url.indexOf('?sort_report=') != -1){
	$('.sort-report').val(getParameterByName('sort_report'));
}
// END: CHANGE REPORT COMBO BOX VALUE AFTER SORT

// // DELETE BANK
$('.form-delete-bank .btn-delete-account-bank').click(function(){
	//only delete if user confirm Yes
	swal({   
		title: "Apakah anda yakin?",
		showCancelButton: true,   
		type: "warning",
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Ya", 
		closeOnConfirm: false,
		cancelButtonText: 'Tidak'
	}, function(){   
		$('.form-delete-bank').submit();
	});	
})
// END: DELETE BANK

// FORM VALIDATE
$('.form-login').validate({
	rules:{
		email:{ required: true, email: true},
		password:{ required: true }
	},
	messages:{
		email:{
			required: "Alamat Email harus diisi.",
			email: "Alamat Email tidak valid.",
		},
		password: {
			required: "Kata Sandi harus diisi.",
		}    		
	},
	showErrors: function(errorMap, errorList) {
		$.each(this.validElements(), function (index, element) {
			var $element = $(element);
				$element.removeClass('error');
				$element.addClass('default');
				$element.data("title", "")
						.tooltip("destroy");
	  	});
	  	$.each(errorList, function (index, error) {
		    var $element = $(error.element);
			$element.removeClass('default');
		    $element.addClass('error');
			$element.tooltip("destroy")
					.data("title", error.message)
					.tooltip();		  		
	  	});
	},
	submitHandler: function(form) {
		form.submit();
	}
});
$('.form-change-password').validate({
	rules: {
		old_password: { required: true },
		password: { required: true, minlength: 6},
		password_confirmation:{ required: true, equalTo: "#new_password"},
	},
	messages: {
		old_password: {
			required: "Password lama harus diisi.",
		},
		password: {
			required: "Password baru harus diisi.",
			minlength: "Password baru terlalu pendek, minimal 6 karakter."
		},
		password_confirmation: {
			required: "Ulangi Password baru harus diisi.",
			equalTo: "Ulangi Password baru tidak sama dengan Password baru."
		},
	},
	showErrors: function(errorMap, errorList) {
		$.each(this.validElements(), function (index, element) {
			var $element = $(element);
			$element.removeClass('error');
			$element.addClass('default-black');
			$element.data("title", "")
					.tooltip("destroy");				
	  	});
	  	$.each(errorList, function (index, error) {
		    var $element = $(error.element);
			$element.removeClass('default-black');
		    $element.addClass('error');
			$element.tooltip("destroy")
					.data("title", error.message)
					.tooltip();		  		
	  	});
	},
	submitHandler: function(form) {
		form.submit();
	}
});
$('.form-save-bank').validate({
	rules: {
		account_number: { required: true, digits: true },
		account_name: { required: true},
	},
	messages: {
		account_number: {
			required: "Nomor Rekening harus diisi.",
			digits: "Nomor Rekening hanya boleh mengandung angka."
		},
		account_name: {
			required: "Nama Pemilik Rekening harus diisi."
		}
	},
	showErrors: function(errorMap, errorList) {
		$.each(this.validElements(), function (index, element) {
			var $element = $(element);
			$element.removeClass('error');
			$element.addClass('default');
			$element.data("title", "")
					.tooltip("destroy");				
	  	});
	  	$.each(errorList, function (index, error) {
		    var $element = $(error.element);
			$element.removeClass('default');
		    $element.addClass('error');
			$element.tooltip("destroy")
					.data("title", error.message)
				.tooltip();		  		
	  	});
	},
	submitHandler: function(form) {
		form.submit();
	}
});
$('.form-change-profile-info').validate({
	rules: {
		name: { required: true},
		address: { required: true},
		email: { required: true, email: true },
		phone_number: { required: true, digits: true },
		open_time: { required: true},
		close_time: { required: true},
	},
	messages: {
		name: {
			required: "Nama harus diisi.",
		},
		address: {
			required: "Alamat harus diisi.",
		},
		email: {
			required: "Alamat Email harus diisi.",
			email: "Alamat Email tidak valid."
		},
		phone_number: {
			required: "No. Telp / Hp harus diisi.",
			digits: "No. Telp / Hp hanya boleh mengandung angka."
		},
		open_time: {
			required: "Jam Buka harus diisi.",
		},
		close_time: {
			required: "Jam Tutup harus diisi.",
		}
	},
	showErrors: function(errorMap, errorList) {
		$.each(this.validElements(), function (index, element) {
			var $element = $(element);
			$element.removeClass('error');
			$element.addClass('default-grey');
			$element.data("title", "")
					.tooltip("destroy");				
	  	});
	  	$.each(errorList, function (index, error) {
		    var $element = $(error.element);
			$element.removeClass('default-grey');
		    $element.addClass('error');
			$element.tooltip("destroy")
					.data("title", error.message)
				.tooltip();		  		
	  	});
	},
	submitHandler: function(form) {
		form.submit();
	}
});
$('.form-add-menu').validate({
	rules: {
		'category_name': { required: true },
		'item_name[]': { required: true},
		'item_price[]': { required: true, digits: true},
	},
	messages: {
		'category_name': {
			required: "Nama Kategori harus diisi."
		},
		'item_name[]': {
			required: "Nama Item harus diisi."
		},
		'item_price[]': {
			required: "Harga Item harus diisi.",
			digits: "Harga Item hanya boleh mengandung angka."
		}
	},
	showErrors: function(errorMap, errorList) {
		$.each(this.validElements(), function (index, element) {
			var $element = $(element);
			$element.removeClass('error');
			$element.addClass('default-grey');
			$element.data("title", "")
					.tooltip("destroy");				
	  	});
	  	$.each(errorList, function (index, error) {
		    var $element = $(error.element);
			$element.removeClass('default-grey');
		    $element.addClass('error');
			$element.tooltip("destroy")
					.data("title", error.message)
				.tooltip();		  		
	  	});
	},
	submitHandler: function(form) {
		form.submit();
	}
});
$('.form-withdraw').validate({
	rules:{
		balance_processed:{ required: true, digits: true},
		restaurant_bank_id:{ required: true},
		password:{ required: true},
	},
	messages:{
		balance_processed:{
			required: "Jumlah Penarikan harus diisi.",
			digits: "Jumlah Penarikan hanya boleh mengandung angka."
		},    		
		restaurant_bank_id:{
			required: "Nomor Rekening harus diisi.",
		},    		
		password:{
			required: "Kata Sandi harus diisi.",
		},    		
	},
	showErrors: function(errorMap, errorList) {
	  	$.each(this.validElements(), function (index, element) {
			var $element = $(element);
			$element.removeClass('error');
			$element.addClass('default-grey');
			$element.data("title", "")
			.tooltip("destroy");					
	  	});
	  	$.each(errorList, function (index, error) {
		    var $element = $(error.element);
			$element.removeClass('default-grey');
	        $element.addClass('error');
			$element.tooltip("destroy")
				.data("title", error.message)
				.tooltip();			      
	  	});
	},
	submitHandler: function(form) {
		form.submit();
	}    	
});
$('.form-print-report-ok').validate({
	rules:{
		date_start:{ required: true},
		date_end:{ required: true},
	},
	messages:{
		date_start:{
			required: "Tanggal Mulai harus diisi.",
		},    		
		date_end:{
			required: "Tanggal Akhir harus diisi.",
		}
	},
	showErrors: function(errorMap, errorList) {
	  	$.each(this.validElements(), function (index, element) {
			var $element = $(element);
			$element.removeClass('error');
			$element.addClass('default-black');
			$element.data("title", "")
			.tooltip("destroy");					
	  	});
	  	$.each(errorList, function (index, error) {
		    var $element = $(error.element);
			$element.removeClass('default-black');
	        $element.addClass('error');
			$element.tooltip("destroy")
				.data("title", error.message)
				.tooltip();			      
	  	});
	},
	submitHandler: function(form) {
		form.submit();
	}    	
});
$('.form-add-bank').validate({
	rules: {
		account_number: { required: true, digits: true },
		account_name: { required: true},
		password: { required: true},
	},
	messages: {
		account_number: {
			required: "Nomor Rekening harus diisi.",
			digits: "Nomor Rekening hanya boleh mengandung angka."
		},
		account_name: {
			required: "Nama Pemilik Rekening harus diisi."
		},
		password: {
			required: "Password harus diisi."
		}
	},
	showErrors: function(errorMap, errorList) {
		$.each(this.validElements(), function (index, element) {
			var $element = $(element);
			$element.removeClass('error');
			$element.addClass('default');
			$element.data("title", "")
					.tooltip("destroy");				
	  	});
	  	$.each(errorList, function (index, error) {
		    var $element = $(error.element);
			$element.removeClass('default');
		    $element.addClass('error');
			$element.tooltip("destroy")
					.data("title", error.message)
					.tooltip();		  		
	  	});
	},
	submitHandler: function(form) {
		form.submit();
	}
});
$('.form-edit-bank').validate({
	rules: {
		account_number: { required: true, digits: true },
		account_name: { required: true},
		password: { required: true},
	},
	messages: {
		account_number: {
			required: "Nomor Rekening harus diisi.",
			digits: "Nomor Rekening hanya boleh mengandung angka."
		},
		account_name: {
			required: "Nama Pemilik Rekening harus diisi."
		},
		password: {
			required: "Password harus diisi."
		}
	},
	showErrors: function(errorMap, errorList) {
		$.each(this.validElements(), function (index, element) {
			var $element = $(element);
			$element.removeClass('error');
			$element.addClass('default');
			$element.data("title", "")
					.tooltip("destroy");				
	  	});
	  	$.each(errorList, function (index, error) {
		    var $element = $(error.element);
			$element.removeClass('default');
		    $element.addClass('error');
			$element.tooltip("destroy")
					.data("title", error.message)
					.tooltip();		  		
	  	});
	},
	submitHandler: function(form) {
		form.submit();
	}
});
// END: FORM VALIDATE

