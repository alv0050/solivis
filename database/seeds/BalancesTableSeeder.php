<?php

use Solivis\User;
use Illuminate\Database\Seeder;
use Carbon\Carbon;

class BalancesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$id = '10'; // user ID
        $balance_processed = 5000;

		$user = User::find($id);
		$balance = $user->balance;
		$balance += $balance_processed; 

        //update user balance
        $user->balance = $balance;
        $user->save();

        //status_balance_id
        // 1 Reservasi ditolak (dana dikembalikan)
        // 2 Kelebihan transfer (dana dikembalikan)

        //insert to history_balance
        DB::table('history_balances')->insert([
            'process_date' => Carbon::now(),
            'balance_processed' => $balance_processed,
            'status_balance_id' => '2',
            'user_id' => $id,
            'transfer_target' => 'Ke saldo solivis'
        ]);  
    }
}
