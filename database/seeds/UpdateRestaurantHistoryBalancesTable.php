<?php

use Solivis\Restaurant;
use Solivis\RestaurantBank;
use Solivis\RestaurantHistoryBalance;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UpdateRestaurantHistoryBalancesTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$id = '3'; // restaurant_management ID
        $history_balance_id = '5'; //History Balance id

        $history_balance = RestaurantHistoryBalance::find($history_balance_id);
        $balance_processed = $history_balance->balance_processed;
        $restaurant_bank_id = $history_balance->restaurant_bank_id;

        // $balance_processed = 3000; //*
        // $restaurant_bank_id = 1; //*
        // $restaurant_bank = RestaurantBank::find($restaurant_bank_id); //*

        //status_balance_id
        // 4 Dana telah dikirim (tuntas)

        //update history_balance from process to done
        $history_balance->update([
            'process_date' => Carbon::now(),
            // 'balance_processed' => $balance_processed, //*
            'status_balance_id' => '4',
            // 'transfer_target' => $restaurant_management_bank->bank->name."-".$restaurant_management_bank->account_number." a/n ".$restaurant_management_bank->account_name;
        ]); 

    }
}
