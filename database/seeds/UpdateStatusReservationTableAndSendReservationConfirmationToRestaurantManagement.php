<?php

use Solivis\Reservation;
use Solivis\Menu;
use \Snowfire\Beautymail\Beautymail as BeautyMail;
use Solivis\Commands\GetTotalReservationPriceCommand;
use Illuminate\Database\Seeder;

// php artisan db:seed --class=UpdateStatusReservationTableAndSendReservationConfirmationToRestaurantManagement

class UpdateStatusReservationTableAndSendReservationConfirmationToRestaurantManagement extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	/*ONLY EDIT RESERVATION_ID IF EVERYTHING RIGHT*/
		$reservation_id = '4'; // reservation ID

        $status_reservation_id = '3'; //Telah Bayar

        $reservation = Reservation::find($reservation_id);
        //update reservation from process to has paid
        $reservation->update([
            'status_reservation_id' => $status_reservation_id,
        ]); 
        $user_name = $reservation->user->name;
        $reservation_date = $reservation->convertDate();
        $reservation_time = $reservation->convertTime();
        $paxes = $reservation->paxes;

        $reservation_menus = $reservation->reservation_menus;
        $getTotalReservationPriceCommand = new GetTotalReservationPriceCommand();        

        for($i=0; $i<count($reservation_menus); $i++){
            $getTotalReservationPriceCommand->setTotalPrice($reservation_menus[$i]);
        }

        $total_reservation_price = $getTotalReservationPriceCommand->getTotalPrice();

        $restaurant_management_name = $reservation->restaurant_management->name;
        $restaurant_management_email = $reservation->restaurant_management->email;

        //deadline for restaurant to response reservation (1 hour need to verify)
        $deadline_date = Carbon::parse(DeadlineResponseDate::where('reservation_id', $reservation->id)->first()->deadline_date)->addHour(2);
        $deadline_date_only = $reservation->convertToCommonDate($deadline_date);
        $deadline_time_only = $reservation->convertToCommonTime($deadline_date);

        $logo['path'] = '';
        $logo['width'] = '0px';
        $logo['height'] = '0px';
        $beautymail = app()->make(Beautymail::class);
        $beautymail->send('emails.new_reservation', 
            [
                'logo' => $logo,
                'reservation_id' => $reservation_id, 
                'user_name' => $user_name, 
                'reservation_date' => $reservation_date, 
                'reservation_time' => $reservation_time, 
                'paxes' => $paxes, 
                'total_reservation_price' => $total_reservation_price, 
                'reservation_menus' => $reservation_menus, 
                'deadline_date_only' => $deadline_date_only, 
                'deadline_time_only' => $deadline_time_only, 
            ],  function($message) use ($restaurant_management_name, $restaurant_management_email){
                    $message->to($restaurant_management_email, $restaurant_management_name)
                            ->subject('Reservasi Baru');
        }); 

    }
}
