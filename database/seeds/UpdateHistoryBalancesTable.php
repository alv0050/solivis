<?php

use Solivis\User;
use Solivis\UserBank;
use Solivis\HistoryBalance;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UpdateHistoryBalancesTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$id = '10'; // user ID
        $history_balance_id = '58'; //History Balance id

        $history_balance = HistoryBalance::find($history_balance_id);
        $balance_processed = $history_balance->balance_processed;
        $user_bank_id = $history_balance->user_bank_id;

        // $balance_processed = 3000; //*
        // $user_bank_id = 1; //*
        // $user_bank = UserBank::find($user_bank_id); //*

        //status_balance_id
        // 4 Dana telah dikirim (tuntas)

        //update history_balance from process to done
        $history_balance->update([
            'process_date' => Carbon::now(),
            // 'balance_processed' => $balance_processed, //*
            'status_balance_id' => '4',
            $this->transfer_target = $user_bank->bank->name."-".$user_bank->account_number." a/n ".$user_bank->account_name;
        ]); 

        //NOTE: * = uncomment if transfer info is different from user
    }
}
