<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RestaurantManagementsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('restaurant_managements')->insert([
            'name' => 'Miramar',
            'email' => 'miramar0611@gmail.com',
            'address' => 'Muara Karang blok G6 Barat no.45, Jakut',
            'password' => bcrypt('admin123'),
            'photo_url' => 'restaurant-default.png',
            'website_url' => 'miramarindonesia.com',
            'phone_number' => '0216672888',
            'open_time' => Carbon::createFromTime(10, 0, 0, 'Asia/Jakarta'),
            'close_time' => Carbon::createFromTime(22, 0, 0, 'Asia/Jakarta'),
        ]);   
    }
}
