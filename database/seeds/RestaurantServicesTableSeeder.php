<?php

use Illuminate\Database\Seeder;

class RestaurantServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$id = '3'; // restaurant management ID

		//1. AC
		//2. Halal
		//3. Non-Halal
		//4. Take Away
		//5. Smoking Area
		//6. Wifi
		//7. Serves Alcohol
		//8. Parking Area

		$services_arr = [1, 6, 8, 7, 5, 3, 4];

		for ($i=0; $i<count($services_arr); $i++){
	        DB::table('restaurant_services')->insert([
	            'service_id' => $services_arr[$i],
	            'restaurant_management_id' => $id,
	        ]); 
		}
    }
}
