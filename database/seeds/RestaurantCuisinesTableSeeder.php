<?php

use Illuminate\Database\Seeder;

class RestaurantCuisinesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$id = '3'; // restaurant management ID

		//1. Italian
		//2. Indonesia
		//3. Chinese
		//4. Korea
		//5. Western

        DB::table('restaurant_cuisines')->insert([
            'cuisine_id' => '1',
            'restaurant_management_id' => $id,
        ]); 
    }
}
