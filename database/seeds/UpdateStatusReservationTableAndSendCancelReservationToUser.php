<?php

use Solivis\Reservation;
use Solivis\Menu;
use Solivis\HistoryBalance;
use Solivis\DeclinedReason;
use \Snowfire\Beautymail\Beautymail as BeautyMail;
use Solivis\Commands\GetTotalReservationPriceCommand;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

// php artisan db:seed --class=UpdateStatusReservationTableAndSendCancelReservationToUser

class UpdateStatusReservationTableAndSendCancelReservationToUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	/*ONLY EDIT RESERVATION_ID, TOTAL USER TRANSFER, USER TRANSFER DATE AND IMAGE INCORRECT TRANSFER PROOF IF EVERYTHING RIGHT*/
		$reservation_id = '96'; // reservation ID

        $reservation = Reservation::find($reservation_id);

        $reason = "Jumlah transfer tidak sesuai dengan total pembayaran reservasi";
		DeclinedReason::create([
			'reservation_id' => $reservation->id,
			'reason' => $reason
		]);

        $status_reservation_id = '5'; //Batal
        //update reservation from process to cancel
        $reservation->update([
            'status_reservation_id' => $status_reservation_id,
        ]); 

        $user_name = $reservation->user->name;
        $user_email = $reservation->user->email;

        $reservation_menus = $reservation->reservation_menus;
        $getTotalReservationPriceCommand = new GetTotalReservationPriceCommand();        
        for($i=0; $i<count($reservation_menus); $i++){
            $getTotalReservationPriceCommand->setTotalPrice($reservation_menus[$i]);
        }
        $total_reservation_price = $getTotalReservationPriceCommand->getTotalPrice();

        $user_transfer_date = "18 Juni 2016";
        $total_user_transfer = 200000; 
        $incorrect_transfer_proof_photo_url_filename = 'incorrect_transfer_proof_1.jpg';

        $logo['path'] = '';
        $logo['width'] = '0px';
        $logo['height'] = '0px';
        $beautymail = app()->make(Beautymail::class);
        $beautymail->send('emails.cancel_reservation', 
            [
                'logo' => $logo,
                'reservation_id' => $reservation_id, 
                'user_name' => $user_name, 
                'total_reservation_price' => $total_reservation_price,
                'user_transfer_date' => $user_transfer_date,
                'total_user_transfer' => $total_user_transfer,
                'incorrect_transfer_proof_photo_url_filename' => $incorrect_transfer_proof_photo_url_filename          
            ],  function($message) use ($user_name, $user_email){
                    $message->to($user_email, $user_name)
                            ->subject('Reservasi ditolak oleh Admin');
        });

        // 5. Reservasi ditolak oleh Admin (dana dikembalikan)
        HistoryBalance::create([
            'process_date' => Carbon::now(),
            'balance_processed' => $total_user_transfer,
            'status_balance_id' => 5,
            'user_id' => $reservation->user->id,
            'transfer_target' => 'Ke saldo solivis',
         ]);

        //add to user balance
        $user = $reservation->user;
        $user->balance += $total_user_transfer;
        $user->save();

    }
}
