<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusProcessedToQrCodePlainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('qr_code_plains', function (Blueprint $table) {
            $table->boolean('status_processed')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('qr_code_plains', function (Blueprint $table) {
            Schema::dropColumn('status_processed');
        });
    }
}
