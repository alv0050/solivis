<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('item_name');
            $table->string('item_description')->nullable();
            $table->decimal('item_price', 19, 2);
            $table->integer('menu_category_id')->unsigned();
            $table->integer('restaurant_management_id')->unsigned();
            $table->timestamps();

            $table->foreign('menu_category_id')
                  ->references('id')
                  ->on('menu_categories')
                  ->onDelete('cascade'); 
                              
            $table->foreign('restaurant_management_id')
                  ->references('id')
                  ->on('restaurant_managements')
                  ->onDelete('cascade');             
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('menus');
    }
}
