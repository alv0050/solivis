<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('paxes');
            $table->timestamp('reservation_date');

            $table->integer('user_id')->unsigned();
            $table->integer('restaurant_management_id')->unsigned();
            $table->timestamps();

            $table->foreign('restaurant_management_id')
                  ->references('id')
                  ->on('restaurant_managements')
                  ->onDelete('cascade');  

            $table->foreign('user_id')
                  ->references('id')
                  ->on('users')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reservations');
    }
}
