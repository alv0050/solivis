<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryBalancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_balances', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('process_date');
            $table->decimal('balance_processed', 19, 2);
            $table->integer('status_balance_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('transfer_target');
            $table->timestamps();

            $table->foreign('status_balance_id')
                  ->references('id')
                  ->on('status_balances')
                  ->onDelete('cascade');  

            $table->foreign('user_id')
                  ->references('id')
                  ->on('users')
                  ->onDelete('cascade');    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('history_balances');
    }
}
