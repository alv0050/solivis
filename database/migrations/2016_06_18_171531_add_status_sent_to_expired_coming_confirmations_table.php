<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusSentToExpiredComingConfirmationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('expired_coming_confirmations', function (Blueprint $table) {
            $table->boolean('status_sent')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expired_coming_confirmations', function (Blueprint $table) {
            Schema::dropColumn('status_sent');
        });
    }
}
