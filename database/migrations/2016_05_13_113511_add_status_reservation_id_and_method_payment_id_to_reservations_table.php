<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusReservationIdAndMethodPaymentIdToReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reservations', function (Blueprint $table) {
            $table->integer('status_reservation_id')->unsigned()->nullable();
            $table->integer('method_payment_id')->unsigned()->nullable();

            $table->foreign('status_reservation_id')
                  ->references('id')
                  ->on('status_reservations')
                  ->onDelete('cascade');

            $table->foreign('method_payment_id')
                  ->references('id')
                  ->on('method_payments')
                  ->onDelete('cascade');              
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservations', function (Blueprint $table) {
            $table->dropForeign('status_reservations_status_reservation_id_foreign');
            $table->dropForeign('method_payments_method_payment_id_foreign');
        });
    }
}
