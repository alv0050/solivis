<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantInsidePhotos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_inside_photos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('inside_restaurant_photo_url');
            $table->string('inside_restaurant_photo_name');
            $table->integer('restaurant_management_id')->unsigned();

            $table->timestamps();

            $table->foreign('restaurant_management_id')
                  ->references('id')
                  ->on('restaurant_managements')
                  ->onDelete('cascade'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('restaurant_inside_photos');
    }
}
