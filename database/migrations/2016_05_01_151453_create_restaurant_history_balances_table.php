<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantHistoryBalancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_history_balances', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('process_date');
            $table->decimal('balance_processed', 19, 2);
            $table->integer('status_balance_id')->unsigned();
            $table->integer('restaurant_management_id')->unsigned();
            $table->string('transfer_target');
            $table->timestamps();

            $table->foreign('status_balance_id')
                  ->references('id')
                  ->on('status_balances')
                  ->onDelete('cascade');  

            $table->foreign('restaurant_management_id')
                  ->references('id')
                  ->on('restaurant_managements')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('restaurant_history_balances');
    }
}
