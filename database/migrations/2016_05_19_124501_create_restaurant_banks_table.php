<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_banks', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('account_number');
            $table->string('account_name');
            $table->integer('bank_id')->unsigned();
            $table->integer('restaurant_management_id')->unsigned();
            $table->timestamps();        

            $table->foreign('bank_id')
                  ->references('id')
                  ->on('banks')
                  ->onDelete('cascade');

            $table->foreign('restaurant_management_id')
                  ->references('id')
                  ->on('restaurant_managements')
                  ->onDelete('cascade'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('restaurant_banks');
    }
}
