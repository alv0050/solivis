<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMenuBasePriceToReservationMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reservation_menus', function (Blueprint $table) {
            $table->decimal('menu_base_price', 19, 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservation_menus', function (Blueprint $table) {
            Schema::dropColumn('menu_base_price');
        });
    }
}
