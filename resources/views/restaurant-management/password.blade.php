@extends('restaurant-management.layouts.master')

@section('title', 'Password')

@section('content')

@include('restaurant-management.layouts.navigation')
<section class="wrapper">
	@include('restaurant-management.layouts.sidebar')
	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main-change-password">
		<div class="main-content">
			<div class="container-fluid">
				<div class="change-password">
					<h3>Ganti Password Anda</h3>
					@include('partials.error')
					@include('partials.message')
		            {!! Form::open(['action'=>['RestaurantManagementController@putEditPassword', $restaurant_management->id], 'method'=>'PUT','class' => 'form-change-password']) !!}
		                <div class="form-group">
		                    <img src="/images/restaurants/password-lock.png" alt="password-lock"/>
	                        {!! Form::password('old_password', $array = ['class' => 'form-control default-black', 'placeholder' => 'Password lama']); !!}
		                </div>          
		                <div class="form-group">
		                    <img src="/images/restaurants/password-lock.png" alt="password-lock"/>
	                        {!! Form::password('password', $array = ['class' => 'form-control default-black', 'placeholder' => 'Password baru', 'id' => 'new_password']); !!}
		                </div>          
		                <div class="form-group">
		                    <img src="/images/restaurants/password-lock.png" alt="password-lock"/>
	                        {!! Form::password('password_confirmation', $array = ['class' => 'form-control default-black', 'placeholder' => 'Ulangi password baru']); !!}
		                </div>  
	                    <button type="submit" class="btn text-center btn-change-password">Save</button>
		            {!! Form::close() !!}   
				</div>
			</div>
		</div>		
	</div>
	@include('restaurant-management.layouts.footer')
</section>
@stop