<div class="modal fade" id="addAccountBankModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
                <h4 class="modal-title text-uppercase">Tambah Rekening Bank</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    {!! Form::open(['action'=>['RestaurantManagementController@postAddBank', $restaurant_management->id], 'class' => 'form-add-bank']) !!}
                        <div class="bank_name-dropdown">
                            <label for="bank_name" class="col-sm-4">Nama Bank</label>
                            <div class="col-sm-8">
                                <div class="form-group">
                                    {!! Form::select('bank_id', $banks, null, $attributes = ['class' => 'form-control default-grey']) !!}
                                </div>                        
                            </div>
                            <div class="clearfix"></div>
                        </div>
                         <div class="form-group">
                            <label for="account_number" class="col-sm-4">No. Rekening</label>
                            <div class="col-sm-8">
                            {!! Form::text('account_number', $value = null, $attributes = ['class' => 'form-control default-grey']); !!}
                            </div>
                            <div class="clearfix"></div>
                        </div>
                         <div class="form-group">
                            <label for="account_name" class="col-sm-4">Nama Pemilik Rek.</label>
                            <div class="col-sm-8">
                            {!! Form::text('account_name', $value = null, $attributes = ['class' => 'form-control default-grey']); !!}
                            </div>
                            <div class="clearfix"></div>
                        </div>
                         <div class="form-group">
                            <label for="password" class="col-sm-4">Kata Sandi</label>
                            <div class="col-sm-8">
                            {!! Form::password('password', $array = ['class' => 'form-control default-grey', 'placeholder' => 'Kata sandi akun solivis anda']); !!}
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <button class="btn text-center btn-save">Simpan</button>
                        </div>
                    {!! Form::close() !!}       
                </div>
            </div>
        </div>
    </div>
</div>  
