<div class="modal fade" id="addMenuModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
                <h4 class="modal-title">+ Tambah Menu</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    {!! Form::open(['action'=>['RestaurantManagementController@postAddMenu', $restaurant_management->id], 'class' => 'form-add-menu']) !!}
                        <div class="form-group category-group">
                            {!! Form::text('category_name', $value = null, $attributes = ['class' => 'form-control default-grey', 'placeholder' => 'Nama Kategori']); !!}
                        </div>
                        <div class="add-menu-content-group">
                            <div class="add-menu-content">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        {!! Form::text('item_name[]', $value = null, $attributes = ['class' => 'form-control default-grey', 'placeholder' => 'Nama Item']); !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::textarea('item_description[]', $value = null, $attributes = ['class' => 'form-control default-grey', 'placeholder' => 'Deskripsi Item (opsional)']); !!}
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group price-group">
                                        {!! Form::text('item_price[]', $value = null, $attributes = ['class' => 'form-control default-grey', 'placeholder' => 'Harga Item']); !!}
                                    </div>
                                    <div class="button-group">
                                        <button class="btn btn-add-per-item" type="button"><img src="/images/restaurants/add-small.png" alt="add-small">Tambah</button>
                                        <button class="btn btn-remove-per-item" type="button"><img src="/images/restaurants/remove-small.png" alt="remove-small">Hapus</button>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <button class="btn text-uppercase btn-save-add-menu-content">Simpan</button>
                    {!! Form::close() !!}       
                </div>
            </div>
        </div>
    </div>
</div>  
