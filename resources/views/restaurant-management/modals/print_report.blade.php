<div class="modal fade" id="printReportModal" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>        
        <h4 class="modal-title"><img src="/images/restaurants/calendar-small.png" alt="calendar-small"/> Pilih Tanggal</h4>
      </div>
      <div class="modal-body">
        {!! Form::open(['action' => 'RestaurantManagementController@getPrintReport', 'method' => 'GET', 'class' => 'form-print-report-ok']) !!}
          {!! Form::text('date_start', null, $attributes = ['class' => 'form-control default-black', 'placeholder' => 'Tanggal Mulai', 'id' => 'input_print_date_start']); !!}
          {!! Form::text('date_end', null, $attributes = ['class' => 'form-control default-black', 'placeholder' => 'Tanggal Akhir', 'id' => 'input_print_date_end']); !!}
          <button class="btn text-uppercase btn-print-report-ok">Ok</button>
        {!! Form::close() !!} 
      </div>
    </div>
  </div>
</div>  
