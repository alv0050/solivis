<div class="modal fade" id="newReservationModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
                <h4 class="modal-title"><img src="/images/restaurants/new.png" alt="new"><span>New Reservation</span></h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Reservasi ID</label>
                    <label id="reservation_id_detail">{{-- content is filled dynamically --}}</label>
                </div>
                <div class="form-group">
                    <label>Atas Nama</label>
                    <label id="reservation_user_name_detail">{{-- content is filled dynamically --}}</label>
                </div>
                <div class="form-group">
                    <label>Tanggal </label>
                    <label id="reservation_date_detail">{{-- content is filled dynamically --}}</label>
                </div>
                <div class="form-group">
                    <label>Jam </label>
                    <label id="reservation_time_detail">{{-- content is filled dynamically --}}</label>
                </div>
                <div class="form-group">
                    <label>Pax </label>
                    <label id="reservation_paxes_detail">{{-- content is filled dynamically --}} Orang </label>
                </div>
                <div class="form-group">
                    <label>Pembayaran </label>
                    <label id="reservation_total_payment_detail">{{-- content is filled dynamically --}} </label>
                </div>
            </div>
            <div class="order">
                <h4 class="title-order">Pesanan <img src="/images/restaurants/expand.png" alt="expand"></h4>
                <div class="detail-order">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Menu</th>
                                <th class="text-center">@</th>
                                <th class="text-right">Harga</th>
                            </tr>
                        </thead>
                        <tbody>
                            {{-- content is filled dynamically --}}
                        </tbody>                
                    </table>
                </div>
                <p class="text-right total-pay">Total Pembayaran <span>{{-- content is filled dynamically --}}</span></p>
                <p class="text-right total-fee-per-transaction">Biaya Per Transaksi <span>{{-- content is filled dynamically --}}</span></p>
                <p class="text-right total-net-income">Pendapatan Bersih <span>{{-- content is filled dynamically --}}</span></p>
                {!! Form::open(['action' => 'RestaurantManagementController@putReservationStatus', 'method' => 'PUT', 'class' => 'form-put-reservation-result']) !!}
                    <div class="group-answer-reservation">
                        <div class="group-button">
                         {{-- content is filled dynamically --}}                  
                        </div>
                        <p class="text-center info-decline"> {{-- content is filled dynamically --}}</p>
                        <div class="group-decline-reservation hide">
                             {{-- content is filled dynamically --}}
                        </div>
                        <p class="text-center decline-agreement hide"> {{-- content is filled dynamically --}}</p>   
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>  
