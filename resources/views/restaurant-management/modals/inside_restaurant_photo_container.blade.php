<div class="modal fade" id="insideRestaurantPhotoContainerModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Photo</h3>
            </div>
            <div class="modal-body">
                <div class="photo-detail">
                    <img class="inside_restaurant_photo_name_url" class="img-responsive">
                    <h4 class="text-center inside_restaurant_photo_name_modal"></h4>
                    {!! Form::open(['action'=>'RestaurantManagementController@deleteInsideRestaurantPhoto', 'method'=>'DELETE', 'class' => 'form-delete-inside-restaurant-photo']) !!}
                        <input type="hidden" class="inside_restaurant_url_hidden" name="inside_restaurant_photo_url">
                        <input type="hidden" class="inside_restaurant_id_hidden" name="inside_restaurant_photo_id">
                        <button class="btn btn-delete-inside-restaurant-photo"><img src="/images/restaurants/delete.png" alt="delete">Hapus Foto Ini</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
