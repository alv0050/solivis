<div class="modal fade" id="editMenuModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
                <h4 class="modal-title"><img src="/images/restaurants/edit-black.png" alt="edit-black">Edit Menu</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="change-category-container">
                        <p>Pilih kategori yang akan diedit</p>
                        <ul class="category-selection">
                            @foreach($menu_category_id_uniques as $menu_category_name => $menu_category_id)
                            <li id="edit-menu-{{ str_replace(' ', '', $menu_category_name) }}"><a href="#">{{ $menu_category_name }}</a></li>
                            @endforeach                            
                        </ul>
                    </div>
                    @foreach($menu_category_id_uniques as $menu_category_name => $menu_category_id)
                        <form action="{{ action('RestaurantManagementController@putEditMenu') }}" method="POST" class="form-edit-menu-category hide" id="form-edit-menu-{{ str_replace(' ', '', $menu_category_name) }}">
                            <div class="edit-menu-title">
                                <h3>{{ $menu_category_name }}</h3>
                            </div>
                            @foreach($restaurant_management->menus->where('menu_category_id', $menu_category_id) as $menu)
                            <div class="edit-menu-content">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control default-grey" placeholder="Nama Item" name="item_name[]" type="text" value="{{ $menu->item_name }}" disabled>                                        
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control default-grey" placeholder="Deskripsi Item (opsional)" name="item_description[]" cols="50" rows="10" value="{{ $menu->item_description }}">{{ $menu->item_description }}</textarea> 
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group price-group">
                                        <input class="form-control default-grey round-price-input" placeholder="Harga Item" name="item_price[]" type="text" value="{{ $restaurant_management->removeDoubleZero($menu->item_price) }}">
                                    </div>
                                    <div class="button-group">
                                       <button class="btn btn-remove-per-item-edit" type="button" menu-id="{{ $menu->id }}"><img src="/images/restaurants/remove-small.png" alt="add-small">Hapus</button>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="clearfix"></div>
                                <input type="hidden" name="item_id[]" value="{{ $menu->id }}">
                            </div>
                            @endforeach
                            <input name="_token" type="hidden" value="{{ csrf_token() }}">      
                            <input name="_method" type="hidden" value="PUT">
                            <button class="btn btn-save-edit-menu-content">Simpan</button>
                        </form>                            
                    @endforeach      
                </div>
            </div>
        </div>
    </div>
</div>  
