<div class="modal fade" id="editAccountBankModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
                <h4 class="modal-title text-uppercase">Edit Rekening Bank</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    {!! Form::open(['action'=>['RestaurantManagementController@putEditBank', $restaurant_management->id], 'method' => 'PUT', 'class' => 'form-edit-bank']) !!}
                        <div class="bank_name-dropdown">
                            <label for="bank_name" class="col-sm-4">Nama Bank</label>
                            <div class="col-sm-8">
                                <div class="form-group">
                                    {!! Form::select('bank_id', $banks, null, $attributes = ['class' => 'form-control default-grey account_bank_id_detail', 'disabled']) !!}
                                </div>                        
                            </div>
                            <div class="clearfix"></div>
                        </div>
                         <div class="form-group">
                            <label for="account_number" class="col-sm-4">No. Rekening</label>
                            <div class="col-sm-8">
                            {!! Form::text('account_number', $value = null, $attributes = ['class' => 'account_number_detail form-control default-grey']); !!}
                            </div>
                            <div class="clearfix"></div>
                        </div>
                         <div class="form-group">
                            <label for="account_name" class="col-sm-4">Nama Pemilik Rek.</label>
                            <div class="col-sm-8">
                            {!! Form::text('account_name', $value = null, $attributes = ['class' => 'account_name_detail form-control default-grey']); !!}
                            </div>
                            <div class="clearfix"></div>
                        </div>
                         <div class="form-group">
                            <label for="password" class="col-sm-4">Kata Sandi</label>
                            <div class="col-sm-8">
                            {!! Form::password('password', $array = ['class' => 'form-control default-grey', 'placeholder' => 'Kata sandi akun solivis anda']); !!}
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <input type="hidden" name="restaurant_bank_id" class="restaurant_bank_id_hidden">
                        <input type="hidden" name="bank_id_ori" class="bank_id_ori_hidden">
                        <button class="btn text-center btn-save">Simpan</button>
                        </div>
                    {!! Form::close() !!}       
                </div>
            </div>
        </div>
    </div>
</div>  
