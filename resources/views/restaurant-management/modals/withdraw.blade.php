<div class="modal fade" id="withdrawModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
                <h4 class="modal-title text-uppercase">Tarik Dana</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    {!! Form::model($restaurant_management, ['action'=>['RestaurantManagementController@postWithdraw', $restaurant_management->id], 'class' => 'form-withdraw']) !!}
                        <div class="form-group">
                            <label for="balance" class="col-sm-4">Tersedia Saldo</label>
                            <div class="col-sm-8">
                            {!! Form::text('balance', $value = number_format($restaurant_management->removeDoubleZero($restaurant_management->balance),0,'.',','), $attributes = ['class' => 'form-control default-grey', 'disabled']); !!}
                            </div>
                            <div class="clearfix"></div>
                        </div>                    
                         <div class="form-group">
                            <label for="balance_processed" class="col-sm-4">Jumlah Penarikan</label>
                            <div class="col-sm-8">
                            {!! Form::text('balance_processed', $value = null, $attributes = ['class' => 'form-control default-grey']); !!}
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="withdraw-dropdown">
                            <label for="restaurant_bank_id" class="col-sm-4">Nomor Rekening</label>
                            <div class="col-sm-8">
                                <div class="form-group">
                                    {!! Form::select('restaurant_bank_id', $complete_info, null, $attributes = ['class' => 'form-control default-grey']);  
                                    !!} 
                                </div>                        
                            </div>
                            <div class="clearfix"></div>
                        </div>
                         <div class="form-group">
                            <label for="password" class="col-sm-4">Kata Sandi</label>
                            <div class="col-sm-8">
                            {!! Form::password('password', $array = ['class' => 'form-control default-grey', 'placeholder' => 'Kata sandi akun anda']); !!}
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                        <p class="text-center">Demi kelancaran proses penarikan dana, mohon pastikan kembali nama dan nomor rekening yang dicantumkan sudah sesuai dengan yang tertera pada bukti tabungan Anda.</p>
                        <div class="confirmation_withdraw-group">
                            <button type="submit" class="btn text-center text-uppercase btn-confirmation_withdraw">Submit</button>
                        </div>
                    {!! Form::close() !!}       
                </div>
            </div>
        </div>
    </div>
</div>  
