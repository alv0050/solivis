@extends('restaurant-management.layouts.master')

@section('title', 'Report')

@section('content')

@include('restaurant-management.modals.new_reservation');
@include('restaurant-management.modals.print_report');
@include('restaurant-management.layouts.navigation')
<section class="wrapper">
	@include('restaurant-management.layouts.sidebar')
	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main-general">
		<div class="main-content">
			<div class="container-fluid">
				<div class="flash-manage">
					@include('partials.error')
					@include('partials.message')	                	
				</div>
				<div class="report-container add-margin-top">
					<div class="report-header-container">
						<div class="col-md-12 col-lg-6 left-side">
							<h3><img src="/images/restaurants/report-black.png" alt="report-black">Report</h3>
		                	{!! Form::open(['action' => 'RestaurantManagementController@getHistoryReportDate', 'method' => 'GET', 'class' => 'form-search-history-report']) !!}
								<img src="/images/restaurants/calendar-small.png" alt="calendar-small"/>
	                            {!! Form::text('date_start', null, $attributes = ['class' => 'form-control default-black', 'id' => 'input_date_start']); !!}
								<span>-</span>
	                            {!! Form::text('date_end', null, $attributes = ['class' => 'form-control default-black', 'id' => 'input_date_end']); !!}
								<button class="btn btn-search"><img src="/images/restaurants/search.png" alt="search"></button>
		               		{!! Form::close() !!} 
		               		<div class="clearfix"></div>
						</div>
						<div class="col-md-12 col-lg-6 right-side">
							<h4>Urutkan: </h4>
							{!! Form::open(['action' => 'RestaurantManagementController@getSortReport', 'method' => 'GET', 'class' => 'form-sort-report']) !!}
                            	{!! Form::select('sort_report', array(
                                        'all' => 'Semua',
                                        'reservation_id' => 'ID Reservasi',
                                        'user_name' => 'Nama User',
                                        'reservation_date' => 'Tgl Reservasi',
                                        'payment' => 'Total Transaksi',
                                        ), null, $attributes = ['class' => 'form-control sort-form-control sort-report default-black']); !!}
                            {!! Form::close() !!}
		                	{!! Form::open(['action' => 'RestaurantManagementController@getHistoryReportKeyword', 'method' => 'GET', 'class' => 'form-search-history-report-2']) !!}
                            	{!! Form::text('search', null, $attributes = ['class' => 'form-control search-form-control default-black', 'placeholder' => 'Cari']); !!}
		               		{!! Form::close() !!} 
	               			<button class="btn btn-print-report"><img src="/images/restaurants/print.png" alt="print">Cetak</button>
	               			<div class="clearfix"></div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="report-body-container">
						<table class="table table-striped table-report">
							<thead>
								<tr>
									<th class="text-center">ID Reservasi</th>
									<th class="text-center">User</th>
									<th class="text-center">Tgl Reservasi</th>
									<th class="text-center">Menu</th>
									<th class="text-center">@</th>
									<th class="text-center">Total Transaksi</th>
									<th class="text-center">Pendapatan Bersih</th>
								</tr>
							</thead>
							<tbody>
								@if(isset($restaurant_reservations_date_search))
								@for($i=0; $i<count($restaurant_reservations_date_search); $i++)
								<tr>
									<td class="text-center">{{ $restaurant_reservations_date_search[$i]->id }}</td>
									<td class="text-center">{{ $restaurant_reservations_date_search[$i]->user->name }}</td>
									<td class="text-center">{{ $restaurant_reservations_date_search[$i]->convertReservationDate() }}</td>
									<td class="text-center">{{ $restaurant_reservations_date_search[$i]->reservation_menus[0]->menu_name }}</td>
									<td class="text-center">{{ $restaurant_reservations_date_search[$i]->reservation_menus[0]->item_count }}</td>
									<td class="text-center">Rp. {{ number_format($restaurant_reservations_date_search[$i]->getTotalReservationPrice(),0,'.',',') }}</td>
									<td class="text-center">Rp. {{ number_format($restaurant_reservations_date_search[$i]->getTotalNetIncome(),0,'.',',') }}</td>
								</tr>
								@for($j=1; $j<count($restaurant_reservations_date_search[$i]->reservation_menus); $j++)
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td class="text-center">{{ $restaurant_reservations_date_search[$i]->reservation_menus[$j]->menu_name }}</td>
									<td class="text-center">{{ $restaurant_reservations_date_search[$i]->reservation_menus[$j]->item_count }}</td>
									<td></td>
									<td></td>
								</tr>
								@endfor
								@endfor									
								@elseif(isset($restaurant_reservations_search))
								@for($i=0; $i<count($restaurant_reservations_search); $i++)
								<tr>
									<td class="text-center">{{ $restaurant_reservations_search[$i]->id }}</td>
									<td class="text-center">{{ $restaurant_reservations_search[$i]->user->name }}</td>
									<td class="text-center">{{ $restaurant_reservations_search[$i]->convertReservationDate() }}</td>
									<td class="text-center">{{ $restaurant_reservations_search[$i]->reservation_menus[0]->menu_name }}</td>
									<td class="text-center">{{ $restaurant_reservations_search[$i]->reservation_menus[0]->item_count }}</td>
									<td class="text-center">Rp. {{ number_format($restaurant_reservations_search[$i]->getTotalReservationPrice(),0,'.',',') }}</td>
									<td class="text-center">Rp. {{ number_format($restaurant_reservations_search[$i]->getTotalNetIncome(),0,'.',',') }}</td>
								</tr>
								@for($j=1; $j<count($restaurant_reservations_search[$i]->reservation_menus); $j++)
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td class="text-center">{{ $restaurant_reservations_search[$i]->reservation_menus[$j]->menu_name }}</td>
									<td class="text-center">{{ $restaurant_reservations_search[$i]->reservation_menus[$j]->item_count }}</td>
									<td></td>
									<td></td>
								</tr>
								@endfor
								@endfor									
								@else
								@for($i=0; $i<count($restaurant_reservations); $i++)
								<tr>
									<td class="text-center">{{ $restaurant_reservations[$i]->id }}</td>
									<td class="text-center">{{ $restaurant_reservations[$i]->user->name }}</td>
									<td class="text-center">{{ $restaurant_reservations[$i]->convertReservationDate() }}</td>
									<td class="text-center">{{ $restaurant_reservations[$i]->reservation_menus[0]->menu_name }}</td>
									<td class="text-center">{{ $restaurant_reservations[$i]->reservation_menus[0]->item_count }}</td>
									<td class="text-center">Rp. {{ number_format($restaurant_reservations[$i]->getTotalReservationPrice(),0,'.',',') }}</td>
									<td class="text-center">Rp. {{ number_format($restaurant_reservations[$i]->getTotalNetIncome(),0,'.',',') }}</td>
								</tr>
								@for($j=1; $j<count($restaurant_reservations[$i]->reservation_menus); $j++)
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td class="text-center">{{ $restaurant_reservations[$i]->reservation_menus[$j]->menu_name }}</td>
									<td class="text-center">{{ $restaurant_reservations[$i]->reservation_menus[$j]->item_count }}</td>
									<td></td>
									<td></td>
								</tr>
								@endfor
								@endfor
								@endif							
							</tbody>
						</table>						
					</div>
					<div class="report-footer-container">
						<div class="col-sm-6 no-padding">
							<h4 class="text-uppercase">Total pendapatan bersih</h4>
							<h2>Rp. {{ number_format($total_income,0,'.',',') }}</h2>
						</div>
						<div class="col-sm-6 no-padding">
				            <div class="report-pagination">
				            	@if(isset($restaurant_reservations_search))
				            	{!! $restaurant_reservations_search->appends(Request::only(['search'=>'search']))->render() !!}
				            	@elseif(isset($restaurant_reservations_date_search))
				            	{!! $restaurant_reservations_date_search->appends(Request::only(['date_start'=>'date_start', 'date_end'=>'date_end']))->render() !!}
				            	@else
					            	{!! $restaurant_reservations->appends(Request::only(['sort_report'=>'sort_report']))->render() !!}
				            	@endif
				            </div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="text-center info-report-unshow">
						<h2>Sorry :(</h2>
						<h3>Minimal lebar layout yang dibutuhkan untuk menampilkan laporan reservasi adalah 992px atau ke atasnya.
						</h3>
					</div>
				</div>
			</div>
		</div>		
	</div>
	@include('restaurant-management.layouts.footer')
</section>
@stop