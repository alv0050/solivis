@extends('restaurant-management.layouts.master')

@section('title', 'Dashboard')

@section('content')

@include('restaurant-management.modals.new_reservation');
@include('restaurant-management.modals.withdraw');
@include('restaurant-management.layouts.navigation')
<section class="wrapper">
	@include('restaurant-management.layouts.sidebar')
	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main-general">
		<div class="main-content">
			<div class="container-fluid">
				<div class="flash-manage">
					@include('partials.message')
					@include('partials.error')
				</div>
				<div class="col-md-8 left-container">
					<div class="income-container add-margin-top">
						<h3><img src="/images/restaurants/income.png" alt="income">Income</h3>
						<div class="income-group-container">
							<canvas id="income" width="500" height="230"></canvas>
							<h4>Penghasilan dari tanggal 01 Januari 2016 - sekarang</h4>
						</div>
					</div>
				</div>
				<div class="col-md-4 right-container">
					<div class="total-reservation-container add-margin-top">
						<h3><img src="/images/restaurants/reservation-black.png" alt="reservation-black">Reservation Count</h3>
						<h1 class="text-center">{{ $total_reservation_2016_years }}</h1>
						<h4 class="text-center">reservasi</h4>
					</div>
					<h4 class="info-total-reservation">Perhitungan dari tanggal 1 Januari 2016 - sekarang</h4>
				</div>
				<div class="clearfix"></div>
				<div class="col-md-6 left-container margin-adapt">
					<div class="balance-container">
						<h3><img src="/images/restaurants/balance-black.png" alt="balance-black">Balance</h3>
						<div class="balance-group-container">
							<h4>Tersedia dana sebesar</h4>
							<h1>Rp. {{ number_format($restaurant_management->removeDoubleZero($restaurant_management->balance),0,'.',',') }}</h1>
							<div class="button-group">
								<button class="btn btn-withdraw">Tarik Dana <img src="/images/restaurants/next.png" alt="next"></button>
							</div>
							<div class="clearfix"></div>
						</div>
						@for($i=1; $i<=count($total_income_2016_years); $i++)
						<input type="hidden" class="income_per_month" name="income_restaurant_per_month[]" value="{{ $total_income_2016_years[$i] }}">
						@endfor
					</div>
					<div class="upcoming-reservation-container">
						<h3><img src="/images/restaurants/upcoming.png" alt="upcoming">Upcoming Reservation <span class="badge">{{-- content is filled dynamically --}}</span></h3>
						<div class="separator"></div>
						@for($i = 0; $i < count($restaurant_reservations); $i++)
						<div class="reservation">
							@if($restaurant_reservations[$i]->status_reservation_id == 3)
							<img src="/images/restaurants/new.png" alt="new" class="new-reserve new_reservation">
							@elseif($restaurant_reservations[$i]->status_reservation_id == 4)
							<img src="/images/restaurants/accept.png" alt="accept" class="new-reserve">
							@else
							<img src="/images/restaurants/decline.png" alt="decline" class="new-reserve">
							@endif
							<p>{{ $restaurant_reservations[$i]->user->name }} akan melakukan reservasi pada tanggal {{ $restaurant_reservations[$i]->convertReservationDate() }} sebanyak {{ $restaurant_reservations[$i]->paxes }} orang pada jam {{ $restaurant_reservations[$i]->convertTime() }}</p>
							<a href="#" class="view-link"><img src="/images/restaurants/view.png" alt="view" class="view">
	                            <span class="reservation-id-detail hide">{{ $restaurant_reservations[$i]->id }}</span>
	                            <span class="reservation-user-name-detail hide">{{ $restaurant_reservations[$i]->user->name }}</span>
	                            <span class="reservation-date-detail hide">{{ $restaurant_reservations[$i]->convertReservationDate() }}</span>
	                            <span class="reservation-time-detail hide">{{ $restaurant_reservations[$i]->convertTime() }}</span>
	                            <span class="reservation-paxes-detail hide">{{ $restaurant_reservations[$i]->paxes }}</span>
	                            <span class="food-names hide">{{ $restaurant_reservations[$i]->getFoodNames() }}</span>
	                            <span class="food-counts hide">{{ $restaurant_reservations[$i]->getFoodCounts() }}</span>
	                            <span class="food-prices hide">{{ $restaurant_reservations[$i]->getFoodPrices() }}</span>
	                            <span class="status-reservation hide">{{ $restaurant_reservations[$i]->status_reservation_id }}</span>
	                        </a>
						</div>
						@endfor
						<div class="reservation-info">
							<div class="side">
								<img src="/images/restaurants/new.png" alt="new">
								<p>New Reservation</p>
							</div>
							<div class="side">
								<img src="/images/restaurants/accept.png" alt="accept">
								<p>Accepted</p>
							</div>
							<div class="side">
								<img src="/images/restaurants/decline.png" alt="decline">
								<p>Declined</p>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="all-reservation-hidden-container">
							@for($i = 0; $i < count($only_accepted_restaurant_reservations); $i++)
                            <span class="all-reservation-success-date-detail hide">{{ $only_accepted_restaurant_reservations[$i]->convertNumericDate() }}</span>
							<div class="all-reservation-hidden-detail">
	                            <span class="all-reservation-id-detail hide">{{ $only_accepted_restaurant_reservations[$i]->id }}</span>
	                            <span class="all-reservation-user-name-detail hide">{{ $only_accepted_restaurant_reservations[$i]->user->name }}</span>
	                            <span class="all-reservation-date-detail hide">{{ $only_accepted_restaurant_reservations[$i]->convertNumericDate() }}</span>
	                            <span class="all-reservation-time-detail hide">{{ $only_accepted_restaurant_reservations[$i]->convertTime() }}</span>
	                            <span class="all-reservation-paxes-detail hide">{{ $only_accepted_restaurant_reservations[$i]->paxes }}</span>
	                            <span class="all-reservation-food-names hide">{{ $only_accepted_restaurant_reservations[$i]->getFoodNames() }}</span>
	                            <span class="all-reservation-food-counts hide">{{ $only_accepted_restaurant_reservations[$i]->getFoodCounts() }}</span>
							</div>
							@endfor	
						</div>						
					</div>
				</div>
				<div class="col-md-6 right-container">
					<div class="reservation-date-container">
						<h3><img src="/images/restaurants/calendar.png" alt="calendar">Calendar Reservation</h3>
						<div class="reservation-date-group-container">
							<div id="datepicker-dashboard"></div>
							<div class="left-info">
								<div class="round-info"></div>
								<p>Reservasi</p>
							</div>
							<p class="right-info">Total Reservasi : <span class="total_reservation_per_month">{{-- content is filled dynamically --}}</span></p>
							<div class="clearfix"></div>
						</div>
					</div>
					<div class="download-app-container margin-adapt2">
						<h3><img src="/images/restaurants/download.png" alt="download">Download</h3>
						<div class="download-app-group-container">
							<div class="left-side-container">
								<img src="/images/restaurants/device.png" alt="device" class="img-responsive">
							</div>
							<div class="right-side-container">
								<img src="/images/restaurants/logo-scanner.png" alt="logo-scanner">
								<p class="text-center version">ver 1.0.0</p>
								<h4 class="text-center">Kenali customer anda dengan mudah melalui aplikasi kami.</h4>
								<button class="btn btn-download" onclick="location.href='/mobile_app/app-debug.apk';">Download</button>
								<p class="text-center">Aplikasi hanya bekerja di Android</p>
								<div class="line"></div>
								<p class="text-center">Pergunakan username dan password anda untuk memasuki aplikasi</p>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
			</div>
		</div>		
	</div>
	@include('restaurant-management.layouts.footer')
</section>
@stop