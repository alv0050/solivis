@extends('restaurant-management.layouts.master')

@section('title', 'Print Report')

@section('content')

@include('restaurant-management.layouts.navigation')
<section class="wrapper">
	<section class="print-report-container">
		<div class="container-fluid">
			<div class="print-area">
				<h4 class="report-title text-center">Laporan Penjualan dari tgl. {{ $restaurant_reservations_date_print->first()->convertNumericHypenDate($date_start) }} s/d {{ $restaurant_reservations_date_print->first()->convertNumericHypenDate($date_end) }}</h4>
				<table class="table table-striped table-print-report">
					<thead>
						<tr>
							<th class="text-center">ID Reservasi</th>
							<th class="text-center">User</th>
							<th class="text-center">Tgl Reservasi</th>
							<th class="text-center">Menu</th>
							<th class="text-center">@</th>
							<th class="text-center">Total Transaksi</th>
							<th class="text-center">Pendapatan Bersih</th>
						</tr>
					</thead>
					<tbody>
						@for($i=0; $i<count($restaurant_reservations_date_print); $i++)
						<tr>
							<td class="text-center">{{ $restaurant_reservations_date_print[$i]->id }}</td>
							<td class="text-center">{{ $restaurant_reservations_date_print[$i]->user->name }}</td>
							<td class="text-center">{{ $restaurant_reservations_date_print[$i]->convertReservationDate() }}</td>
							<td class="text-center">{{ $restaurant_reservations_date_print[$i]->reservation_menus[0]->menu_name }}</td>
							<td class="text-center">{{ $restaurant_reservations_date_print[$i]->reservation_menus[0]->item_count }}</td>
							<td class="text-center">Rp. {{ number_format($restaurant_reservations_date_print[$i]->getTotalReservationPrice(),0,'.',',') }}</td>
							<td class="text-center">Rp. {{ number_format($restaurant_reservations_date_print[$i]->getTotalNetIncome(),0,'.',',') }}</td>
						</tr>
						@for($j=1; $j<count($restaurant_reservations_date_print[$i]->reservation_menus); $j++)
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td class="text-center">{{ $restaurant_reservations_date_print[$i]->reservation_menus[$j]->menu_name }}</td>
							<td class="text-center">{{ $restaurant_reservations_date_print[$i]->reservation_menus[$j]->item_count }}</td>
							<td></td>
							<td></td>
						</tr>
						@endfor
						@endfor									
						<tr>
							<td class="text-right" colspan="6">Total Pendapatan Bersih</td>
							<td class="text-center"><span class="total-income-value">Rp. {{ number_format($total_income,0,'.',',') }}</span></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</section>
</section>
@stop