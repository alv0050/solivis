<!-- FOOTER -->
<div class="clearfix"></div>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2">
	<footer>
		<div class="container-fluid">
			<h5 class="text-right">&copy; 2016 Solivis</h5>
		</div>
	</footer>		
</div>
<div class="clearfix"></div>
<!-- END: FOOTER -->