<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Solivis | @yield('title')</title>
	<link rel="shortcut icon" href="{{asset('images/solivis-icon.ico')}}">
	<link rel="stylesheet" href="/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/vendor/library/css/datepicker.min.css">
    <link rel="stylesheet" href="/vendor/library/css/sweetalert.css">
	<link rel="stylesheet" href="/css/app-rm.css">
</head>
<body>
	@yield('content')
    <script src="/vendor/jquery/jquery.min.js"></script>
    <script src="/vendor/library/js/jquery.nicescroll.min.js"></script>
    <script src="/vendor/library/js/jquery.validate.min.js"></script>
    <script src="/vendor/library/js/chart.min.js"></script>
    <script src="/vendor/library/js/datepicker.min.js"></script>
    <script src="/vendor/library/js/datepicker.en.js"></script>
    <script src="/vendor/library/js/sweetalert.min.js"></script>
    <script src="/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="/js/script-rm.js"></script>
</body>
</html>