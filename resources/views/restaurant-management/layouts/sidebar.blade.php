<!-- SIDEBAR -->
<div class="col-sm-3 col-md-2 sidebar">
	<ul class="nav nav-sidebar">
		<li {{{ (Request::is('restaurant-management/dashboard') ? 'class=active' : '') }}}>
			<a href="/restaurant-management/dashboard" class="_dashboard">
				<img src="/images/restaurants/dashboard.png" alt="dashboard">Dashboard 
				<span class="sr-only">(current)</span>
			</a>
		</li>
		<li {{{ (Request::is('restaurant-management/reservation') ? 'class=active' : '') }}}>
			<a href="/restaurant-management/reservation" class="_reservation">
				<img src="/images/restaurants/reservation.png" alt="reservation">Reservation
			</a>
		</li>
		<li {{{ (Request::is('restaurant-management/restaurant') ? 'class=active' : '') }}}>
			<a href="/restaurant-management/restaurant" class="_restaurant">
				<img src="/images/restaurants/restaurant.png" alt="restaurant">Restaurant
			</a>
		</li>
		<li {{{ (Request::is('restaurant-management/balance') ? 'class=active' : '') }}}>
			<a href="/restaurant-management/balance" class="_balance">
				<img src="/images/restaurants/balance.png" alt="balance">Balance
			</a>
		</li>
		<li {{{ (Request::is('restaurant-management/report') ? 'class=active' : '') }}}>
			<a href="/restaurant-management/report" class="_report">
				<img src="/images/restaurants/report.png" alt="report">Report
			</a>
		</li>
		<li {{{ (Request::is('restaurant-management/password') ? 'class=active' : '') }}}>
			<a href="/restaurant-management/password" class="_password">
				<img src="/images/restaurants/password.png" alt="password">Password
			</a>
		</li>
	</ul>
</div>
<!-- END: SIDEBAR -->
