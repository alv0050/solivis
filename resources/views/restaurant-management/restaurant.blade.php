@extends('restaurant-management.layouts.master')

@section('title', 'Restaurant')

@section('content')

@include('restaurant-management.modals.add_menu');
@include('restaurant-management.modals.edit_menu');
@include('restaurant-management.modals.food_photo_container');
@include('restaurant-management.modals.inside_restaurant_photo_container');
@include('restaurant-management.layouts.navigation')
<section class="wrapper">
	@include('restaurant-management.layouts.sidebar')
	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main-general">
		<div class="main-content">
			<div class="container-fluid">
				<div class="flash-manage">
					@include('partials.message')
					@include('partials.error')
				</div>
				<div class="col-md-6 left-container">
					<div class="profile-restaurant-container add-margin-top">
						<h3><img src="/images/restaurants/profile.png" alt="profile">Profile</h3>
                        {!! Form::open(['action'=>['RestaurantManagementController@putEditPicture', $restaurant_management->id], 'class' => 'form-change-picture', 'method'=>'PUT', 'enctype' => 'multipart/form-data']) !!}  
                            <div class="btn btn-change-picture" type="file">
                            	<img src="/images/restaurants/change.png" alt="change">
                                <span>Change Picture</span>
                                <input type="file" class="upload" name="photo_url"/>
                            </div>
                        {!! Form::close() !!} 	
                        <div class="clearfix"></div>
                        <div class="profile-group-container">
                        	<img src="/images/restaurant_uploads/{{ $restaurant_management->photo_url }}" alt="{{ $restaurant_management->name }}" class="img-circle img-responsive">
                        	<h2 class="text-center">{{ $restaurant_management->name }}</h2>
                        	{!! Form::model($restaurant_management, ['action'=>['RestaurantManagementController@putEditProfile', $restaurant_management->id], 'method' => 'PUT', 'class' => 'form-change-profile-info']) !!}  
	                        	<div class="form-group">
		                        	<label class="col-sm-4">Nama</label>
		                        	<div class="col-sm-8">
                        				{!! Form::text('name', $value = null, $attributes = ['class' => 'form-control default-grey']); !!}
                        			</div>
                        			<div class="clearfix"></div>
	                        	</div>
	                        	<div class="form-group">
		                        	<label class="col-sm-4">Alamat</label>
		                        	<div class="col-sm-8">
                        				{!! Form::text('address', $value = null, $attributes = ['class' => 'form-control default-grey']); !!}
                        			</div>
                        			<div class="clearfix"></div>
	                        	</div>
	                        	<div class="form-group">
		                        	<label class="col-sm-4">Email</label>
		                        	<div class="col-sm-8">
                        				{!! Form::email('email', $value = null, $attributes = ['class' => 'form-control default-grey']); !!}
                        			</div>
                        			<div class="clearfix"></div>
	                        	</div>
	                        	<div class="form-group">
		                        	<label class="col-sm-4">Website</label>
		                        	<div class="col-sm-8">
                        				{!! Form::text('website_url', $value = null, $attributes = ['class' => 'form-control default-grey']); !!}
                        			</div>
                        			<div class="clearfix"></div>
	                        	</div>
	                        	<div class="form-group">
		                        	<label class="col-sm-4">No. Telp / Hp</label>
		                        	<div class="col-sm-8">
                        				{!! Form::text('phone_number', $value = null, $attributes = ['class' => 'form-control default-grey']); !!}
                        			</div>
                        			<div class="clearfix"></div>
	                        	</div>
	                        	<div class="form-group open-time">
		                        	<label class="col-sm-4">Jam Buka</label>
		                        	<div class="col-sm-8">
		                            {!! Form::select('open_time', array(
		                                        '06:00:00' => '06:00',
		                                        '06:30:00' => '06:30',
		                                        '07:00:00' => '07:00',
		                                        '07:30:00' => '07:30',
		                                        '08:00:00' => '08:00',
		                                        '08:30:00' => '08:30',
		                                        '09:00:00' => '09:00',
		                                        '09:30:00' => '09:30',
		                                        '10:00:00' => '10:00',
		                                        '10:30:00' => '10:30',
		                                        '11:00:00' => '11:00',
		                                        '11:30:00' => '11:30',
		                                        '12:00:00' => '12:00',
		                                        '12:30:00' => '12:30',
		                                        '13:00:00' => '13:00',
		                                        '13:30:00' => '13:30',
		                                        '14:00:00' => '14:00',
		                                        '14:30:00' => '14:30',
		                                        '15:00:00' => '15:00',
		                                        '15:30:00' => '15:30',
		                                        '16:00:00' => '16:00',
		                                        '16:30:00' => '16:30',
		                                        '17:00:00' => '17:00',
		                                        '17:30:00' => '17:30',
		                                        '18:00:00' => '18:00',
		                                        '18:30:00' => '18:30',
		                                        '19:00:00' => '19:00',
		                                        '19:30:00' => '19:30',
		                                        '20:00:00' => '20:00',
		                                        '20:30:00' => '20:30',
		                                        '21:00:00' => '21:00',
		                                        '21:30:00' => '21:30',
		                                        '22:00:00' => '22:00',
		                                        '22:30:00' => '22:30',
		                                        '23:00:00' => '23:00',
		                                        '23:30:00' => '23:30',
		                                        ), null, $attributes = ['class' => 'form-control default-grey']); !!}
                        				<span>-</span>
		                            {!! Form::select('close_time', array(
		                                        '06:00:00' => '06:00',
		                                        '06:30:00' => '06:30',
		                                        '07:00:00' => '07:00',
		                                        '07:30:00' => '07:30',
		                                        '08:00:00' => '08:00',
		                                        '08:30:00' => '08:30',
		                                        '09:00:00' => '09:00',
		                                        '09:30:00' => '09:30',
		                                        '10:00:00' => '10:00',
		                                        '10:30:00' => '10:30',
		                                        '11:00:00' => '11:00',
		                                        '11:30:00' => '11:30',
		                                        '12:00:00' => '12:00',
		                                        '12:30:00' => '12:30',
		                                        '13:00:00' => '13:00',
		                                        '13:30:00' => '13:30',
		                                        '14:00:00' => '14:00',
		                                        '14:30:00' => '14:30',
		                                        '15:00:00' => '15:00',
		                                        '15:30:00' => '15:30',
		                                        '16:00:00' => '16:00',
		                                        '16:30:00' => '16:30',
		                                        '17:00:00' => '17:00',
		                                        '17:30:00' => '17:30',
		                                        '18:00:00' => '18:00',
		                                        '18:30:00' => '18:30',
		                                        '19:00:00' => '19:00',
		                                        '19:30:00' => '19:30',
		                                        '20:00:00' => '20:00',
		                                        '20:30:00' => '20:30',
		                                        '21:00:00' => '21:00',
		                                        '21:30:00' => '21:30',
		                                        '22:00:00' => '22:00',
		                                        '22:30:00' => '22:30',
		                                        '23:00:00' => '23:00',
		                                        '23:30:00' => '23:30',
		                                        ), null, $attributes = ['class' => 'form-control default-grey']); !!}   
                        			</div>
                        			<div class="clearfix"></div>
	                        	</div>
		                        <div class="filter-title-cuisine border-filter-spc-collapse">
		                            <label>Jenis Kuliner</label>
		                            <img src="/images/restaurants/collapse.png" alt="collapse"/>
		                        </div>
		                        <div class="detail-filter-cuisine">
		                            <div class="col-sm-3 right-side-checkbox">                      
			                            <div class="checkbox checkbox-group-cuisine">
			                                <label>
			                                    {!! Form::checkbox('cuisine[0]', '1', false, $attributes = ["class" => "checkbox-cuisine", 'id' => 'cuisine-1']); !!} 
			                                    <div class="info-cuisine">Italian</div>
			                                </label>
		                                </div>
			                            <div class="checkbox checkbox-group-cuisine">
			                                <label>
			                                    {!! Form::checkbox('cuisine[1]', '2', false, $attributes = ["class" => "checkbox-cuisine", 'id' => 'cuisine-2']); !!} 
			                                    <div class="info-cuisine">Indonesia</div>
			                                </label>
		                                </div>
			                            <div class="checkbox checkbox-group-cuisine">
			                                <label>
			                                    {!! Form::checkbox('cuisine[2]', '3', false, $attributes = ["class" => "checkbox-cuisine", 'id' => 'cuisine-3']); !!} 
			                                    <div class="info-cuisine">Chinese</div>
			                                </label>
		                                </div>
			                        </div>
	                            	<div class="col-sm-3 middle-side-checkbox">
			                            <div class="checkbox checkbox-group-cuisine">
			                                <label>
			                                    {!! Form::checkbox('cuisine[3]', '4', false, $attributes = ["class" => "checkbox-cuisine", 'id' => 'cuisine-4']); !!}
			                                    <div class="info-cuisine">Korea</div>
			                                </label>
		                                </div>
			                            <div class="checkbox checkbox-group-cuisine">
			                                <label>
			                                    {!! Form::checkbox('cuisine[4]', '5', false, $attributes = ["class" => "checkbox-cuisine", 'id' => 'cuisine-5']); !!}
			                                    <div class="info-cuisine">Western</div>
			                                </label>
		                                </div>
	                            	</div>  
	                            	<div class="clearfix"></div>    
                            	</div>               
		                        <div class="filter-title-service border-filter-spc-collapse">
		                            <label>Services</label>
		                            <img src="/images/restaurants/collapse.png" alt="collapse"/>
		                        </div>
		                        <div class="detail-filter-service">
		                            <div class="col-sm-3 right-side-checkbox"> 
			                            <div class="checkbox checkbox-group-service">
			                                <label>
			                                    {!! Form::checkbox('service[0]', '1', false, $attributes = ["class" => "checkbox-service", 'id' => 'service-1']); !!} 
			                                    <div class="info-service">AC</div>
			                                </label>
			                            </div>                            
			                            <div class="checkbox checkbox-group-service">
			                                <label>
			                                    {!! Form::checkbox('service[1]', '2', false, $attributes = ["class" => "checkbox-service", 'id' => 'service-2']); !!} 
			                                    <div class="info-service">Halal</div>
			                                </label>
			                            </div>                            
			                            <div class="checkbox checkbox-group-service">
			                                <label>
			                                    {!! Form::checkbox('service[2]', '3', false, $attributes = ["class" => "checkbox-service", 'id' => 'service-3']); !!} 
			                                    <div class="info-service">Non-Halal</div>
			                                </label>
			                            </div>                            
	                            	</div>
	                            	<div class="col-sm-4 middle-side-checkbox">
			                            <div class="checkbox checkbox-group-service">
			                                <label>
			                                    {!! Form::checkbox('service[3]', '4', false, $attributes = ["class" => "checkbox-service", 'id' => 'service-4']); !!} 
			                                    <div class="info-service">Take Away</div>
			                                </label>
			                            </div>                            
			                            <div class="checkbox checkbox-group-service">
			                                <label>
			                                    {!! Form::checkbox('service[4]', '5', false, $attributes = ["class" => "checkbox-service", 'id' => 'service-5']); !!} 
			                                    <div class="info-service">Smoking Area</div>
			                                </label>
			                            </div>                            
			                            <div class="checkbox checkbox-group-service">
			                                <label>
			                                    {!! Form::checkbox('service[5]', '6', false, $attributes = ["class" => "checkbox-service", 'id' => 'service-6']); !!} 
			                                    <div class="info-service">Wifi</div>
			                                </label>
			                            </div>                            
	                            	</div>
		                            <div class="col-sm-4 right-side-checkbox">
										<div class="checkbox checkbox-group-service">
			                                <label>
			                                    {!! Form::checkbox('service[6]', '7', false, $attributes = ["class" => "checkbox-service", 'id' => 'service-7']); !!} 
			                                    <div class="info-service">Serves Alcohol</div>
			                                </label>
			                            </div>                
										<div class="checkbox checkbox-group-service">
			                                <label>
			                                    {!! Form::checkbox('service[7]', '8', false, $attributes = ["class" => "checkbox-service", 'id' => 'service-8']); !!} 
			                                    <div class="info-service">Parking Area</div>
			                                </label>
			                            </div>                
		                            </div>   
	                            	<div class="clearfix"></div>    
                            	</div>               
                            	<button class="btn btn-save-profile-info">Simpan</button>
	                            {{-- showing data in behind --}}
	                            @foreach($restaurant_management->restaurant_cuisines as $restaurant_cuisine)
	                            <div class="restaurant-cuisine {{$restaurant_management->cuisine_id}} hide">{{ $restaurant_cuisine->cuisine_id }}</div>
	                            @endforeach                         
	                            @foreach($restaurant_management->restaurant_services as $restaurant_service)
	                            <div class="restaurant-service {{$restaurant_management->service_id}} hide">{{ $restaurant_service->service_id }}</div>
	                            @endforeach                         
                            	<div class="clearfix"></div>
	                        {!! Form::close() !!} 	
                        </div>				
                    </div>
					<div class="food-photos-container add-margin-top">
						<h3><img src="/images/restaurants/photos.png" alt="photos">Photos</h3>
                        {!! Form::open(['action'=>['RestaurantManagementController@putEditFoodPhoto', $restaurant_management->id], 'class' => 'form-upload-food-photo', 'method'=>'PUT', 'enctype' => 'multipart/form-data']) !!}  
                            <div class="btn btn-upload-photo" type="file">
                            	<img src="/images/restaurants/upload.png" alt="upload">
                                <span>Upload Photos</span>
                                <input type="file" class="upload" name="food_photo_url"/>
                            </div>
                        {!! Form::close() !!} 	
                        <div class="clearfix"></div>
                        <div class="photos-group-container">
                        	@foreach($restaurant_management->restaurant_food_photos as $restaurant_food_photo)
                        	<div class="col-xs-6 col-sm-3">
	                        	<a href="#" class="food-photo" food-photo-name="{{ $restaurant_food_photo->food_photo_name }}" food-photo-url="{{ $restaurant_food_photo->food_photo_url }}" food-photo-id="{{ $restaurant_food_photo->id }}">
	                        		<img src="/images/restaurant_uploads/food_photos/{{ $restaurant_food_photo->food_photo_url }}" alt="{{ $restaurant_food_photo->food_photo_name }}" class="img-responsive" title="{{ $restaurant_food_photo->food_photo_name }}"/>
	                    		</a>
                        	</div>
                        	@endforeach
                        	<div class="clearfix"></div>
                        </div>
                    </div>
					<div class="inside-restaurant-photos-container add-margin-top">
						<h3><img src="/images/restaurants/photos.png" alt="photos">Inside Restaurant</h3>
                        {!! Form::open(['action'=>['RestaurantManagementController@putEditInsideRestaurantPhoto', $restaurant_management->id], 'class' => 'form-upload-inside-restaurant-photo', 'method'=>'PUT', 'enctype' => 'multipart/form-data']) !!}
                            <div class="btn btn-upload-photo" type="file">
                            	<img src="/images/restaurants/upload.png" alt="upload">
                                <span>Upload Photos</span>
                                <input type="file" class="upload" name="inside_restaurant_photo_url"/>
                            </div>
                        {!! Form::close() !!} 	
                        <div class="clearfix"></div>
                        <div class="photos-group-container">
                        	@foreach($restaurant_management->restaurant_inside_photos as $restaurant_inside_photo)
                        	<div class="col-xs-6">
	                        	<a href="#" class="inside-restaurant-photo" inside-restaurant-photo-name="{{ $restaurant_inside_photo->inside_restaurant_photo_url }}" inside-restaurant-photo-url="{{ $restaurant_inside_photo->inside_restaurant_photo_url }}" inside-restaurant-photo-id="{{ $restaurant_inside_photo->id }}">
	                        		<img src="/images/restaurant_uploads/inside_restaurant_photos/{{ $restaurant_inside_photo->inside_restaurant_photo_url }}" alt="{{ $restaurant_inside_photo->inside_restaurant_photo_url }}" class="img-responsive"/>
	                    		</a>
                        	</div>
                        	@endforeach
                        	<div class="clearfix"></div>
                        </div>
                    </div>
				</div>
				<div class="col-md-6 right-container">
					<div class="menu-container add-margin-top">
						<h3><img src="/images/restaurants/menu.png" alt="menu">Menus</h3>
						<div class="button-group">
							<button class="btn btn-add-menu"><img src="/images/restaurants/add-white.png" alt="add-white">Add</button>
							<button class="btn btn-edit-menu"><img src="/images/restaurants/edit.png" alt="edit">Edit</button>
						</div>
						<div class="clearfix"></div>
						<div class="menu-group-container">
							@foreach($menu_category_id_uniques as $menu_category_name => $menu_category_id)
	                        <div class="filter-title-category" id="sub-{{ str_replace(' ', '', $menu_category_name) }}">
	                            <label>{{ $menu_category_name }}</label>
	                            <img src="/images/restaurants/collapse.png" alt="collapse"/>
	                        </div>
	                        <div class="detail-filter-category" id="detail-sub-{{  str_replace(' ', '', $menu_category_name) }}">
	                        	@foreach($restaurant_management->menus->where('menu_category_id', $menu_category_id) as $menu)
                                <div class="col-sm-8">
                                    <h4 class="menu-title">{{ $menu->item_name }}</h4>
                                    <p>{{ $menu->item_description }}</p>
                                </div>
                                <div class="col-sm-4">
                                    <h4 class="price-value" value="{{ $restaurant_management->removeDoubleZero($menu->item_price) }}">Rp. {{ number_format($restaurant_management->removeDoubleZero($menu->item_price),0,'.',',') }}</h4>
                                </div>	
                                @endforeach
	                        </div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>		
	</div>
	@include('restaurant-management.layouts.footer')
</section>
@stop