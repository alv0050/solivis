@extends('restaurant-management.layouts.master')

@section('title', 'Balance')

@section('content')

@include('restaurant-management.modals.withdraw');
@include('restaurant-management.modals.add_account_bank');
@include('restaurant-management.modals.edit_account_bank');
@include('restaurant-management.layouts.navigation')
<section class="wrapper">
	@include('restaurant-management.layouts.sidebar')
	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main-general">
		<div class="main-content">
			<div class="container-fluid">
				<div class="flash-manage">
					@include('partials.error')
					@include('partials.message')	                	
				</div>
				<div class="col-md-6 left-container">
					<div class="balance-container add-margin-top">
						<h3><img src="/images/restaurants/balance-black.png" alt="balance-black">Balance</h3>
						<div class="balance-group-container">
							<h4>Tersedia dana sebesar</h4>
							<h1>Rp. {{ number_format($restaurant_management->removeDoubleZero($restaurant_management->balance),0,'.',',')  }}</h1>
							<div class="button-group">
								<button class="btn btn-withdraw">Tarik Dana <img src="/images/restaurants/next.png" alt="next"></button>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="info-withdraw">
							<p class="text-center">Penarikan dana dapat diproses di hari kerja (senin - sabtu).</p> 
							<p class="text-center">Tidak termasuk hari libur nasional</p>
						</div>						
					</div>
					<div class="bank-container">
                        <div class="bank_container_header">
							<h3><img src="/images/restaurants/bank.png" alt="bank">Bank Account</h3>
                            <button type="button" class="btn btn-add-account-bank"><img src="/images/restaurants/add-white.png" alt="add-white">Tambah</button>
                            <div class="clearfix"></div>
                        </div>
	                	{!! Form::open(['action' => 'RestaurantManagementController@deleteBank', 'method' => 'DELETE', 'class' => 'form-delete-bank']) !!}
	                		<div class="form-group">
	                			<label for="bank" class="col-sm-5 col-md-6 col-lg-5">Nama Bank</label>
	                			<div class="col-sm-7 col-md-6 col-lg-7">
                                    {!! Form::select('restaurant_bank_id', $restaurant_banks_info, null, $attributes = ['class' => 'form-control default-grey', 'id' => 'select-bank_name']); !!}
	                			</div>
	                			<div class="clearfix"></div>
	                		</div>
	                		<div class="form-group">
                                <label for="account_number" class="col-sm-5 col-md-6 col-lg-5">No. Rekening</label>
                                <div class="col-sm-7 col-md-6 col-lg-7">
                                    {!! Form::text('account_number', null, $attributes = ['class' => 'form-control default-grey', 'id'=>'accountNumber', 'disabled']); !!}
                                </div>	   
                                <div class="clearfix"></div>	
	                		</div>
	                		<div class="form-group">
                                <label for="account_name" class="col-sm-5 col-md-6 col-lg-5">Nama Pemilik Rek.</label>
                                <div class="col-sm-7 col-md-6 col-lg-7">
                                    {!! Form::text('account_name', null, $attributes = ['class' => 'form-control default-grey', 'id'=>'accountName', 'disabled']); !!}
                                </div>	   
                                <div class="clearfix"></div>
	                		</div>
                            {{-- showing data in behind --}}
                            @foreach($restaurant_management->restaurant_banks as $restaurant_bank)
                            <div class="restaurant_management-account-bank_name {{$restaurant_bank->id}}-{{$restaurant_bank->bank->name}} hide">{{ $restaurant_bank->bank->name }}</div>
                            <div class="restaurant_management-account-number-{{$restaurant_bank->id}}-{{$restaurant_bank->bank->name}} hide">{{ $restaurant_bank->account_number }}</div>
                            <div class="restaurant_management-account-name-{{$restaurant_bank->id}}-{{$restaurant_bank->bank->name}} hide">{{ $restaurant_bank->account_name }}</div>
                            <div class="restaurant_management-bank-id-hidden hide">{{ $restaurant_bank->id }}</div>
                            <div class="bank-id-hidden hide">{{ $restaurant_bank->bank->id }}</div>
                            @endforeach
                            <input type="hidden" class="restaurant_bank_id_del_hidden" name="restaurant_bank_id_del" value="">
                            <div class="button-group">
                                <button type="button" class="btn btn-edit-account-bank"><img src="/images/restaurants/edit.png" alt="edit">Edit</button>
                                <button type="button" class="btn btn-delete-account-bank"><img src="/images/restaurants/remove-small.png" alt="remove-small">Hapus</button>
                            </div>
	                		<div class="clearfix"></div>
	               		 {!! Form::close() !!} 
					</div>
				</div>
				<div class="col-md-6 right-container">
					<div class="history-balance-container add-margin-top">
						<h3><img src="/images/restaurants/history.png" alt="history">History</h3>
	                	{!! Form::open(['action' => 'RestaurantManagementController@getHistoryBalance', 'method' => 'GET', 'class' => 'form-search-history-balance']) !!}
							<img src="/images/restaurants/calendar-small.png" alt="calendar-small"/>
                            {!! Form::text('date_start', null, $attributes = ['class' => 'form-control default-black', 'id' => 'input_date_start']); !!}
							<span>-</span>
                            {!! Form::text('date_end', null, $attributes = ['class' => 'form-control default-black', 'id' => 'input_date_end']); !!}
							<button class="btn btn-search"><img src="/images/restaurants/search.png" alt="search"></button>
	               		{!! Form::close() !!} 
	               		<table class="table table-striped table-history-balance">
	               			<thead>
	               				<tr>
	               					<th>Tanggal</th>
	               					<th>Kirim ke</th>
	               					<th>Nominal</th>
	               					<th>Status</th>
	               				</tr>
	               			</thead>
	               			<tbody>
                            @if (!isset($history_balances_search))
                                @foreach($restaurant_history_balances as $restaurant_history_balance)
                                <tr>
                                    <td>{{ $restaurant_management->getProcessDate($restaurant_history_balance->process_date) }}/{{ $restaurant_management->getProcessMonth($restaurant_history_balance->process_date) }}/{{ $restaurant_management->getProcessYear($restaurant_history_balance->process_date) }}
                                    </td>
                                    <td>{{ $restaurant_history_balance->transfer_target }}</td>
                                    <td>Rp. {{ number_format($restaurant_management->removeDoubleZero($restaurant_history_balance->balance_processed),0,'.',',') }}</td>
                                    <td>
                                        {{ $restaurant_history_balance->status_balance->status }}
                                    </td>
                                </tr>
                                @endforeach
                            @else
                                @foreach($history_balances_search as $history_balance)
                                <tr>
                                    <td>{{ $restaurant_management->getProcessDate($history_balance->process_date) }}/{{ $restaurant_management->getProcessMonth($history_balance->process_date) }}/{{ $restaurant_management->getProcessYear($history_balance->process_date) }}
                                    </td>
                                    <td>{{ $history_balance->transfer_target }}</td>
                                    <td>Rp. {{ number_format($restaurant_management->removeDoubleZero($history_balance->balance_processed),0,'.',',') }}</td>
                                    <td>
                                        {{ $history_balance->status_balance->status }}
                                    </td>
                                </tr>
                                @endforeach
                            @endif
	               			</tbody>
	               		</table>
	                    <div class="balance-pagination">
	                        @if(!isset($history_balances_search))
	                            {!! $restaurant_history_balances->render() !!}
	                        @else
	                            {!! $history_balances_search->appends(Request:: only(['date_start'=>'date_start', 'date_end'=>'date_end']))->render() !!}
	                        @endif
	                    </div>  
	               		<div class="clearfix"></div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>		
	</div>
	@include('restaurant-management.layouts.footer')
</section>
@stop