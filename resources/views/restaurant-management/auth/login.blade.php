@extends('restaurant-management.layouts.master')

@section('title', 'Login Restaurant Management')

@section('content')
	<section class="section-login">
		<div class="login">
	        <a href="/"><img src="/images/restaurants/logo-big.png" alt="logo-big"/></a>
	        <div class="title-group">
	        	<div class="half-line"></div>
	        	<h2 class="text-uppercase text-center">Login</h2>
	        	<div class="half-line"></div>
	        	<div class="clearfix"></div>
	        </div>
            @include('partials.message') 
            @include('partials.error') 
			{!! Form::open(['class' => 'form-login']) !!}	
				<div class="form-group">
                    <img src="/images/restaurants/email-black.png" alt="email-black">
                    {!! Form::email('email', $value = null, $attributes = ['class' => 'form-control default', 'placeholder' => 'Email']); !!}
                </div>			
				<div class="form-group">
                    <img src="/images/restaurants/password-auth.png" alt="password-auth">
                    {!! Form::password('password', $array = ['class' => 'form-control default', 'placeholder' => 'Password']); !!}
                </div>			
                <button type="submit" class="btn text-center btn-login">Login</button>
            {!! Form::close() !!}   
            <p class="text-center">Memiliki kendala masuk ke dalam website atau lupa password ? Hubungi Kami.</p>   
            <p class="text-center contact"><img src="/images/restaurants/email.png" alt="email">solivisads@gmail.com</p>           
	    </div>
	</section>
@stop