@extends('restaurant-management.layouts.master')

@section('title', 'Reservation')

@section('content')

@include('restaurant-management.modals.new_reservation');
@include('restaurant-management.layouts.navigation')
<section class="wrapper">
	@include('restaurant-management.layouts.sidebar')
	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main-general">
		<div class="main-content reservation-main-content">
			<div class="container-fluid">
				<div class="flash-manage">
					@include('partials.message')
					@include('partials.error')
				</div>
				<div class="col-md-6 left-container">
					<div class="upcoming-reservation-container add-margin-top" id="load-upcoming-reservation">
						<h3><img src="/images/restaurants/upcoming.png" alt="upcoming">Upcoming Reservation <span class="badge">{{-- content is filled dynamically --}}</span></h3>
						<div class="separator"></div>
						@for($i = 0; $i < count($restaurant_reservations); $i++)
						<div class="reservation">
							@if($restaurant_reservations[$i]->status_reservation_id == 3)
							<img src="/images/restaurants/new.png" alt="new" class="new-reserve new_reservation">
							@elseif($restaurant_reservations[$i]->status_reservation_id == 4)
							<img src="/images/restaurants/accept.png" alt="accept" class="new-reserve">
							@else
							<img src="/images/restaurants/decline.png" alt="decline" class="new-reserve">
							@endif
							<p>{{ $restaurant_reservations[$i]->user->name }} akan melakukan reservasi pada tanggal {{ $restaurant_reservations[$i]->convertReservationDate() }} sebanyak {{ $restaurant_reservations[$i]->paxes }} orang pada jam {{ $restaurant_reservations[$i]->convertTime() }}</p>
							<a href="#" class="view-link"><img src="/images/restaurants/view.png" alt="view" class="view">
	                            <span class="reservation-id-detail hide">{{ $restaurant_reservations[$i]->id }}</span>
	                            <span class="reservation-user-name-detail hide">{{ $restaurant_reservations[$i]->user->name }}</span>
	                            <span class="reservation-date-detail hide">{{ $restaurant_reservations[$i]->convertReservationDate() }}</span>
	                            <span class="reservation-time-detail hide">{{ $restaurant_reservations[$i]->convertTime() }}</span>
	                            <span class="reservation-paxes-detail hide">{{ $restaurant_reservations[$i]->paxes }}</span>
	                            <span class="food-names hide">{{ $restaurant_reservations[$i]->getFoodNames() }}</span>
	                            <span class="food-counts hide">{{ $restaurant_reservations[$i]->getFoodCounts() }}</span>
	                            <span class="food-prices hide">{{ $restaurant_reservations[$i]->getFoodPrices() }}</span>
	                            <span class="status-reservation hide">{{ $restaurant_reservations[$i]->status_reservation_id }}</span>
	                        </a>
						</div>
						@endfor
						<div class="reservation-info">
							<div class="side">
								<img src="/images/restaurants/new.png" alt="new">
								<p>New Reservation</p>
							</div>
							<div class="side">
								<img src="/images/restaurants/accept.png" alt="accept">
								<p>Accepted</p>
							</div>
							<div class="side">
								<img src="/images/restaurants/decline.png" alt="decline">
								<p>Declined</p>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="all-reservation-hidden-container">
							@for($i = 0; $i < count($only_accepted_restaurant_reservations); $i++)
                            <span class="all-reservation-success-date-detail hide">{{ $only_accepted_restaurant_reservations[$i]->convertNumericDate() }}</span>
							<div class="all-reservation-hidden-detail">
	                            <span class="all-reservation-id-detail hide">{{ $only_accepted_restaurant_reservations[$i]->id }}</span>
	                            <span class="all-reservation-user-name-detail hide">{{ $only_accepted_restaurant_reservations[$i]->user->name }}</span>
	                            <span class="all-reservation-date-detail hide">{{ $only_accepted_restaurant_reservations[$i]->convertNumericDate() }}</span>
	                            <span class="all-reservation-time-detail hide">{{ $only_accepted_restaurant_reservations[$i]->convertTime() }}</span>
	                            <span class="all-reservation-paxes-detail hide">{{ $only_accepted_restaurant_reservations[$i]->paxes }}</span>
	                            <span class="all-reservation-food-names hide">{{ $only_accepted_restaurant_reservations[$i]->getFoodNames() }}</span>
	                            <span class="all-reservation-food-counts hide">{{ $only_accepted_restaurant_reservations[$i]->getFoodCounts() }}</span>
							</div>
							@endfor	
						</div>
						<div class="upcoming-reservation-pagination">
							{!! $restaurant_reservations->render() !!}
						</div>
					</div>
				</div>
				<div class="col-md-6 right-container">
					<div class="reservation-date-container add-margin-top">
						<h3><img src="/images/restaurants/calendar.png" alt="calendar">Calendar Reservation</h3>
						<div class="reservation-date-group-container">
							<div id="datepicker-reservation"></div>
							<div class="left-info">
								<div class="round-info"></div>
								<p>Reservasi</p>
							</div>
							<p class="right-info">Total Reservasi : <span class="total_reservation_per_month">{{-- content is filled dynamically --}}</span></p>
							<div class="clearfix"></div>
						</div>
					</div>
					<div class="schedule-container margin-adapt2">
						<h3>Schedule</h3>
						<div class="sub-schedule-container hide">
							<div class="col-sm-4">
								<h2 class="reservation_date_event text-center">{{-- content is filled dynamically --}}</h2>
								<h4 class="reservation_month_and_year_event text-center text-uppercase">{{-- content is filled dynamically --}}</h4>
							</div>
							<div class="col-sm-8">
								<p>Jumlah Reservasi : <span class="total_reservations">{{-- content is filled dynamically --}}</span></p>
								<ol class="reservation-event-detail">
									{{-- content is filled dynamically --}}
								</ol>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
			</div>
		</div>		
	</div>
	@include('restaurant-management.layouts.footer')
</section>
@stop