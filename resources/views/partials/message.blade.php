@if (Session::has('info'))
    <div class="alert alert-info">
		{{ Session::get('info') }}
	</div>
@endif
@if (Session::has('error'))
    <div class="alert alert-danger">
		{{ Session::get('error') }}
	</div>
@endif		
@if (Session::has('status'))
    <div class="alert alert-info">
		{{ Session::get('status') }}
	</div>
@endif		
@if (Session::has('alert-send-business-message'))
	<span id="success-send-business-message"></span>
@endif
@if (Session::has('alert-pay-transfer-bank'))
	<span id="success-pay-transfer-bank" reservation-id={{ Session::get("alert-pay-transfer-bank") }}></span>
@endif