@extends('user.layouts.master')

@section('title', 'Contact Us')

@section('content')
    @include('user.layouts.header-line')
    @include('user.layouts.navigation')
    <!-- SECTION-CONTACT -->
    <section class="contact">
        <div class="contact-title-container">
            <div class="container">
                <div class="contact-title">
                    <h2 class="text-center text-uppercase">Contact Us</h2>
                    <h3 class="text-center">Apa yang bisa kami bantu?</h3>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="contact-content">
                {!! Form::open(['class' => 'form-contact']) !!}
                    <div class="form-group">
                        {!! Form::text('name', $value = null, $attributes = ['class' => 'form-control default', 'placeholder' => 'Nama anda']); !!}
                    </div>          
                    <div class="form-group">
                        {!! Form::email('email', $value = null, $attributes = ['class' => 'form-control default', 'placeholder' => 'Email anda']); !!}
                    </div>   
                    <div class="form-group">
                        {!! Form::textarea('message', $value = null, $attributes = ['class' => 'form-control default', 'placeholder' => 'Pesan', 'rows' => '5']); !!}
                    </div>
                    <div class="send-message-group">
                        <img src="/images/send-message.png" alt="send-message">
                        <button class="btn btn-send-message">Kirim</button>       
                    </div>
                    <div class="clearfix"></div>
                {!! Form::close() !!}   
            </div>
        </div>
    </section>
    <!-- END: SECTION-CONTACT -->
    @include('user.layouts.footer-info')
    @include('user.layouts.footer')
@stop