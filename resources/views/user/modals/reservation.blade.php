<div class="modal fade" id="reservationModal" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>        
        <h4 class="modal-title text-uppercase text-center">Info</h4>
      </div>
      <div class="modal-body">
        <p class="text-center">Pax, Tanggal dan Jam Reservasi anda harus diisi.</p>
      </div>
    </div>
  </div>
</div>  
