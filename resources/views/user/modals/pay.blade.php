<div class="modal fade" id="payModal" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
        <h4 class="modal-title text-uppercase text-center">Terima kasih</h4>
      </div>
      <div class="modal-body">
        <img src="/images/success-send-message.png" alt="success-send-message">
        <a href="" class="btn to-confirm-payment">Konfirmasi Pembayaran</a>        
        <p class="text-center info">Konfirmasi pembayaran dapat dilakukan di menu reservasi.</p>
      </div>
    </div>
  </div>
</div>  
