<div class="modal fade" id="shareModal" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
        <h4 class="modal-title text-uppercase text-center">Share via</h4>
      </div>
      <div class="modal-body">
        <a class="fb-link" href="http://www.facebook.com/sharer.php?u=solivis.com/restaurant/show/{{ $restaurant_management->id }}" target="_blank">
          <img src="/images/fb.png" alt="facebook"><div class="text">Facebook</div>
        </a>
        <a class="twitter-link" href="https://twitter.com/intent/tweet?text=Visit {{ $restaurant_management->name }} in solivis.com/restaurant/show/{{ $restaurant_management->id }}&amp;/&amp;via=solivisads" target="_blank" target="_blank">
          <img src="/images/twitter.png" alt="twitter"><div class="text">Twitter</div>
        </a>
      </div>
    </div>
  </div>
</div>  
