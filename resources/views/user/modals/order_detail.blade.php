<div class="modal fade" id="orderDetailModal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>        
        <h4 class="modal-title text-uppercase text-center">Pesanan</h4>
      </div>
      <div class="modal-body">
        <div class="order-detail">
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Menu</th>
                  <th class="text-center">@</th>
                  <th class="text-right">Harga</th>
                </tr>
              </thead>
              <tbody>
                {{-- content is filled dynamically --}}
              </tbody>                
            </table>
          </div>
          <p class="text-right total-pay">Total Pembayaran <span>{{-- content is filled dynamically --}}</span></p>
      </div>
    </div>
  </div>
</div>  
