<div class="modal fade" id="success_send_business_messageModal" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
        <h4 class="modal-title text-uppercase text-center">Terima kasih</h4>
      </div>
      <div class="modal-body">
        <img src="/images/success-send-message.png" alt="success-send-message">
        <p class="text-center">Kami akan menghubungi anda dalam waktu dekat.</p>
        <p class="text-center">Terima kasih telah mempercayai kami.</p>
      </div>
    </div>
  </div>
</div>  
