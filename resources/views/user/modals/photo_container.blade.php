<div class="modal fade" id="photoContainerModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Photo</h3>
            </div>
            <div class="modal-body">
                <div class="photo-detail">
                    <img class="food_photo_name_url" class="img-responsive">
                    <h4 class="text-center food_photo_name_modal"></h4>
                    {!! Form::open(['action'=>'RestaurantManagementController@deleteFoodPhoto', 'method'=>'DELETE', 'class' => 'form-delete-food-photo']) !!}
                        <input type="hidden" class="food_photo_url_hidden" name="food_photo_url">
                        <input type="hidden" class="food_photo_id_hidden" name="food_photo_id">
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
