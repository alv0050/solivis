<div class="modal fade" id="reserveTimeLessModal" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>        
        <h4 class="modal-title text-uppercase text-center">Info</h4>
      </div>
      <div class="modal-body">
        <p class="text-center">Maaf :(</p>
        <p class="text-center">Reservasi anda hari ini hanya dapat diproses apabila selisih jam saat anda reservasi (sekarang) dengan jam reservasi anda adalah 6 jam (minimal).</p>
      </div>
    </div>
  </div>
</div>  
