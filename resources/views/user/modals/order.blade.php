<div class="modal fade" id="orderModal" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>        
        <h4 class="modal-title text-uppercase text-center">Info</h4>
      </div>
      <div class="modal-body">
        <p class="text-center">Silahkan pilih makanan atau minuman yang ingin direservasi.</p>
      </div>
    </div>
  </div>
</div>  
