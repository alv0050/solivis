<div class="modal fade" id="onlyUseBalanceModal" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>        
        <h4 class="modal-title text-uppercase text-center">Info</h4>
      </div>
      <div class="modal-body">
        <p>Reservasi anda hari ini hanya dapat dibayar menggunakan saldo.</p>
        <p>Transfer Bank tidak dapat dilakukan lagi karena waktu untuk memproses reservasi anda tidak mencukupi.</p>
        <p>Pilih lanjut apabila anda ingin membayar dengan saldo.</p>
        <button class="btn btn-continue-pay-with-balance">Lanjut</button>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>  
