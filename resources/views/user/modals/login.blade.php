<div class="modal fade" id="loginModal" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
        <h4 class="modal-title text-uppercase">Masuk Ke Solivis</h4>
      </div>
      <div class="modal-body">
      {!! Form::open(['action'=>'Auth\AuthController@postLogin', 'class' => 'form-login']) !!}
        <div class="form-group form-group-login">
          {!! Form::email('email', $value = null, $attributes = ['class' => 'form-control default', 'placeholder' => 'Alamat Email', 'id' => 'email_data']); !!}
        </div>
        <div class="form-group form-group-login">
          {!! Form::password('password', $array = ['class' => 'form-control default', 'placeholder' => 'Kata Sandi', 'id' => 'password_data']); !!}
        </div>
        <div class="checkbox remember-me">
          <label>
            {!! Form::checkbox('remember', 1, true); !!} <span class="remember-info">Biarkan saya tetap masuk</span>
          </label>
        </div>
        <div class="forget-password"><a href="/password/email">Lupa kata sandi?</a></div>
        <div class="clearfix"></div>
        <div class="error-message"></div>
        <button type="submit" class="btn text-center btn-login">Masuk</button>
      {!! Form::close() !!}       
      </div>
    </div>
  </div>
</div>  
