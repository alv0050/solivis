<div class="modal fade" id="reviewModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
                <h3 class="modal-title text-uppercase">Review</h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    {!! Form::open(['action' => 'ReservationController@postReservationReview', 'class' => 'form-review']) !!}
                        <div class="rating-dropdown">
                            <label for="rating" class="col-sm-4">Rating</label>
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <select class="form-control default" name="rating" id="rating_dropdown">
                                        <option value="1" data-imagesrc="/images/one-star.png"></option>
                                        <option value="2" data-imagesrc="/images/two-star.png"></option>
                                        <option value="3" data-imagesrc="/images/three-star.png"></option>
                                        <option value="4" data-imagesrc="/images/four-star.png"></option>
                                        <option value="5" data-imagesrc="/images/five-star.png"></option>
                                    </select>              
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>                    
                        <div class="form-group">
                            <label for="comment" class="col-sm-4">Komentar (Opsional)</label>
                            <div class="col-sm-8">
                            {!! Form::textarea('comment', null, $attributes = ['class' => 'form-control default', 'rows' => '5']); !!}
                            </div>
                            <div class="clearfix"></div>
                        </div>  
                        <input type="hidden" name="reservation_id" class="reservation_id">                  
                        <button class="btn text-center btn-save-review">Simpan</button>
                    {!! Form::close() !!}       
                </div>
            </div>
        </div>
    </div>
</div>  
