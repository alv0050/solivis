<div class="modal fade" id="listHolidayModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title text-uppercase">Daftar Libur Nasional</h3>
            </div>
            <div class="modal-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Hari besar</th>
                            <th>Tanggal</th>
                            <th>Hari</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Tahun baru Imlek</td>
                            <td>1 Januari 2016</td>
                            <td>Jumat</td>
                        </tr>
                        <tr>
                            <td>Tahun baru Imlek</td>
                            <td>8 Februari 2016</td>
                            <td>Senin</td>
                        </tr>
                        <tr>
                            <td>Nyepi</td>
                            <td>9 Maret 2016</td>
                            <td>Rabu</td>
                        </tr>
                        <tr>
                            <td>Hari Wafat Isa Al-Masih</td>
                            <td>25 Maret 2016</td>
                            <td>Jumat</td>
                        </tr>
                        <tr>
                            <td>Hari Buruh Internasional</td>
                            <td>1 Mei 2016</td>
                            <td>Minggu</td>
                        </tr>
                        <tr>
                            <td>Kenaikan Isa Almasaih</td>
                            <td>5 Mei 2016</td>
                            <td>Kamis</td>
                        </tr>
                        <tr>
                            <td>Isa Miraj Nabi Muhammad SAW </td>
                            <td>6 Mei 2016</td>
                            <td>Jumat</td>
                        </tr>
                        <tr>
                            <td>Waisak</td>
                            <td>22 Mei 2016</td>
                            <td>Minggu</td>
                        </tr>
                        <tr>
                            <td>Idul Fitri</td>
                            <td>6 Juli 2016</td>
                            <td>Rabu</td>
                        </tr>
                        <tr>
                            <td>Idul Fitri Hari 2</td>
                            <td>7 Juli 2016</td>
                            <td>Kamis</td>
                        </tr>
                        <tr>
                            <td>Kemerdekaan RI</td>
                            <td>17 Agustus 2016</td>
                            <td>Rabu</td>
                        </tr>
                        <tr>
                            <td>Idul Adha</td>
                            <td>12 September 2016</td>
                            <td>Senin</td>
                        </tr>
                        <tr>
                            <td>Tahun Baru Hijriah</td>
                            <td>2 Oktober 2016</td>
                            <td>Minggu</td>
                        </tr>
                        <tr>
                            <td>Maulid Nabi Muhammad SAW</td>
                            <td>12 Desember 2016</td>
                            <td>Senin</td>
                        </tr>
                        <tr>
                            <td>Natal</td>
                            <td>25 Desember 2016</td>
                            <td>Minggu</td>
                        </tr>        
                    </tbody>
                </table>                
            </div>
            <div class="modal-footer">
                <p>* segala kemungkinan perubahan penanggalan yang dapat terjadi akan kami informasikan lebih lanjut, apabila mendapat pengumuman resmi dari yang berwenang</p>
            </div>
        </div>
    </div>
</div>
