<div class="modal fade" id="consult_businessModal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
        <h4 class="modal-title text-uppercase">Konsultasi dengan kami</h4>
      </div>
      <div class="modal-body">
      {!! Form::open(['action' => 'BaseController@postBusiness', 'class' => 'form-horizontal form-consult_business']) !!}
        <div class="form-group">
          <label for="user_name" class="col-sm-3 control-label">Nama Lengkap</label>
          <div class="col-sm-9">
            {!! Form::text('user_name', $value = null, $attributes = ['class' => 'form-control default']); !!}
          </div>
        </div>
        <div class="form-group">
          <label for="name" class="col-sm-3 control-label">Nama Usaha</label>
          <div class="col-sm-9">
            {!! Form::text('name', $value = null, $attributes = ['class' => 'form-control default']); !!}
          </div>
        </div>
        <div class="form-group">
          <label for="address" class="col-sm-3 control-label">Alamat</label>
          <div class="col-sm-9">
            {!! Form::text('address', $value = null, $attributes = ['class' => 'form-control default']); !!}
          </div>
        </div>
        <div class="form-group">
          <label for="phone_number" class="col-sm-3 control-label">No. Telepon/HP</label>
          <div class="col-sm-9">
            {!! Form::text('phone_number', $value = null, $attributes = ['class' => 'form-control default']); !!}
          </div>
        </div>
        <div class="form-group">
          <label for="email" class="col-sm-3 control-label">Alamat Email</label>
          <div class="col-sm-9">
            {!! Form::email('email', $value = null, $attributes = ['class' => 'form-control default']); !!}
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-3 col-sm-9">
            <p>Dengan menekan tombol Kirim, saya mengkonfirmasi telah menyetujui Syarat dan Ketentuan serta Kebijakan Privasi Solivis.</p>   
          </div>       
        </div>
        <div class="form-group">
          <div class="col-sm-offset-3 col-sm-9">
            <button class="btn btn-send-consult-business" id="send_consult_business">Kirim</button>
          </div>
        </div>
      {!! Form::close() !!}       
      </div>
    </div>
  </div>
</div>  
