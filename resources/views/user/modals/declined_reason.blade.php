<div class="modal fade" id="declinedReasonModal" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>        
        <h4 class="modal-title text-uppercase text-center">Alasan Penolakan</h4>
      </div>
      <div class="modal-body">
        <p class="text-center declined_reason_detail">{{-- content is filled dynamically --}}</p>
      </div>
    </div>
  </div>
</div>  
