@extends('user.layouts.master')

@section('title', 'Search')

@section('content')
	@include('user.layouts.header-line')
	@include('user.layouts.navigation')
    <!-- SECTION-SEARCH -->
    <section class="section-search">
        <div class="container">
            <section class="info-search">
                <h3 class="text-uppercase">Hasil pencarian</h3>
                <div class="info-results">
                    @if(count($restaurants_search) != 0)
                    @if(isset($address) && isset($restaurant_name) && isset($cuisine_name))
                    @if($address || $restaurant_name || $cuisine_name)
                    <h4>Terdapat <span class="quantity-results">{{ count($restaurants_search) }}</span> restoran dengan keyword pencarian: 
                        @if($address)
                        <span class="keyword">
                            {{ $address }}, 
                         </span>
                         @endif
                        @if($restaurant_name)
                        <span class="keyword">
                            {{ $restaurant_name }}, 
                         </span>
                         @endif
                         @if($cuisine_name)
                         <span class="keyword">{{ $cuisine_name }}</span>
                         @endif
                    </h4>
                    @endif
                    @else
                    <h4><span class="quantity-results">{{ count($total_search_results) }}</span> Restoran</h4>
                    @endif
                    @else
                    <h4>Maaf :( Hasil pencarian tidak ditemukan.</span></h4>
                    @endif
                    <div class="sort-result">
                        @if(count($restaurants_search) != 0)
                        <h4 class="text-left">Urutkan : </h4>
                        <div class="form-group form-group-sort">
                        {!! Form::open(['action' => 'BaseController@getSortRestaurant', 'method' => 'GET', 'class' => 'form-sort-search-restaurant']) !!}
                            {!! Form::select('sort_search_restaurant', array('all' => 'Semua', 'restaurant_name' => 'Nama', 'rating' => 'Rating', 'most_booked' => 'Most Booked'),  null, ['class' => 'form-control sort-search-restaurant']);!!}
                        {!! Form::close() !!}                             
                        </div>
                        @endif
                    </div>
                    <div class="clearfix"></div>
                </div>
            </section>
            <section class="search-results">
                <div class="row">
                    <div class="col-sm-4 col-md-3">
                        {!! Form::open(['action' => 'BaseController@getFilterSearch', 'method' => 'GET']) !!}
                            <div class="filter-title-prices">
                                <h4>Prices</h4>
                                <img src="/images/expand.png" alt="expand"/>
                            </div>
                            <div class="detail-filter-prices">
                                <div class="radio radio-group-prices">
                                    <label>
                                        {!! Form::radio('prices', 'lt100000', false, $attributes = ["class" => "radio-prices", "id" => "radio-prices-lt100000"]); !!} 
                                        <div class="info-prices">Dibawah Rp. 100,000</div>
                                    </label>
                                </div>
                                <div class="radio radio-group-prices">
                                     <label>
                                        {!! Form::radio('prices', '100001-300000', false, $attributes = ["class" => "radio-prices", "id" => "radio-prices-100001-300000"]); !!} 
                                        <div class="info-prices">Rp. 100,001 - Rp. 300,000</div>      
                                    </label>
                                </div>                  
                                 <div class="radio radio-group-prices">
                                     <label>
                                        {!! Form::radio('prices', '300001-500000', false, $attributes = ["class" => "radio-prices", "id" => "radio-prices-300001-500000"]); !!} 
                                        <div class="info-prices">Rp. 300,001 - Rp. 500,000</div>      
                                    </label>
                                </div>
                                <div class="radio radio-group-prices">  
                                    <label>                    
                                        {!! Form::radio('prices', 'gt500000', false, $attributes = ["class" => "radio-prices", "id" => "radio-prices-gt500000"]); !!} 
                                        <div class="info-prices">Diatas Rp. 500,000</div> 
                                    </label>
                                </div>
                            </div>
                            <div class="filter-title-cuisine border-filter-spc-collapse">
                                <h4>Jenis Kuliner</h4>
                                <img src="/images/collapse.png" alt="collapse"/>
                            </div>
                            <div class="detail-filter-cuisine">
                                <div class="checkbox checkbox-group-cuisine">
                                    <label>
                                        {!! Form::checkbox('cuisines[0]', 'Italian', false, $attributes = ["class" => "checkbox-cuisine", "id" => "checkbox-cuisine-Italian"]); !!} 
                                        <div class="info-cuisine">Italian</div>
                                    </label>
                                </div>                            
                                <div class="checkbox checkbox-group-cuisine">
                                    <label>
                                        {!! Form::checkbox('cuisines[1]', 'Indonesia', false, $attributes = ["class" => "checkbox-cuisine", "id" => "checkbox-cuisine-Indonesia"]); !!} 
                                        <div class="info-cuisine">Indonesia</div>
                                    </label>
                                </div>                            
                                <div class="checkbox checkbox-group-cuisine">
                                    <label>
                                        {!! Form::checkbox('cuisines[2]', 'Chinese', false, $attributes = ["class" => "checkbox-cuisine", "id" => "checkbox-cuisine-Chinese"]); !!} 
                                        <div class="info-cuisine">Chinese</div>
                                    </label>
                                </div>                            
                                <div class="checkbox checkbox-group-cuisine">
                                    <label>
                                        {!! Form::checkbox('cuisines[3]', 'Korea', false, $attributes = ["class" => "checkbox-cuisine", "id" => "checkbox-cuisine-Korea"]); !!} 
                                        <div class="info-cuisine">Korea</div>
                                    </label>
                                </div>                            
                                <div class="checkbox checkbox-group-cuisine">
                                    <label>
                                        {!! Form::checkbox('cuisines[4]', 'Western', false, $attributes = ["class" => "checkbox-cuisine", "id" => "checkbox-cuisine-Western"]); !!} 
                                        <div class="info-cuisine">Western</div>
                                    </label>
                                </div>                            
                            </div>                        
                            <div class="filter-title-service border-filter-spc-collapse">
                                <h4>Services</h4>
                                <img src="/images/collapse.png" alt="collapse"/>
                            </div>
                            <div class="detail-filter-service">
                                <div class="checkbox checkbox-group-service">
                                    <label>
                                        {!! Form::checkbox('services[0]', 'AC', false, $attributes = ["class" => "checkbox-service", "id" => "checkbox-service-AC"]); !!} 
                                        <div class="info-service">AC</div>
                                    </label>
                                </div>                            
                                <div class="checkbox checkbox-group-service">
                                    <label>
                                        {!! Form::checkbox('services[1]', 'Halal', false, $attributes = ["class" => "checkbox-service", "id" => "checkbox-service-Halal"]); !!} 
                                        <div class="info-service">Halal</div>
                                    </label>
                                </div>                            
                                <div class="checkbox checkbox-group-service">
                                    <label>
                                        {!! Form::checkbox('services[2]', 'Non-Halal', false, $attributes = ["class" => "checkbox-service", "id" => "checkbox-service-Non-Halal"]); !!} 
                                        <div class="info-service">Non-Halal</div>
                                    </label>
                                </div>                            
                                <div class="checkbox checkbox-group-service">
                                    <label>
                                        {!! Form::checkbox('services[3]', 'Take Away', false, $attributes = ["class" => "checkbox-service", "id" => "checkbox-service-TakeAway"]); !!} 
                                        <div class="info-service">Take Away</div>
                                    </label>
                                </div>                            
                                <div class="checkbox checkbox-group-service">
                                    <label>
                                        {!! Form::checkbox('services[4]', 'Smoking Area', false, $attributes = ["class" => "checkbox-service", "id" => "checkbox-service-SmokingArea"]); !!} 
                                        <div class="info-service">Smoking Area</div>
                                    </label>
                                </div>                            
                                <div class="checkbox checkbox-group-service">
                                    <label>
                                        {!! Form::checkbox('services[5]', 'Wifi', false, $attributes = ["class" => "checkbox-service", "id" => "checkbox-service-Wifi"]); !!} 
                                        <div class="info-service">Wifi</div>
                                    </label>
                                </div>                            
                                <div class="checkbox checkbox-group-service">
                                    <label>
                                        {!! Form::checkbox('services[6]', 'Serves Alcohol', false, $attributes = ["class" => "checkbox-service", "id" => "checkbox-service-ServesAlcohol"]); !!} 
                                        <div class="info-service">Serves Alcohol</div>
                                    </label>
                                </div>                            
                                <div class="checkbox checkbox-group-service">
                                    <label>
                                        {!! Form::checkbox('services[7]', 'Parking Area', false, $attributes = ["class" => "checkbox-service", "id" => "checkbox-service-ParkingArea"]); !!} 
                                        <div class="info-service">Parking Area</div>
                                    </label>
                                </div>                            
                            </div>
                            <button class="btn text-center text-uppercase btn-filter-search">Filter</button>                             
                        {!! Form::close() !!}                        
                    </div>
                    <div class="col-sm-8 col-md-6">
                        @foreach($restaurants_search as $restaurant_search)
                        <div class="search-restaurant-info-group">
                            <div class="col-sm-12 col-md-4 no-padding">
                                <div class="reservation-restaurant-group">
                                    <img src="/images/restaurant_uploads/{{ $restaurant_search->photo_url }}" alt="{{ $restaurant_search->name }}" class="restaurant-profile-picture"/>
                                    {{-- SHOW ONLY IN LAPTOP AND PC --}}
                                    {!! Form::open(['action' => ['RestaurantController@getShow', $restaurant_search->id], 'method' => 'GET', 'target' => '_blank', 'class' => 'first-reservation-form']) !!}
                                        <p class="info-btn-reservation">
                                            <button class="btn text-center text-uppercase" role="button">Reservasi</button> 
                                        </p>
                                        @if(isset($cuisine_name) || isset($paxes) || isset($reservation_date) || isset($reservation_time))
                                        <input type="hidden" name="cuisine_name" value="{{ $cuisine_name }}">
                                        <input type="hidden" name="paxes" value="{{ $paxes }}">
                                        <input type="hidden" name="reservation_date" value="{{ $reservation_date }}">
                                        <input type="hidden" name="reservation_time" value="{{ $reservation_time }}">
                                        @endif
                                    {!! Form::close() !!}
                                </div>                                
                            </div>
                            <div class="col-xs-6 col-sm-5 col-md-5 no-padding">
                                <h3>{{ $restaurant_search->name }}</h3>
                                <div class="restaurant-info">
                                    <img src="/images/location.png" alt="location">
                                    <h4 class="location-title">{{ $restaurant_search->address }}</h4>
                                    <h4>Jenis Kuliner: {{ $restaurant_search->getCuisines() }}</h4>
                                    <h4>Opening Hours: {{ $restaurant_search->getOpenTimeHourAndMinute() }}-{{ $restaurant_search->getCloseTimeHourAndMinute() }}</h4>
                                    <h4>Services: {{ $restaurant_search->getServices() }}</h4>
                                    <h4>Prices: {{ $restaurant_search->getLowestPrice() }}-{{ $restaurant_search->getHighestPrice() }}</h4>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-5 col-md-3 no-padding">
                                <div class="restaurant-rating-and-review-info">
                                    <div class="star-rating">
                                        <p>Makanan</p>
                                         @for($i=0; $i<$restaurant_search->getTotalRatingsAverage(); $i++)
                                        <img src="/images/star-yellow.png" alt="star-yellow"/>
                                        @endfor
                                        @for($i=0; $i<(5 - $restaurant_search->getTotalRatingsAverage()); $i++)
                                        <img src="/images/star-black.png" alt="star-black"/>
                                        @endfor
                                    </div>
                                    <div class="review-rating">
                                        <p>{{ $restaurant_search->getTotalRatings() }} Review</p>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            {{-- SHOW ONLY IN SMARTPHONE AND TABLET --}}
                            {!! Form::open(['action' => ['RestaurantController@getShow', $restaurant_search->id], 'method' => 'GET', 'target' => '_blank', 'class' => 'second-reservation-form']) !!}
                                <p class="info-btn-reservation">
                                    <button class="btn text-center text-uppercase" role="button">Reservasi</button> 
                                </p>
                                @if(isset($cuisine_name) || isset($paxes) || isset($reservation_date) || isset($reservation_time))
                                <input type="hidden" name="cuisine_name" value="{{ $cuisine_name }}">
                                <input type="hidden" name="paxes" value="{{ $paxes }}">
                                <input type="hidden" name="reservation_date" value="{{ $reservation_date }}">
                                <input type="hidden" name="reservation_time" value="{{ $reservation_time }}">
                                @endif
                            {!! Form::close() !!}                            
                        </div>    
                        @endforeach                    
                    </div>
                    <div class="col-sm-8 col-md-3 position-adapt-container">
                        @if(count($recommendation_restaurants) != 0)
                        <div class="recommendation-search-container">
                            <div class="recommendation-search-title">
                                <img src="/images/recommendation.png" alt="recommendation"/>
                                <h4>Rekomendasi</h4>
                            </div>
                            <div class="recommendation-search-divider-title"></div>
                            @foreach($recommendation_restaurants as $recommendation_restaurant)
                            <div class="recommendation-search-content">
                                <div class="col-md-4 no-padding">
                                    <a href="/restaurant/show/{{ $recommendation_restaurant->id }}" class="recommendation-restaurant-profile-picture" target="_blank"><img src="/images/restaurant_uploads/{{ $recommendation_restaurant->photo_url }}" alt="{{ $recommendation_restaurant->name }}"/></a>
                                </div>
                                <div class="col-md-8 no-padding">
                                    <div class="recommendation-search-info">
                                        <h4 class="no-margin"><a href="/restaurant/show/{{ $recommendation_restaurant->id }}" target="_blank">{{ $recommendation_restaurant->name }}</a></h4>
                                        <h5 class="no-margin">{{ $recommendation_restaurant->getCuisines() }}</h5>
                                        <div class="reviews">
                                            <h5 class="no-margin">Makanan</h5>
                                            <div class="stars">
                                                 @for($i=0; $i<$recommendation_restaurant->getTotalRatingsAverage(); $i++)
                                                <img src="/images/normal_star-yellow.png" alt="normal_star-yellow"/>
                                                @endfor
                                                @for($i=0; $i<(5 - $recommendation_restaurant->getTotalRatingsAverage()); $i++)
                                                <img src="/images/normal_star-black.png" alt="normal_star-black"/>
                                                @endfor
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="recommendation-search-divider"></div>
                            @endforeach
                            <div class="clearfix"></div>
                        </div>
                        @endif
                    </div>
                    <div class="clearfix"></div>
                </div>
            </section>
            <div class="results-pagination">
                <div class="col-md-offset-3 col-md-6 col-sm-offset-4 col-sm-8 no-padding">
                    {!! $restaurants_search->appends(Request::only(['address'=>'address', 'restaurant_name' => 'restaurant_name', 'cuisine' => 'cuisine', 'paxes' => 'paxes', 'reservation_date' => 'reservation_date', 'reservation_time' => 'reservation_time', 'sort_search_restaurant', 'sort_search_restaurant', 'prices' => 'prices', 'cuisines' => 'cuisines', 'services' => 'services']))->render() !!}
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </section>
    <!-- END: SECTION-SEARCH -->
    @include('user.layouts.footer-info')
    @include('user.layouts.footer')
@stop