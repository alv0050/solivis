@extends('user.layouts.master')

@section('title', 'Terms')

@section('content')
    @include('user.layouts.header-line')
    @include('user.layouts.navigation')
    <!-- SECTION-PRIVACY-POLICY -->
    <section class="terms">
        <div class="container">
            <div class="one-line"></div>
            <div class="col-sm-3 col-sm-push-9 no-padding">
                <ul class="link-terms">
                    <li><a href="#definition">Definisi</a></li>
                    <li><a href="#account-balance-password-security">Akun, Saldo Solivis, Password dan Keamanan</a></li>
                    <li><a href="#transaction-process">Proses Reservasi</a></li>
                    <li><a href="#price">Harga</a></li>
                    <li><a href="#content">Konten</a></li>
                    <li><a href="#balance-withdrawed">Penarikan Dana</a></li>
                    <li><a href="#business-term">Syarat Daftar Usaha</a></li>
                </ul>
            </div>
            <div class="col-sm-9 col-sm-pull-3 no-padding">
                <h2>Syarat & Ketentuan</h2>
                <p>Syarat & ketentuan yang ditetapkan di bawah ini mengatur pemakaian jasa yang ditawarkan terkait penggunaan situs www.solivis.com. Pengguna disarankan membaca dengan seksama karena dapat berdampak kepada hak dan kewajiban Pengguna di bawah hukum. Dengan mendaftar dan/atau menggunakan situs www.solivis.com, maka pengguna dianggap telah membaca, mengerti, memahami dan menyetujui semua isi dalam Syarat & ketentuan. Syarat & ketentuan ini merupakan bentuk kesepakatan yang dituangkan dalam sebuah perjanjian yang sah antara Pengguna dengan pihak solivis. Jika pengguna tidak menyetujui salah satu, sebagian, atau seluruh isi yarat & ketentuan, maka pengguna tidak diperkenankan menggunakan layanan di www.solivis.com.</p>
                <h3 id="definition">A. DEFINISI</h3>
                <p>1. Solivis adalah suatu perseroan terbatas yang menjalankan kegiatan usaha jasa web portal www.solivis.com, yakni situs reservasi restoran yang disediakan oleh restoran terdaftar. Selanjutnya disebut Solivis.</p>
                <p>2. Situs Solivis adalah www.solivis.com.</p>
                <p>3. Syarat & ketentuan adalah perjanjian antara Pengguna dan Solivis yang berisikan seperangkat peraturan yang mengatur hak, kewajiban, tanggung jawab pengguna dan Solivis, serta tata cara penggunaan sistem layanan Solivis.</p>
                <p>4. Pengguna adalah pihak yang menggunakan layanan Solivis, tidak terbatas pada user, restoran ataupun pihak lain yang sekedar berkunjung ke Situs Solivis.</p>
                <p>5. Reservasi adalah proses pemesanan tempat pada restoran yang terdaftar dalam situs Solivis.</p>
                <p>6. Rekening Resmi Solivis adalah rekening bersama yang disepakati oleh Solivis dan para pengguna untuk proses transaksi jual beli di Situs Solivis.</p>
                <h3 id="account-balance-password-security">B. AKUN, SALDO SOLIVIS, PASSWORD DAN KEAMANAN</h3>
                <p>1. Pengguna dengan ini menyatakan bahwa pengguna adalah orang yang cakap dan mampu untuk mengikatkan dirinya dalam sebuah perjanjian yang sah menurut hukum.</p>
                <p>2. Solivis tidak memungut biaya pendaftaran kepada Pengguna.</p>
                <p>3. Pengguna yang telah mendaftar berhak melakukan reservasi</p>
                <p>4. Solivis tanpa pemberitahuan terlebih dahulu kepada Pengguna, memiliki kewenangan untuk melakukan tindakan yang perlu atas setiap dugaan pelanggaran atau pelanggaran Syarat & ketentuan dan/atau hukum yang berlaku, yakni tindakan berupa suspensi akun, dan/atau penghapusan akun pengguna.</p>
                <p>5. Solivis memiliki kewenangan untuk menutup akun Pengguna baik sementara maupun permanen apabila didapati adanya tindakan kecurangan dalam bertransaksi.</p>
                <p>6. Pengguna dilarang untuk menciptakan dan/atau menggunakan alat dan/atau fitur otomatis yang bertujuan untuk melakukan manipulasi pada sistem Solivis.</p>
                <p>7. Solivis memiliki kewenangan untuk melakukan pembekuan saldo Solivis Pengguna apabila ditemukan / diduga adanya tindak kecurangan dalam bertransaksi dan/atau pelanggaran terhadap syarat da ketentuan Solivis.</p>
                <p>8. Pengguna bertanggung jawab secara pribadi untuk menjaga kerahasiaan akun dan password untuk semua aktivitas yang terjadi dalam akun Pengguna.</p>
                <p>9. Solivis tidak akan meminta password akun Pengguna untuk alasan apapun, oleh karena itu Solivis menghimbau Pengguna agar tidak memberikan password akun Anda kepada pihak manapun, baik kepada pihak ketiga maupun kepada pihak yang mengatasnamakan Solivis.</p>
                <p>10. Pengguna setuju untuk memastikan bahwa Pengguna keluar dari akun di akhir setiap sesi dan memberitahu Solivis jika ada penggunaan tanpa izin atas sandi atau akun Pengguna.</p>
                <p>11. Pengguna dengan ini menyatakan bahwa Solivis tidak bertanggung jawab atas kerugian ata kerusakan yang timbul dari penyalahgunaan akun Pengguna.</p>
                <h3 id="transaction-process">C. PROSES RESERVASI</h3>
                <p>1. User wajib bertransaksi melalui prosedur transaksi yang telah ditetapkan oleh Solivis. User melakukan pembayaran dengan menggunakan metode pembayaran yang sebelumnya telah dipilih oleh User, dan kemudian Solivis akan meneruskan dana ke pihak Restoran apabila tahapan transaksi reservasi pada sistem Solivis telah selesai.</p>
                <p>2. Saat melakukan Reservasi, User menyetujui bahwa User bertanggung jawab untuk membaca, memahami, dan menyetujui informasi/deskripsi keseluruhan proses reservasi sebelum komitmen untuk melakukan reservasi.</p>
                <p>3. User memahami sepenuhnya dan menyetujui bahwa segala transaksi yang dilakukan selain melalui Rekening Resmi Solivis dan/atau tanpa sepengetahuan Solivis (melalui fasilitas/jaringan pribadi, pengiriman pesan, pengaturan transaksi khusus diluar situs Solivis atau upaya lainnya) adalah merupakan tanggung jawab pribadi dari User.</p>
                <p>4. Solivis memiliki kewenangan sepenuhnya untuk menolak pembayaran tanpa pemberitahuan terlebih dahulu.</p>
                <p>5. Ketika User ingin melakukan reservasi dengan menggunakan transaksi transfer bank pada hari yang sama dengan hari saat reservasi, Solivis mewajibkan interval waktu reservasinya minimal 6 jam dan belum lewat dari waktu verifikasi Solivis. Misalnya User A ingin melakukan reservasi pada sore ini jam 17.00 dengan menggunakan transfer bank, maka sistem Solivis memperbolehkan A melakukan reservasi tersebut paling lambat di jam 11.00. Jika User A melakukan transaksi di atas jam 11.00, maka reservasi tidak dapat dilakukan kecuali melalui pembayaran saldo.</p>
                <p>6. Jika reservasi yang dilakukan seperti yang disebutkan di poin 6, maka Solivis akan membatasi waktu konfirmasi pembayaran menjadi beberapa jam, tergantung kapan User melakukan reservasi. Jika User tidak melakukan pembayaran ataupun melakukan pembayaran lewat dari batas waktu yang diberikan, maka reservasi User akan dibatalkan. Untuk pembayaran yang telah lewat dari batas waktu yang diberikan, maka jumlah dana yang telah ditransfer akan dikembalikan ke saldo User.</p>
                <p>7. Pembayaran oleh User wajib dilakukan segera (selambat-lambatnya dalam batas waktu 1 hari) setelah User mengklik Bayar. Jika dalam batas waktu tersebut pembayaran atau konfirmasi pembayaran belum dilakukan oleh User, Solivis memiliki kewenangan untuk membatalkan transaksi tersebut.</p>
                <p>8. Reservasi yang telah dibayar namun tidak diproses oleh pihak restoran dalam batas waktu yang diberikan akan dibatalkan secara otomatis oleh sistem Solivis.</p>
                <p>9. Pembayaran oleh User dengan Saldo Solivis tidak memerlukan konfirmasi pembayaran, sistem solivis otomatis menindaklanjuti permintaan reservasi User ke pihak restoran.</p>
                <p>10. Konfirmasi pembayaran dengan transaksi bank disertai dengan slip bukti transfer bank. Konfirmasi pembayaran dengan transaksi bank tanpa slip bukti transfer bank tidak akan diproses oleh Solivis.</p>
                <p>11. Pada saat hari H reservasi, User akan dikirimkan email konfirmasi kedatangan. Apabila User tidak dapat datang di hari reservasi, User diberi kesempatan sekali untuk mengganti jadwal reservasi. Jika User tidak dapat datang juga setelah melakukan pergantian jadwal, maka reservasi akan dianggap hangus dan dana tidak dapat dikembalikan.</p>
                <p>12. Setelah pihak restoran melakukan scan QR CODE, maka dana pihak User yang dikirimkan ke Rekening resmi Solivis akan di lanjut kirimkan ke pihak Restoran(transaksi dianggap selesai).</p>
                <p>13. Batas waktu keterlambatan User tiba di restoran adalah 20 menit. Apabila User tiba di restoran lewat dari batas waktu tersebut, maka reservasi User akan dianggap telah dipergunakan. Kegagalan User dalam mempergunakan reservasinya merupakan tanggung jawab dari User itu sendiri.</p>
                <p>14. User memahami dan menyetujui bahwa masalah keterlambatan proses pembayaran dan biaya tambahan yang disebabkan oleh perbedaan bank yang User pergunakan dengan bank Rekening resmi Solivis adalah tanggung jawab User secara pribadi.</p>
                <p>15. Pengembalian dana dari Solivis kepada User hanya dapat dilakukan jika dalam keadaan-keadaan tertentu berikut ini:</p>
                <ul>
                    <li>Kelebihan pembayaran dari User melalui transaksi bank</li>
                    <li>Restoran menolak reservasi User dengan alasan tertentu</li>
                    <li>Solivis menolak reservasi User dengan alasan tertentu</li>
                </ul>
                <p>16. Apabila terjadi proses pengembalian dana dari Solivis maka Saldo Solivis milik Pengguna akan bertambah sesuai dengan jumlah pengembalian dana.</p>
                <p>17. Transaksi hanya dapat dibatalkan sebelum User melakukan konfirmasi pembayaran dan disampaikan secara resmi kepada Solivis melalui metode yang tersedia, kecuali dalam kondisi adanya penolakan dari pihak Restoran atau dari Solivis</p>
                <h3 id="price">D. HARGA</h3>
                <p>1. Harga menu yang terdapat dalam situs Solivis adalah harga yang ditetapkan oleh Restoran. Restoran dilarang memanipulasi harga menu dengan cara apapun.</p>
                <p>2. User memahami dan menyetujui bahwa kesalahan keterangan harga dan informasi lainnya yang disebabkan tidak terbaharuinya halaman situs Solivis dikarenakan browser/ISP yang dipakai user adalah tanggung jawab user.</p>
                <p>3. Pihak Restoran memahami dan menyetujui bahwa kesalahan ketik yang menyebabkan keterangan harga atau informasi lain menjadi tidak benar/sesuai adalah tanggung jawab Pihak Restoran. Perlu diingat dalam hal ini, apabila terjadi kesalahan pengetikkan keterangan harga menu yang tidak disengaja, Pihak Restoran berhak menolak pesanan menu yang dilakukan oleh user.</p>
                <p>4. User memahami dan menyetujui bahwa setiap masalah dan/atau perselisihan yang terjadi akibat ketidaksepahaman antara User dan Restoran tentang harga bukanlah merupakan tanggung jawab Solivis.</p>
                <p>5. Dengan melakukan reservasi melalui Solivis, Pengguna menyetujui untuk membayar total biaya yang harus dibayarkan sebagaimana tertera dalam halaman pembayaran. User setuju untuk melakukan pembayaran melalui metode pembayaran yang telah dipilih sebelumnya oleh User.</p>
                <p>6. Situs Solivis untuk saat ini hanya melayani transaksi reservasi restoran dalam mata uang Rupiah.</p>
                <h3 id="content">E. KONTEN</h3>
                <p>1. Dalam menggunakan setiap fitur dan/atau layanan Solivis, User dilarang untuk mengunggah atau mempergunakan kata-kata, komentar, atau konten apapun yang mengandung unsur SARA, diskriminasi, merendahkan atau menyudutkan orang lain, vulgar, bersifat ancaman, atau hal-hal lain yang dapat dianggap tidak sesuai dengan nilai dan norma sosial. Solivis berhak melakukan tindakan yang diperlukan atas pelanggaran ketentuan ini, antara lain penghapusan konten, pemblokiran akun, dan lain-lain.</p>
                <p>2. Restoran dengan ini memahami dan menyetujui bahwa penyalahgunaan foto/gambar yang di unggah oleh Restoran adalah tanggung jawab Restoran secara pribadi.</p>
                <p>3. Pengguna menjamin bahwa tidak melanggar hak kekayaan intelektual dalam mengunggah konten Pengguna ke dalam situs Solivis. Setiap Pengguna dengan ini bertanggung jawab secara pribadi atas pelanggaran hak kekayaan intelektual dalam mengunggah konten di Situs Solivis.</p>
                <h3 id="balance-withdrawed">F. PENARIKAN DANA</h3>
                <p>1. Penarikan dana sesama bank akan diproses dalam waktu 1x24 jam hari kerja, sedangkan penarikan dana antar bank akan diproses dalam waktu 2x24 jam hari kerja.</p>
                <p>2. Untuk penarikan dana dengan tujuan nomor rekening di luar bank BCA, Mandiri, dan BRI apabila ada biaya tambahan yang dibebankan akan menjadi tanggungan dari Pengguna.</p>
                <h3 id="business-term">G. SYARAT DAFTAR USAHA</h3>
                <p>1. Pihak pemilik usaha yang ingin mendaftarkan usahanya di Solivis harus berdomisili di kota yang sama dengan pihak Solivis.</p>
                <p>2. Setiap transaksi akan dikenakan biaya 2% dari total pembayaran. Misalnya total pembayaran reservasi adalah Rp. 300.000, maka biaya yang dikenakan adalah sebesar Rp. 6.000. Jadi, pendapatan bersih yang didapatkan pihak pemilik usaha adalah sebesar Rp. 294.000.</p>                
            </div>
        </div>
    </section>
    <!-- END: SECTION-PRIVACY-POLICY -->
    @include('user.layouts.footer-info')
    @include('user.layouts.footer')
@stop