@extends('user.layouts.master')

@section('title', 'Payment')

@section('content')
	@include('user.layouts.header-line')
	@include('user.layouts.navigation')
    @include ('user.modals.photo_container')
    <!-- SECTION-PAYMENT -->
    <section class="payment">
        <div class="container">
            <div class="row">
                <div class="payment-container">
                    <div class="col-sm-offset-1 col-sm-10 col-md-offset-3 col-md-6">
                        <h3 class="text-center">Terima kasih, Anda telah melakukan reservasi di website kami</h3>
                        <div class="total-user-payment">
                            <h4 class="payment-info">Jumlah yang harus dibayar</h4>
                            <h4 class="total-payment">Rp. {{ number_format($total_reservation_price,0,'.',',') }}</h4>
                            <div class="clearfix"></div>
                        </div>
                        <h3 class="text-center payment-method-title">Metode Pembayaran</h3>
                        <div class="radio radio-payment_method">
                            <label class="label-payment_method-1">
                                @if($reservation->method_payment_id == '2' && $reservation->status_reservation_id == '1')
                                {!! Form::radio('payment_method', '0', false, $attributes = ['class' => 'radio-payment_method', 'id' => 'checked_transfer_bank']); !!}
                                @else
                                {!! Form::radio('payment_method', '0', true, $attributes = ['class' => 'radio-payment_method', 'id' => 'checked_transfer_bank']); !!} 
                                @endif
                                <div class="transfer-bank">Transfer Bank</div>
                            </label>
                            <label class="label-payment_method-2">
                                @if($reservation->method_payment_id == '2' && $reservation->status_reservation_id == '1')
                                {!! Form::radio('payment_method', '1', true, $attributes = ['class' => 'radio-payment_method', 'id' => 'checked_balance']); !!} 
                                @else
                                {!! Form::radio('payment_method', '1', false, $attributes = ['class' => 'radio-payment_method', 'id' => 'checked_balance']); !!} 
                                @endif
                                <div class="ewallet">Saldo Solivis</div>
                            </label>
                            <div class="clearfix"></div>
                        </div>
                        <div class="info-payment-results-transfer_bank">
                            <h4 class="text-center">1. Transfer sejumlah <span class="payment-value">Rp. {{ number_format($total_reservation_price,0,'.',',') }}</span> ke salah satu rekening berikut:</h4>
                            <div class="bank">
                                @for($i=0; $i<count($admin_banks); $i++)
                                <div class="bank-{{ $i+1 }}">
                                    @if($i == 0)
                                    <img src="/images/bca-logo.png" alt="bca-logo"/>
                                    @elseif($i == 1)
                                    <img src="/images/mandiri-logo.png" alt="mandiri-logo"/>
                                    @else
                                    <img src="/images/bri-logo.png" alt="bri-logo"/>
                                    @endif
                                    <div class="bank-content">
                                        <h3 class="text-center">{{ $admin_banks[$i]->bank->name }}</h3>
                                        <h4 class="text-center">{{ $admin_banks[$i]->account_number }}</h4>
                                        <h4 class="text-center">a/n {{ $admin_banks[$i]->account_name }}</h4>
                                    </div>
                                </div>                            
                                @endfor
                                <div class="clearfix"></div>
                            </div>
                            <h4 class="text-center">2. Setelah melakukan transfer, segera melakukan Konfirmasi Pembayaran. Pembayaran tanpa melakukan konfirmasi tidak dapat kami proses lebih lanjut. Pastikan anda mengisi data yang tepat saat melakukan konfirmasi pembayaran.</h4>
                            <h4 class="text-center">3. Reservasi akan otomatis dibatalkan dalam waktu 24 jam jika anda tidak melakukan pembayaran dan konfirmasi pembayaran.</h4>
                            {!! Form::open(['action' => 'PaymentController@patchTransferBankPayment', 'method' => 'PATCH']) !!}
                                <button class="btn btn-pay">Bayar</button>
                                <input type="hidden" name="reservation_id" value="{{ $reservation->id }}">
                            {!! Form::close() !!}                            
                        </div>
                        <div class="info-payment-results-ewallet hide">
                            <h3 class="text-center">Sisa saldo anda</h3>
                            <h3 class="text-center e-wallet_remains">Rp. {{ number_format($reservation->user->removeDoubleZero($reservation->user->balance),0,'.',',') }}</h3>
                            @if ($total_reservation_price <= $reservation->user->balance)
                            <h4 class="text-center"><img src="/images/available.png" alt="available"/>Saldo anda cukup untuk melakukan transaksi sebesar Rp. {{ number_format($total_reservation_price,0,'.',',') }}</h4>
                            <h3 class="text-center">Sisa saldo setelah transaksi</h3>
                            <h3 class="text-center e-wallet_after_pay">Rp. {{ number_format($reservation->user->balance-$total_reservation_price,0,'.',',') }}</h3>
                            {!! Form::open(['action' => 'PaymentController@patchBalancePayment', 'method' => 'PATCH']) !!}
                                <button class="btn btn-pay">Bayar</button>
                                    <input type="hidden" name="reservation_id" value="{{ $reservation->id }}">
                            {!! Form::close() !!}                            
                            @else                            
                            <h4 class="text-center not-available-info">Saldo anda tidak cukup untuk melakukan transaksi</h4>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </section>
    <!-- END: SECTION-PAYMENT -->
    @include('user.layouts.footer-info')
    @include('user.layouts.footer')
@stop