@extends('user.layouts.master')

@section('title', 'Confirm Payment')

@section('content')
    @include('user.layouts.header-line')
    @include('user.layouts.navigation')
    @include ('user.modals.photo_container')
    @include ('user.modals.transfer_proof')
    @include ('user.modals.need_to_add_account_bank')
    <!-- SECTION-CONFIRM-PAYMENT -->
    <section class="confirm_payment">
        <div class="container">
            <div class="confirm_payment-container">
                @include('partials.error')
                <h3>Konfirmasi Pembayaran</h3>
                {!! Form::open(['action' => 'PaymentController@postConfirm', 'class' => 'form-confirm_payment', 'enctype' => 'multipart/form-data']) !!}
                    <div class="form-group">
                        <label class="col-xs-5 col-sm-3 col-md-2 no-padding">Reservation ID</label>
                        <div class="col-xs-1 no-padding">:</div>
                        <label class="col-xs-6 col-sm-8 col-md-9 no-padding">{{ $reservation->id }}</label>
                        <div class="clearfix"></div>
                    </div>      
                    <div class="form-group">
                        <label class="col-xs-5 col-sm-3 col-md-2 no-padding">Total Pembayaran</label>
                        <div class="col-xs-1 no-padding">:</div>
                        <label class="col-xs-6 col-sm-8 col-md-9 no-padding">Rp. {{ number_format($total_reservation_price,0,'.',',') }}</label>
                        <div class="clearfix"></div>            
                    </div>      
                    <div class="form-group">
                        <label class="col-xs-5 col-sm-3 col-md-2 no-padding">Dari Rekening</label>
                        <div class="col-xs-1 no-padding">:</div>
                        <div class="col-xs-12 col-sm-8 col-md-9 no-padding">
                            @if(count($reservation->user->user_banks))
                            {!! Form::select('transfer_from_bank_id', $user_bank_complete_info , null, $attributes = ['class' => 'form-control default-grey']);  
                                    !!} 
                            @else
                            <a class="btn btn-go-to-profile" target="_blank" href="/profile/edit/{{ $reservation->user->id }}">Tambah Rekening Bank</a>
                            @endif
                        </div>
                        <div class="clearfix"></div>            
                    </div>      
                    <div class="form-group">
                        <label class="col-xs-5 col-sm-3 col-md-2 no-padding">Rekening Tujuan</label>
                        <div class="col-xs-1 no-padding">:</div>
                        <div class="col-xs-12 col-sm-8 col-md-9 no-padding">
                            {!! Form::select('transfer_to_bank_id', $admin_bank_complete_info , null, $attributes = ['class' => 'form-control default-grey']);  
                                    !!} 
                        </div>
                        <div class="clearfix"></div>            
                    </div>      
                    <div class="form-group">
                        <label class="col-xs-5 col-sm-3 col-md-2 no-padding">Upload Bukti Pembayaran</label>
                        <div class="col-xs-1 no-padding">:</div>
                        <div class="col-xs-12 col-sm-8 col-md-9 no-padding">
                            <div class="btn btn-upload-transfer-proved" type="file">
                                <span>Telusuri Berkas</span>
                                <input type="file" id="transfer_proof" class="upload-only" name="transfer_proof_photo_url"/>
                            </div>
                            <span id="path_name"></span>
                        </div>
                        <div class="clearfix"></div>            
                    </div>      
                    <div class="form-group">
                        <label class="col-xs-5 col-sm-3 col-md-2 no-padding">Keterangan (Optional)</label>
                        <div class="col-xs-1 no-padding">:</div>
                        <div class="col-xs-12 col-sm-8 col-md-9 no-padding">
                            <textarea name="information" rows="5"></textarea>
                        </div>
                        <div class="clearfix"></div>            
                    </div> 
                    <h4>Note:</h4>
                    <h4>*Verifikasi pembayaran dilakukan maksimal 1x24 jam</h4>
                    <input type="hidden" name="reservation_id" value="{{ $reservation->id }}">
                    <button class="btn btn-confirm_payment">Konfirmasi</button>
                {!! Form::close() !!}   
            </div>
        </div>
    </section>
    <!-- END: SECTION-CONFIRM-PAYMENT -->
    @include('user.layouts.footer-info')
    @include('user.layouts.footer')
@stop