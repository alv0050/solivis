@extends('user.layouts.master')

@section('title', 'Balance')

@section('content')
	@include('user.layouts.header-line')
	@include('user.layouts.navigation')
    @include('user.modals.withdraw')    
    @include('user.modals.list_holidays')
    <!-- SECTION-BALANCE -->
    <section class="section-balance">
        <div class="container">
            <h1>Detil Saldo Solivis</h1>
            @include('partials.message')
            @include('partials.error')
            <div class="row">
                <div class="col-sm-3">
                    <h2 class="info-total-saldo">Total saldo solivis anda adalah </h2>
                    <h3>Rp. {{ number_format($user->removeDoubleZero($user->balance),0,'.',',') }}</h3>
                    <button type="submit" class="btn text-center btn-withdraw" id="withdraw">Tarik Dana</button>
                    <h4 class="text-uppercase">Keterangan</h4>
                    <p>Permintaan Tarik Dana akan diproses dalam waktu 1x24 jam hari kerja bank (tidak termasuk hari Sabtu/Minggu/Libur) Penarikan dana dengan tujuan nomor rekening di luar bank BCA/Mandiri/BRI, dana akan masuk dalam waktu maksimal 2x24 jam hari kerja bank (tidak termasuk hari Sabtu/Minggu/Libur) dan apabila ada biaya tambahan yang dibebankan akan menjadi tanggungan pengguna.</p>
                    <button type="submit" class="btn text-center btn-list-holidays" id="list_holidays">Daftar Libur Nasional</button>
                </div>
                <div class="col-sm-9">
                    <div class="col-sm-3 no-padding">
                        <h2 class="history-saldo-title">History Saldo</h2>
                    </div>
                    <div class="col-sm-9 no-padding">
                        {!! Form::open(['action' => 'BalanceController@getHistoryBalance', 'method' => 'GET', 'class' => 'form-search-history-balance']) !!}
                            <div class="form-group form-group-date-start">
                                {!! Form::text('date_start', $value = null, $attributes = ['class' => 'form-control', 'placeholder' => 'Tanggal Mulai', 'id' => 'input-date-start']); !!}
                            </div>
                            -
                             <div class="form-group form-group-date-end">
                            {!! Form::text('date_end', $value = null, $attributes = ['class' => 'form-control', 'placeholder' => 'Tanggal Akhir', 'id' => 'input-date-end']); !!}
                            </div>
                       <button type="submit" class="btn btn-search-history">Cari</button>
                       {!! Form::close() !!}
                    </div>
                    <table class="table table-striped table-history-saldo">
                        <thead>
                            <tr>
                                <th>Tanggal</th>
                                <th>Transfer ke</th>
                                <th>Nominal</th>
                                <th>Keterangan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (!isset($history_balances_search))
                                @foreach($history_balances as $history_balance)
                                <tr>
                                    <td>{{ $user->getProcessDate($history_balance->process_date) }}/{{ $user->getProcessMonth($history_balance->process_date) }}/{{ $user->getProcessYear($history_balance->process_date) }}
                                    </td>
                                    <td>{{ $history_balance->transfer_target }}</td>
                                    <td>Rp. {{ number_format($user->removeDoubleZero($history_balance->balance_processed),0,'.',',') }}</td>
                                    <td>
                                        {{ $history_balance->status_balance->status }}
                                    </td>
                                </tr>
                                @endforeach
                            @else
                                @foreach($history_balances_search as $history_balance)
                                <tr>
                                    <td>{{ $user->getProcessDate($history_balance->process_date) }}/{{ $user->getProcessMonth($history_balance->process_date) }}/{{ $user->getProcessYear($history_balance->process_date) }}
                                    </td>
                                    <td>{{ $history_balance->transfer_target }}</td>
                                    <td>Rp. {{ number_format($user->removeDoubleZero($history_balance->balance_processed),0,'.',',') }}</td>
                                    <td>
                                        {{ $history_balance->status_balance->status }}
                                    </td>
                                </tr>
                                @endforeach
                            @endif                                
                        </tbody>
                    </table>
                    <div class="balance-pagination">
                        @if(!isset($history_balances_search))
                            {!! $history_balances->render() !!}
                        @else
                            {!! $history_balances_search->appends(Request:: only(['date_start'=>'date_start', 'date_end'=>'date_end']))->render() !!}
                        @endif
                    </div>  
                    <div class="clearfix"></div>                  
                </div>
            </div>
        </div>
    </section>
    <!-- END: SECTION-BALANCE -->
    @include('user.layouts.footer-info')
    @include('user.layouts.footer')
@stop