@extends('user.layouts.master')

@section('title', 'Show Reservation')

@section('content')
	@include('user.layouts.header-line')
    <!-- SECTION-SHOW-RESERVATION -->
    <section class="show-reservation">
        <div class="container">
            <div class="show-reservation-container">
                <div class="show-reservation-group">
                    <a href="#">
                        <img src="/images/logo.png" alt="logo"/>
                    </a>
                    <h3>{{ $reservation->restaurant_management->name }}</h3>
                    <div class="col-sm-4 col-sm-push-8">
                        <div class="qr-code-container">
                            {!! QrCode::size(150)->generate($reservation->qr_code_plain->code); !!}
                        </div>
                    </div>                    
                    <div class="col-sm-4 col-sm-pull-4">
                        <div class="reservation-detail-info">
                            <label class="info-title">Tanggal</label> 
                            <label>: {{ $reservation->convertReservationDate() }}</label>
                        </div>
                        <div class="reservation-detail-info">
                            <label class="info-title">Jam</label> 
                            <label>: {{ $reservation->convertTime() }}</label>
                        </div>
                        <div class="reservation-detail-info">
                            <label class="info-title">Pax</label> 
                            <label>: {{ $reservation->paxes }} Orang</label>
                        </div>
                    </div>
                    <div class="col-sm-4 col-sm-pull-4">
                        <div class="reservation-detail-info">
                            <label class="info-title">Reservation ID</label> 
                            <label>: {{ $reservation->id }}</label>
                        </div>
                        <div class="reservation-detail-info">
                            <label class="info-title">Atas Nama</label> 
                            <label>: {{ $reservation->user->name }}</label>
                        </div>                    
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </section>
@stop