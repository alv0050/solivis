@extends('user.layouts.master')

@section('title', 'Change Schedule')

@section('content')
    @include('user.layouts.header-line')
    @include ('user.modals.change_reservation')
    @include ('user.modals.over_reserve_time')    
    @include ('user.modals.reserve_time_less')    
    @include ('user.modals.only_use_balance')       
    <!-- SECTION-CHOOSE-CANCEL-RESERVATION-OPTIONS -->
    <section class="section-choose-cancel-reservation-options">
        <div class="container">
            <div class="choose-cancel-reservation-options">
                <a href="/" class="link-logo"><img src="/images/logo.png" alt="logo"/></a>
                @include('partials.error')
                <h2 class="text-center text-uppercase">Pilih</h2>
                {!! Form::open(['class' => 'form-scorch-reservation', 'action' => 'ReservationController@postChooseScorchReservation']) !!}
                    <input type="hidden" name="reservation_id" value="{{ $reservation_id }}">
                    <button class="btn btn-scorch-reservation" type="button">Hangus</button>
                {!! Form::close() !!}
                {{-- delayed reservation can not choose change schedule again --}}
                @if($reservation->status_arrival_id != 3)
                <h2 class="text-center text-uppercase">Atau</h2>
                {!! Form::open(['class' => 'form-change-reservation', 'action' => ['ReservationController@getChangeReservation', $reservation_id], 'method' => 'GET']) !!}
                    {{-- pending, add code confirmation here --}}
                    <button class="btn btn-change-reservation-date">Ganti Jadwal</button>
                {!! Form::close() !!}
                @endif
                <p class="text-center">Tombol hangus akan memberikan notifikasi kepada pihak restoran untuk tidak lagi mempersiapkan makanan anda.</p>                
            </div>
        </div>
    </section>
    <!-- END: SECTION-CHOOSE-CANCEL-RESERVATION-OPTIONS -->
    @include('user.layouts.footer-dm')
@stop