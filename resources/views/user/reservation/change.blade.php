@extends('user.layouts.master')

@section('title', 'Change Schedule')

@section('content')
    @include('user.layouts.header-line')
    @include ('user.modals.change_reservation')
    @include ('user.modals.over_reserve_time')    
    @include ('user.modals.reserve_time_less')    
    @include ('user.modals.only_use_balance')       
    <!-- SECTION-CHANGE-RESERVATION-SCHEDULE -->
    <section class="section-change-reservation-schedule">
        <div class="container">
            <div class="change-reservation-schedule">
                <a href="/" class="link-logo"><img src="/images/logo.png" alt="logo"/></a>
                <h2 class="text-center text-uppercase">Ganti Jadwal Reservasi</h2>
                @include('partials.error')
                @include('partials.message')
                {!! Form::open(['class' => 'form-change-reservation-schedule', 'action' => 'ReservationController@postChangeReservationDate']) !!}
                    <div class="form-group">
                        <label for="date">Tanggal</label>
                        <div class="change-date-reserve">
                            <img src="/images/date.png" alt="date">
                            {!! Form::text('date', $value = null, $attributes = ['class' => 'form-control', 'id' => 'input-change-date-reserve', 'onkeydown="return false;"']); !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="time">Jam</label>
                        <div class="change-time-reserve">
                            <img src="/images/time.png" alt="time">
                            {!! Form::text('time', $value = null, $attributes = ['class' => 'form-control', 'id' => 'input-change-time-reserve', 'onkeydown="return false;"']); !!}
                            <div class="group-change-time-reserve">
                                <img src="/images/pointer.png" alt="pointer" class="icon-pointer third_pointer hide">
                                <div class="info-change-time-reserve">
                                    <div class="info-change-time-reserve-title">
                                        <img src="/images/time.png" alt="time" class="icon-time">
                                        <h4>Jam berapa ?</h4>
                                    </div>
                                    <div class="info-change-time-reserve-detail">
                                        <h4 class="table-change-time-reserve-title text-center">-Breakfast-</h4>
                                        <table class="table table-bordered table-change-time-reserve">
                                            <tr>
                                                <td id="time-600">06:00</td>
                                                <td id="time-630">06:30</td>
                                                <td id="time-700">07:00</td>
                                            </tr>
                                            <tr>
                                                <td id="time-730">07:30</td>
                                                <td id="time-800">08:00</td>
                                                <td id="time-830">08:30</td>
                                            </tr>
                                            <tr>
                                                <td id="time-900">09:00</td>
                                                <td id="time-930">09:30</td>
                                                <td id="time-1000">10:00</td>
                                            </tr>
                                            <tr>
                                                <td id="time-1030">10:30</td>
                                                <td id="time-1100">11:00</td>
                                            </tr>

                                        </table>    
                                        <h4 class="table-change-time-reserve-title text-center">-Lunch-</h4>
                                        <table class="table table-bordered table-change-time-reserve">
                                            <tr>
                                                <td id="time-1130">11:30</td>
                                                <td id="time-1200">12:00</td>
                                                <td id="time-1230">12:30</td>
                                            </tr>
                                            <tr>
                                                <td id="time-1300">13:00</td>
                                                <td id="time-1330">13:30</td>
                                            </tr>
                                        </table>                        
                                        <h4 class="table--changetime-reserve-title text-center">-Late Lunch-</h4>
                                        <table class="table table-bordered table-change-time-reserve">
                                            <tr>
                                                <td id="time-1400">14:00</td>
                                                <td id="time-1430">14:30</td>
                                                <td id="time-1500">15:00</td>
                                            </tr>
                                            <tr>
                                                <td id="time-1530">15:30</td>
                                                <td id="time-1600">16:00</td>
                                                <td id="time-1630">16:30</td>
                                            </tr>
                                            <tr>
                                                <td id="time-1700">17:00</td>
                                                <td id="time-1730">17:30</td>
                                            </tr>
                                        </table>                        
                                        <h4 class="table-change-time-reserve-title text-center">-Dinner-</h4>
                                        <table class="table table-bordered table-change-time-reserve">
                                            <tr>
                                                <td id="time-1800">18:00</td>
                                                <td id="time-1830">18:30</td>
                                                <td id="time-1900">19:00</td>
                                            </tr>
                                            <tr>
                                                <td id="time-1930">19:30</td>
                                                <td id="time-2000">20:00</td>
                                                <td id="time-2030">20:30</td>
                                            </tr>
                                        </table>                        
                                        <h4 class="table-change-time-reserve-title text-center">-Late Dinner-</h4>
                                        <table class="table table-bordered table-change-time-reserve">
                                            <tr>
                                                <td id="time-2100">21:00</td>
                                                <td id="time-2130">21:30</td>
                                                <td id="time-2200">22:00</td>
                                            </tr>
                                            <tr>
                                                <td id="time-2230">22:30</td>
                                                <td id="time-2300">23:00</td>
                                                <td id="time-2330">23:30</td>
                                            </tr>
                                        </table>                        
                                    </div>
                                </div>
                            </div>                                 
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <p class="text-center">Ganti jadwal hanya dapat dilakukan 1 kali untuk setiap reservasi.</p>
                    <input type="hidden" name="reservation_id" value="{{ $reservation_id }}"/>
                    <button class="btn btn-change-reserve" type="button">Ganti</button>
                {!! Form::close() !!}
            </div>
        </div>
    </section>
    <!-- END: SECTION-CHANGE-RESERVATION-SCHEDULE -->
    @include('user.layouts.footer-dm')
@stop