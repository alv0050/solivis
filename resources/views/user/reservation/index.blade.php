@extends('user.layouts.master')

@section('title', 'Reservation')

@section('content')
	@include('user.layouts.header-line')
	@include('user.layouts.navigation')
    @include ('user.modals.photo_container')
    <!-- SECTION-RESERVATION -->
    <section class="reservation">
        <div class="container">
            <div class="reservation-user-container">
                <h4 class="reservation-title">Anda akan membuat reservasi untuk</h4>
                <div class="line-title"></div>
                <div class="reservation-user-group">
                    <div class="image-restaurant-container">
                        <img src="/images/restaurant_uploads/{{ $restaurant_management->photo_url }}" alt="{{ $restaurant_management->name }}">
                    </div>
                    <div class="info-reservation-restaurant">
                        <h3>{{ $restaurant_management->name }}</h3>
                        <div class="location-container">
                            <img src="/images/location.png" alt="location">
                            <h5 class="location-restaurant">{{ $restaurant_management->address }}</h5>
                        </div>
                        <div class="pax-container">
                            <img src="/images/pax.png" alt="pax">
                            <h5>Pax</h5>
                        </div>
                        <h5 class="total-paxes">: {{ $paxes }} Orang</h5>
                        <div class="clearfix"></div>
                        <div class="date-container">
                            <img src="/images/date.png" alt="date">
                            <h5>Tanggal</h5>
                        </div>
                        <h5 class="date-reserve">: {{ $restaurant_management->getOnlyDate($reservation_date) }}</h5>
                        <div class="clearfix"></div>
                        <div class="time-container">
                            <img src="/images/time.png" alt="time">
                            <h5>Jam</h5>
                        </div>
                        <h5 class="time-reserve">: {{ $restaurant_management->getOnlyTime($reservation_date) }}</h5>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="user-order">
                    <h3>Orderan Anda</h3>
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Menu</th>
                                <th class="text-center">@</th>
                                <th class="text-right">Harga</th>
                                <th class="text-right">Jumlah</th>
                            </tr>
                        </thead>
                        <tbody>
                            @for($i=0; $i<count($item_names); $i++)
                            <tr>
                                <td>{{ $item_names[$i] }}</td>
                                <td class="text-center">{{ $item_counts[$i] }}</td>
                                <td class="text-right">{{ number_format($restaurant_management->removeDoubleZero($each_item_prices[$i]),0,'.',',') }}</td>
                                <td class="text-right">{{ number_format(($item_counts[$i] * $each_item_prices[$i]),0,'.',',') }}</td>
                            </tr>
                            @endfor
                        </tbody>
                    </table>
                    <div class="total-payment-info">
                        <h4>Total Pembayaran</h4>
                        <h4 class="total-payment">Rp. {{ number_format($total_reservation_price,0,'.',',') }}</h4>
                    </div>
                    <div class="clearfix"></div>
                    <div class="link-reservation-group">
                        {!! Form::open(['class' => 'form-edit-reservation', 'action'=>['ReservationController@getEditReservation', $restaurant_management->id], 'method'=>'GET']) !!}
                            <input type="hidden" name="edit_paxes" value="{{ $paxes }}">
                            <input type="hidden" name="edit_reservation_date" value="{{ $reservation_date }}">
                            @for($i=0; $i<count($item_names); $i++)
                            <input type="hidden" name="edit_item_names[{{ $i }}]" value="{{ $item_names[$i] }}">
                            <input type="hidden" name="edit_item_counts[{{ $i }}]" value="{{ $item_counts[$i] }}">
                            @endfor
                            <button class="btn btn-edit-reservation">Edit Reservasi</button>
                        {!! Form::close() !!}         
                        {!! Form::open(['class' => 'form-post-payment', 'action' => 'PaymentController@postIndex']) !!}
                            <input type="hidden" id="hidden_paxes" name="paxes" value="{{ $paxes }}">
                            <input type="hidden" id="hidden_reservation_date" name="reservation_date" value="{{ $reservation_date }}">
                            <input type="hidden" id="hidden_restaurant_management_id" name="restaurant_management_id" value="{{ $restaurant_management->id }}">
                            <input type="hidden" id="hidden_user_id" name="user_id" value="{{ Auth::user('')->id }}">
                            @for($i=0; $i<count($item_names); $i++)
                            <input type="hidden" name="item_names[{{ $i }}]" value="{{ $item_names[$i] }}">
                            <input type="hidden" name="item_counts[{{ $i }}]" value="{{ $item_counts[$i] }}">
                            @endfor
                            <input type="hidden" name="reservation_id_before" id="hidden_reservation_id_before">
                            <button type="button" class="btn btn-continue">Lanjut <img src="/images/next.png" alt="next"></button>
                        {!! Form::close() !!}         
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </section>
    <!-- END: SECTION-RESERVATION -->
    @include('user.layouts.footer-info')
    @include('user.layouts.footer')
@stop
