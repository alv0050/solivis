@extends('user.layouts.master')

@section('title', 'History Reservation')

@section('content')
	@include('user.layouts.header-line')
    @include('user.layouts.navigation')
    @include('user.modals.order_detail')
    @include('user.modals.declined_reason')
	@include('user.modals.review')
    <!-- SECTION-HISTORY-RESERVATION -->
    <div class="container">
        <section class="history-reservation">
            @include('partials.message')
            <h1>History Reservasi</h1>
            <div class="sort-history">
                <h4 class="text-left">Urutkan : </h4>
                <div class="form-group form-group-sort">
                {!! Form::open(['action' => 'ReservationController@getSortHistory', 'method' => 'GET', 'class' => 'form-sort-history']) !!}
                    {!! Form::select('sort_history', array('all' => 'Semua', 'reservation_id' => 'ID Reservasi', 'restaurant_name' => 'Nama Restoran',  'reservation_date' => 'Tanggal Reservasi', '1' => 'Belum diproses', '2' => 'Diproses', '3' => 'Telah bayar', '4' => 'Sukses', '5' => 'Batal'),  null, ['class' => 'form-control sort-history']);!!}
                {!! Form::close() !!}                    
                </div>
            </div>
            <div class="row">
                <div class="col-sm-9">
                    {{-- sort near date in top --}}
                    @foreach($user_reservations as $reservation)
                    <div class="history-reservation-container">
                        <div class="col-md-12 col-lg-9 no-padding">
                            <div class="reservation-info-group">
                                <h2><a href="/restaurant/show/{{ $reservation->restaurant_management->id }}">{{ $reservation->restaurant_management->name }}</a></h2>
                                <div class="col-sm-12 col-md-3 no-padding">
                                    <a href="/restaurant/show/{{ $reservation->restaurant_management->id }}">
                                        <img src="/images/restaurant_uploads/{{ $reservation->restaurant_management->photo_url }}" alt="$reservation->restaurant_management->photo_url">
                                    </a>
                                </div>
                                <div class="col-sm-5 col-md-4 no-padding">
                                    <div class="restaurant-info">
                                        <img src="/images/location.png" alt="location">
                                        <h4 class="location-title">{{ $reservation->restaurant_management->address }}</h4>
                                        <h4>Jenis Kuliner: 
                                        @for($i = 0; $i < count($reservation->restaurant_management->restaurant_cuisines); $i++)
                                            {{ $reservation->restaurant_management->restaurant_cuisines[$i]->cuisine->name }}, 
                                        @endfor
                                        </h4>
                                        <h4>Opening Hours: {{ $reservation->restaurant_management->getOpenTimeHourAndMinute() }}-{{ $reservation->restaurant_management->getCloseTimeHourAndMinute() }}</h4>
                                        <h4>Services: 
                                        @for($i = 0; $i < count($reservation->restaurant_management->restaurant_services); $i++)
                                            {{ $reservation->restaurant_management->restaurant_services[$i]->service->name }}, 
                                        @endfor          
                                        </h4>
                                        <h4 class="food-title">Makanan</h4>
                                        <div class="restaurant-review">
                                            @for($i=0; $i<$reservation->restaurant_management->getTotalRatingsAverage(); $i++)
                                            <img src="/images/star-yellow.png" alt="star-yellow">
                                            @endfor
                                            @for($i=0; $i<(5 - $reservation->restaurant_management->getTotalRatingsAverage()); $i++)
                                            <img src="/images/star-black.png" alt="star-black">
                                            @endfor
                                        </div>
                                        <div class="total-reviews">{{ $reservation->restaurant_management->getTotalRatings() }} Review</div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="col-sm-2 col-md-1 no-padding">
                                    <div class="divider"></div>
                                </div>
                                <div class="col-sm-5 col-md-4 no-padding">
                                    <div class="reservation-info">
                                        <div class="col-xs-5 no-padding">
                                            <h4>Reservation ID</h4>
                                            <h4>Tanggal</h4>
                                            <h4>Jam</h4>
                                            <h4>Pax</h4>
                                            <h4>Pembayaran</h4>
                                            <h4>Status</h4>
                                        </div>
                                        <div class="col-xs-2">
                                            <h4>:</h4>
                                            <h4>:</h4>
                                            <h4>:</h4>
                                            <h4>:</h4>
                                            <h4>:</h4>
                                            <h4>:</h4>
                                        </div>
                                        <div class="col-xs-5 no-padding">
                                            <h4>{{ $reservation->id }}</h4>
                                            <h4>{{ $reservation->convertDate() }}</h4>
                                            <h4>{{ $reservation->convertTime() }}</h4>
                                            <h4>{{ $reservation->paxes }} Orang</h4>
                                            <div class="total-payment">
                                                <h4>Rp. {{ number_format($reservation->getTotalReservationPrice(),0,'.',',') }}</h4>
                                                <a href="#" class="view-order-detail-link">
                                                    <img src="/images/food-info.png" alt="food-info"/>
                                                    <span class="food-names hide">{{ $reservation->getFoodNames() }}</span>
                                                    <span class="food-counts hide">{{ $reservation->getFoodCounts() }}</span>
                                                    <span class="food-prices hide">{{ $reservation->getFoodPrices() }}</span>
                                                </a>
                                            </div>
                                            <h4>{{ $reservation->status_reservation->status }}</h4>
                                        </div>
                                        @if($reservation->status_reservation->id == '1')
                                        @if(!$reservation->method_payment || $reservation->method_payment_id == '2')
                                        <a href="{{ action('PaymentController@getShow', [$reservation->id]) }}" class="btn btn-choose-payment">Pilih Pembayaran</a>
                                        @else
                                        <a href="{{ action('PaymentController@getConfirm', [$reservation->id]) }}" class="btn btn-confirm-reservation">Konfirmasi Pembayaran</a>
                                        @endif
                                        {!! Form::open(['class' => 'form-delete-reservation', 'action' => ['ReservationController@deleteReservation', $reservation->id], 'method' => 'DELETE']) !!}
                                            <button type="button" class="btn btn-cancel-reservation">Batal</button>
                                        {!! Form::close() !!}
                                        @elseif($reservation->status_reservation->id == '2')
                                        <button class="btn btn-disabled-reservation" disabled>Menunggu Verifikasi Admin</button> 
                                        @elseif($reservation->status_reservation->id == '3') 
                                        <button class="btn btn-disabled-reservation" disabled>Menunggu Konfirmasi Restoran</button> 
                                        @elseif($reservation->status_reservation->id == '4')
                                        <div class="clearfix"></div>
                                        <div class="history-reservation-group">
                                            <img src="/images/reservation-history.png" alt="history-reservation">
                                            <a href="{{ action('ReservationController@getShowReservation', [$reservation->id]) }}" class="btn text-uppercase btn-show-reservation" target="_blank">Tampilkan Reservasi</a>
                                        </div>
                                        @else
                                        <button class="btn btn-show-declined-reason">Lihat Alasan Penolakan <span class="hide">{{ $reservation->declined_reason->reason }}</span></button> 
                                        @endif
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-3 no-padding">
                            <div class="user-comments">
                                @if($reservation->review_rating)
                                <div class="user-comments-results">
                                    <div class="user-rating-results">
                                    @if($reservation->review_rating->rating == 1)
                                    <img src="/images/one-star.png" alt="one-star">
                                    @elseif($reservation->review_rating->rating == 2)
                                    <img src="/images/two-star.png" alt="two-star">
                                    @elseif($reservation->review_rating->rating == 3)
                                    <img src="/images/three-star.png" alt="three-star">
                                    @elseif($reservation->review_rating->rating == 4)
                                    <img src="/images/four-star.png" alt="four-star">
                                    @else
                                    <img src="/images/five-star.png" alt="five-star">
                                    @endif
                                    </div>                                
                                    @if($reservation->review_comment)
                                    <p class="text-center">{{ $reservation->review_comment->comment }}</p>
                                    @endif
                                </div>
                                @else
                                <p class="text-center">Berikan rating & review anda setelah anda menikmati hidangan anda</p>
                                <div class="review-results">
                                    <img src="/images/comment.png" alt="comment">
                                    @if($reservation->status_reservation->id == '4' && $reservation->status_arrival->id == '1')
                                    <button class="btn btn-comment">Berikan Review</button>
                                    <span class="reservation-id-hidden hide">{{ $reservation->id }}</span>
                                    @else
                                    <button class="btn btn-comment" disabled>Berikan Review</button>
                                    @endif
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="clearfix"></div>                        
                    </div>
                    @endforeach
                    <div class="history-pagination">
                        {!! $user_reservations->appends(Request::only(['sort_history'=>'sort_history']))->render() !!}
                    </div>
                    <div class="clearfix"></div>                    
                </div>
                <div class="col-sm-3">
                    @if(count($recommendation_restaurants) != 0)
                    <div class="recommendation-container">
                        <div class="recommendation-title">
                            <img src="/images/recommendation.png" alt="recommendation"/>
                            <h4>Rekomendasi</h4>
                        </div>
                        <div class="recommendation-divider-title"></div>
                        @foreach($recommendation_restaurants as $recommendation_restaurant)
                        <div class="recommendation-content">
                            <div class="col-md-4 no-padding">
                                <a href="/restaurant/show/{{ $recommendation_restaurant->id }}" class="recommendation-restaurant-profile-picture" target="_blank"><img src="/images/restaurant_uploads/{{ $recommendation_restaurant->photo_url }}" alt="{{ $recommendation_restaurant->name }}"/></a>
                            </div>                            
                            <div class="col-md-8 no-padding">
                                <div class="recommendation-info">
                                    <h4 class="no-margin"><a href="/restaurant/show/{{ $recommendation_restaurant->id }}" target="_blank">{{ $recommendation_restaurant->name }}</a></h4>
                                    <h5 class="no-margin">{{ $recommendation_restaurant->getCuisines() }}</h5>
                                    <div class="reviews">
                                        <h5 class="no-margin">Makanan</h5>
                                        <div class="stars">
                                             @for($i=0; $i<$recommendation_restaurant->getTotalRatingsAverage(); $i++)
                                            <img src="/images/normal_star-yellow.png" alt="normal_star-yellow"/>
                                            @endfor
                                            @for($i=0; $i<(5 - $recommendation_restaurant->getTotalRatingsAverage()); $i++)
                                            <img src="/images/normal_star-black.png" alt="normal_star-black"/>
                                            @endfor
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="recommendation-divider"></div>
                        @endforeach
                        <div class="clearfix"></div>
                    </div>
                    @endif
                </div>
            </div>
        </section>            
    </div>
    <!-- END: SECTION-HISTORY-  -->
    @include('user.layouts.footer-info')
    @include('user.layouts.footer')
@stop
     