@extends('user.layouts.master')

@section('title', 'Business')

@section('content')
	@include('user.layouts.header-line')
    @include('user.layouts.navigation')
    @include('user.modals.consult_business')
	@include('user.modals.success_send_business_message')
    <!-- SECTION-BUSINESS -->
    <section class="business">
        <div class="container">
            @include('partials.message')
            @include('partials.error')
        </div>
        <div class="jumbotron-business">
            <img src="/images/jumbotron-bg-business.png" alt="jumbotron-bg-business">
            <div class="jumbotron-info">
                <img src="/images/logo-white.png" alt="logo-white">
                <div class="clearfix"></div>
                <h2 class="text-center">Membuat usaha kuliner anda menjadi lebih mudah</h2>
            </div>
        </div>
        <div class="left-side-container">
            <div class="col-sm-6 col-sm-push-6 col-md-6">
                <img src="/images/sign-as-restaurant.png" alt="sign-as-restaurant">
            </div>
            <div class="col-sm-offset-1 col-sm-5 col-sm-pull-6 col-md-offset-2 col-md-4">
                <div class="solivis-solution">
                    <h1>Solusi Solivis</h1>
                    <div class="each-solution">
                        <img src="/images/check.png" alt="check">
                        <h4>Meningkatkan jumlah pelanggan anda</h4>
                    </div>
                    <div class="each-solution">
                        <img src="/images/check.png" alt="check">
                        <h4>Membantu mengembangkan bisnis kuliner anda</h4>
                    </div>
                    <div class="each-solution">
                        <img src="/images/check.png" alt="check">
                        <h4>Bekerja secara online</h4>
                    </div>
                    <div class="each-solution">
                        <img src="/images/check.png" alt="check">
                        <h4>Meningkatkan kepuasan pelanggan anda</h4>
                    </div>
                    <div class="each-solution">
                        <img src="/images/check.png" alt="check">
                        <h4>Memperkenalkan kuliner anda ke berbagai kalangan</h4>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <button class="btn btn-consult-business" id="consult_business">Konsultasi dengan kami</button>
        <div class="container">
            <!-- SECTION-TRUSTED -->
            <section class="section-trusted">
                <h3 class="text-center text-uppercase">Dipercayai oleh</h3>
                <div class="row">
                    <div class="col-sm-offset-1 col-sm-2">
                        <img src="/images/trusted-resto-1.png" alt="trusted-resto-1"/>
                    </div>
                    <div class="col-sm-2">
                        <img src="/images/trusted-resto-2.png" alt="trusted-resto-2"/>
                    </div>
                    <div class="col-sm-2">
                        <img src="/images/trusted-resto-3.png" alt="trusted-resto-3"/>
                    </div>
                    <div class="col-sm-2">
                        <img src="/images/trusted-resto-4.png" alt="trusted-resto-4"/>
                    </div>
                    <div class="col-sm-2">
                        <img src="/images/trusted-resto-5.png" alt="trusted-resto-5"/>
                    </div>
                </div>
            </section>
        </div> 
    </section>
    @include('user.layouts.footer-info')
    @include('user.layouts.footer')
@stop