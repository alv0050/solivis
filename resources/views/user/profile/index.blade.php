@extends('user.layouts.master')

@section('title', 'Profile')

@section('content')
	@include('user.layouts.header-line')
	@include('user.layouts.navigation')
    <!-- SECTION-PROFILE (GUEST) -->
    <section class="profile">
        <div class="container">
        <div class="section-separator"></div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="profile-container">  
                        <div class="col-sm-4">
                            <img src="/images/uploads/{{ $user->photo_url }}" alt="{{ $user->name }}" class="img-responsive profile_picture"/>
                        </div>
                        <div class="col-sm-8">
                            <h2 class="profile_name_view">{{ $user->name }}</h2>
                            <div class="horizontal-separator"></div>
                            <div class="info-rrf">
                                <div class="col-xs-3 no-padding">
                                    <h4>Reservasi</h4>
                                    <div class="info-reservation">
                                        <img src="/images/reservation.png" alt="reservation">
                                        <h2>{{ count($user->reservations) }}</h2>
                                    </div>
                                </div>
                                <div class="col-xs-1 no-padding vertical-separator"></div>
                                <div class="col-xs-3 no-padding">
                                    <h4>Review</h4>
                                    <div class="info-review">
                                        <img src="/images/review.png" alt="review">
                                        <h2>{{ $total_reviews_from_user }}</h2>
                                    </div>
                                </div>
                                <div class="col-xs-1 no-padding vertical-separator"></div>
                                <div class="col-xs-3 no-padding">
                                    <h4>Favorite</h4>
                                    <div class="info-favorite">
                                        <img src="/images/favorite.png" alt="favorite">
                                        <h2>{{ count($user->favorites) }}</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="profile-reviews">
               <h3>Reviews</h3>
               <div class="divider"></div>
               @if(count($reservations_rated) == 0)
               <h4 class="info_not_yet_review">Belum pernah review</h4>
               @else
               @foreach($reservations_rated as $reservation_rated)
               <div class="profile-reviews-content">
                    <div class="col-sm-5">
                        <div class="reservation-user-info-group">
                            <div class="col-sm-5 no-padding">
                                <a href="/restaurant/show/{{ $reservation_rated->restaurant_management->id }}"><img src="/images/restaurant_uploads/{{ $reservation_rated->restaurant_management->photo_url }}" alt="{{ $reservation_rated->restaurant_management->name }}" class="img-responsive"></a>
                            </div>
                            <div class="col-sm-7 no-padding">
                                <div class="reservation-restaurant-info">
                                    <h3><a href="/restaurant/show/{{ $reservation_rated->restaurant_management->id }}">{{ $reservation_rated->restaurant_management->name }}</a></h3>
                                    <img src="/images/location.png" alt="location">
                                    <h4 class="location-title">{{ $reservation_rated->restaurant_management->address }}</h4>
                                    <h4>Jenis Kuliner: {{ $reservation_rated->restaurant_management->getCuisines() }}</h4>
                                    <h4>Opening Hours: {{ $reservation_rated->restaurant_management->getOpenTimeHourAndMinute() }}-{{ $reservation_rated->restaurant_management->getCloseTimeHourAndMinute() }}</h4>
                                    <h4>Services: {{ $reservation_rated->restaurant_management->getServices() }}</h4>
                                    <h4>Prices: {{ $reservation_rated->restaurant_management->getLowestPrice() }}-{{ $reservation_rated->restaurant_management->getHighestPrice() }}</h4>
                                    <div class="clearfix"></div>
                                </div>
                            </div>  
                            <div class="clearfix"></div>
                            <h4>Rating makanan</h4>
                            <div class="total-user-reviews">
                                @for($i=0; $i<$reservation_rated->restaurant_management->getTotalRatingsAverage(); $i++)
                                    <img src="/images/star-yellow.png" alt="star-yellow"/>
                                @endfor
                                @for($i=0; $i<(5 - $reservation_rated->restaurant_management->getTotalRatingsAverage()); $i++)
                                    <img src="/images/star-black.png" alt="star-black"/>
                                @endfor                                
                            </div>                  
                            <h4>dari {{ $reservation_rated->restaurant_management->getTotalRatings() }} rating</h4>
                        </div>
                    </div>
                    <div class="col-sm-7">
                        <div class="profile-info-reviews">
                            <div class="total-stars-reviews">
                                <h4>{{ $reservation_rated->review_rating->rating }}</h4>
                                <div class="total-stars-user">
                                    @for($i=0; $i<$reservation_rated->review_rating->rating; $i++)
                                        <img src="/images/star-yellow.png" alt="star-yellow">
                                    @endfor
                                    @for($i=0; $i<(5 - $reservation_rated->review_rating->rating); $i++)
                                        <img src="/images/star-black.png" alt="star-black">
                                    @endfor
                                </div>
                            </div>
                            <h4 class="comment_time">{{$reservation_rated->review_rating->created_at->diffForHumans() }}</h4>
                            <div class="clearfix"></div>
                            @if($reservation_rated->review_comment)
                                <h4 class="comment_content">{{ $reservation_rated->review_comment->comment }}</h4>
                            @endif
                            </div>
                       </div>
                    <div class="clearfix"></div>
                </div>
                @endforeach
                @endif
            </div>
            <div class="reservation-rated-pagination">
                {!! $reservations_rated->render() !!}
            </div>
            <div class="clearfix"></div>            
        </div>
    </section>
    <!-- END: SECTION (PROFILE) -->
    @include('user.layouts.footer-info')
    @include('user.layouts.footer')
@stop