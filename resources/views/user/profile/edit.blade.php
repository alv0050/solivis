@extends('user.layouts.master')

@section('title', 'Edit Profile')

@section('content')
    @include('user.layouts.header-line')
    @include('user.layouts.navigation')
    @include('user.modals.add_account_bank')
	@include('user.modals.edit_account_bank')
    <!-- SECTION-PROFILE -->
    <section class="profile">
        <div class="container">
        <div class="section-separator"></div>
        @include('partials.message')
        @include('partials.error')
            <div class="row">
                <div class="col-sm-6">
                    <div class="profile-container">  
                        <div class="col-sm-4">
                            <img src="/images/uploads/{{ $user->photo_url }}" alt="{{ $user->name }}" class="img-responsive profile_picture"/>
                            {!! Form::open(['action'=>['ProfileController@putEditPicture', $user->id], 'method'=>'PUT', 'enctype' => 'multipart/form-data']) !!}  
                                <div class="btn btn-changepicture" type="file">
                                    <span>Change Picture</span>
                                    <input type="file" class="upload" name="photo_url"/>
                                </div>
                            {!! Form::close() !!}
                        </div>
                        <div class="col-sm-8">
                            <h2 class="profile_name">{{ $user->name }}</h2>
                            <div class="saldo-info">
                                <div class="col-xs-6 saldo-title no-padding">
                                    <img src="/images/saldo.png" alt="saldo">
                                    <h4>Saldo Solivis</h4>
                                </div>
                                <div class="col-xs-6 saldo-detail no-padding">Rp. {{ number_format($user->removeDoubleZero($user->balance),0,'.',',') }}</div>
                            </div>
                            <div class="horizontal-separator"></div>
                            <div class="info-rrf">
                                <div class="col-xs-3 no-padding">
                                    <h4>Reservasi</h4>
                                    <div class="info-reservation">
                                        <img src="/images/reservation.png" alt="reservation">
                                        <h2>{{ count($user->reservations) }}</h2>
                                    </div>
                                </div>
                                <div class="col-xs-1 no-padding vertical-separator"></div>
                                <div class="col-xs-3 no-padding">
                                    <h4>Review</h4>
                                    <div class="info-review">
                                        <img src="/images/review.png" alt="review">
                                        <h2>{{ $total_reviews_from_user }}</h2>
                                    </div>
                                </div>
                                <div class="col-xs-1 no-padding vertical-separator"></div>
                                <div class="col-xs-3 no-padding">
                                    <h4>Favorite</h4>
                                    <div class="info-favorite">
                                        <img src="/images/favorite.png" alt="favorite">
                                        <h2>{{ count($user->favorites) }}</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-sm-5 col-sm-offset-1">
                    <div class="bank_container">
                        <div class="bank_container_header">
                            <h3>Rekening Bank</h3>
                            <button type="button" class="btn btn-add-account-bank"><img src="/images/add-white.png" alt="add-white">Tambah</button>
                            <div class="clearfix"></div>
                        </div>
                        <div class="separator"></div>
                        {!! Form::open(['action' => 'ProfileController@deleteBank', 'method' => 'DELETE', 'class' => 'form-horizontal form-delete-bank']) !!}
                            <div class="bank-dropdown">
                                <label for="bank_name" class="col-sm-5">Nama Bank</label>
                                <div class="col-sm-7">
                                    <div class="form-group">
                                        {!! Form::select('user_bank_id', $user_banks_info, null, $attributes = ['class' => 'form-control default', 'id' => 'select-bank_name']);  
                                        !!}
                                    </div>                        
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="account_number" class="col-sm-5">No. Rekening</label>
                                <div class="col-sm-7">
                                    {!! Form::text('account_number', null, $attributes = ['class' => 'form-control default', 'id' => 'accountNumber', 'disabled']); !!}
                                </div>
                            </div>                    
                            <div class="form-group">
                                <label for="account_name" class="col-sm-5">Nama Pemilik Rek.</label>
                                <div class="col-sm-7">
                                    {!! Form::text('account_name', $value = null, $attributes = ['class' => 'form-control default', 'id' => 'accountName', 'disabled']); !!}
                                </div>
                            </div>
                            {{-- showing data in behind --}}
                            @foreach($user->user_banks as $user_bank)
                            <div class="user-account-bank_name {{$user_bank->id}}-{{$user_bank->bank->name}} hide">{{ $user_bank->bank->name }}</div>
                            <div class="user-account-number-{{$user_bank->id}}-{{$user_bank->bank->name}} hide">{{ $user_bank->account_number }}</div>
                            <div class="user-account-name-{{$user_bank->id}}-{{$user_bank->bank->name}} hide">{{ $user_bank->account_name }}</div>
                            <div class="user-bank-id-hidden hide">{{ $user_bank->id }}</div>
                            <div class="bank-id-hidden hide">{{ $user_bank->bank->id }}</div>
                            @endforeach
                            <input type="hidden" class="user_bank_id_del_hidden" name="user_bank_id_del" value="">
                            <div class="button-group">
                                <button type="button" class="btn btn-edit-account-bank"><img src="/images/edit.png" alt="edit">Edit</button>
                                <button type="button" class="btn btn-delete-account-bank"><img src="/images/remove-small.png" alt="remove-small">Hapus</button>
                            </div>          
                            <div class="clearfix"></div>
                        {!! Form::close() !!}   
                    </div>
                </div>
            </div>
            <div class="section-separator"></div>
            <div class="row">
                <div class="col-sm-6">
                    <h2 class="title-profile">Profil</h2>
                        {!! Form::model($user, ['action'=>['ProfileController@putEditProfile', $user->id], 'method'=>'PUT', 'class' => 'form-horizontal form-save-identity']) !!}
                            <div class="form-group">
                                <label for="name" class="col-sm-5">Nama Lengkap</label>
                                <div class="col-sm-7 no-padding">
                                    {!! Form::text('name', $value = null, $attributes = ['class' => 'form-control default']); !!}
                                </div>
                            </div>                    
                            <div class="form-group">
                                <label for="email" class="col-sm-5">Alamat Email</label>
                                <div class="col-sm-7 no-padding">
                                    {!! Form::email('email', $value = null, $attributes = ['class' => 'form-control default']); !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="phone_number" class="col-sm-5">No. Hp</label>
                                <div class="col-sm-7 no-padding">
                                    {!! Form::text('phone_number', $value = null, $attributes = ['class' => 'form-control default']); !!}
                                </div>
                            </div>
                            <div class="radio radio-group-gender_profile">
                                <div class="radio-gender-title col-sm-5">Jenis Kelamin</div>
                                <div class="col-sm-7 no-padding">
                                    <label class="col-xs-3">
                                        {!! Form::radio('gender', 'Pria', true, $attributes = ['class' => 'radio-gender']); !!} 
                                        <div class="gender-info">Pria</div>
                                    </label>
                                    <label class="col-xs-3">
                                        {!! Form::radio('gender', 'Wanita', false, $attributes = ['class' => 'radio-gender']); !!} 
                                        <div class="gender-info">Wanita</div>
                                    </label>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="birth_profile-dropdown">
                                <label for="date" class="col-sm-5">Tanggal Lahir</label>
                                <div class="col-sm-7 no-padding">
                                    <div class="form-group col-xs-4 no-padding">
                                        {!! Form::selectRange('date', 1, 31, $user->getDate(), $attributes = ['class' => 'form-control default', 'placeholder' => 'Tgl']) !!}
                                    </div>
                                    <div class="form-group col-xs-4 no-padding">
                                        {!! Form::select('month', array(
                                                '1' => 'Jan', 
                                                '2' => 'Feb', 
                                                '3' => 'Mar', 
                                                '4' => 'Apr', 
                                                '5' => 'Mei', 
                                                '6' => 'Jun', 
                                                '7' => 'Jul', 
                                                '8' => 'Agt', 
                                                '9' => 'Sep', 
                                                '10' => 'Okt', 
                                                '11' => 'Nov', 
                                                '12' => 'Des', 
                                            ), $user->getMonth(), $attributes = ['class' => 'form-control default', 'placeholder' => 'Bln']); !!} 
                                    </div>                                    
                                    <div class="form-group col-xs-4 no-padding">
                                        {!! Form::selectRange('year', 1936, 2022, $user->getYear(), $attributes = ['class' => 'form-control default', 'placeholder' => 'Thn']) !!}
                                    </div>
                                </div>
                            </div>                       
                            <button class="btn text-center btn-save">Simpan Perubahan</button>
                            <div class="clearfix"></div>
                        {!! Form::close() !!}   
                </div>
                <div class="col-sm-1">
                    <div class="vertical-divider"></div>
                </div>
                <div class="col-sm-5">
                    <h2 class="title-password">Ganti Kata Sandi</h2>
                    {!! Form::open(['action'=>['ProfileController@putEditPassword', $user->id], 'method'=>'PUT', 'class' => 'form-horizontal form-change-password']) !!}
                        <div class="form-group">
                            <label for="old_password" class="col-sm-5">Kata Sandi Lama</label>
                            <div class="col-sm-7 no-padding">
                                {!! Form::password('old_password', $array = ['class' => 'form-control']); !!}
                            </div>
                        </div>                    
                        <div class="form-group">
                            <label for="password" class="col-sm-5">Kata Sandi Baru</label>
                            <div class="col-sm-7 no-padding">
                                {!! Form::password('password', $array = ['class' => 'form-control', 'id' => 'new_password']); !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password_confirmation" class="col-sm-5">Ulangi Kata Sandi</label>
                            <div class="col-sm-7 no-padding">
                                {!! Form::password('password_confirmation', $array = ['class' => 'form-control']); !!}
                            </div>
                        </div>
                        <button type="submit" class="btn text-center btn-change">Simpan Perubahan</button>
                        <div class="clearfix"></div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
    <!-- END: SECTION-PROFILE -->
    @include('user.layouts.footer-info')
    @include('user.layouts.footer')
@stop