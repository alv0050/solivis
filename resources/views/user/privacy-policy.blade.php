@extends('user.layouts.master')

@section('title', 'Privacy Policy')

@section('content')
    @include('user.layouts.header-line')
    @include('user.layouts.navigation')
    <!-- SECTION-PRIVACY-POLICY -->
    <section class="privacy-policy">
        <div class="container">
            <div class="one-line"></div>
            <div class="col-sm-3 col-sm-push-9 no-padding">
                <ul class="link-privacy-policy">
                    <li><a href="#user-information">Informasi Pengguna</a></li>
                    <li><a href="#information-used">Penggunaan Informasi</a></li>
                    <li><a href="#user-information-expressed">Penggunaan Informasi Pengguna</a></li>
                </ul>
            </div>
            <div class="col-sm-9 col-sm-pull-3 no-padding">
                <h2>Kebijakan Privasi</h2>
                <p>Adanya Kebijakan Privasi ini adalah komitmen nyata dari Solivis untuk menghargai dan melindungi setiap informasi pribadi Pengguna situs www.solivis.com (situs Solivis). Kebijakan ini (beserta syarat- syarat penggunaan dari situs Solivis sebagaimana tercantum dalam "Syarat & Ketentuan" dan dokumen lain yang tercantum di situs Solivis) menetapkan dasar atas segala data pribadi yang Pengguna berikan kepada Solivis atau yang Solivis kumpulkan dari Pengguna, kemudian akan diproses oleh Solivis mulai pada saat melakukan pendaftaran, mengakses dan menggunakan layanan Solivis. Harap memperhatikan ketentuan di bawah ini secara seksama untuk memahami pandangan dan kebiasaan Solivis berkenaan dengan data Solivis memperlakukan informasi tersebut.</p>
                <p>Dengan mengakses dan menggunakan layanan situs Solivis, Pengguna dianggap telah membaca, memahami dan memberikan persetujuannya terhadap pengumpulan dan penggunaan data pribadi Pengguna sebagaimana diuraikan di bawah ini.</p>
                <p>Kebijakan Privasi ini mungkin di ubah dan/atau diperbaharui dari waktu ke waktu tanpa    pemberitahuan sebelumnya. Solivis menyarankan agar anda membaca secara seksama dan memeriksa halaman Kebijakan Privasi ini dari waktu ke waktu untuk mengetahui perubahan apapun. Dengan tetap mengakses dan menggunakan layanan Solivis, maka Pengguna dianggap menyetujui perubahan- perubahan dalam Kebijakan Privasi ini.</p>
                <h3 id="user-information">Informasi Pengguna</h3>
                <p>1. Solivis mengumpulkan informasi Pengguna dengan tujuan untuk memperoses dan memperlancar proses penggunaan situs Solivis. Tindakan tersebut telah memperoleh persetujuan Pengguna pada saat
                pengumpulan informasi.</p>
                <p>2. Solivis mengumpulkan informasi pribadi sewaktu Pengguna mendaftar ke Solivis, sewaktu Pengguna menggunakan layanan Solivis, sewaktu Pengguna mengunjungi halaman Solivis atau halaman-halaman mitra usaha dari Solivis, dan sewaktu Anda menghubungi Solivis. Solivis dapat menggabung informasi mengenai Pengguna yang kami miliki dengan informasi yang kami dapatkan dari para mitra usaha atau perusahaan-perusahaan lain.</p>
                <p>3. Sewaktu Anda mendaftar untuk menjadi Pengguna Solivis, maka Solivis akan mengumpulkan data pribadi Pengguna, yakni nama lengkap, alamat e-mail, tempat dan tanggal lahir, nomor telepon yang dapat dihubungi, password dan informasi lainnya yang diperlukan. Dengan memberikan informasi / data tersebut, Anda memahami, bahwa Solivis dapat meminta dan mendapatkan setiap informasi / data pribadi Pengguna untuk kesinambungan dan keamanan situs ini.</p>
                <p>4. Solivis akan mengumpulkan dan mengolah keterangan lengkap mengenai transaksi atau penawaran atau hubungan financial lainnya yang Pengguna laksanakan melalui situs Solivis dan mengenai pemenuhan reservasi Pengguna.</p>
                <p>5. Solivis akan mengumpulkan dan mengolah data mengenai kunjungan Pengguna ke situs Solivis, namun tidak terbatas pada data lalu-lintas, data lokasi, tautan ataupun data komunikasi lainnya, apakah hal tersebut disyaratkan untuk tujuan penagihan atau lainnya, serta sumberdaya yang Pengguna akses.</p>
                <p>6. Pada saat Pengguna menghubungi Solivis, maka Solivis menyimpan catatan mengenai korespondensi tersebut dan isi dari komunikasi antara Pengguna dan Solivis.</p>
                <p>7. Pengguna memahami dan menyetujui bahwa nama Pengguna merupakan informasi umum yang tertera di halaman profile Solivis Pengguna.</p>
                <p>8. Solivis dengan sendirinya menerima dan mencatat informasi dari komputer dan browser Pengguna, termasuk alamat IP, informasi cookie Solivis, atribut piranti lunak dan piranti keras, serta halaman yang Pengguna minta.</p>
                <p>9. Setiap informasi / data Pengguna yang disampaikan kepada Solivis dan/atau yang dikumpulkan oleh Solivis dilindungi dengan upaya sebaik mungkin oleh perangkat keamanan teruji, yang digunakan oleh Solivis secara elektronik. Meskipun demikian, Solivis tidak menjamin kerahasiaan informasi yang Pengguna sampaikan tersebut, dalam kondisi adanya pihak-pihak lain yang mengambil atau mempergunakan informasi Pengguna dengan melawan hukum serta tanpa izin Solivis.</p>
                <p>10. Kerahasiaan kata sandi atau password merupakan tanggung jawab masing-masing Pengguna. Solivis tidak bertanggung jawab atas kerugian yang dapat ditimbulkan akibat kelalaian Pengguna dalam menjaga kerahasiaan passwordnya.</p>   
                <h3 id="information-used">Penggunaan Informasi</h3>
                <p>1. Solivis dapat menggunakan keseluruhan informasi / data Pengguna sebagai acuan untuk upaya
                peningkatan pelayanan.</p>
                <p>2. Solivis dapat mempergunakan dan mengolah Informasi / data Pengguna dengan tujuan untuk menyesuaikan situs Solivis sesuai dengan minat Pengguna.</p>
                <p>3. Solivis dapat menggunakan keseluruhan informasi / data Pengguna untuk kebutuhan internal Solivis tentang riset pasar, promosi tentang mitra baru, maupun informasi lain, dimana Solivis dapat menghubungi Pengguna melalui email, surat, telepon, fax.</p>
                <p>4. Solivis dapat meminta Pengguna melengkapi survei yang Solivis gunakan untuk tujuan penelitian atau lainnya, meskipun Pengguna tidak harus menanggapinya.</p>
                <p>5. Solivis dapat menghubungi Pengguna melalui email, surat, telepon, fax, termasuk namun tidak terbatas, untuk membantu dan/atau menyelesaikan proses reservasi Pengguna.</p>
                <p>6. Situs Solivis memilki kemungkinan terhubung dengan situs-situs lain diluar situs Solivis, dengan demikian Pengguna menyadari dan memahami bahwa Solivis tidak turut bertanggung jawab terhadap kerahasiaan informasi Pengguna setelah Pengguna mengakses situs-situs tersebut dengan meninggalkan atau berada diluar situs Solivis.</p>
                <h3 id="user-information-expressed">Pengungkapan Informasi Pengguna</h3> 
                <p>Solivis menjamin tidak ada penjualan, pengalihan, distribusi atau meminjamkan informasi / data pribadi Anda kepada pihak ketiga lain, tanpa terdapat izin dari Anda. Kecuali dalam hal-hal sebagai berikut:</p>
                <p>• Apabila Solivis secara keseluruhan atau sebagian assetnya diakuisisi atau merger dengan pihak ketiga, maka data pribadi yang dimiliki oleh pihak Solivis akan menjadi salah satu aset yang dialihkan atau digabung.</p>
                <p>• Apabila Solivis berkewajiban mengungkapkan dan/atau berbagi data pribadi Pengguna dalam upaya mematuhi kewajiban hukum dan/atau dalam upaya memberlakukan atau menerapkan syarat-syarat penggunaan Solivis sebagaimana tercantum dalam Syarat dan Ketentuan Layanan Solivis dan/atau perikatan - perikatan lainnya antara Solivis dengan pihak ketiga, atau untuk melindungi hak, properti, atau keselamatan Solivis, pelanggan Solivis, atau pihak lain. Di sini termasuk pertukaran informasi dengan perusahaan dan organisasi lain untuk tujuan perlindungan Solivis beserta Penggunanya
                termasuk namun tidak terbatas pada penipuan, kerugian financial atau pengurangan resiko lainnya.</p>
                <h3>Kritik dan Saran</h3>
                <p>Segala jenis kritik, saran, maupun keperluan lain dapat disampaikan ke Kontak Kami.</p>
                <p>Pembaruan Terakhir : 07/07/2016 11:30</p>                         
            </div>
        </div>
    </section>
    <!-- END: SECTION-PRIVACY-POLICY -->
    @include('user.layouts.footer-info')
    @include('user.layouts.footer')
@stop