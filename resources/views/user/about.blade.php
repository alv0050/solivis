@extends('user.layouts.master')

@section('title', 'About Us')

@section('content')
	@include('user.layouts.header-line')
	@include('user.layouts.navigation')
    <!-- SECTION-ABOUT -->
    <section class="about">
        <div class="jumbotron-about">
            <img src="/images/jumbotron-bg-aboutus.png" alt="jumbotron-bg-aboutus">
            <div class="jumbotron-info">
                <img src="/images/logo-white.png" alt="logo-white">
                <h2 class="text-center">Memudahkan anda melakukan reservasi di restoran favorit anda</h2>
            </div>
        </div>
        <div class="left-side-container">
            <div class="col-sm-6 col-sm-push-6 col-md-6">
                <img src="/images/big-map.png" alt="big-map" class="img-responsive reservation-step">
            </div>
            <div class="col-sm-offset-1 col-sm-5 col-sm-pull-6 col-md-offset-2 col-md-4">
                <div class="info-step">
                    <div class="number">1</div>
                    <h3 class="text-uppercase number-info">Cari restoran favorit anda</h3>
                    <div class="clearfix"></div>
                    <h4>Tersedia berbagai jenis restoran favorit yang dapat anda reservasi melalui aplikasi kami.</h4>
                    <div class="list-restaurant-group">
                        <button class="btn btn-list-restaurant" onclick="location.href='/sort-restaurant?sort_search_restaurant=all';">Daftar Restoran</button>
                        <img src="/images/next.png" alt="next">
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="right-side-container">
            <div class="col-sm-offset-1 col-sm-3 col-md-offset-2 col-md-3">
                <img src="/images/big-order.png" alt="big-order" class="img-responsive reservation-step">
            </div>                
            <div class="col-sm-8 col-md-7">
                <div class="info-step step-2">
                    <div class="number">2</div>
                    <h3 class="text-uppercase number-info">Reservasi tempat dan makanan sesuai keinginan anda</h3>
                    <div class="clearfix"></div>
                    <h4>Terdapat beragam jenis fasilitas dan makanan yang dapat anda nikmati dengan menyesuaikan budget anda.</h4>
                </div>
            </div>
            <div class="clearfix"></div>                    
        </div>
        <div class="left-side-container last-container">
            <div class="col-sm-6 col-sm-push-6 col-md-6">
                <img src="/images/big-qrcode.png" alt="big-qrcode" class="img-responsive reservation-step">
            </div>
            <div class="col-sm-offset-1 col-sm-5 col-sm-pull-6 col-md-offset-2 col-md-4">
                <div class="info-step step-3">
                    <div class="number">3</div>
                    <h3 class="text-uppercase number-info">Datang & tunjukkan reservasi anda</h3>
                    <div class="clearfix"></div>
                    <h4>Reservasi anda dapat dengan mudah dan cepat dikenali oleh pihak restoran.</h4>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="sign-as-container">
            <h1 class="text-center text-uppercase">Daftar sekarang sebagai</h1>
            <div class="col-sm-5">
                <a href="/register" class="btn btn-sign-as">Penikmat Kuliner</a>
                <img src="/images/sign-as-user.png" alt="sign-as-user" class="img-responsive">
            </div>
            <div class="col-sm-2">
                <div class="text-uppercase text-center option">Atau</div>
            </div>
            <div class="col-sm-5">
                <a href="/business" class="btn btn-sign-as">Pemilik Usaha Kuliner</a>
                <img src="/images/sign-as-restaurant.png" alt="sign-as-restaurant" class="img-responsive">
            </div>
            <div class="clearfix"></div>
        </div>
    </section>
    @include('user.layouts.footer-info')
    @include('user.layouts.footer')
@stop