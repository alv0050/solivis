@extends('user.layouts.master')

@section('title', 'Reservation Food & Place in Restaurant')

@section('content')
	@include('user.layouts.header-line')
	@include('user.layouts.navigation')
    <!-- SECTION-CAROUSEL -->
    <div class="container">
        @include('partials.message')
        @include('partials.error')
    </div>
    <div id="carousel-solivis-generic" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators index-indicators">
            <li data-target="#carousel-solivis-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-solivis-generic" data-slide-to="1"></li>
            <li data-target="#carousel-solivis-generic" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <a href="/restaurant/show/4" target="_blank">
                    <img src="/images/dapursolo-slide.png" alt="dapursolo-slide">
                </a>
            </div>
            <div class="item">
                <a href="/restaurant/show/5" target="_blank">
                    <img src="/images/eaton-slide.png" alt="eaton-slide">
                </a>
            </div>
            <div class="item">
                <a href="/restaurant/show/7" target="_blank">
                    <img src="/images/miramar-slide.jpg" alt="miramar-slide">
                </a>
            </div>
            ...
        </div>
    </div>
    <!-- END: SECTION-CAROUSEL -->
    <div class="container">
        <!-- SECTION-GUIDE -->
        <section class="section-guide">
            <h1 class="text-center text-uppercase">Mudahnya reservasi restoran favorit anda</h1>
            <div class="row">
                <div class="col-sm-4">
                    <img src="/images/guide-1.png" alt="guide-1">
                    <h4 class="text-center">Cari restoran favorit anda</h4>
                </div>
                <div class="col-sm-4">
                    <img src="/images/guide-2.png" alt="guide-2"> 
                    <h4 class="text-center">Reservasi tempat & makanan sesuai yang anda inginkan</h4>      
                </div>
                <div class="col-sm-4">
                    <img src="/images/guide-3.png" alt="guide-3">                    
                    <h4 class="text-center">Datang dan tunjukkan reservasi anda melalui aplikasi kami</h4>                
                </div>
            </div>
        </section>   
        <!-- END: SECTION-GUIDE -->
    </div>
    <div class="container-fluid no-padding">
        <!-- SECTION-BUSINESS -->
        <section class="section-business">
            <div class="business-info">
                <h1 class="text-center text-uppercase">Daftarkan bisnis restoran anda</h1>
                <h3 class="text-center">Kembangkan usaha anda & konsultasikan bisnis anda dengan kami sekarang juga!</h3>
            </div>
        </section>
        <!-- END: SECTION-BUSINESS -->
        <!-- SECTION-BUSINESS-ILLUSTRATION -->
        <section class="section-business-illustration">
            <button class="btn text-center text-uppercase btn-start" onclick="location.href='/business';">
                <div class="title-start">Mulai</div> 
                <div class="img-start">
                    <img src="/images/right.png" alt="right">
                </div>
                <div class="clearfix"></div>
            </button>        
            <img src="/images/city.png" alt="city">
        </section>                
        <!-- END: SECTION-BUSINESS-ILLUSTRATION -->        
    </div>
    <div class="container">
        <!-- SECTION-TRUSTED -->
        <section class="section-trusted">
            <h3 class="text-center text-uppercase">Dipercayai oleh</h3>
            <div class="row">
                <div class="col-sm-offset-1 col-sm-2">
                    <img src="/images/trusted-resto-1.png" alt="trusted-resto-1"/>
                </div>
                <div class="col-sm-2">
                    <img src="/images/trusted-resto-2.png" alt="trusted-resto-2"/>
                </div>
                <div class="col-sm-2">
                    <img src="/images/trusted-resto-3.png" alt="trusted-resto-3"/>
                </div>
                <div class="col-sm-2">
                    <img src="/images/trusted-resto-4.png" alt="trusted-resto-4"/>
                </div>
                <div class="col-sm-2">
                    <img src="/images/trusted-resto-5.png" alt="trusted-resto-5"/>                    
                </div>
            </div>
        </section>
    </div> 
    @include('user.layouts.footer-info')
    @include('user.layouts.footer')
@stop