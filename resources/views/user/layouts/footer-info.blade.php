<!-- SECTION-FOOTER-INFO -->
<section class="section-footer-info">
    <div class="container"> 
		<div class="row">
			<div class="col-sm-6 col-md-3">
				<ul class="bottom-nav no-padding">
					<li><a href="/about">Tentang Kami</a></li>
					<li><a href="#">Most Booked</a></li>
					<li><a href="#">Recommended</a></li>
					<li><a href="/contact">Contact Us</a></li>
				</ul>
			</div>
			<div class="col-sm-6 col-md-3">
				<div class="footer-info-title">Subscribe Now</div>
				{!! Form::open($attributes = ['action' => 'BaseController@postSubscribe', 'class' => 'form-inline form-subscribe']) !!}
					<div class="form-group form-group-subscribe">
						{!! Form::email('email', $value = null, $attributes = ['class' => 'form-control default', 'placeholder' => 'Alamat Email']); !!}
					</div>    		
					<button type="submit" class="btn btn-subscribe"><img class="img-responsive" src="/images/message.png" alt="message"></button>
				{!! Form::close(); !!}	
			</div>
			<div class="clearfix fix_float-footer"></div>
			<div class="col-xs-7 col-sm-6 col-md-3 follow-info-footer">
				<div class="footer-info-title">Follow Us</div>
				<div class="col-xs-3 no-padding">
					<a href="https://www.facebook.com/solivisads" class="thumbnail round-apps fb" target="_blank">
						<img src="/images/fb.png" alt="fb">
				    </a>				
			    </div>
			    <div class="col-xs-3 no-padding">
					<a href="https://twitter.com/solivisads" class="thumbnail round-apps twitter" target="_blank">
						<img src="/images/twitter.png" alt="twitter">
				    </a>				
			    </div>
			    <div class="col-xs-3 no-padding">
					<a href="https://www.instagram.com/solivisads/" class="thumbnail round-apps ig" target="_blank">
						<img src="/images/ig.png" alt="ig">
				    </a>				
			    </div>
			    <div class="col-xs-3 no-padding">
					<a href="https://www.youtube.com/channel/UCBh-doITATKi3W1tV4VrAoQ" class="thumbnail round-apps youtube" target="_blank">
						<img src="/images/youtube.png" alt="youtube">
				    </a>				
			    </div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</section>
<!-- END: SECTION-FOOTER-INFO -->
