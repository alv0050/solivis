@include('user.modals.login')
<!-- NAVBAR -->
<nav class="navbar navbar-static-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand logo" href="/"><img src="/images/logo.png" alt="logo"/></a>
		</div>
		<div class="collapse navbar-collapse" id="navbar">
			<ul class="nav navbar-nav navbar-right top-nav">
				<li {{ (Request::is('about') ? 'class=active' : '') }}><a href="/about">About Us</a></li>
				<li {{ (Request::getQueryString() == 'sort_search_restaurant=most_booked' ? 'class=active' : '') }}><a href="/sort-restaurant?sort_search_restaurant=most_booked">Most Booked</a></li>
				<li {{ (Request::getQueryString() == 'sort_search_restaurant=rating' ? 'class=active' : '') }}><a href="/sort-restaurant?sort_search_restaurant=rating">Recommended</a></li>
				<li {{ (Request::is('contact') ? 'class=active' : '') }}><a href="/contact">Contact Us</a></li>
				@if(!Auth::user('user'))
				<li>
					<a href="/register" class="link-dm">
					<img src="/images/daftar.png" alt="daftar" class="icon-dm">
					<img src="/images/daftar-black.png" alt="daftar" class="icon-dm">
					<img src="/images/daftar-orange.png" alt="daftar" class="icon-dm">
					<div class="btn-info-register">Daftar</div>
					</a>
				</li>
				<li>
					<a href="#" id="login" class="link-dm">
						<img src="/images/masuk.png" alt="masuk" class="icon-dm">
						<img src="/images/masuk-black.png" alt="masuk" class="icon-dm">
						<img src="/images/masuk-orange.png" alt="masuk" class="icon-dm">
						<div class="btn-info-login">Masuk</div>
					</a>
				</li>	
				@else			
				<li class="_user-auth">
					<div class="user-auth-group">
						<img src="/images/uploads/{{ Auth::user()->photo_url }}" alt="profile-picture-4" class="img-circle">
						<h3>{{ Auth::user()->name }}</h3>
					</div>
					<div class="user-auth-info">
						<img src="/images/pointer.png" alt="pointer" class="icon-pointer">
						<ul class="user-auth-link">
							<li>
								<a href="/profile/edit/{{ Auth::user()->id }}" class="link-auth">
									<img src="/images/profile-black.png" alt="profile-black">
									<img src="/images/profile.png" alt="profile">
									<p>Profile</p>
								</a>								
							</li>
							<li>
								<a href="/balance" class="link-auth balance-link">
									<img src="/images/saldo-black.png" alt="saldo-black">
									<img src="/images/saldo-white.png" alt="saldo-white">
									<p>Saldo</p>
								</a>								
							</li>
							<li>
								<a href="/reservation/history" class="link-auth">
									<img src="/images/reservation-black.png" alt="reservation-black">
									<img src="/images/reservation-white.png" alt="reservation-white">
									<p>Reservasi</p>
								</a>								
							</li>
							<li>
								<a href="/logout" class="link-auth">
									<img src="/images/keluar-black.png" alt="keluar-black">
									<img src="/images/keluar.png" alt="keluar">
									<p>Keluar</p>
								</a>								
							</li>
						</ul>
					</div>
				</li>
				@endif
			</ul>
		</div>
        {!! Form::open(['action' => 'BaseController@getSearchReservation', 'method' => 'GET', 'class' => 'navbar-form navbar-right no-padding form-search']) !!}
			<div class="form-group form-group-restaurant_name">
				<div class="info">Cari</div>
				{!! Form::text('restaurant_name', $value = null, $attributes = ['class' => 'form-control', 'placeholder' => 'Nama Restoran', 'id' => 'input-restaurant_name']); !!}
				<div class="info-keywords text-center">Masukkan kata kunci...</div>
			</div>
			<div class="group-results">
				<h5 class="recommendation-title-food">Makanan</h5>
				<ul id="results_food"></ul>
				<h5 class="recommendation-title-location">Lokasi</h5>
				<ul id="results_location"></ul>
				<h5 class="recommendation-title-cuisine">Jenis Kuliner</h5>
				<ul id="results_cuisine"></ul>
				<h5 class="recommendation-title-restaurant">Restaurant</h5>
				<ul id="results_restaurant"></ul>
				<ul id="results_notfound"></ul>
			</div>
			<div class="form-group form-group-cuisine">
			{!! Form::select('cuisine', array('Italian' => 'Italian', 'Indonesia' => 'Indonesia', 'Chinese' => 'Chinese', 'Korea' => 'Korea', 'Western' => 'Western'), null, ['class' => 'form-control', 'id' => 'select-cuisine', 'placeholder' => 'Jenis Kuliner']); !!}
			</div>
			<div class="form-group form-group-pax">
				<div class="info"><img src="/images/pax.png" alt="pax"></div>
				{!! Form::text('paxes', $value = null, $attributes = ['class' => 'form-control', 'placeholder' => 'Pax', 'id' => 'input-pax']); !!}
				<div class="info-pax">
					<img src="/images/pointer.png" alt="pointer" class="icon-pointer">
					<div class="info-pax-title">
						<img src="/images/pax.png" alt="pax" class="icon-pax">
						<h4>Berapa banyak orang ?</h4>
					</div>
					<div class="info-pax-detail">
						<table class="table table-bordered table-pax">
							<tr>
								<td id="pax-1">1</td>
								<td id="pax-2">2</td>
								<td id="pax-3">3</td>
								<td id="pax-4">4</td>
								<td id="pax-5">5</td>
							</tr>
							<tr>
								<td id="pax-6">6</td >
								<td id="pax-7">7</td>
								<td id="pax-8">8</td>
								<td id="pax-9">9</td>
								<td id="pax-10">10</td>
							</tr>
						</table>
						<p class="text-right"><em>input manual apabila banyak orang > 10</em></p>
					</div>
				</div>
			</div>
			<div class="form-group form-group-date">
				<div class="info"><img src="/images/date.png" alt="date"></div>
				{!! Form::text('reservation_date', $value = null, $attributes = ['class' => 'form-control', 'placeholder' => 'Tanggal', 'id' => 'input-date', 'onkeydown="return false;"']); !!}
			</div>
			<div class="form-group form-group-time">
				<div class="info"><img src="/images/time.png" alt="time"></div>
				{!! Form::text('reservation_time', $value = null, $attributes = ['class' => 'form-control', 'placeholder' => 'Jam', 'id' => 'input-time', 'onkeydown="return false;"']); !!}
				<div class="group-time">
					<img src="/images/pointer.png" alt="pointer" class="icon-pointer pointer hide">
					<div class="info-time">
						<div class="info-time-title">
							<img src="/images/time.png" alt="time" class="icon-time">
							<h4>Jam berapa ?</h4>
						</div>
						<div class="info-time-detail">
							<h4 class="table-time-title text-center">-Breakfast-</h4>
							<table class="table table-bordered table-time">
								<tr>
									<td id="time-600">06:00</td>
									<td id="time-630">06:30</td>
									<td id="time-700">07:00</td>
								</tr>
								<tr>
									<td id="time-730">07:30</td>
									<td id="time-800">08:00</td>
									<td id="time-830">08:30</td>
								</tr>
								<tr>
									<td id="time-900">09:00</td>
									<td id="time-930">09:30</td>
									<td id="time-1000">10:00</td>
								</tr>
								<tr>
									<td id="time-1030">10:30</td>
									<td id="time-1100">11:00</td>
								</tr>

							</table>	
							<h4 class="table-time-title text-center">-Lunch-</h4>
							<table class="table table-bordered table-time">
								<tr>
									<td id="time-1130">11:30</td>
									<td id="time-1200">12:00</td>
									<td id="time-1230">12:30</td>
								</tr>
								<tr>
									<td id="time-1300">13:00</td>
									<td id="time-1330">13:30</td>
								</tr>
							</table>						
							<h4 class="table-time-title text-center">-Late Lunch-</h4>
							<table class="table table-bordered table-time">
								<tr>
									<td id="time-14:00">14:00</td>
									<td id="time-14:30">14:30</td>
									<td id="time-15:00">15:00</td>
								</tr>
								<tr>
									<td id="time-15:30">15:30</td>
									<td id="time-16:00">16:00</td>
									<td id="time-16:30">16:30</td>
								</tr>
								<tr>
									<td id="time-17:00">17:00</td>
									<td id="time-17:30">17:30</td>
								</tr>
							</table>						
							<h4 class="table-time-title text-center">-Dinner-</h4>
							<table class="table table-bordered table-time">
								<tr>
									<td id="time-18:00">18:00</td>
									<td id="time-18:30">18:30</td>
									<td id="time-19:00">19:00</td>
								</tr>
								<tr>
									<td id="time-19:30">19:30</td>
									<td id="time-20:00">20:00</td>
									<td id="time-20:30">20:30</td>
								</tr>
							</table>						
							<h4 class="table-time-title text-center">-Late Dinner-</h4>
							<table class="table table-bordered table-time">
								<tr>
									<td id="time-21:00">21:00</td>
									<td id="time-21:30">21:30</td>
									<td id="time-22:00">22:00</td>
								</tr>
								<tr>
									<td id="time-22:30">22:30</td>
									<td id="time-23:00">23:00</td>
									<td id="time-23:30">23:30</td>
								</tr>
							</table>						
						</div>
					</div>
				</div>
			</div>
			<button type="submit" class="btn btn-search"><img src="/images/search.png" alt="search"/></button>
		{!! Form::close() !!}
	</div>
</nav>
<!-- END: NAVBAR -->