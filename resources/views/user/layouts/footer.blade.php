<!-- FOOTER -->
<footer>
	<div class="container">
		<ul class="nav navbar-nav">
			<li><a href="/terms">Syarat & Ketentuan</a></li>
			<li><a href="/privacy-policy">Kebijakan Privasi</a></li>
		</ul>
		<div class="border-separator"></div>
		<ul class="nav navbar-nav navbar-right">
			<li>Copyright 2016 Solivis</li>
		</ul>
	</div>
	<div class="border-separator"></div>
</footer>
<!-- END: FOOTER -->
