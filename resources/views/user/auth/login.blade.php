@extends('user.layouts.master')

@section('title', 'Login')

@section('content')
    @include('user.modals.login')
	@include('user.layouts.header-line')

    <!-- SECTION-LOGIN -->
    <section class="section-login">
        <div class="container">
            <div class="login">
                <a href="/" class="link-logo"><img src="/images/logo.png" alt="logo"/></a>
                @include('partials.error') 
                @include('partials.message') 
                {!! Form::open(['class' => 'form-login2']) !!}
                    <div class="form-group form-group-login2">
                        <img class="icon-msg" src="/images/email.png" alt="email"/>
                        {!! Form::email('email', $value = null, $attributes = ['class' => 'form-control default', 'placeholder' => 'Alamat Email']); !!}
                    </div>          
                    <div class="form-group form-group-login2">
                        <img class="icon-pswd" src="/images/password.png" alt="password"/>
                        {!! Form::password('password', $array = ['class' => 'form-control default', 'placeholder' => 'Kata Sandi', 'id' => 'password']); !!}
                    </div> 
                    <div class="checkbox remember-me">
                        <label>
                            {!! Form::checkbox('remember', 1, true); !!} <span class="remember-info">Biarkan saya tetap masuk</span>
                        </label>
                    </div>
                    <div class="forget-password"><a href="/password/email">Lupa kata sandi?</a></div>
                    <div class="clearfix"></div>
                    <button type="submit" class="btn text-center btn-login2">Masuk</button>
                {!! Form::close() !!}   
            </div>
        </div>
    </section>
    <!-- END: SECTION-LOGIN -->
    @include('user.layouts.footer-dm')
@stop