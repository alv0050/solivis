@extends('user.layouts.master')

@section('title', 'Reset')

@section('content')
	@include('user.layouts.header-line')
	@include('user.layouts.navigation')
    <!-- SECTION-RESET-PASSWORD -->
    <section class="section-reset-password">
    	<div class="container">
    		<div class="reset-password">
                @include('partials.error')
                @include('partials.message')
                <h2 class="text-center text-uppercase">Reset Password</h2>
				{!! Form::open(['action' => 'Auth\PasswordController@postReset', 'class' => 'form-reset-password']) !!}
                    {{-- Token for reset password --}}
                    <input type="hidden" name="token" value="{{ $token }}">
					<div class="form-group form-group-reset-password">
						{!! Form::email('email', $value = null, $attributes = ['class' => 'form-control default', 'placeholder' => 'Alamat Email']); !!}
					</div>
                    <div class="form-group form-group-reset-password">
                        {!! Form::password('password', $array = ['class' => 'form-control default', 'placeholder' => 'Kata Sandi', 'id' => 'password']); !!}
                    </div>          
                    <div class="form-group form-group-reset-password">
                        {!! Form::password('password_confirmation', $array = ['class' => 'form-control default', 'placeholder' => 'Ulangi Kata Sandi']); !!}
                    </div>                         		
					<button type="submit" class="btn text-center btn-reset-password">Reset</button>
				{!! Form::close() !!}	
    		</div>
    	</div>
    </section>
    <!-- END: SECTION-RESET-PASSWORD -->
    @include('user.layouts.footer-info')
    @include('user.layouts.footer')
@stop