@extends('user.layouts.master')

@section('title', 'Password')

@section('content')
	@include('user.layouts.header-line')
	@include('user.layouts.navigation')
    <!-- SECTION-SEND-RESET -->
    <section class="section-send-reset">
    	<div class="container">
    		<div class="send-reset">
                @include('partials.error')
                @include('partials.message')
    			<h2 class="text-center">Kesulitan mengakses akun Anda?</h2>
    			<p class="text-center">Lupa password? Masukkan email login anda di bawah ini.</p>
    			<p class="text-center">Kami akan mengirimkan link reset password ke email anda.</p>
				{!! Form::open(['action' => 'Auth\PasswordController@postEmail', 'class' => 'form-send-reset']) !!}
					<div class="form-group form-group-send-reset">
						{!! Form::email('email', $value = null, $attributes = ['class' => 'form-control default', 'placeholder' => 'Alamat Email']); !!}
					</div>    		
					<button type="submit" class="btn text-center btn-send-reset">Kirim</button>
				{!! Form::close() !!}	
    		</div>
    	</div>
    </section>
    <!-- END: SECTION-RESET -->
    @include('user.layouts.footer-info')
    @include('user.layouts.footer')
@stop