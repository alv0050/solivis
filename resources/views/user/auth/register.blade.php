@extends('user.layouts.master')

@section('title', 'Register')

@section('content')
    @include('user.modals.login')
	@include('user.layouts.header-line')
    <!-- SECTION-REGISTER -->
    <section class="section-register">
        <div class="container">
            <div class="register">
                <a href="/" class="link-logo"><img src="/images/logo.png" alt="logo"/></a>
                <h2 class="text-center text-uppercase">Daftar gratis di solivis</h2>
                <p class="text-center">Sudah punya akun Solivis? Masuk <a href="#" class="link-register"  id="login"><em>di sini</em></a>.</p>
                @include('partials.error')
                {!! Form::open(['class' => 'form-register']) !!}
                    <div class="form-group form-group-register">
                        <img src="/images/person.png" alt="person" class="icon-register"/>
                        {!! Form::text('name', $value = null, $attributes = ['class' => 'form-control default', 'placeholder' => 'Nama Lengkap']); !!}
                    </div>          
                    <div class="form-group form-group-register">
                        <img src="/images/phone.png" alt="phone" class="icon-register"/>
                        {!! Form::text('phone_number', $value = null, $attributes = ['class' => 'form-control default', 'placeholder' => 'Nomor Handphone']); !!}
                    </div>          
                    <div class="form-group form-group-register">
                        <img class="icon-register icon-msg" src="/images/email.png" alt="email"/>
                        {!! Form::email('email', $value = null, $attributes = ['class' => 'form-control default', 'placeholder' => 'Alamat Email']); !!}
                    </div>          
                    <div class="form-group form-group-register">
                        <img class="icon-register icon-pswd" src="/images/password.png" alt="password"/>
                        {!! Form::password('password', $array = ['class' => 'form-control default', 'placeholder' => 'Kata Sandi', 'id' => 'password']); !!}
                    </div>          
                    <div class="form-group form-group-register">
                        <img class="icon-register icon-pswd" src="/images/password.png" alt="password"/>
                        {!! Form::password('password_confirmation', $array = ['class' => 'form-control default', 'placeholder' => 'Ulangi Kata Sandi']); !!}
                    </div> 
                    <div class="birth-dropdown">
                        <label for="date">Tanggal Lahir</label>
                        <div class="form-group">
                            {!! Form::selectRange('date', 1, 31, null, $attributes = ['class' => 'form-control default', 'placeholder' => 'Tanggal']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::select('month', array(
                                    '1' => 'Januari', 
                                    '2' => 'Februari', 
                                    '3' => 'Maret', 
                                    '4' => 'April', 
                                    '5' => 'Mei', 
                                    '6' => 'Juni', 
                                    '7' => 'Juli', 
                                    '8' => 'Agustus', 
                                    '9' => 'September', 
                                    '10' => 'Oktober', 
                                    '11' => 'November', 
                                    '12' => 'Desember', 
                                ), null, $attributes = ['class' => 'form-control default', 'placeholder' => 'Bulan']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::selectRange('year', 1936, 2022, null, $attributes = ['class' => 'form-control default', 'placeholder' => 'Tahun']) !!}
                        </div>
                    </div>
                    <div class="radio radio-group-gender">
                        <div class="radio-gender-title">Jenis Kelamin</div>
                        <label>
                            {!! Form::radio('gender', 'Pria', true, $attributes = ['class' => 'radio-gender']); !!} 
                            <div class="gender-info">Pria</div>
                        </label>
                        <label>
                            {!! Form::radio('gender', 'Wanita', false, $attributes = ['class' => 'radio-gender']); !!} 
                            <div class="gender-info">Wanita</div>
                        </label>
                    </div>
                    <p class="text-center">Dengan menekan tombol Daftar Akun, saya mengkonfirmasi telah menyetujui Syarat dan Ketentuan serta Kebijakan Privasi Solivis</p>
                    <button type="submit" class="btn text-center btn-register">Daftar Akun</button>
                {!! Form::close() !!} 
            </div>
        </div>
    </section>
    <!-- END: SECTION-REGISTER -->
    @include('user.layouts.footer')
@stop