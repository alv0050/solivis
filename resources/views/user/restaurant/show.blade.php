@extends('user.layouts.master')

@section('title', 'Restaurant Profile')

@section('content')
	@include('user.layouts.header-line')
	@include('user.layouts.navigation')
    @include ('user.modals.photo_container')
    @include ('user.modals.share')
    @include ('user.modals.reservation')
    @include ('user.modals.order')
    @include ('user.modals.pay')    
    @include ('user.modals.over_reserve_time')    
    @include ('user.modals.reserve_time_less')    
    @include ('user.modals.only_use_balance')   
    <!-- SECTION-RESTAURANT_PROFILE -->
    <section class="restaurant_profile">
        <div class="container">
            @include('partials.message')
            <div class="restaurant_data">
                <h1>{{ $restaurant_management->name }}</h1>
                <div class="restaurant_location">
                    <img src="/images/location-black.png" alt="location-black">
                    <h4>{{ $restaurant_management->address }}</h4>
                    <a href="https://www.google.co.id/maps/search/{{ $restaurant_management->address }}" class="btn btn-show-map" target="_blank">Tampilkan Map</a>
                </div>
            </div>
            <div class="restaurant_social">
                <div class="restaurant_reviewandstars">
                    <h5>Rating Makanan dari <strong>{{ $total_ratings }}</strong> rating</h5>
                    @for($i=0; $i<$total_ratings_average; $i++)
                    <img src="/images/big_star.png" alt="big_star">
                    @endfor
                    @for($i=0; $i<(5 - $total_ratings_average); $i++)
                    <img src="/images/big_star-black.png" alt="big_star-black">
                    @endfor
                </div>
                <div class="social-group">
                    <div class="favorite-group">
                        <img src="/images/favorite-white.png" alt="favorite">
                        {!! Form::open(['action' => 'RestaurantController@postFavorite']) !!}
                            <input type="hidden" name="restaurant_management_hidden_id" value="{{ $restaurant_management->id }}">
                            @if (Auth::user('user'))
                            <input type="hidden" name="user_hidden_id" value="{{ Auth::user('user')->id }}">
                            @else
                            <input type="hidden" name="user_id" value="">
                            @endif
                            @if($has_favorite == true)
                            <button class="btn text-uppercase btn-favorite favorite_true" >Favorite</button>
                            @else
                            <button class="btn text-uppercase btn-favorite favorite_false" >Favorite</button>
                            @endif
                        {!! Form::close() !!}
                    </div>
                    <div class="share-group">
                        <img src="/images/share.png" alt="share">
                        <button class="btn text-uppercase btn-share">Share</button>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-9">
                    <div class="banner">
                        <div class="banner-content">
                            <div class="total-favorite">
                                <img src="/images/favorite-red.png" alt="favorite-red"/>
                                <h4>{{ $total_favorites }} Favorited</h4>
                            </div>
                            <div class="info-left-side">
                                <h4 class="text-uppercase facility-title">Jenis Kuliner</h4>
                                <h5 class="facility-detail">{{ $cuisine_names }}</h5>
                                <h4 class="text-uppercase facility-title">Opening Hours</h4>
                                <h5 class="facility-detail">{{ $restaurant_management->getOpenTimeHourAndMinute() }}-{{ $restaurant_management->getCloseTimeHourAndMinute() }}</h5>
                            </div>
                            <div class="info-right-side">
                                <h4 class="text-uppercase facility-title">Services</h4>
                                <h5 class="facility-detail">{{ $service_names }}</h5>
                                <h4 class="text-uppercase facility-title">Prices</h4>
                                <h5 class="facility-detail">{{ $restaurant_management->getLowestPrice() }} - {{ $restaurant_management->getHighestPrice() }}</h5>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div id="carousel-restaurant-profile" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators restaurant_profile-indicators">
                            @for ($i=0; $i<count($photo_urls); $i++)
                            @if ($i == 0)
                                <li data-target="#carousel-restaurant-profile" data-slide-to="0" class="active small-indicator"></li>
                            @else
                                <li data-target="#carousel-restaurant-profile" data-slide-to="{{ $i }}" class="small-indicator"></li>
                            @endif
                            @endfor
                        </ol>
                        <div class="carousel-inner" role="listbox">
                            @for ($i=0; $i<count($photo_urls); $i++)
                            @if ($i == 0)
                                <div class="item active">
                                    <img src="/images/restaurant_uploads/inside_restaurant_photos/{{ $photo_urls[$i] }}" alt="{{ $photo_urls[$i] }}">
                                </div>
                            @else
                                <div class="item">
                                    <img src="/images/restaurant_uploads/inside_restaurant_photos/{{ $photo_urls[$i] }}" alt="{{ $photo_urls[$i] }}">
                                </div>
                            @endif
                            @endfor
                        </div>
                    </div>                    
                </div>
                <div class="col-md-3">
                    <div class="reserve-container">
                        <div class="reserve-content">
                            {!! Form::open(['class' => 'form-reservation-info']) !!}
                                <div class="form-group">
                                    <label for="pax">Pax</label>
                                    <div class="total-paxes">
                                        <img src="/images/pax.png" alt="pax">
                                        @if (Session::has('edit_paxes'))
                                        {!! Form::text('pax', $value = Session::get('edit_paxes'), $attributes = ['class' => 'form-control', 'id' => 'input-pax-reserve']); !!}
                                        @else
                                        {!! Form::text('pax', $value = null, $attributes = ['class' => 'form-control', 'id' => 'input-pax-reserve']); !!}
                                        @endif
                                        <div class="info-pax-reserve">
                                            <img src="/images/pointer.png" alt="pointer" class="icon-pointer">
                                            <div class="info-pax-reserve-title">
                                                <img src="/images/pax.png" alt="pax" class="icon-pax-reserve">
                                                <h4>Berapa banyak orang ?</h4>
                                            </div>
                                            <div class="info-pax-reserve-detail">
                                                <table class="table table-bordered table-pax-reserve">
                                                    <tr>
                                                        <td id="pax-1">1</td>
                                                        <td id="pax-2">2</td>
                                                        <td id="pax-3">3</td>
                                                        <td id="pax-4">4</td>
                                                        <td id="pax-5">5</td>
                                                    </tr>
                                                    <tr>
                                                        <td id="pax-6">6</td >
                                                        <td id="pax-7">7</td>
                                                        <td id="pax-8">8</td>
                                                        <td id="pax-9">9</td>
                                                        <td id="pax-10">10</td>
                                                    </tr>
                                                </table>
                                                <p class="text-right"><em>input manual apabila banyak orang > 10</em></p>
                                            </div>
                                        </div>                                        
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="date">Tanggal</label>
                                    <div class="date-reserve">
                                        <img src="/images/date.png" alt="date">
                                        @if (Session::get('edit_reservation_date'))
                                        {!! Form::text('date', $value = $restaurant_management->getOnlyDate(Session::get('edit_reservation_date')), $attributes = ['class' => 'form-control', 'id' => 'input-date-reserve', 'onkeydown="return false;"']); !!}
                                        @else
                                        {!! Form::text('date', $value = null, $attributes = ['class' => 'form-control', 'id' => 'input-date-reserve', 'onkeydown="return false;"']); !!}
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="time">Jam</label>
                                    <div class="time-reserve">
                                        <img src="/images/time.png" alt="time">
                                        @if (Session::get('edit_reservation_date'))
                                        {!! Form::text('time', $value = $restaurant_management->getOnlyTime(Session::get('edit_reservation_date')), $attributes = ['class' => 'form-control', 'id' => 'input-time-reserve', 'onkeydown="return false;"']); !!}
                                        @else
                                        {!! Form::text('time', $value = null, $attributes = ['class' => 'form-control', 'id' => 'input-time-reserve', 'onkeydown="return false;"']); !!}
                                        @endif
                                        <div class="group-time-reserve">
                                            <img src="/images/pointer.png" alt="pointer" class="icon-pointer second_pointer hide">
                                            <div class="info-time-reserve">
                                                <div class="info-time-reserve-title">
                                                    <img src="/images/time.png" alt="time" class="icon-time">
                                                    <h4>Jam berapa ?</h4>
                                                </div>
                                                <div class="info-time-reserve-detail">
                                                    <h4 class="table-time-reserve-title text-center">-Breakfast-</h4>
                                                    <table class="table table-bordered table-time-reserve">
                                                        <tr>
                                                            <td id="time-600">06:00</td>
                                                            <td id="time-630">06:30</td>
                                                            <td id="time-700">07:00</td>
                                                        </tr>
                                                        <tr>
                                                            <td id="time-730">07:30</td>
                                                            <td id="time-800">08:00</td>
                                                            <td id="time-830">08:30</td>
                                                        </tr>
                                                        <tr>
                                                            <td id="time-900">09:00</td>
                                                            <td id="time-930">09:30</td>
                                                            <td id="time-1000">10:00</td>
                                                        </tr>
                                                        <tr>
                                                            <td id="time-1030">10:30</td>
                                                            <td id="time-1100">11:00</td>
                                                        </tr>

                                                    </table>    
                                                    <h4 class="table-time-reserve-title text-center">-Lunch-</h4>
                                                    <table class="table table-bordered table-time-reserve">
                                                        <tr>
                                                            <td id="time-1130">11:30</td>
                                                            <td id="time-1200">12:00</td>
                                                            <td id="time-1230">12:30</td>
                                                        </tr>
                                                        <tr>
                                                            <td id="time-1300">13:00</td>
                                                            <td id="time-1330">13:30</td>
                                                        </tr>
                                                    </table>                        
                                                    <h4 class="table-time-reserve-title text-center">-Late Lunch-</h4>
                                                    <table class="table table-bordered table-time-reserve">
                                                        <tr>
                                                            <td id="time-1400">14:00</td>
                                                            <td id="time-1430">14:30</td>
                                                            <td id="time-1500">15:00</td>
                                                        </tr>
                                                        <tr>
                                                            <td id="time-1530">15:30</td>
                                                            <td id="time-1600">16:00</td>
                                                            <td id="time-1630">16:30</td>
                                                        </tr>
                                                        <tr>
                                                            <td id="time-1700">17:00</td>
                                                            <td id="time-1730">17:30</td>
                                                        </tr>
                                                    </table>                        
                                                    <h4 class="table-time-reserve-title text-center">-Dinner-</h4>
                                                    <table class="table table-bordered table-time-reserve">
                                                        <tr>
                                                            <td id="time-1800">18:00</td>
                                                            <td id="time-1830">18:30</td>
                                                            <td id="time-1900">19:00</td>
                                                        </tr>
                                                        <tr>
                                                            <td id="time-1930">19:30</td>
                                                            <td id="time-2000">20:00</td>
                                                            <td id="time-2030">20:30</td>
                                                        </tr>
                                                    </table>                        
                                                    <h4 class="table-time-reserve-title text-center">-Late Dinner-</h4>
                                                    <table class="table table-bordered table-time-reserve">
                                                        <tr>
                                                            <td id="time-2100">21:00</td>
                                                            <td id="time-2130">21:30</td>
                                                            <td id="time-2200">22:00</td>
                                                        </tr>
                                                        <tr>
                                                            <td id="time-2230">22:30</td>
                                                            <td id="time-2300">23:00</td>
                                                            <td id="time-2330">23:30</td>
                                                        </tr>
                                                    </table>                        
                                                </div>
                                            </div>
                                        </div>                                 
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <ul class="nav nav-pills nav-justified nav-results-fade">
                        <li class="menus active"><a href="#">Menus ({{ $restaurant_management->menus->count() }})</a></li>
                        <li class="photos"><a href="#">Photos ({{ $restaurant_management->restaurant_food_photos->count() }})</a></li>
                        <li class="reviews"><a href="#">Reviews ({{ $total_users_rate }})</a></li>
                    </ul>
                    <div class="menu-container">
                        <h3>Menus</h3>
                        <div class="divider"></div>
                        <div class="col-sm-2 no-padding">
                            <ul class="category-menu">
                                @for($i=0; $i<count($category_names); $i++)
                                <li class="{{ $category_names[$i] }}" id="category-menu-{{ str_replace(' ', '', $category_names[$i]) }}"><a href="#">{{ $category_names[$i] }}</a></li>
                                @endfor
                            </ul>
                        </div>
                        <div class="col-sm-10 no-padding">
                            @foreach($menu_category_id_uniques as $menu_category_name => $menu_category_id)
                            <div class="menu_category-container hide" id="container-category-menu-{{ str_replace(' ', '', $menu_category_name) }}">
                                <h4>{{ $menu_category_name }}</h4>
                                @foreach($restaurant_management->menus->where('menu_category_id', $menu_category_id) as $menu)
                                <div class="menu-detail">
                                    <div class="col-sm-6 col-lg-7 no-padding">
                                        <h4 class="menu-title">{{ $menu->item_name }}</h4>
                                        <p>{{ $menu->item_description }}</p>
                                    </div>
                                    <div class="col-xs-6 col-sm-2 no-padding">
                                        <h4 class="price-value" value="{{ $restaurant_management->removeDoubleZero($menu->item_price) }}">Rp. {{ number_format($restaurant_management->removeDoubleZero($menu->item_price),0,'.',',') }}</h4>
                                    </div>
                                    <div class="col-xs-6 col-sm-4 col-lg-3 no-padding">
                                        {!! Form::text('menu_select', $value = '0', $attributes = ['class' => 'form-control', 'disabled']); !!} 
                                        <button class="btn btn-add"><img src="/images/add.png" alt="add" class="add-icon"></button>
                                        <button class="btn btn-subtract"><img src="/images/subtract.png" class="subtract-icon" alt="subtract"></button>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                @endforeach
                            </div>
                            @endforeach
                        </div>
                        @if(isset($edit_item_names) && isset($edit_item_counts))
                        <div class="edit_menu_hidden hide">
                            @for($i=0; $i<count($edit_item_names); $i++)
                                <span class="edit_item_name_hidden hide">{{ $edit_item_names[$i] }}</span>
                                <span class="edit_item_count_hidden hide">{{ $edit_item_counts[$i] }}</span>
                            @endfor                    
                        </div>
                        @endif       
                        <div class="clearfix"></div>
                    </div>
                    <div class="photo-container hide">
                        <h3>Photos</h3>
                        <div class="divider"></div>
                        <div class="photo-detail-container">
                            @foreach ($restaurant_management->restaurant_food_photos as $restaurant_food_photo)
                            <div class="thumbnail-photo">
                                <a href="#" class="food-photo" food-photo-name="{{ $restaurant_food_photo->food_photo_name }}" food-photo-url="{{ $restaurant_food_photo->food_photo_url }}" food-photo-id="{{ $restaurant_food_photo->id }}">
                                    <img src="/images/restaurant_uploads/food_photos/{{ $restaurant_food_photo->food_photo_url }}" alt="{{ $restaurant_food_photo->food_photo_name }}" class="img-responsive" title="{{ $restaurant_food_photo->food_photo_name }}"/>
                                </a>
                            </div>                            
                            @endforeach
                            <div class="clearfix"></div>
                        </div>         
                    </div>
                    <div class="review-container hide">
                        <div class="review-title">
                            <h3>Reviews</h3>
                            <div class="total-reviews">
                                <h2>{{ $total_ratings_average }}</h2>
                                <div class="total-stars">
                                @for($i=0; $i<$total_ratings_average; $i++)
                                    <img src="/images/big_star.png" alt="big_star">
                                @endfor
                                @for($i=0; $i<(5 - $total_ratings_average); $i++)
                                    <img src="/images/big_star-black.png" alt="big_star-black">
                                @endfor                                    
                                </div>
                                <small>dari {{ $total_users_rate }} reviews</small>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="divider"></div>
                        <div class="user_reviews-container">
                            @if(count($reservations_rated)) 
                            @foreach($reservations_rated as $reservation_rated)
                            @if($reservation_rated->review_rating) 
                            <div class="user_reviews">
                                <div class="col-xs-2 col-sm-2 col-lg-1 no-padding">
                                    <a href="/profile/show/{{ $reservation_rated->user->id }}"><img src="/images/uploads/{{ $reservation_rated->user->photo_url }}" alt="profile-picture-1" class="img-circle img-profile-user-rate"></a>
                                </div>
                                <div class="col-xs-10 col-sm-10 col-lg-11 no-padding">
                                    <div class="user_info-reviews">
                                        <h4 class="user_name">
                                            <a href="/profile/show/{{ $reservation_rated->user->id }}">{{ $reservation_rated->user->name }}</a>
                                        </h4>
                                        <h4 class="comment_time">{{ $reservation_rated->review_rating->created_at->diffForHumans() }}</h4>
                                        <div class="clearfix"></div>
                                        <h4 class="stars-number">{{ $reservation_rated->review_rating->rating }}</h4>
                                        <div class="total-stars-user">
                                        @for($i=0; $i<$reservation_rated->review_rating->rating; $i++)
                                            <img src="/images/star-yellow.png" alt="star-yellow">
                                        @endfor
                                        @for($i=0; $i<(5 - $reservation_rated->review_rating->rating); $i++)
                                            <img src="/images/star-black.png" alt="star-black">
                                        @endfor
                                        </div>
                                        @if($reservation_rated->review_comment)
                                        <h4 class="comment_content">{{ $reservation_rated->review_comment->comment }}</h4>
                                        @endif
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div> 
                            @endif
                            @endforeach
                            @else
                            <h4 class="info_not_yet_reviewed">Belum ada review</h4>
                            @endif
                            <div class="review-pagination">
                                {!! $reservations_rated->render() !!}
                            </div>
                            <div class="clearfix"></div>
                        </div>                     
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="order-container">
                        <div class="order-title">
                            <img src="/images/order.png" alt="order"/>
                            <h3 class="text-uppercase">Orderan anda</h3>
                        </div>
                        <div class="order-detail">
                            <div class="col-xs-6 col-md-5 col-lg-6 no-padding">
                                <h5>Menu</h5>
                            </div>
                            <div class="col-xs-2 col-md-2 col-lg-2 no-padding">
                                <h5 class="text-center">@</h5>
                            </div>
                            <div class="col-xs-4 col-md-5 col-lg-4 no-padding">
                                <h5 class="price_no_border">Harga (Rp.)</h5>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        {!! Form::open(['action' => 'ReservationController@postIndex', 'class' => 'form-reservation']) !!}
                        <div class="order-content">
                            <!-- content is filled dynamically -->
                        </div>
                        <div class="total-prices">
                            <h4 class="info-prices">Total Harga</h4>
                            <h4 class="prices" value="0">
                                <!-- content is filled dynamically -->
                                Rp. 0
                            </h4>
                            <div class="clearfix"></div>
                        </div>
                        <input type="hidden" name="restaurant_management_hidden_id" value="{{ $restaurant_management->id }}">
                        @if (Auth::user('user'))
                        <input type="hidden" name="user_hidden_id" id="hidden_user_id_reserve" value="{{ Auth::user('user')->id }}">
                        @endif
                        @if(isset($reservation))
                        <input type="hidden" name="reservation_id" value="{{ $reservation->id }}">                   
                        @endif
                        <button type="button" class="btn btn-reserve text-uppercase">Reservasi</button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END: SECTION-RESTAURANT_PROFILE -->
    @include('user.layouts.footer-info')
    @include('user.layouts.footer')
@stop