@extends('admin.layouts.master')

@section('title', 'Flux User Balance')

@section('content')

@include('admin.modals.fill_balance');
@include('admin.layouts.navigation')
<section class="wrapper">
	@include('admin.layouts.sidebar')
	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main-general">
		<div class="main-content">
			<div class="container-fluid">
				<div class="flash-manage">
					@include('partials.message')
					@include('partials.error')
				</div>				
				<div class="flux-balance-container add-margin-top">
					<div class="flux-header-container">
						<div class="col-md-12 col-lg-6 left-side">
							<h3><img src="/images/admin/flux-black.png" alt="flux-balance" class="img-fill-black">Flux User Balance</h3>
							<div class="button-group">
								{!! Form::open(['class' => 'form-put-flux-user-balance', 'action' => 'AdminController@putFluxUserBalance', 'method' => 'PUT']) !!}
									<input type="hidden" name="history_balance_id" value="" class="history_balance_id_hidden">
									<button type="button" class="btn btn-flux" disabled><img src="/images/admin/flux.png" alt="flux" class="img-flux">Flux</button>
								{!! Form::close() !!}
							</div>
		               		<div class="clearfix"></div>
						</div>
						<div class="col-md-12 col-lg-6 right-side">
							<h4>Urutkan: </h4>
							{!! Form::open(['action' => 'AdminController@getSortHistoryBalance', 'method' => 'GET', 'class' => 'form-sort-restaurant-management-database']) !!}
                            	{!! Form::select('sort_history_balance', array(
                                        'all' => 'Semua',
                            			'id' => 'ID History Balance',
                            			'process_date' => 'Tanggal Permintaan',
                            			'balance_processed' => 'Jumlah Dana',
                                        'user_id' => 'ID User',
                                        'transfer_target' => 'Transfer Ke',
                                        ), null, $attributes = ['class' => 'form-control sort-form-control sort-history-balance']); !!}
                            {!! Form::close() !!}
		                	{!! Form::open(['action' => 'AdminController@getSearchHistoryBalance', 'class' => 'form-search-database', 'method' => 'GET']) !!}
                            	{!! Form::text('search', null, $attributes = ['class' => 'form-control search-form-control', 'placeholder' => 'Cari']); !!}
		               		{!! Form::close() !!} 
	               			<div class="clearfix"></div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="flux-body-container">
						<table class="table table-flux-user-balance">
							<thead>
								<tr>
									<th class="text-center">ID History Balance</th>
									<th class="text-center">Tanggal Permintaan</th>
									<th class="text-center">Jumlah Dana</th>
									<th class="text-center">ID User</th>
									<th class="text-center">Transfer Ke</th>
								</tr>
							</thead>
							<tbody>
								@if(isset($history_balances_search))
								@foreach($history_balances_search as $history_balance_search)
								<tr>
									<td class="text-center">{{ $history_balance_search->id }}</td>
									<td class="text-center">{{ $history_balance_search->getOnlyDate($history_balance_search->process_date) }}</td>
									<td class="text-center">Rp. {{ number_format($history_balance_search->balance_processed,0,'.',',') }}</td>
									<td class="text-center">{{ $history_balance_search->user_id }}</td>
									<td class="text-center">{{ $history_balance_search->transfer_target }}</td>
								</tr>
								@endforeach								
								@else
								@foreach($history_balances as $history_balance)
								<tr>
									<td class="text-center">{{ $history_balance->id }}</td>
									<td class="text-center">{{ $history_balance->getOnlyDate($history_balance->process_date) }}</td>
									<td class="text-center">Rp. {{ number_format($history_balance->balance_processed,0,'.',',') }}</td>
									<td class="text-center">{{ $history_balance->user_id }}</td>
									<td class="text-center">{{ $history_balance->transfer_target }}</td>
								</tr>
								@endforeach
								@endif
							</tbody>
						</table>
					</div>
					<div class="flux-footer-container">
						<div class="col-sm-offset-6 col-sm-6 no-padding">
				            <div class="flux-balance-pagination">
								@if(isset($history_balances_search))
		                        {!! $history_balances_search->appends(Request::only(['search'=>'search']))->render() !!}
		                        @else
		                        {!! $history_balances->appends(Request::only(['sort_history_balance'=>'sort_history_balance']))->render() !!}

		                        @endif
				            </div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="text-center info-flux-unshow">
						<h2>:(</h2>
						<h3>Minimal lebar layout yang dibutuhkan untuk melihat halaman ini adalah 992px atau ke atasnya.
						</h3>
					</div>
				</div>
			</div>
		</div>		
	</div>
	@include('admin.layouts.footer')
</section>
@stop