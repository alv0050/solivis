<div class="modal fade" id="fillBalanceModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
                <h4 class="modal-title text-uppercase">Isi Saldo</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    {!! Form::open(['action' => 'AdminController@putFillBalance', 'class' => 'form-fill-balance', 'method' => 'PUT']) !!}
                        <div class="form-group">
                            <label for="name" class="col-sm-4">Nama</label>
                            <div class="col-sm-8">
                                {!! Form::text('name', $value = null, $attributes = ['class' => 'form-control default-grey', 'disabled', 'id' => 'user_name_fill_balance']); !!}
                            </div>
                            <div class="clearfix"></div>
                        </div>                    
                         <div class="form-group">
                            <label for="balance_processed" class="col-sm-4">Jumlah Saldo</label>
                            <div class="col-sm-8">
                                {!! Form::text('balance_processed', $value = null, $attributes = ['class' => 'form-control default-grey']); !!}
                            </div>
                            <div class="clearfix"></div>
                        </div>                         
                        <div class="form-group">
                            <label for="reason" class="col-sm-4">Alasan</label>
                            <div class="col-sm-8">
                                {!! Form::select('reason_choice', array(
                                        '2' => 'Kelebihan transfer (dana dikembalikan)',
                                        '5' => 'Reservasi ditolak (dana dikembalikan)'                                      
                                    ), null, $attributes = ['class' => 'form-control default-grey']); !!}
                            </div>
                            <div class="clearfix"></div>
                        </div>                       
                        <div class="clearfix"></div>
                        <input type="hidden" name="user_id" value="" class="user_id_hidden_fill_balance">
                        <button type="submit" class="btn text-center text-uppercase btn-fill_balance">Submit</button>
                        <div class="clearfix"></div>
                    {!! Form::close() !!}       
                </div>
            </div>
        </div>
    </div>
</div>  
