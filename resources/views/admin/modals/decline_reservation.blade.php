<div class="modal fade" id="declineReservationModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
                <h4 class="modal-title text-uppercase">Penolakan Reservasi</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    {!! Form::open(['action' => 'AdminController@postDeclineReservation', 'class' => 'form-decline-reservation']) !!}
                        <div class="form-group">
                            <label for="reservation_id" class="col-sm-4">ID Reservasi</label>
                            <div class="col-sm-8">
                                {!! Form::text('id', $value = null, $attributes = ['class' => 'form-control default-grey', 'id' => 'reservation_id_decline', 'disabled']); !!}
                            </div>
                            <div class="clearfix"></div>
                        </div>                    
                         <div class="form-group">
                            <label for="reason" class="col-sm-4">Alasan Penolakan</label>
                            <div class="col-sm-8">
                            {!! Form::textarea('reason', $value = null, $attributes = ['class' => 'form-control default-grey', 'rows' => '5']); !!}
                            </div>
                            <div class="clearfix"></div>
                        </div>                       
                        <div class="clearfix"></div>
                        <input type="hidden" name="reservation_id" id="reservation_id_decline_hidden">
                        <button type="submit" class="btn text-center text-uppercase btn-decline_reservation">Submit</button>
                        <div class="clearfix"></div>
                    {!! Form::close() !!}       
                </div>
            </div>
        </div>
    </div>
</div>  
