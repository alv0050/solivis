@extends('admin.layouts.master')

@section('title', 'Login Admin')

@section('content')
	<section class="section-login">
		<div class="login">
	        <a href="/"><img src="/images/admin/logo-big.png" alt="logo-big"/></a>
        	<h3 class="text-uppercase text-center">Login as administrator</h3>
            @include('partials.message') 
            @include('partials.error') 
			{!! Form::open(['class' => 'form-login']) !!}	
				<div class="form-group">
                    <img src="/images/admin/email-black.png" alt="email-black">
                    {!! Form::text('username', $value = null, $attributes = ['class' => 'form-control default', 'placeholder' => 'Username']); !!}
                </div>			
				<div class="form-group">
                    <img src="/images/admin/password-auth.png" alt="password-auth">
                    {!! Form::password('password', $array = ['class' => 'form-control default', 'placeholder' => 'Password']); !!}
                </div>			
                <button type="submit" class="btn text-center btn-login">Login</button>
            {!! Form::close() !!}   
	    </div>
	</section>
@stop