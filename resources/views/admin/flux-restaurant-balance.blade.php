@extends('admin.layouts.master')

@section('title', 'Flux Restaurant Balance')

@section('content')

@include('admin.modals.fill_balance');
@include('admin.layouts.navigation')
<section class="wrapper">
	@include('admin.layouts.sidebar')
	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main-general">
		<div class="main-content">
			<div class="container-fluid">
				<div class="flash-manage">
					@include('partials.message')
					@include('partials.error')
				</div>				
				<div class="flux-balance-container add-margin-top">
					<div class="flux-header-container">
						<div class="col-md-12 col-lg-6 left-side">
							<h3><img src="/images/admin/flux-black.png" alt="flux-balance" class="img-fill-black">Flux Restaurant Balance</h3>
							<div class="button-group">
								{!! Form::open(['class' => 'form-put-flux-restaurant-balance', 'action' => 'AdminController@putFluxRestaurantBalance', 'method' => 'PUT']) !!}
									<input type="hidden" name="restaurant_history_balance_id" value="" class="restaurant_history_balance_id_hidden">
									<button type="button" class="btn btn-flux" disabled><img src="/images/admin/flux.png" alt="flux" class="img-flux">Flux</button>
								{!! Form::close() !!}
							</div>
		               		<div class="clearfix"></div>
						</div>
						<div class="col-md-12 col-lg-6 right-side">
							<h4>Urutkan: </h4>
							{!! Form::open(['action' => 'AdminController@getSortRestaurantHistoryBalance', 'method' => 'GET', 'class' => 'form-sort-restaurant-management-database']) !!}
                            	{!! Form::select('sort_restaurant_history_balance', array(
                                        'all' => 'Semua',
                            			'id' => 'ID History Balance',
                            			'process_date' => 'Tanggal Permintaan',
                            			'balance_processed' => 'Jumlah Dana',
                                        'restaurant_management_id' => 'ID Restaurant',
                                        'transfer_target' => 'Transfer Ke',
                                        ), null, $attributes = ['class' => 'form-control sort-form-control sort-restaurant-management-database']); !!}
                            {!! Form::close() !!}
		                	{!! Form::open(['action' => 'AdminController@getSearchRestaurantHistoryBalance', 'class' => 'form-search-database', 'method' => 'GET']) !!}
                            	{!! Form::text('search', null, $attributes = ['class' => 'form-control search-form-control', 'placeholder' => 'Cari']); !!}
		               		{!! Form::close() !!} 
	               			<div class="clearfix"></div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="flux-body-container">
						<table class="table table-flux-restaurant-balance">
							<thead>
								<tr>
									<th class="text-center">ID History Balance</th>
									<th class="text-center">Tanggal Permintaan</th>
									<th class="text-center">Jumlah Dana</th>
									<th class="text-center">ID Restaurant</th>
									<th class="text-center">Transfer Ke</th>
								</tr>
							</thead>
							<tbody>
								@if(isset($restaurant_history_balances_search))
								@foreach($restaurant_history_balances_search as $restaurant_history_balance_search)
								<tr>
									<td class="text-center">{{ $restaurant_history_balance_search->id }}</td>
									<td class="text-center">{{ $restaurant_history_balance_search->getOnlyDate($restaurant_history_balance_search->process_date) }}</td>
									<td class="text-center">Rp. {{ number_format($restaurant_history_balance_search->balance_processed,0,'.',',') }}</td>
									<td class="text-center">{{ $restaurant_history_balance_search->restaurant_management_id }}</td>
									<td class="text-center">{{ $restaurant_history_balance_search->transfer_target }}</td>
								</tr>
								@endforeach								
								@else
								@foreach($restaurant_history_balances as $restaurant_history_balance)
								<tr>
									<td class="text-center">{{ $restaurant_history_balance->id }}</td>
									<td class="text-center">{{ $restaurant_history_balance->getOnlyDate($restaurant_history_balance->process_date) }}</td>
									<td class="text-center">Rp. {{ number_format($restaurant_history_balance->balance_processed,0,'.',',') }}</td>
									<td class="text-center">{{ $restaurant_history_balance->restaurant_management_id }}</td>
									<td class="text-center">{{ $restaurant_history_balance->transfer_target }}</td>
								</tr>
								@endforeach
								@endif
							</tbody>
						</table>
					</div>
					<div class="flux-footer-container">
						<div class="col-sm-offset-6 col-sm-6 no-padding">
				            <div class="flux-balance-pagination">
								@if(isset($restaurant_history_balances_search))
		                        {!! $restaurant_history_balances_search->appends(Request::only(['search'=>'search']))->render() !!}
		                        @else
		                        {!! $restaurant_history_balances->appends(Request::only(['sort_restaurant_history_balance'=>'sort_restaurant_history_balance']))->render() !!}
		                        @endif				            
		                    </div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="text-center info-flux-unshow">
						<h2>:(</h2>
						<h3>Minimal lebar layout yang dibutuhkan untuk melihat halaman ini adalah 992px atau ke atasnya.
						</h3>
					</div>
				</div>
			</div>
		</div>		
	</div>
	@include('admin.layouts.footer')
</section>
@stop