@extends('admin.layouts.master')

@section('title', 'Accepted Reservation')

@section('content')

@include('admin.modals.add_restaurant');
@include('admin.layouts.navigation')
<section class="wrapper">
	@include('admin.layouts.sidebar')
	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main-general">
		<div class="main-content">
			<div class="container-fluid">
				<div class="col-md-6 left-container">
					<div class="notification-container add-margin-top">
						<h3><img src="/images/admin/notification.png" alt="notification">Accepted Reservations</h3>
						<div class="separator"></div>
						<div class="notification-group-container margin-adapt2">
							@foreach($reservations as $reservation)
							<div class="reservation">
								<div class="reservation-detail">
									<img src="/images/admin/accept.png" alt="accept" class="new-reserve">
									<h5>Reservation ID : {{ $reservation->id }}</h5>
									<h5>User : {{ $reservation->user->name }}</h5>
									<p>{{ $reservation->convertFullReservationDate() }} - {{ $reservation->convertTime() }}</p>
								</div>
								<button class="btn btn-detail" value="">Detail <img src="/images/admin/next.png" alt="next">
	                            <span class="reservation-id-detail hide">{{ $reservation->id }}</span>
	                            <span class="reservation-user-name-detail hide">{{ $reservation->user->name }}</span>
	                            <span class="reservation-restaurant-name-detail hide">{{ $reservation->restaurant_management->name }}</span>
	                            <span class="reservation-date-detail hide">{{ $reservation->convertReservationDate() }}</span>
	                            <span class="reservation-time-detail hide">{{ $reservation->convertTime() }}</span>
	                            <span class="reservation-paxes-detail hide">{{ $reservation->paxes }}</span>
	                            <span class="reservation-total-price-detail hide">{{ number_format($reservation->getTotalReservationPrice(),0,'.',',') }}</span>
	                            <span class="reservation-method-payment-detail hide">{{ $reservation->method_payment->method }}</span>
	                            <span class="reservation-qr-code-plain-detail hide">{{ $reservation->qr_code_plain->code }}</span>
								<span class="reservation-qr-code-detail hide">
	                            	{!! QrCode::size(150)->generate($reservation->qr_code_plain->code); !!}
	                            </span>	                            
								</button>
								<div class="clearfix"></div>
							</div>
							@endforeach
						</div>
					</div>
				</div>
				<div class="col-md-6 right-container">
					<div class="detail-reservation-container detail-reservation add-margin-top">
						<h3><img src="/images/admin/detail.png" alt="detail">Detail Reservation</h3>
						<div class="separator"></div>
						<div class="detail-reservation-group-container">
							<div class="form-group">
								<label class="info-detail">Reservation ID</label>
								<label class="result-detail">
									<span>:</span> 
									<span class="reservation_id_detail">{{-- content is filled dynamically --}}</span>
								</label>
							</div>
							<div class="form-group">
								<label class="info-detail">User</label>
								<label class="result-detail">
									<span>:</span> 
									<span class="reservation_user_name_detail">{{-- content is filled dynamically --}}</span>
								</label>
							</div>
							<div class="form-group">
								<label class="info-detail">Restoran</label>
								<label class="result-detail">
									<span>:</span> 
									<span class="reservation_restaurant_name_detail">{{-- content is filled dynamically --}}</span>
								</label>
							</div>
							<div class="form-group">
								<label class="info-detail">Tanggal Res.</label>
								<label class="result-detail">
									<span>:</span> 
									<span class="reservation_date_detail">{{-- content is filled dynamically --}}</span>	
								</label>
							</div>
							<div class="form-group">
								<label class="info-detail">Jam</label>
								<label class="result-detail">
									<span>:</span> 
									<span class="reservation_time_detail">{{-- content is filled dynamically --}}</span>
								</label>
							</div>
							<div class="form-group">
								<label class="info-detail">Pax</label>
								<label class="result-detail">
									<span>:</span> 
									<span class="reservation_paxes_detail">{{-- content is filled dynamically --}}</span>
								</label>
							</div>
							<div class="form-group">
								<label class="info-detail">Pembayaran</label>
								<label class="result-detail">
									<span>:</span>  
									<span class="reservation_total_price_detail">{{-- content is filled dynamically --}}</span>
								</label>
							</div>
							<div class="form-group">
								<label class="info-detail">Pembayaran via</label>
								<label class="result-detail">
									<span>:</span>  
									<span class="reservation_method_payment_detail">{{-- content is filled dynamically --}}</span>
								</label>
							</div>
							<div class="qrcode-container">
								<div class="thumbnail no-padding">
									{{-- content is filled dynamically --}}
								</div>
								<h5>{{-- content is filled dynamically --}}</h5>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>		
	</div>
	@include('admin.layouts.footer')
</section>
@stop