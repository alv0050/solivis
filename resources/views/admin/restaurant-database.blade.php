@extends('admin.layouts.master')

@section('title', 'Restaurant Database')

@section('content')

@include('admin.modals.add_restaurant');
@include('admin.layouts.navigation')
<section class="wrapper">
	@include('admin.layouts.sidebar')
	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main-general">
		<div class="main-content">
			<div class="container-fluid">
				<div class="flash-manage">
					@include('partials.message')
					@include('partials.error')
				</div>				
				<div class="report-database-container add-margin-top">
					<div class="report-header-container">
						<div class="col-md-12 col-lg-6 left-side">
							<h3><img src="/images/admin/database-restaurant.png" alt="database-restaurant">Database</h3>
							<div class="button-group">
								{!! Form::open(['class' => 'form-accept-restaurant-management', 'action' => 'AdminController@putAcceptRestaurantManagement', 'method' => 'PUT']) !!}
									<input type="hidden" name="restaurant_management_id" class="restaurant_management_id_accepted_hidden">
									<button type="button" class="btn btn-accept" id="btn-accept-restaurant-management" disabled>Accept</button>
								{!! Form::close() !!}								
								{!! Form::open(['class' => 'form-delete-restaurant-management', 'action' => 'AdminController@deleteRestaurantManagement', 'method' => 'DELETE']) !!}
									<input type="hidden" name="restaurant_management_id" value="" class="restaurant_management_id_hidden">
									<button type="button" class="btn btn-delete" disabled><img src="/images/admin/delete.png" alt="delete">Delete</button>
								{!! Form::close() !!}
							</div>
		               		<div class="clearfix"></div>
						</div>
						<div class="col-md-12 col-lg-6 right-side">
							<h4>Urutkan: </h4>
							{!! Form::open(['action' => 'AdminController@getSortRestaurantManagementDatabase', 'method' => 'GET', 'class' => 'form-sort-restaurant-management-database']) !!}
                            	{!! Form::select('sort_restaurant_management', array(
                                        'all' => 'Semua',
                            			'id' => 'ID',
                            			'name' => 'Nama',
                                        'address' => 'Alamat',
                                        'total_reservation' => 'Banyak Reservasi',
                                        'status' => 'Status',
                                        'last_login' => 'Terakhir Login',
                                        ), null, $attributes = ['class' => 'form-control sort-form-control sort-restaurant-management-database']); !!}
                            {!! Form::close() !!}
		                	{!! Form::open(['action' => 'AdminController@getSearchRestaurantManagementDatabase', 'class' => 'form-search-database', 'method' => 'GET']) !!}
                            	{!! Form::text('search', null, $attributes = ['class' => 'form-control search-form-control', 'placeholder' => 'Cari']); !!}
		               		{!! Form::close() !!} 
	               			<div class="clearfix"></div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="report-body-container">
						<table class="table table-restaurant-database-report">
							<thead>
								<tr>
									<th>ID</th>
									<th>Nama Restoran</th>
									<th class="text-center">Email</th>
									<th class="text-center">Alamat Restoran</th>
									<th class="text-center">Byk Res.</th>
									<th class="text-center">Status</th>
									<th class="text-center">Terakhir Login</th>
								</tr>
							</thead>
							<tbody>
								@if(isset($restaurant_managements_search))
								@foreach($restaurant_managements_search as $restaurant_management_search)
								<tr>
									<td>{{ $restaurant_management_search->id }}</td>
									<td>{{ $restaurant_management_search->name }}</td>
									<td class="text-center">{{ $restaurant_management_search->email }}</td>
									<td class="text-center">{{ $restaurant_management_search->address }}</td>
									<td class="text-center">{{ count($restaurant_management_search->reservations->where('status_reservation_id', 4)) }}
									@if($restaurant_management_search->status == 0)
									<td class="text-center">Guest</td>
									@else
									<td class="text-center">Member</td>
									@endif
									<td class="text-center">{{ $restaurant_management_search->getLastLogin($restaurant_management_search->last_login)->diffForHumans() }}</td>
								</tr>
								@endforeach
								@else
								@foreach($restaurant_managements as $restaurant_management)
								<tr>
									<td>{{ $restaurant_management->id }}</td>
									<td>{{ $restaurant_management->name }}</td>
									<td class="text-center">{{ $restaurant_management->email }}</td>
									<td class="text-center">{{ $restaurant_management->address }}</td>
									<td class="text-center">{{ count($restaurant_management->reservations->where('status_reservation_id', 4)) }}
									@if($restaurant_management->status == 0)
									<td class="text-center">Guest</td>
									@else
									<td class="text-center">Member</td>
									@endif
									<td class="text-center">{{ $restaurant_management->getLastLogin($restaurant_management->last_login)->diffForHumans() }}</td>
								</tr>
								@endforeach
								@endif
							</tbody>
						</table>
					</div>
					<div class="report-footer-container">
						<div class="col-sm-offset-6 col-sm-6 no-padding">
				            <div class="restaurant-database-report-pagination">
				            	@if(isset($restaurant_managements_search))
				            	{!! $restaurant_managements_search->appends(Request::only(['search'=>'search']))->render() !!}
								@else				            	
		                        {!! $restaurant_managements->appends(Request::only(['sort_restaurant_management'=>'sort_restaurant_management']))->render() !!}
		                        @endif
				            </div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="text-center info-report-unshow">
						<h2>:(</h2>
						<h3>Minimal lebar layout yang dibutuhkan untuk menampilkan laporan user database adalah 992px atau ke atasnya.
						</h3>
					</div>
				</div>
			</div>
		</div>		
	</div>
	@include('admin.layouts.footer')
</section>
@stop