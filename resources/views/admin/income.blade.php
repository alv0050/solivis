@extends('admin.layouts.master')

@section('title', 'Income')

@section('content')

@include('admin.modals.fill_balance');
@include('admin.layouts.navigation')
<section class="wrapper">
	@include('admin.layouts.sidebar')
	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main-general">
		<div class="main-content">
			<div class="container-fluid">
				<div class="flash-manage">
					@include('partials.message')
					@include('partials.error')
				</div>				
				<div class="income-container add-margin-top">
					<div class="income-header-container">
						<div class="col-md-12 col-lg-6 left-side">
							<h3><img src="/images/admin/income-black.png" alt="income" class="img-income-black">Income</h3>
		               		<div class="clearfix"></div>
						</div>
						<div class="col-md-12 col-lg-6 right-side">
							<h4>Urutkan: </h4>
							{!! Form::open(['action' => 'AdminController@getSortReservation', 'method' => 'GET', 'class' => 'form-sort-restaurant-management-database']) !!}
                            	{!! Form::select('sort_reservation', array(
                                        'all' => 'Semua',
                            			'id' => 'ID Reservasi',
                                        'reservation_date' => 'Tgl Reservasi',
                                        'status' => 'Status Penggunaan',
                                        'payment' => 'Total Transaksi',
                                        ), null, $attributes = ['class' => 'form-control sort-form-control sort-reservation']); !!}
                            {!! Form::close() !!}
		                	{!! Form::open(['action' => 'AdminController@getSearchReservation', 'class' => 'form-search-database', 'method' => 'GET']) !!}
                            	{!! Form::text('search', null, $attributes = ['class' => 'form-control search-form-control', 'placeholder' => 'Cari']); !!}
		               		{!! Form::close() !!} 
	               			<div class="clearfix"></div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="income-body-container">
						<table class="table table-income">
							<thead>
								<tr>
									<th class="text-center">ID Reservasi</th>
									<th class="text-center">Tanggal Reservasi</th>
									<th class="text-center">Status Penggunaan</th>
									<th class="text-center">Total Transaksi</th>
									<th class="text-center">Pendapatan</th>
								</tr>
							</thead>
							<tbody>
								@if(isset($reservations_search))
								@foreach($reservations_search as $reservation_search)
								<tr>
									<td class="text-center">{{ $reservation_search->id }}</td>
									<td class="text-center">{{ $reservation_search->convertReservationDate() }}</td>
									@if($reservation_search->qr_code_plain->status_processed == '0')
									<td class="text-center">Belum</td>
									@else
									<td class="text-center">Sudah</td>
									@endif
									<td class="text-center">Rp. {{ number_format($reservation_search->getTotalReservationPrice(),0,'.',',') }}</td>
									<td class="text-center">Rp. {{ number_format($reservation_search->getAdminIncome(),0,'.',',') }}</td>
								</tr>
								@endforeach
								@else
								@foreach($reservations as $reservation)
								<tr>
									<td class="text-center">{{ $reservation->id }}</td>
									<td class="text-center">{{ $reservation->convertReservationDate() }}</td>
									@if($reservation->qr_code_plain->status_processed == '0')
									<td class="text-center">Belum</td>
									@else
									<td class="text-center">Sudah</td>
									@endif
									<td class="text-center">Rp. {{ number_format($reservation->getTotalReservationPrice(),0,'.',',') }}</td>
									<td class="text-center">Rp. {{ number_format($reservation->getAdminIncome(),0,'.',',') }}</td>
								</tr>
								@endforeach
								@endif
							</tbody>
						</table>
					</div>
					<div class="income-footer-container">
						<div class="col-sm-6 no-padding">
							<h4 class="text-uppercase">Total pendapatan</h4>
							<h2>Rp. {{ number_format($admin_income,0,'.',',') }}</h2>
						</div>						
						<div class="col-sm-6 no-padding">
				            <div class="income-pagination">
				            	@if(isset($reservations_search))
				            	{!! $reservations_search->appends(Request::only(['search'=>'search']))->render() !!}
								@else				            	
		                        {!! $reservations->appends(Request::only(['sort_reservation'=>'sort_reservation']))->render() !!}
		                        @endif
		                    </div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="text-center info-income-unshow">
						<h2>:(</h2>
						<h3>Minimal lebar layout yang dibutuhkan untuk melihat halaman ini adalah 992px atau ke atasnya.
						</h3>
					</div>
				</div>
			</div>
		</div>		
	</div>
	@include('admin.layouts.footer')
</section>
@stop