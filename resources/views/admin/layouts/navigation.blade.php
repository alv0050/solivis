<!-- NAVBAR -->
<nav class="navbar navbar-fixed-top navbar-admin">
	<div class="container-fluid no-padding">
  		<div class="col-sm-3 col-md-2 left-side">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">
					<img src="/images/admin/logo.png" alt="logo"/>
				</a>
			</div>      			
		</div>
  		<div class="col-sm-9 col-md-10 right-side">
	  		<div class="container-fluid">
				<div id="navbar" class="navbar-collapse collapse">
					<h4>{{ $section_title }}</h4>
					<ul class="nav navbar-nav navbar-right top-nav">
						<li class="login-info"><span>Login as</span><span class="admin-username">{{ Auth::user('admin')->username }}</span></li>
						<li>|</li>
						<li><a href="/admin/logout">Log out</a></li>
					</ul>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</nav>
<!-- END: NAVBAR -->
