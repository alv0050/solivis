<!-- SIDEBAR -->
<div class="col-sm-3 col-md-2 sidebar">
	<ul class="nav nav-sidebar">
		<li {{{ (Request::is('admin/accepted-reservation') ? 'class=active' : '') }}}>
			<a href="/admin/accepted-reservation" class="_accepted-reservation">
				<img src="/images/admin/generate.png" alt="accepted">Reservation 
				<span class="sr-only">(current)</span>
			</a>
		</li>
		<li {{{ (Request::is('admin/user-database') ? 'class=active' : '') }}}>
			<a href="/admin/user-database" class="_user">
				<img src="/images/admin/user.png" alt="user">User
			</a>
		</li>
		<li {{{ (Request::is('admin/flux-user-balance') ? 'class=active' : '') }}}>
			<a href="/admin/flux-user-balance" class="_flux-user-balance">
				<img src="/images/admin/flux.png" alt="flux" class="img-flux-sidebar">User Balance
			</a>
		</li>
		<li {{{ (Request::is('admin/restaurant-management-database') ? 'class=active' : '') }}}>
			<a href="/admin/restaurant-management-database" class="_restaurant">
				<img src="/images/admin/restaurant.png" alt="restaurant" class="img-restaurant-sidebar">Restaurant
			</a>
		</li>
		<li {{{ (Request::is('admin/flux-restaurant-balance') ? 'class=active' : '') }}}>
			<a href="/admin/flux-restaurant-balance" class="_flux-restaurant-balance">
				<img src="/images/admin/flux.png" alt="flux" class="img-flux-sidebar">Restaurant Balance
			</a>
		</li>
		<li {{{ (Request::is('admin/verify-payment') ? 'class=active' : '') }}}>
			<a href="/admin/verify-payment" class="_verify-payment">
				<img src="/images/admin/verify.png" alt="verify" class="img-verify-sidebar">Verify Payment
			</a>
		</li>
		<li {{{ (Request::is('admin/income') ? 'class=active' : '') }}}>
			<a href="/admin/income" class="_income">
				<img src="/images/admin/income.png" alt="income" class="img-income-sidebar">Income
			</a>
		</li>
	</ul>
</div>
<!-- END: SIDEBAR -->
