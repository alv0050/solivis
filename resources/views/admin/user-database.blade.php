@extends('admin.layouts.master')

@section('title', 'User Database')

@section('content')

@include('admin.modals.add_restaurant');
@include('admin.modals.fill_balance');
@include('admin.layouts.navigation')
<section class="wrapper">
	@include('admin.layouts.sidebar')
	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main-general">
		<div class="main-content">
			<div class="container-fluid">
				<div class="flash-manage">
					@include('partials.message')
					@include('partials.error')
				</div>				
				<div class="report-database-container add-margin-top">
					<div class="report-header-container">
						<div class="col-md-12 col-lg-6 left-side">
							<h3><img src="/images/admin/database.png" alt="database">Database</h3>
							<div class="button-group">
								<button class="btn btn-fill" id="btn-fill-balance" disabled><img src="/images/admin/fill.png" alt="fill" class="img-fill">Fill Balance</button>
								{!! Form::open(['class' => 'form-delete-user', 'action' => 'AdminController@deleteUser', 'method' => 'DELETE']) !!}
									<input type="hidden" name="user_id" value="" class="user_id_hidden">
									<button type="button" class="btn btn-delete" disabled><img src="/images/admin/delete.png" alt="delete">Delete</button>
								{!! Form::close() !!}
							</div>
		               		<div class="clearfix"></div>
						</div>
						<div class="col-md-12 col-lg-6 right-side">
							<h4>Urutkan: </h4>
							{!! Form::open(['action' => 'AdminController@getSortUserDatabase', 'method' => 'GET', 'class' => 'form-sort-user-database']) !!}
                            	{!! Form::select('sort_user', array(
                                        'all' => 'Semua',
                                        'id' => 'ID',
                                        'name' => 'Nama',
                                        'gender' => 'Jenis Kelamin',
                                        'total_reservation' => 'Banyak Reservasi',
                                        'last_login' => 'Terakhir Login',
                                        ), null, $attributes = ['class' => 'form-control sort-form-control sort-user-database']); !!}
                            {!! Form::close() !!}
		                	{!! Form::open(['action' => 'AdminController@getSearchUserDatabase', 'class' => 'form-search-database', 'method' => 'GET']) !!}
                            	{!! Form::text('search', null, $attributes = ['class' => 'form-control search-form-control', 'placeholder' => 'Cari']); !!}
		               		{!! Form::close() !!} 
	               			<div class="clearfix"></div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="report-body-container">
						<table class="table table-user-database-report">
							<thead>
								<tr>
									<th>ID</th>
									<th>Nama User</th>
									<th class="text-center">Email</th>
									<th class="text-center">Jenis Kel.</th>
									<th class="text-center">No. Hp</th>
									<th class="text-center">Byk Res.</th>
									<th class="text-center">Terakhir Login</th>
								</tr>
							</thead>
							<tbody>
				            	@if(isset($users_search))
								@foreach($users_search as $user_search)
								<tr>
									<td>{{ $user_search->id }}</td>
									<td>{{ $user_search->name }}</td>
									<td class="text-center">{{ $user_search->email }}</td>
									<td class="text-center">{{ $user_search->gender }}</td>
									<td class="text-center">{{ $user_search->phone_number }}</td>
									<td class="text-center">{{ count($user_search->reservations->where('status_reservation_id', 4)) }}</td>
									<td class="text-center">{{ $user_search->getLastLogin($user_search->last_login)->diffForHumans() }}</td>
								</tr>
								@endforeach
								@else
								@foreach($users as $user)
								<tr>
									<td>{{ $user->id }}</td>
									<td>{{ $user->name }}</td>
									<td class="text-center">{{ $user->email }}</td>
									<td class="text-center">{{ $user->gender }}</td>
									<td class="text-center">{{ $user->phone_number }}</td>
									<td class="text-center">{{ count($user->reservations->where('status_reservation_id', 4)) }}</td>
									<td class="text-center">{{ $user->getLastLogin($user->last_login)->diffForHumans() }}</td>
								</tr>	
								@endforeach							
								@endif
							</tbody>
						</table>
					</div>
					<div class="report-footer-container">
						<div class="col-sm-offset-6 col-sm-6 no-padding">
				            <div class="user-database-report-pagination">
				            	@if(isset($users_search))
	                        	{!! $users_search->appends(Request::only(['search'=>'search']))->render() !!}
				            	@else
	                        	{!! $users->appends(Request::only(['sort_user'=>'sort_user']))->render() !!}
	                        	@endif
				            </div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="text-center info-report-unshow">
						<h2>:(</h2>
						<h3>Minimal lebar layout yang dibutuhkan untuk menampilkan laporan user database adalah 992px atau ke atasnya.
						</h3>
					</div>
				</div>
			</div>
		</div>		
	</div>
	@include('admin.layouts.footer')
</section>
@stop