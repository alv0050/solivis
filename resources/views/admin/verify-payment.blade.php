@extends('admin.layouts.master')

@section('title', 'Verify Payment')

@section('content')

@include('admin.modals.decline_reservation');
@include('admin.layouts.navigation')
<section class="wrapper">
	@include('admin.layouts.sidebar')
	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main-general">
		<div class="main-content">
			<div class="container-fluid">
				<div class="flash-manage">
					@include('partials.message')
					@include('partials.error')
				</div>				
				<div class="verify-payment-container add-margin-top">
					<div class="verify-header-container">
						<div class="col-md-12 col-lg-6 left-side">
							<h3><img src="/images/admin/verify-black.png" alt="verify-payment" class="img-verify-black">Verify Payment</h3>
							<div class="button-group">
								{!! Form::open(['class' => 'form-put-verify-payment', 'action' => 'AdminController@putVerifyPayment', 'method' => 'PUT']) !!}
									<input type="hidden" name="reservation_id" value="" class="reservation_id_hidden">
									<button type="button" class="btn btn-approve" disabled>Approve</button>
								{!! Form::close() !!}
								<button class="btn btn-decline" id="btn-decline-reservation" disabled>Decline</button>
							</div>
		               		<div class="clearfix"></div>
						</div>
						<div class="col-md-12 col-lg-6 right-side">
							<h4>Urutkan: </h4>
							{!! Form::open(['action' => 'AdminController@getSortConfirmPayment', 'method' => 'GET', 'class' => 'form-sort-restaurant-management-database']) !!}
                            	{!! Form::select('sort_confirm_payment', array(
                                        'all' => 'Semua',
                            			'id' => 'ID Confirm Payment',
                            			'reservation_id' => 'ID Reservasi',
                            			'balance_processed' => 'Total Pembayaran',
                                        'transfer_from_bank' => 'Transfer Dari',
                                        'transfer_to_bank' => 'Transfer Ke',
                                        ), null, $attributes = ['class' => 'form-control sort-form-control sort-confirm-payment']); !!}
                            {!! Form::close() !!}
		                	{!! Form::open(['action' => 'AdminController@getSearchConfirmPayment', 'class' => 'form-search-database', 'method' => 'GET']) !!}
                            	{!! Form::text('search', null, $attributes = ['class' => 'form-control search-form-control', 'placeholder' => 'Cari']); !!}
		               		{!! Form::close() !!} 
	               			<div class="clearfix"></div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="verify-body-container">
						<table class="table table-verify-payment">
							<thead>
								<tr>
									<th class="text-center">ID Confirm Payment</th>
									<th class="text-center">ID Reservasi</th>
									<th class="text-center">Total Pembayaran</th>
									<th class="text-center">Transfer Dari</th>
									<th class="text-center">Transfer Ke</th>
								</tr>
							</thead>
							<tbody>
			            	@if(isset($confirm_payments_search))
								@foreach($confirm_payments_search as $confirm_payment_search)
								<tr>
									<td class="text-center">{{ $confirm_payment_search->id }}</td>
									<td class="text-center">{{ $confirm_payment_search->reservation_id }}</td>
									<td class="text-center">Rp. {{ number_format($confirm_payment_search->reservation->getTotalReservationPrice()) }}</td>
									<td class="text-center">{{ $confirm_payment_search->transfer_from_bank }}</td>
									<td class="text-center">{{ $confirm_payment_search->transfer_to_bank }}
								</tr>
								@endforeach
							@else
								@foreach($confirm_payments as $confirm_payment)
								<tr>
									<td class="text-center">{{ $confirm_payment->id }}</td>
									<td class="text-center">{{ $confirm_payment->reservation_id }}</td>
									<td class="text-center">Rp. {{ number_format($confirm_payment->reservation->getTotalReservationPrice()) }}</td>
									<td class="text-center">{{ $confirm_payment->transfer_from_bank }}</td>
									<td class="text-center">{{ $confirm_payment->transfer_to_bank }}
								</tr>
								@endforeach
							@endif
							</tbody>
						</table>
					</div>
					<div class="verify-footer-container">
						<div class="col-sm-offset-6 col-sm-6 no-padding">
				            <div class="verify-payment-pagination">
			            	@if(isset($confirm_payments_search))
                        	{!! $confirm_payments_search->appends(Request::only(['search'=>'search']))->render() !!}
			            	@else
                        	{!! $confirm_payments->appends(Request::only(['sort_confirm_payment'=>'sort_confirm_payment']))->render() !!}
                        	@endif				            
                        	</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="text-center info-verify-unshow">
						<h2>:(</h2>
						<h3>Minimal lebar layout yang dibutuhkan untuk melihat halaman ini adalah 992px atau ke atasnya.
						</h3>
					</div>
				</div>
			</div>
		</div>		
	</div>
	@include('admin.layouts.footer')
</section>
@stop