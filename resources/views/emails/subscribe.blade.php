@extends('beautymail::templates.widgets')

@section('content')

    @include('beautymail::templates.widgets.articleStart', ['color' => '#fcb023'])

        <h4 class="secondary"><strong>Subscribe</strong></h4>
    	<p>Alamat Email: {{ $user_email }} </p>

    @include('beautymail::templates.widgets.articleEnd')

@stop        
