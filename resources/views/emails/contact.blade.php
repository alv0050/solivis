@extends('beautymail::templates.widgets')

@section('content')

    @include('beautymail::templates.widgets.articleStart', ['color' => '#fcb023'])

        <h4 class="secondary"><strong>Contact Us</strong></h4>
    	<p>Nama: {{ $user_name }}</p>
    	<p>Email: {{ $user_email }}</p>
        <p>Isi Pesan: {{ $user_message }}</p>

    @include('beautymail::templates.widgets.articleEnd')

@stop        
