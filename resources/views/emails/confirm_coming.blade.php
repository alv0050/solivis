@extends('beautymail::templates.widgets')

@section('content')

    @include('beautymail::templates.widgets.articleStart', ['color' => '#fcb023'])

        <h4 class="secondary"><strong>Konsumen Pasti Datang</strong></h4>
        <p>Konsumen bernama {{ $user_name }} akan datang ke restoran sesuai jadwal reservasinya.</p>
        <p>Berikut adalah informasi reservasinya : </p>
        <p>Reservasi ID: {{ $reservation_id }} </p>
        <p>Atas Nama: {{ $user_name }} </p>
        <p>Tanggal: {{ $reservation_date }} </p>
        <p>Jam: {{ $reservation_time }} </p>
        <p>Pax: {{ $paxes }} </p>
        <p>Pembayaran: Rp. {{ number_format($total_reservation_price,0,'.',',') }} </p>

        <table border="1" style="width:100%; border-collapse: collapse; color: #444444">
            <caption>Pesanan</caption>
            <thead>
                <tr>
                    <th>Menu</th>
                    <th>@</th>
                    <th>Harga</th>
                </tr>
            </thead>
            <tbody>
            @for($i=0; $i<count($reservation_menus); $i++)
            <tr>
                <td style="text-align: left;">{{ $reservation_menus[$i]->menu_name }}</td>
                <td style="text-align: center;">{{ $reservation_menus[$i]->item_count }}</td>
                <td style="text-align: right;">{{ number_format($reservation_menus[$i]->item_count * $reservation_menus[$i]->menu_base_price,0,'.',',') }}</td>
            </tr>
            @endfor
            </tbody>
        </table>
        <p>Total Pembayaran: <b>Rp. {{ number_format($total_reservation_price,0,'.',',') }}</b></p>        

    @include('beautymail::templates.widgets.articleEnd')

@stop        
