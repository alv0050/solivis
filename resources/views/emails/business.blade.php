@extends('beautymail::templates.widgets')

@section('content')

    @include('beautymail::templates.widgets.articleStart', ['color' => '#fcb023'])

        <h4 class="secondary"><strong>Konsultasi Bisnis</strong></h4>
        <p>Nama: {{ $user_name }} </p>
        <p>Nama Usaha: {{ $user_business_name }} </p>
        <p>Alamat: {{ $user_address }} </p>
        <p>No. Telepon/HP: {{ $user_phone_number }} </p>
        <p>Alamat Email: {{ $user_email }} </p>

    @include('beautymail::templates.widgets.articleEnd')

@stop        
