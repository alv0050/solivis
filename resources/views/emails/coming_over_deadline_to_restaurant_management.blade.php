@extends('beautymail::templates.widgets')

@section('content')

    @include('beautymail::templates.widgets.articleStart', ['color' => '#fcb023'])

        <h4 class="secondary"><strong>Reservasi Konsumen Sudah Hangus</strong></h4>
        <p>Konsumen bernama {{ $user_name }} telah terlambat lebih 20 menit dari batas waktu tiba ke restoran, sehingga reservasi ID : {{ $reservation_id }}. Tgl.{{ $reservation_date }}. Jam {{ $reservation_time }} sudah dinyatakan hangus.</p>
        <p>Dengan demikian, saldo restoran anda telah bertambah sebesar Rp. {{ number_format($total_net_income,0,'.',',') }}.</p>

    @include('beautymail::templates.widgets.articleEnd')

@stop        
