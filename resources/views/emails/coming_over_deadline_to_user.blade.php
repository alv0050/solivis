@extends('beautymail::templates.widgets')

@section('content')

    @include('beautymail::templates.widgets.articleStart', ['color' => '#fcb023'])

        <h4 class="secondary"><strong>Reservasi Anda Sudah Hangus</strong></h4>
        <p>Reservasi anda dengan ID : {{ $reservation_id }}. Tgl.{{ $reservation_date }}. Jam {{ $reservation_time }} sudah dinyatakan hangus karena keterlambatan anda sudah melewati batas waktu tiba ke restoran.</p>

    @include('beautymail::templates.widgets.articleEnd')

@stop        
