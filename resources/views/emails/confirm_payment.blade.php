@extends('beautymail::templates.widgets')

@section('content')

    @include('beautymail::templates.widgets.articleStart', ['color' => '#fcb023'])

        <h4 class="secondary"><strong>Konfirmasi Pembayaran</strong></h4>
        <p>Reservation ID: {{ $reservation_id }} </p>
        <p>Total Pembayaran: Rp. {{ number_format($total_reservation_price,0,'.',',') }} </p>
        <p>Dari Rekening: {{ $transfer_from_bank }} </p>
        <p>Rekening Tujuan: {{ $transfer_to_bank }} </p>
        <img src="{{ $message->embed(public_path().'/images/transfer_proof_uploads/'.$transfer_proof_photo_url_filename) }}" />
        <p>Keterangan (Optional): {{ $information }}</p>

    @include('beautymail::templates.widgets.articleEnd')

@stop        
