@extends('beautymail::templates.widgets')

@section('content')

    @include('beautymail::templates.widgets.articleStart', ['color' => '#fcb023'])

        <h4 class="secondary"><strong>Konsumen Batal Datang</strong></h4>
        <p>Konsumen bernama {{ $user_name }} memutuskan untuk menghanguskan reservasi ID : {{ $reservation_id }}. Tgl. {{ $reservation_date_only }}. Jam {{ $reservation_time_only }}.</p>
        <p>Dengan demikian, saldo restoran anda telah bertambah sebesar Rp. {{ number_format($total_net_income,0,'.',',') }}.</p>

    @include('beautymail::templates.widgets.articleEnd')

@stop        
