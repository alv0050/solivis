@extends('beautymail::templates.widgets')

@section('content')

    @include('beautymail::templates.widgets.articleStart', ['color' => '#fcb023'])

        <h4 class="secondary"><strong>Reservasi</strong></h4>

        <p>Sukses! Anda telah berhasil melakukan reservasi dengan informasi sebagai berikut :</p>

        <p>Reservasi ID: {{ $reservation_id }}</p>
        <p>Atas Nama: {{ $user_name }}</p>
        <p>Tanggal: {{ $reservation_date_only }}</p>
        <p>Jam: {{ $reservation_time_only }}</p>
        <p>Pax: {{ $paxes }}</p>
        <p>Pembayaran: Rp. {{ number_format($total_reservation_price,0,'.',',') }}</p>

        <table border="1" style="width:100%; border-collapse: collapse; color: #444444">
            <caption>Pesanan</caption>
            <thead>
                <tr>
                    <th>Menu</th>
                    <th>@</th>
                    <th>Harga</th>
                </tr>
            </thead>
            <tbody>
            @for($i=0; $i<count($reservation_menus); $i++)
            <tr>
                <td style="text-align: left;">{{ $reservation_menus[$i]->menu_name }}</td>
                <td style="text-align: center;">{{ $reservation_menus[$i]->item_count }}</td>
                <td style="text-align: right;">{{ number_format($reservation_menus[$i]->item_count * $reservation_menus[$i]->menu_base_price,0,'.',',') }}</td>
            </tr>
            @endfor
            </tbody>
        </table>
        <p>Total Pembayaran: <b>Rp. {{ number_format($total_reservation_price,0,'.',',') }}  </b></p>

        <p>Tanggal jatuh tempo anda untuk melakukan pembayaran dan konfirmasi pembayaran adalah {{ $deadline_date_only }}. Jam {{ $deadline_time_only }}. Apabila lewat dari jatuh tempo, reservasi anda akan dibatalkan.</p>

        <p>Apabila anda melakukan pembayaran dengan transfer Bank, pastikan untuk selalu melakukan konfirmasi pembayaran antara jam 08:00 s/d 16:00.</p>

    @include('beautymail::templates.widgets.articleEnd')

@stop        
