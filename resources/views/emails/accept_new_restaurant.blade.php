@extends('beautymail::templates.sunny')

@section('content')

    @include ('beautymail::templates.sunny.heading' , [
        'heading' => 'Anda Telah Bergabung ke Solivis',
        'level' => 'h1',
    ])

    @include('beautymail::templates.sunny.contentStart')
    
		<p style="text-align: center">Selamat! Restoran anda telah disetujui oleh Admin untuk menjadi member di Solivis.</p>
        <p style="text-align: center">Anda dapat melakukan login dengan menggunakan informasi di bawah ini.</p>
        <p style="text-align: center">Email: {{ $restaurant_management_email }}</p>
        <p style="text-align: center">Password: {{ $generated_password }}</p>
        <p style="text-align: justify">Password ini digenerate secara otomatis oleh sistem. Kami sarankan untuk segera mengubah password anda.</p>
        <p style="text-align: justify">Klik link <a href="http://solivis.com/restaurant-management/login" target="_blank">http://solivis.com/restaurant-management/login</a> untuk menuju halaman login restoran. Link ini akan anda perlukan setiap kali ingin masuk ke halaman login restoran.</p>

    @include('beautymail::templates.sunny.contentEnd')

@stop