@extends('beautymail::templates.widgets')

@section('content')

    @include('beautymail::templates.widgets.articleStart', ['color' => '#fcb023'])

        <h4 class="secondary"><strong>{{ $reservation_result_info }}</strong></h4>

        <p>Reservasi ID: {{ $reservation_id }} </p>
        <p>Atas Nama: {{ $user_name }} </p>
        <p>Tanggal: {{ $reservation_date }} </p>
        <p>Jam: {{ $reservation_time }} </p>
        <p>Pax: {{ $paxes }} </p>
        <p>Pembayaran: Rp. {{ number_format($total_reservation_price,0,'.',',') }} </p>

        <table border="1" style="width:100%; border-collapse: collapse; color: #444444">
            <caption>Pesanan</caption>
            <thead>
                <tr>
                    <th>Menu</th>
                    <th>@</th>
                    <th>Harga</th>
                </tr>
            </thead>
            <tbody>
            @for($i=0; $i<count($reservation_menus); $i++)
            <tr>
                <td style="text-align: left;">{{ $reservation_menus[$i]->menu_name }}</td>
                <td style="text-align: center;">{{ $reservation_menus[$i]->item_count }}</td>
                <td style="text-align: right;">{{ number_format($reservation_menus[$i]->item_count * $reservation_menus[$i]->menu_base_price,0,'.',',') }}</td>
            </tr>
            @endfor
            </tbody>
        </table>
        <p>Total Pembayaran: <b>Rp. {{ number_format($total_reservation_price,0,'.',',') }}</b></p>

        @if(isset($code))
        <img src="{!! $message->embedData(QrCode::format('png')->size(150)->generate($code), 'QrCode.png', 'image/png') !!}">        
        <p>Terima kasih telah melakukan reservasi. Jangan lupa untuk membawa QR Code ini saat berkunjung ke restoran.</p>
        @endif

        @if(isset($reason))
        <p>Alasan Penolakan: {{ $reason }}</p>
        @endif

    @include('beautymail::templates.widgets.articleEnd')

@stop        
