@extends('beautymail::templates.sunny')

@section('content')

    @include ('beautymail::templates.sunny.heading' , [
        'heading' => 'Pergantian Jadwal Reservasi',
        'level' => 'h1',
    ])

    @include('beautymail::templates.sunny.contentStart')

        <p>Konsumen bernama {{ $user_name }} memutuskan untuk mengganti jadwal reservasi ID : {{ $reservation_id }} menjadi Tgl. {{ $reservation_date_only }}. Jam {{ $reservation_time_only }}. </p>

        <p>Anda memiliki waktu s/d tgl. {{ $deadline_date_only }}, jam {{ $deadline_time_only }} untuk merespon reservasi ini. Apabila lewat dari waktu tersebut, reservasi ini akan secara otomatis ditolak.</p>

        <p>Klik tombol di bawah ini untuk menuju halaman reservasi.</p>

    @include('beautymail::templates.sunny.contentEnd')

    @include('beautymail::templates.sunny.button', [
            'title' => 'Click Me',
            'link' => 'solivis.com/restaurant-management/reservation'
    ])

@stop

