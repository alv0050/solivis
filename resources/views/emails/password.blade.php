@extends('beautymail::templates.sunny')

@section('content')

    @include ('beautymail::templates.sunny.heading' , [
        'heading' => 'Reset password',
        'level' => 'h1',
    ])

    @include('beautymail::templates.sunny.contentStart')
    
		<p style="text-align: center">Anda telah meminta untuk mereset password. Silahkan klik tombol dibawah ini!</p>

    @include('beautymail::templates.sunny.contentEnd')

    @include('beautymail::templates.sunny.button', [
            'title' => 'Reset Password',
            'link'  => 'solivis.com/password/reset/'.$token,
    ])

@stop