@extends('beautymail::templates.widgets')

@section('content')

    @include('beautymail::templates.widgets.articleStart', ['color' => '#fcb023'])

        <h4 class="secondary"><strong>Reservasi ditolak oleh Admin</strong></h4>
        <p>Kepada {{ $user_name }},</p>
        <p>Reservasi anda dengan ID: {{ $reservation_id }} tgl. {{ $reservation_date }}. Jam {{ $reservation_time }} ditolak oleh Admin dengan alasan: {{ $reason }}.</p>
        
    @include('beautymail::templates.widgets.articleEnd')

@stop        
