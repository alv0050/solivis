@extends('beautymail::templates.widgets')

@section('content')

    @include('beautymail::templates.widgets.articleStart', ['color' => '#fcb023'])

        <h4 class="secondary"><strong>Reservasi Anda Tidak Direspon Oleh Pihak Restoran</strong></h4>
        <p>Reservasi anda dengan No. ID : {{ $reservation_id }} tgl. {{ $reservation_date }}. Jam {{ $reservation_time }} tidak direspon oleh pihak restoran dalam kurun waktu yang telah ditetapkan oleh Admin.</p>        
        <p>Untuk itu, reservasi anda terpaksa kami batalkan. Jumlah pembayaran anda sebesar Rp. {{ number_format($total_reservation_price,0,'.',',') }} telah kami tambahkan ke saldo anda.</p>
        <p>Mohon maaf atas ketidaknyamanan ini.</p>

    @include('beautymail::templates.widgets.articleEnd')

@stop        
