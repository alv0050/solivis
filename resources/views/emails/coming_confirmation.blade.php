@extends('beautymail::templates.sunny')

@section('content')

    @include ('beautymail::templates.sunny.heading' , [
        'heading' => 'Konfirmasi Kedatangan',
        'level' => 'h1',
    ])

    @include('beautymail::templates.sunny.contentStart')
    
        <p class="text-align: center">Anda memiliki reservasi dengan ID : {{ $reservation_id }}. Tgl. {{ $reservation_date }}. Jam {{ $reservation_time }}, sebanyak {{ $paxes }} orang. </p>
        @if($ever_delayed_before)
        <p>Jatah ganti jadwal reservasi anda sudah habis. Jika anda berhalangan untuk hadir, tetapi bersedia memberikan notifikasi kepada pihak restoran untuk tidak lagi mempersiapkan makanan anda, silahkan <a style="cursor: pointer" href="solivis.com/reservation/choose-cancel-reservation-options/{{ $reservation_id }}">klik tautan ini</a>.</p>        
        @else
        <p>Jika anda berhalangan untuk hadir, silahkan <a style="cursor: pointer" href="solivis.com/reservation/choose-cancel-reservation-options/{{ $reservation_id }}">klik tautan ini</a>.</p>        
        @endif
        <p class="text-align: center">Anda dapat memilih tombol datang di bawah ini untuk memastikan kehadiran anda atau abaikan saja email ini jika anda pasti datang.</p>
        <p>Batas keterlambatan paling lama tiba di restoran adalah 20 menit dari waktu reservasi anda. Apabila lewat dari 20 menit, maka reservasi anda akan dinyatakan hangus.</p>
        <p>Email konfirmasi ini akan expired tgl. {{ $expired_confirmation_date }}. Jam {{ $expired_confirmation_time }}.</p>

    @include('beautymail::templates.sunny.contentEnd')

    @include('beautymail::templates.sunny.button', [
            'title' => 'Datang',
            'link'  => 'solivis.com/reservation/confirm-coming/'.$reservation_id,
    ])

@stop