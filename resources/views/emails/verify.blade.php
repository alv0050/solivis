@extends('beautymail::templates.sunny')

@section('content')

    @include ('beautymail::templates.sunny.heading' , [
        'heading' => 'Verifikasi Alamat Email',
        'level' => 'h1',
    ])

    @include('beautymail::templates.sunny.contentStart')
    
		<p style="text-align: center">Terima kasih sudah mendaftar di aplikasi Solivis. Silahkan klik tombol dibawah ini untuk mengkonfirmasi alamat email anda.</p>

    @include('beautymail::templates.sunny.contentEnd')

    @include('beautymail::templates.sunny.button', [
            'title' => 'Confirm',
            'link'  => 'solivis.com/register/verify/'.$confirmation_code,
    ])

@stop