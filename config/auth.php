<?php

return [
    'multi' => [
        'user' => [
            'driver' => 'eloquent',
            'model'  => Solivis\User::class,
            'table'  => 'users'
        ],
        'restaurant_management' => [
            'driver' => 'eloquent',
            'model'  => Solivis\RestaurantManagement::class,
            'table'  => 'restaurant_managements'
        ],
        'admin' => [
            'driver' => 'eloquent',
            'model'  => Solivis\Admin::class,
            'table'  => 'admins'
        ]
     ],
    'password' => [
        'email' => 'emails.password',
        'table' => 'password_resets',
        'expire' => 60,
    ],
];
